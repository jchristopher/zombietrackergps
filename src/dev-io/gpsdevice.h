/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GPSDEVICE_H
#define GPSDEVICE_H

#include <QString>
#include <QStringList>

// Encapsulate operations for a type of GPS device (copy data to/from device, find device name & mountpoint, etc).
// This is a base class for vendor or device specific classes to derive from.
class GpsDevice
{
public:
    GpsDevice(const QString& mount);
    virtual ~GpsDevice();

    // Types of data we can copy to or from the device
    enum Data {
        GPS,
        Photos,
        POI,
        MapsBase,
        MapsCustom,
    };

    // Transfer direction
    enum Transfer {
        Input,
        Output,
        InputOutput,
    };

    virtual QString     mountPoint() const;              // return filesystem mountpoint of device
    virtual QString     device() const;                  // return block device node
    virtual QString     make() const = 0;                // return device make
    virtual QString     model() const = 0;               // return model name
    virtual bool        isMounted() const = 0;           // true if device is mounted
    virtual QStringList files(Data, Transfer) const = 0; // device filenames

protected:
    QString m_mount;
};

#endif // GPSDEVICE_H
