/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "gpsdevice.h"

#include <QStorageInfo>

GpsDevice::GpsDevice(const QString& mount) :
    m_mount(mount)
{
}

GpsDevice::~GpsDevice()
{
}

QString GpsDevice::mountPoint() const
{
    return m_mount;
}

QString GpsDevice::device() const
{
    return QStorageInfo(mountPoint()).device();
}

