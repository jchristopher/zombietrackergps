/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <QStorageInfo>
#include <QFileInfo>

#include <src/util/units.h>
#include <src/util/roles.h>

#include "gpsmodel.h"
#include "gpsgarmin.h"

GpsModel::GpsModel(const CfgData& cfgData, QObject *parent) :
    TreeModel(nullptr, parent),
    refreshTimer(this),
    mountHash(-1),
    cfgData(cfgData)
{
    setupTimers();
    refresh(); // seed initial device list
}

GpsModel::~GpsModel()
{
    // Individual GPS device pointers will be deleted by QSharedPointer.
}

void GpsModel::setupTimers()
{
    // It would be better to do this in some event driven way, but all the ways to do that
    // seem OS specific.  Polling is ugly, but the refresh() is very cheap if nothing changed.
    connect(&refreshTimer, &QTimer::timeout, this, &GpsModel::refresh);
    refreshTimer.start(3000);
}

void GpsModel::refresh()
{
    // Calculate hash for current mounted volumes.  We're hashing all the mountpoints,
    // not just the ones that might be a GPS, but a small amount of needless refresh is OK.
    uint hash = 0x57a33ba; // seed
    for (const auto& mount : QStorageInfo::mountedVolumes()) {
        hash = qHash(std::make_pair(hash, mount.rootPath()));
        hash = qHash(std::make_pair(hash, mount.isReady()));
    }
    
    if (hash == mountHash)  // early out if mounts haven't changed
        return;

    mountHash = hash;

    if (!empty()) {
        beginRemoveRows(QModelIndex(), 0, size() - 1);
        clear(); // QSharedPointer deletes GpsDevices on heap
        endRemoveRows();
    }

    reserve(4);  // Typically there won't be more than 1 device.

    // Build list of GPS devices
    for (const auto& mount : QStorageInfo::mountedVolumes()) {
        if (GpsDevice* device = getDevice(mount.rootPath())) {
            beginInsertRows(QModelIndex(), size(), size());
            append(value_type(device));
            endInsertRows();
        }
    }
}

// Factory for device specific derived GPS classes, or nullptr if none apply.
GpsDevice* GpsModel::getDevice(const QString& mount)
{
    // Check each type we know about in turn.
    if (GpsGarmin::is(mount)) return new GpsGarmin(mount);
    // Other types go here, when available.

    return nullptr;
}

QVariant GpsModel::data(const QModelIndex& idx, int role) const
{
    if (!idx.isValid() || idx.row() >= rowCount() || idx.column() >= columnCount())
        return QVariant();

    // Text alignment:
    if (role == Qt::TextAlignmentRole)
        return QVariant(mdAlignment(idx.column()));

    // Data
    if (role == Qt::DisplayRole || role == Util::CopyRole) {
        switch (idx.column()) {
        case GpsModel::Make:       return at(idx.row())->make();
        case GpsModel::Model:      return at(idx.row())->model();
        case GpsModel::Device:     return at(idx.row())->device();
        case GpsModel::MountPoint: return at(idx.row())->mountPoint();
        case _Count:               break;
        }
    }

    return QVariant();
}

QVariant GpsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (QVariant val = ModelMetaData::headerData<GpsModel>(section, orientation, role); val.isValid())
        return val;

    if (orientation == Qt::Horizontal)
        if (role == Qt::DisplayRole)
            return mdName(section);

    return QAbstractItemModel::headerData(section, orientation, role);
}

QModelIndex GpsModel::index(int row, int column, const QModelIndex&) const
{
    return createIndex(row, column);
}

QModelIndex GpsModel::parent(const QModelIndex&) const
{
    return QModelIndex();
}

int GpsModel::rowCount(const QModelIndex& parent) const
{
    return (parent.isValid()) ? 0 : size();
}

Qt::ItemFlags GpsModel::flags(const QModelIndex& idx) const
{
    return ModelMetaData::flags<GpsModel>(idx) | QAbstractItemModel::flags(idx);
}

bool GpsModel::setData(const QModelIndex&, const QVariant&, int)
{
    assert(0);
    return false; // not supported
}

bool GpsModel::setHeaderData(int /*section*/, Qt::Orientation /*orientation*/,
                            const QVariant& /*value*/, int /*role*/)
{
    assert(0);
    return false; // not supported
}

bool GpsModel::insertRows(int /*position*/, int /*rows*/, const QModelIndex& /*parent*/)
{
    assert(0);
    return false; // not supported
}

bool GpsModel::removeRows(int /*position*/, int /*rows*/, const QModelIndex& /*parent*/)
{
    assert(0);
    return false; // not supported
}

const Units& GpsModel::units(const QModelIndex& idx) const
{
    return mdUnits(idx.column(), cfgData);
}

QString GpsModel::mdName(ModelType mt)
{
    switch (mt) {
    case GpsModel::Make:       return QObject::tr("Make");
    case GpsModel::Model:      return QObject::tr("Model");
    case GpsModel::Device:     return QObject::tr("Device");
    case GpsModel::MountPoint: return QObject::tr("Mount Point");
    case _Count:               break;
    }

    assert(0 && "Unknown GpsList column");
    return "";
}

bool GpsModel::mdIsEditable(ModelType)
{
    return false;
}

QString GpsModel::mdTooltip(ModelType mt)
{
#define cd_header(i)       "<p><b><u>" i ":</u></b><p>"
#define cd_info cd_header("Column Information")

    switch (mt) {
    case GpsModel::Make:       return QObject::tr(cd_info "GPS device manufacturer");
    case GpsModel::Model:      return QObject::tr(cd_info "GPS device model");
    case GpsModel::Device:     return QObject::tr(cd_info "GPS device node");
    case GpsModel::MountPoint: return QObject::tr(cd_info "GPS device mountpoint");
    case _Count:               break;
    }

    assert(0 && "Unknown GpsList column");
    return "";
}

QString GpsModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);   // just pass through the tooltip
}

Qt::Alignment GpsModel::mdAlignment(ModelType)
{
    return Qt::AlignLeft | Qt::AlignVCenter;
}

const Units& GpsModel::mdUnits(ModelType, const CfgData&)
{
    static const Units rawString(Format::String);
    return rawString;
}

QStringList GpsModel::importFiles(const QModelIndexList& indexList) const
{
    QStringList files;

    files.reserve(indexList.size());

    for (const auto& idx : indexList)
        if (idx.row() < rowCount())
            for (const auto fileName : at(idx.row())->files(GpsDevice::Data::GPS, GpsDevice::Transfer::Output))
                if (const QFileInfo file(fileName); file.isReadable())
                    files.append(file.filePath());

    return files;
}
