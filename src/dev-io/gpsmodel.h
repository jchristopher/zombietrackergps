/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GPSLIST_H
#define GPSLIST_H

#include <QSharedPointer>
#include <QVector>
#include <QTimer>
#include <QModelIndexList>
#include <QStringList>
#include <src/core/modelmetadata.h>
#include <src/core/treemodel.h>

class GpsDevice;
class CfgData;

class GpsModel final : public QVector<QSharedPointer<GpsDevice>>, public TreeModel, public ModelMetaData
{
public:
    enum {
        _First,
        Make = _First,
        Model,
        Device,
        MountPoint,
        _Count
    };

    GpsModel(const CfgData& cfgData, QObject *parent = nullptr);
    ~GpsModel();
    
    QStringList importFiles(const QModelIndexList&) const;

    const Units& units(const QModelIndex& idx) const override;

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static const Units&   mdUnits(ModelType, const CfgData&);
    // *** end ModelMetaData API

    // *** begin QAbstractItemModel API
    using TreeModel::data;
    QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& idx) const override;

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const override { return _Count; }

    Qt::ItemFlags flags(const QModelIndex& idx) const override;
    bool setData(const QModelIndex& idx, const QVariant& value,
                 int role = Qt::DisplayRole) override;
    bool setHeaderData(int section, Qt::Orientation orientation,
                       const QVariant& value, int role = Qt::DisplayRole) override;
    bool insertColumns(int /*position*/, int /*columns*/,
                       const QModelIndex& = QModelIndex()) override { return false; }
    bool removeColumns(int /*position*/, int /*columns*/,
                       const QModelIndex& = QModelIndex()) override { return false; }
    bool insertRows(int position, int rows,
                    const QModelIndex& parent = QModelIndex()) override;
    bool removeRows(int position, int rows,
                    const QModelIndex& parent = QModelIndex()) override;
    // *** end QAbstractItemModel API

public slots:
    void refresh();  // refresh from currently mounted devices

private:
    void setupTimers();
    static GpsDevice* getDevice(const QString& mount);

    QTimer         refreshTimer; // see comment in setupTimers
    uint           mountHash;    // hash of mounted volumes
    const CfgData& cfgData;
};

#endif // GPSLIST_H
