/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QStorageInfo>
#include <QDir>
#include <QFile>
#include <QStringList>
#include <QRegularExpression>

#include "src/util/xmlstreamreader.h"
#include "gpsgarmin.h"

GpsGarmin::GpsGarmin(const QString& mount) :
    GpsDevice(mount)
{
    parse();
}

void GpsGarmin::parse()
{
    QFile deviceFile(garminDeviceFile());
    if (deviceFile.fileName().isEmpty())
        return;

    if (!deviceFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    XmlStreamReader xml(&deviceFile);

    if (xml.error() != QXmlStreamReader::NoError)
        return;

    xml.parseKeys([this](XmlStreamReader& xml) {
        if (xml.name() == "Device") parseDevice(xml);
        else xml.skipCurrentElement();
    });

    parseInfo.clear();
}

void GpsGarmin::parseDevice(XmlStreamReader& xml)
{
    xml.parseKeys([this](XmlStreamReader& xml) {
        if      (xml.name() == "Model")           parseModel(xml);
        else if (xml.name() == "MassStorageMode") parseMassStorageMode(xml);
        else xml.skipCurrentElement();
    });
}

void GpsGarmin::parseModel(XmlStreamReader& xml)
{
    xml.parseKeys([this](XmlStreamReader& xml) {
        if   (xml.name() == "Description") m_model = xml.readElementText();
        else xml.skipCurrentElement();
    });
}

void GpsGarmin::parseMassStorageMode(XmlStreamReader& xml)
{
    xml.parseKeys([this](XmlStreamReader& xml) {
        if   (xml.name() == "DataType") parseDataType(xml);
        else xml.skipCurrentElement();
    });
}

void GpsGarmin::parseDataType(XmlStreamReader& xml)
{
    parseInfo.clear();

    xml.parseKeys([this](XmlStreamReader& xml) {
        if      (xml.name() == "Name") parseInfo.name = xml.readElementText();
        else if (xml.name() == "File") parseFile(xml);
        else xml.skipCurrentElement();
    });
}

void GpsGarmin::parseFile(XmlStreamReader& xml)
{
    parseInfo.clearFileInfo(); // clear out per-file info

    xml.parseKeys([this](XmlStreamReader& xml) {
        if      (xml.name() == "Location")          parseLocation(xml);
        else if (xml.name() == "TransferDirection") parseInfo.transferDirection = xml.readElementText();
        else xml.skipCurrentElement();
    });

    const QString ext = parseInfo.fileExtension.toLower();
    const QString mountDir = mountPoint() + QDir::separator();

    // TODO: we shouldn't duplicate the extensions here for the known formats.  The GPS loaders
    // already have this data, and it's ungainly to have to maintain this separately.
    if (parseInfo.transferDirection == "OutputFromUnit" &&
        (ext == "gpx" || ext == "tcx" || ext == "fit")) {

        if (!parseInfo.baseName.isEmpty()) {
            // We have a filename
            m_GpsOutput.append(mountDir + parseInfo.path + QDir::separator() + parseInfo.baseName + "." + parseInfo.fileExtension);
        } else {
            // We have a directory.  Snarf all the files in it matching the given extension.
            const QDir dir(mountDir + parseInfo.path);

            for (const auto file : dir.entryList({ QString("*.") + ext }, QDir::Files | QDir::Readable))
                m_GpsOutput.append(dir.absolutePath() + QDir::separator() + file);
        }
    }
}

void GpsGarmin::parseLocation(XmlStreamReader& xml)
{
    xml.parseKeys([&](XmlStreamReader& xml) {
        if      (xml.name() == "Path")          parseInfo.path = xml.readElementText();
        else if (xml.name() == "BaseName")      parseInfo.baseName = xml.readElementText();
        else if (xml.name() == "FileExtension") parseInfo.fileExtension = xml.readElementText();
        else xml.skipCurrentElement();
    });

    // Ignore some special files we shouldn't try to read.  This is a little ad-hoc.
    if (parseInfo.baseName == "LastKnownPosition")
        parseInfo.clear();
}

QString GpsGarmin::make() const
{
    return "Garmin";
}

QString GpsGarmin::model() const
{
    return m_model;
}

bool GpsGarmin::isMounted(const QString& mount)
{
    const QStorageInfo mountInfo = QStorageInfo(mount);

    return mountInfo.isValid() &&
           mountInfo.isReady() &&
           QDir(mountInfo.rootPath()).canonicalPath() == QDir(mount).canonicalPath();
}

bool GpsGarmin::isMounted() const
{
    return isMounted(mountPoint());
}

// Obtain the GarminDevice.xml file for the device.  This is a bag of XML with info about output
// files, input files, the device name, etc.
QString GpsGarmin::garminDeviceFile(const QString& mount)
{
    if (!isMounted(mount))
        return QString();

    if (const QFile garminDevice(mount + QDir::separator() + "Garmin" + QDir::separator() + "GarminDevice.xml"); garminDevice.exists())
        return garminDevice.fileName();

    return QString();
}

QString GpsGarmin::garminDeviceFile() const
{
    return garminDeviceFile(mountPoint());
}

bool GpsGarmin::is(const QString& mount)
{
    return !garminDeviceFile(mount).isEmpty();
}

// Obtain a device specific file path for the given data and transfer direction.
QStringList GpsGarmin::files(Data data, Transfer transfer) const
{
    if (data == Data::GPS && transfer == Transfer::Output)
        return m_GpsOutput;

    // TODO: Other files not yet supported.
    return QStringList();
}
