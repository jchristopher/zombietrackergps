/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SELECTIONSUMMARY_H
#define SELECTIONSUMMARY_H

#include <QtGlobal>
#include <QItemSelection>

class TrackModel;
class PointModel;
class QItemSelectionModel;
class QAbstractProxyModel;

class SelectionSummary
{
public:
    SelectionSummary();

    void clear(int totalItems, int visibleItems);

    template <class MODEL>
    void update(const MODEL&, const QAbstractProxyModel&, const QItemSelectionModel*,
                const QItemSelection& selected, const QItemSelection& deselected);

    // Recalculate entirely
    template <class MODEL>
    inline void refresh(const MODEL&, const QAbstractProxyModel&, const QItemSelectionModel*);

    template <class MODEL>
    void accumulate(const MODEL&, const QModelIndex& idx, int sign);

    int    total;
    int    visible;
    int    selected;
    qreal  length;
    qreal  ascent;
    qreal  descent;
    qint64 duration;

private:
    template <class MODEL>
    void trackSelected(const MODEL& model, const QItemSelectionModel* selectionModel,
                       const QItemSelection& selected, const QItemSelection& deselected);
};

template <class MODEL>
void inline SelectionSummary::refresh(const MODEL& model, const QAbstractProxyModel& proxy,
                                      const QItemSelectionModel* selectionModel)
{
    clear(0, 0);
    update(model, proxy, selectionModel, selectionModel->selection(), QItemSelection());
}

#endif // SELECTIONSUMMARY_H
