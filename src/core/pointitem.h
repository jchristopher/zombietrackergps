/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POINTITEM_H
#define POINTITEM_H

#include <QDateTime>
#include <marble/GeoDataCoordinates.h>

#include "powerdata.h"

class PointModel;
class Units;
class CfgData;
class QDataStream;

// Single track point, and any associated data
class PointItem final
{
public:
    enum {
        Measured = (1<<0),
        Computed = (1<<1),
        Either   = (Measured | Computed),
    };

    PointItem(qreal lon = NaN, qreal lat = NaN, const QDateTime& time = QDateTime(), qreal ele = NaN,
            uint8_t hr = badHr, uint8_t cad = badCad, float power = NaN) :
        m_flags(NoFlags),
        m_time(time),
        m_lon(lon),    m_lat(lat),    m_ele(ele),
        m_fltLon(NaN), m_fltLat(NaN), m_fltEle(NaN),
        m_length(NaN), m_vert(fNaN), m_grade(fNaN), m_duration(0),
        m_atemp(NaN), m_wtemp(NaN), m_depth(NaN), m_speed(NaN), m_accel(NaN),
        m_hr(hr), m_cad(cad), m_power(power), m_course(NaN), m_bearing(NaN)
    { }

    void clear();

    qreal lon(bool flt = true) const { return hasFltLon() && flt ? m_fltLon : m_lon; }
    qreal lat(bool flt = true) const { return hasFltLat() && flt ? m_fltLat : m_lat; }
    qreal ele(bool flt = true) const { return hasFltEle() && flt ? m_fltEle : m_ele; }
    qreal length()             const { return m_length; } // horizontal distance (not slant distance)
    qreal distance()           const { return m_dist; }   // total distance within the track
    float vert()               const { return m_vert; } // vertical distance
    float grade()              const { return m_grade; }
    int   duration()           const { return m_duration; }
    uint64_t elapsed(const QDateTime& start) const { return start.msecsTo(time()); }
    const QDateTime& time()    const { return m_time; }
    float    atemp()           const { return m_atemp; }
    float    wtemp()           const { return m_wtemp; }
    float    depth()           const { return m_depth; }
    qreal    speed()           const { return hasSpeed(Measured) ? m_speed : (length() * 1000.0 / duration()); }
    qreal    accel()           const { return m_accel; }
    uint8_t  hr()              const { return m_hr; }
    uint8_t  cad()             const { return m_cad; }
    float    course()          const { return m_course; }
    float    bearing()         const { return m_bearing; }
    float    power()           const { return m_power; }
    inline float power(const PowerData& pd) const;

    bool hasLoc()      const    { return !std::isnan(m_lon) && !std::isnan(m_lat); }
    bool hasTime()     const    { return m_time.isValid(); }
    bool hasEle()      const    { return !std::isnan(m_ele);  }
    bool hasFltLon()   const    { return !std::isnan(m_fltLon); }
    bool hasFltLat()   const    { return !std::isnan(m_fltLat); }
    bool hasFltEle()   const    { return !std::isnan(m_fltEle); }
    bool hasLength()   const    { return !std::isnan(m_length); }
    bool hasDistance() const    { return !std::isnan(m_dist); }
    bool hasVert()     const    { return !std::isnan(m_vert); }
    bool hasGrade()    const    { return !std::isnan(m_grade); }
    bool hasDuration() const    { return m_duration != 0; }
    bool hasElapsed()  const    { return hasTime(); }
    bool hasSpeed(int m)  const {
        return ((m & Measured) && !std::isnan(m_speed)) ||
               ((m & Computed) && hasLength() && hasDuration());
    }
    bool hasAccel()   const { return !std::isnan(m_accel); }

    bool hasAtemp()   const   { return !std::isnan(m_atemp);   }
    bool hasWtemp()   const   { return !std::isnan(m_wtemp);   }
    bool hasDepth()   const   { return !std::isnan(m_depth);   }
    bool hasHr()      const   { return m_hr != badHr;          }
    bool hasCad()     const   { return m_cad != badCad;        }
    bool hasCourse()  const   { return !std::isnan(m_course);  }
    bool hasBearing() const   { return !std::isnan(m_bearing); }
    bool hasPower(int m) const {
        return ((m & Measured) && !std::isnan(m_power)) ||
               ((m & Computed) && hasGrade() && hasSpeed(Either));
    }

    bool hasExtData() const;
    
    enum : uint8_t {
        NoFlags     = 0,
        Select      = (1<<0), // normally selected point
        AreaSelect  = (1<<1), // lies within area select region
    };

    // Flag query and set
    bool is(uint8_t flag) const      { return (m_flags & flag) != 0; }
    inline void set(bool s, uint8_t bit) {
        m_flags = s ? (m_flags | bit) : (m_flags & ~bit);
    }

    template <typename T = QVariant>
    T data(ModelType, int row, int role, bool filtered, const QDateTime& start, const PowerData&, const CfgData& cfgData) const;
    bool     setData(ModelType, QVariant value, int role, bool& changed, const CfgData& cfgData);
    bool     hasData(ModelType) const;
    void     clearData(ModelType);

    // convert to the format T, provided we know about it.
    template <typename T> T as() const;

    bool operator==(const PointItem&) const;

private:
    // A little ugly: we give the laoders access to private data.
    // TODO handle this in some better way.
    friend class GeoLoadGpx;
    friend class GeoLoadTcx;
    friend class GeoLoadKml;
    friend class GeoLoadFit;
    friend class PointModel;
    friend QDataStream& operator<<(QDataStream&, const PointItem&);
    friend QDataStream& operator>>(QDataStream&, PointItem&);

    uint8_t   m_flags;     // cache for render performance
    QDateTime m_time;      // sample timestamp

    qreal     m_lon;       // longitude, degrees
    qreal     m_lat;       // latitude, degrees
    qreal     m_ele;       // elevation, m

    qreal     m_fltLon;    // noise-filtered longitude
    qreal     m_fltLat;    // noise-filtered latitude
    qreal     m_fltEle;    // noise-filtered elevation

    float     m_length;    // leg distance (to next point)
    float     m_dist;      // total distance within the track to this point
    float     m_vert;      // vertical distance, m
    float     m_grade;     // grade, %
    float     m_gradeSinAtan; // cached
    int       m_duration;  // in mSec

    float     m_atemp;     // air temp
    float     m_wtemp;     // water temp
    float     m_depth;     // water depth
    float     m_speed;     // vehicle speed, m/s
    float     m_accel;     // acceleration, m/s^2
    uint8_t   m_hr;        // heart rate
    uint8_t   m_cad;       // cadence
    float     m_power;     // rider power
    float     m_course;    // course
    float     m_bearing;   // bearing

    static const constexpr auto NaN        = std::numeric_limits<qreal>::quiet_NaN();
    static const constexpr auto fNaN       = std::numeric_limits<float>::quiet_NaN();
    static const constexpr uint8_t  badHr  = std::numeric_limits<decltype(m_hr)>::max();
    static const constexpr uint8_t  badCad = std::numeric_limits<decltype(m_cad)>::max();
};

float PointItem::power(const PowerData& pd) const
{
    if (hasPower(Measured))
        return m_power;

    if (pd.isZero())
        return 0.0;

    const float v = speed();
    const float temp = hasAtemp() ? atemp() : 15.0;                 // use atemp, or default 15C
    const float density = pd.density(ele(), temp);                  // medium density

    return std::max(v*(0.5 * density * pd.CdA * v*v +               // drag resistance
                       pd.mass * pd.g * (m_gradeSinAtan + pd.rr) +  // climb and rolling resistance
                       0.0 /*pd.mass * accel()*/),                  // TODO: acceleration
                    0.0);
}

// Serialization
extern QDataStream& operator<<(QDataStream&, const PointItem&);
extern QDataStream& operator>>(QDataStream&, PointItem&);

// Hashing.
uint qHash(const PointItem& key, uint seed = 0xc725139f);

// Convert to GeoDataCoordinates for Marble
template<> inline Marble::GeoDataCoordinates PointItem::as<Marble::GeoDataCoordinates>() const
{
    return Marble::GeoDataCoordinates(lon(), lat(), ele(), Marble::GeoDataCoordinates::Degree);
}

#endif // POINTITEM_H
