/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CFGDATA_H
#define CFGDATA_H

#include <cstdint>
#include <QList>
#include <QSize>

#include <src/core/cfgdatabase.h>
#include <src/core/svgcolorizer.h>
#include <src/core/colorizermodel.h>
#include <src/util/units.h>
#include "src/core/tagmodel.h"
#include "src/core/personmodel.h"
#include "src/core/pointmodel.h"
#include "src/core/trkptcolormodel.h"

class MainWindow;

class CfgData final : public CfgDataBase
{
public:
    explicit CfgData(const MainWindow& mainWindow);

    // helper to call static initializers before other object construction
    struct InitStatic { InitStatic(CfgData* cfgData); };

    // must be initialized first
    InitStatic      initStatic;

    int32_t         eleFilterSize;            // convolution filter size for elevation data
    int32_t         locFilterSize;            // convolution filter size for lon/lat data

    Units           unitsTrkLength;           // units for track length
    Units           unitsLegLength;           // units for leg (point to point) length
    Units           unitsDuration;            // units for durations
    Units           unitsTrkDate;             // units for track date+time stamp
    Units           unitsTrkTime;             // units for track time of day (no date)
    Units           unitsPointDate;           // units for point time
    Units           unitsElevation;           // units for elevation
    Units           unitsLat;                 // units for latitudes
    Units           unitsLon;                 // units for longitudes
    Units           unitsSpeed;               // units for speeds
    Units           unitsAccel;               // units for acceleration
    Units           unitsClimb;               // units for ascent/descent
    Units           unitsArea;                // units for areas
    Units           unitsTemp;                // units for temperatures
    Units           unitsSlope;               // units for slopes
    Units           unitsPower;               // units for power
    Units           unitsEnergy;              // units for energy
    Units           unitsWeight;              // units for weight
    Units           unitsPct;                 // units for percents

    QColor          unassignedTrackColor;     // color for tracks without one.
    QColor          outlineTrackColor;        // color for current track outline

    float           defaultTrackWidthC;       // normal track draw width, close
    float           defaultTrackWidthF;       // normal track draw width, far
    float           defaultTrackWidthO;       // normal track draw width, outline
    float           currentTrackWidthC;       // current track width, close
    float           currentTrackWidthF;       // current track width, far
    float           currentTrackWidthO;       // current track width, outline

    int32_t         defaultTrackAlphaC;       // default track alpha, close
    int32_t         defaultTrackAlphaF;       // default track alpha, far
    int32_t         currentTrackAlphaC;       // current track alpha, close
    int32_t         currentTrackAlphaF;       // current track alpha, far

    QString         defaultPointIcon;         // unselected point icon
    int32_t         defaultPointIconSize;     // unselected point icon size
    int32_t         defaultPointIconProx;     // unselected point icon minimum proximity
    QString         selectedPointIcon;        // selected point icon
    int32_t         selectedPointIconSize;    // selected point icon size
    QString         currentPointIcon;         // current point icon
    int32_t         currentPointIconSize;     // current point icon size

    QString         trackNoteIcon;            // icon to indicate track has a note.
    bool            colorizeTagIcons;         // colorize tag SVG icons with tag color.
    QSize           iconSizeTrack;            // icon size in track panes
    QSize           iconSizeView;             // icon size in view panes
    QSize           iconSizeTag;              // icon size in tag selectors
    QSize           iconSizeFilter;           // icon size in view panes
    int32_t         maxTrackPaneIcons;        // maximum icons to display in track lists

    TagModel        tags;                     // tag data
    PersonModel     people;                   // people (atheletes, passengers...)
    ColorizerModel  trackColorizer;           // track data colorization
    ColorizerModel  pointColorizer;           // track point colorization
    TrkPtColorModel trkPtColor;               // track point colors for graphs etc.

    QColor          trkPtMarkerColor;         // color for position marker in TrakeLinePane
    float           trkPtLineWidth;           // track point line width

    int32_t         backupDataCount;          // number of GPS data file backups
    int32_t         dataAutosaveInterval;     // auto-save interval, seconds.
    QString         dataAutosavePath;         // auto-save path: use UI .conf path if blank

    uint32_t        cfgDataVersion;           // so we can update old formats on load.

    void reset() override;                    // reset to defaults

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

    static SvgColorizer svgColorizer;        // common SVG colorizer cache
    static const constexpr QSize iconPad = QSize(4, 4);
    static const PointModel*     emptyPointModel;
    const QVector<QColor>        valColorSet() const;
    const QVector<QColor>        ampmColorSet() const;
    static const QVector<QColor> accelColor;

private:
    static const constexpr uint32_t currentCfgDataVersion = 2;
    static const constexpr char* air = "Air";

    static TreeItem::ItemData tag(const QString& dir, const QString& name, QRgb color,
                                  qreal CdA = 0.0, qreal weight = 0.0, qreal rr = 0.0,
                                  qreal efficiency = 0.0, qreal bioPct = -1.0,
                                  const char* medium = nullptr);

    void defaultTags();
    void defaultPeople();
    void defaultTrackColorizers();
    void defaultPointColorizers();
    void updateFormat();

    const MainWindow* mainWindow;
};

#endif // CFGDATA_H
