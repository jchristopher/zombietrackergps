/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TAGMODEL_H
#define TAGMODEL_H

#include <tuple>
#include <QModelIndex>
#include <QString>
#include <QStringList>
#include <QMap>

#include <src/core/modelmetadata.h>
#include <src/core/contentaddrmodel.h>

class CfgData;
class TagItem;
class Units;

class TagModel final : public ContentAddrModel, public ModelMetaData
{
    Q_OBJECT

public:
    enum {
        _First,
        Name = _First, // holds both icon decoration, and its filename
        Color,         // track and icon color, when using this tag
        Icon,          // icon to display with tracks
        CdA,           // optional drag coefficient * frontal area
        Weight,        // optional vehicle weight
        RR,            // optional rolling resistance
        Efficiency,    // power efficiency, %
        BioPct,        // percent biological power (1-vehicle internal power)
        Medium,        // medium name
        UnitSpeed,     // per-tag override of default speed unit
        _Count,
    };

    TagModel(const CfgData&, QObject *parent = 0);

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    bool isCategory(const QModelIndex&) const;
    void setCategory(const QModelIndex&, bool);

    void setUniqueIds(); // for tracking renamings and deletions to update users.
    QHash<QString, QString> newIdNames(const TagModel& old); // new name map

    const Units& units(const QModelIndex& idx) const override;
    const Units& units(const QString& key, ModelType) const;

    static const QMap<QString, std::tuple<float, bool>> medium; // medium, density, and elev variance

    static const QStringList mediumNames();

    // *** begin TreeModel API
    bool appendRows(bool category, const QVector<TreeItem::ItemData>& data,
                    const QModelIndex& parent = QModelIndex());
    bool appendRow(bool category, const TreeItem::ItemData& data,
                   const QModelIndex& parent = QModelIndex());
    // *** end TreeModel API

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static bool           mdIsChartable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static int            mdDataRole(ModelType);
    static const Units&   mdUnits(ModelType, const CfgData&);
    // *** end ModelMetaData API

    TagModel& operator=(const TagModel& rhs);

private:
    const CfgData& cfgData;

    TagItem* getItem(const QModelIndex &idx) const;

    Qt::DropActions supportedDropActions() const override { return Qt::MoveAction; }
    Qt::ItemFlags flags(const QModelIndex&) const override;
};

#endif // TAGMODEL_H
