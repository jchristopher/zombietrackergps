/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include "cfgdata.h"

#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/icons.h>
#include "src/ui/windows/mainwindow.h"

decltype(CfgData::svgColorizer)  CfgData::svgColorizer;
decltype(CfgData::emptyPointModel) CfgData::emptyPointModel = nullptr;

// Colors for accel colorizer entries
decltype(CfgData::accelColor) CfgData::accelColor = {
    QRgb(0xff00ff55),
    QRgb(0xffff0000),
};

CfgData::CfgData(const MainWindow& mainWindow) :
    initStatic(this),
    eleFilterSize(5),
    locFilterSize(1),
    unitsTrkLength(Format::Distkm,         2, false),
    unitsLegLength(Format::Distm,          0, false),
    unitsDuration(Format::DurDHMS,         0, false),
    unitsTrkDate(Format::DateLocaleShort,  0, false),
    unitsTrkTime(Format::Timehhmmss,       0, false),
    unitsPointDate(Format::Datehhmmss,     0, false),
    unitsElevation(Format::Distm,          0, false),
    unitsLat(Format::GeoPosDMS,            2, false, 'S', 'N'),
    unitsLon(Format::GeoPosDMS,            2, false, 'W', 'E'),
    unitsSpeed(Format::SpeedKPH,           1, false),
    unitsAccel(Format::AccelMPS2,          2, false),
    unitsClimb(Format::Distm,              2, false),
    unitsArea(Format::Areakm2,             2, false),
    unitsTemp(Format::TempC,               0, false),
    unitsSlope(Format::SlopePct,           0, false),
    unitsPower(Format::Watt,               0, false),
    unitsEnergy(Format::kcal,              0, false),
    unitsWeight(Format::kg,                0, false),
    unitsPct(Format::Percent,              2, false),
    unassignedTrackColor(QRgb(0x00c80000)),
    outlineTrackColor(QRgb(0x00000000)),
    defaultTrackWidthC(2.0),
    defaultTrackWidthF(3.0),
    defaultTrackWidthO(0.5),
    currentTrackWidthC(6.0),
    currentTrackWidthF(4.0),
    currentTrackWidthO(2.5),
    defaultTrackAlphaC(128),
    defaultTrackAlphaF(196),
    currentTrackAlphaC(255),
    currentTrackAlphaF(255),
    defaultPointIcon(":art/points/Circle/Wht10Blk00.png"),
    defaultPointIconSize(8),
    defaultPointIconProx(16),
    selectedPointIcon(":art/points/Circle/Yel10Blk00.png"),
    selectedPointIconSize(11),
    currentPointIcon(":art/points/Bullseye/Yel10Blk00.png"),
    currentPointIconSize(20),
    trackNoteIcon(Util::LocalThemeIcon("actions/22/note.svg")),
    colorizeTagIcons(true),
    iconSizeTrack(40, 28),
    iconSizeView(45, 31),
    iconSizeTag(45, 31),
    iconSizeFilter(40, 28),
    maxTrackPaneIcons(5),
    tags(*this),
    people(*this),
    trackColorizer(&mainWindow.trackModel(), *this),
    pointColorizer(emptyPointModel, *this),
    trkPtMarkerColor(Util::IsLightTheme() ? QRgb(0xffaaaa00) : QRgb(0xffffff00)),
    trkPtLineWidth(1.5),
    backupDataCount(2),
    dataAutosaveInterval(5),
    dataAutosavePath(""),
    cfgDataVersion(currentCfgDataVersion),
    mainWindow(&mainWindow)
{
    defaultTags();
    defaultPeople();
    defaultTrackColorizers();
    defaultPointColorizers();
}

CfgData::InitStatic::InitStatic(CfgData* cfgData)
{
    if (emptyPointModel == nullptr)
        emptyPointModel = new PointModel(nullptr, *cfgData);
}

// Return tag model data to create an entry from the provided data.
TreeItem::ItemData CfgData::tag(const QString& dir, const QString& name, QRgb color,
                                qreal CdA, qreal weight, qreal rr,
                                qreal efficiency, qreal bioPct, const char* medium)
{
    TreeItem::ItemData data(TagModel::_Count);
    data[TagModel::Name]  = name.mid(name.lastIndexOf('/') + 1);

    if (color != QRgb(0))  data[TagModel::Color]      = QColor(color);
    if (CdA != 0.0)        data[TagModel::CdA]        = CdA;
    if (weight != 0.0)     data[TagModel::Weight]     = weight;
    if (rr != 0.0)         data[TagModel::RR]         = rr;
    if (efficiency != 0.0) data[TagModel::Efficiency] = efficiency;
    if (bioPct >= 0.0)     data[TagModel::BioPct]     = bioPct;
    if (medium != nullptr) data[TagModel::Medium]     = QString(medium);

    data[TagModel::Icon] = QString(":art/tags/") + dir + "/" + name + ".svg";

    return data;
}

// See also: CfgData::updateFormat()
void CfgData::defaultTags()
{
    if (!tags.appendRows(true, { { "Activity" }, { "Transport" }, { "Weather" } }))
        return;

    tags.appendRows(false,
                    { tag("Activity", "Camp",        0xff009548),
                      tag("Activity", "Commute",     0xff4371d5, 0.400, 12.5, 0.00325, 0.97, 1.0, air),
                      tag("Activity", "Errands",     0xffb05075),
                      tag("Activity", "Recreation",  0xff688800),
                      tag("Activity", "Soccer",      0xff779777),
                      tag("Activity", "Work",        0xffafa844),
                      tag("Activity", "Zombies",     0xffb364d4),
                    }, tags.index(0, 0));

    tags.appendRows(false,
                    { tag("Transport", "Board",      0xffd9efff),
                      tag("Transport", "Canoe",      0xff226496),
                      tag("Transport", "Car",        0xff96651f, 0.700, 1400.0, 0.03, 0.25, 0.0, air),
                      tag("Transport", "Climb",      0xff9c977f),
                      tag("Transport", "Cross",      0xff33eedd, 0.400, 13.0, 0.003, 0.97, 1.0, air),
                      tag("Transport", "Handcycle",  0xff33e1dd, 0.399, 12.0, 0.003, 0.98, 1.0, air),
                      tag("Transport", "HangGlide",  0xffa14885),
                      tag("Transport", "Heli",       0xff00c1c1),
                      tag("Transport", "Hike",       0xff04b645, 1.0, 1.0, 0.045),
                      tag("Transport", "Horse",      0xffdf9140),
                      tag("Transport", "Motorcycle", 0xff4f7882),
                      tag("Transport", "Mountain",   0xffc37938, 0.450, 13.0, 0.006, 0.95, 1.0, air),
                      tag("Transport", "Nordic",     0xffa9b7dc),
                      tag("Transport", "Plane",      0xff88478d),
                      tag("Transport", "Parachute",  0xffb34775),
                      tag("Transport", "Paraglide",  0xff813875),
                      tag("Transport", "Road",       0xff00d1ff, 0.325, 12.0, 0.0025, 0.98, 1.0, air),
                      tag("Transport", "Run",        0xffdf9100, 1.0, 1.0, 0.063),
                      tag("Transport", "Sail",       0xff1347cc),
                      tag("Transport", "Ski",        0xffa9b7dc),
                      tag("Transport", "Surf",       0xff1367cc),
                      tag("Transport", "Swim",       0xff008bd0),
                      tag("Transport", "Train",      0xff6e6f94),
                      tag("Transport", "TT",         0xff02ff9e, 0.240, 8.5, 0.002, 0.98, 1.0, air),
                      tag("Transport", "Wheelchair", 0xff55c1dd, 0.399, 12.0, 0.003, 0.98, 1.0, air),
                    }, tags.index(1, 0));

    tags.appendRows(false,
                    { tag("Weather", "Clear",        0x0),
                      tag("Weather", "Clouds",       0x0),
                      tag("Weather", "Fog",          0x0),
                      tag("Weather", "Overcast",     0x0),
                      tag("Weather", "Showers-Day",  0x0),
                      tag("Weather", "Snow",         0x0),
                      tag("Weather", "Storm",        0x0)
                    }, tags.index(2, 0));
}

void CfgData::defaultPeople()
{
    people.appendRows({ { "Male 1",   70.0_kg, 0.22 },
                        { "Female 1", 60.0_kg, 0.22 } });
}

// Not static data because we react to IsLightTheme
const QVector<QColor> CfgData::valColorSet() const
{
    // Colors for default colorizer entries
    return {
        QRgb(Util::IsLightTheme() ? 0xffcc0000 : 0xffff0000),
        QRgb(Util::IsLightTheme() ? 0xffbb6600 : 0xffff9900),
        QRgb(Util::IsLightTheme() ? 0xffaaaa00 : 0xffffff00),
        QRgb(Util::IsLightTheme() ? 0xff00bb77 : 0xff00ffaa),
        QRgb(Util::IsLightTheme() ? 0xff0077bb : 0xff0099ff),
        QRgb(Util::IsLightTheme() ? 0xff8800cc : 0xff9900ff),
    };
}

// Not static data because we react to IsLightTheme
const QVector<QColor> CfgData::ampmColorSet() const
{
    return {
        Util::IsLightTheme() ? QRgb(0xff77aadd) : QRgb(0xff99ccff),
        Util::IsLightTheme() ? QRgb(0xffddaa77) : QRgb(0xffffcc99),
    };
}

void CfgData::defaultTrackColorizers()
{
    const auto valColor  = valColorSet();
    const auto ampmColor = ampmColorSet();

    trackColorizer.appendRows({
        { TrackModel::Ascent,       "Ascent >= 1000m",     valColor.at(0) },
        { TrackModel::Ascent,       "Ascent >= 750m",      valColor.at(1) },
        { TrackModel::Ascent,       "Ascent >= 500m",      valColor.at(2) },
        { TrackModel::Ascent,       "Ascent >= 250m",      valColor.at(3) },
        { TrackModel::Ascent,       "Ascent >= 100m",      valColor.at(4) },

        { TrackModel::BeginDate,    "Begin_Time < 12:00",  ampmColor.at(0) }, // date colorized by am/pm time
        { TrackModel::BeginDate,    "Begin_Time >= 12:00", ampmColor.at(1) }, // ...
        { TrackModel::EndDate,      "End_Time < 12:00",    ampmColor.at(0) }, // ...
        { TrackModel::EndDate,      "End_Time >= 12:00",   ampmColor.at(1) }, // ...
        { TrackModel::BeginTime,    "Begin_Time < 12:00",  ampmColor.at(0) },
        { TrackModel::BeginTime,    "Begin_Time >= 12:00", ampmColor.at(1) },
        { TrackModel::EndTime,      "End_Time < 12:00",    ampmColor.at(0) },
        { TrackModel::EndTime,      "End_Time >= 12:00",   ampmColor.at(1) },

        { TrackModel::MovingTime,   "Moving_Time >= 5h",   valColor.at(0) },
        { TrackModel::MovingTime,   "Moving_Time >= 4h",   valColor.at(1) },
        { TrackModel::MovingTime,   "Moving_Time >= 3h",   valColor.at(2) },
        { TrackModel::MovingTime,   "Moving_Time >= 2h",   valColor.at(3) },
        { TrackModel::MovingTime,   "Moving_Time >= 1h",   valColor.at(4) },

        { TrackModel::TotalTime,    "Total_Time >= 5h",    valColor.at(0) },
        { TrackModel::TotalTime,    "Total_Time >= 4h",    valColor.at(1) },
        { TrackModel::TotalTime,    "Total_Time >= 3h",    valColor.at(2) },
        { TrackModel::TotalTime,    "Total_Time >= 2h",    valColor.at(3) },
        { TrackModel::TotalTime,    "Total_Time >= 1h",    valColor.at(4) },

        { TrackModel::Length,       "Length >= 25km & Tags =~ Hike", valColor.at(0) },
        { TrackModel::Length,       "Length >= 20km & Tags =~ Hike", valColor.at(1) },
        { TrackModel::Length,       "Length >= 15km & Tags =~ Hike", valColor.at(2) },
        { TrackModel::Length,       "Length >= 10km & Tags =~ Hike", valColor.at(3) },
        { TrackModel::Length,       "Length >= 5km  & Tags =~ Hike", valColor.at(4) },

        { TrackModel::Length,       "Length >= 100km",     valColor.at(0) },
        { TrackModel::Length,       "Length >= 80km",      valColor.at(1) },
        { TrackModel::Length,       "Length >= 60km",      valColor.at(2) },
        { TrackModel::Length,       "Length >= 40km",      valColor.at(3) },
        { TrackModel::Length,       "Length >= 20km",      valColor.at(4) },

        { TrackModel::MaxElevation, "Max_Elev >= 5000m",   valColor.at(0) },
        { TrackModel::MaxElevation, "Max_Elev >= 4000m",   valColor.at(1) },
        { TrackModel::MaxElevation, "Max_Elev >= 3000m",   valColor.at(2) },
        { TrackModel::MaxElevation, "Max_Elev >= 2000m",   valColor.at(3) },
        { TrackModel::MaxElevation, "Max_Elev >= 1000m",   valColor.at(4) },

        { TrackModel::MinElevation, "Min_Elev >= 5000m",   valColor.at(0) },
        { TrackModel::MinElevation, "Min_Elev >= 4000m",   valColor.at(1) },
        { TrackModel::MinElevation, "Min_Elev >= 3000m",   valColor.at(2) },
        { TrackModel::MinElevation, "Min_Elev >= 2000m",   valColor.at(3) },
        { TrackModel::MinElevation, "Min_Elev >= 1000m",   valColor.at(4) },

        { TrackModel::BasePeak,     "Peak-Base >= 1200m",  valColor.at(0) },
        { TrackModel::BasePeak,     "Peak-Base >= 800m",   valColor.at(1) },
        { TrackModel::BasePeak,     "Peak-Base >= 600m",   valColor.at(2) },
        { TrackModel::BasePeak,     "Peak-Base >= 400m",   valColor.at(3) },
        { TrackModel::BasePeak,     "Peak-Base >= 200m",   valColor.at(4) },

        { TrackModel::MaxGrade,     "Max_Grade >= 20%",    valColor.at(0) },
        { TrackModel::MaxGrade,     "Max_Grade >= 15%",    valColor.at(1) },
        { TrackModel::MaxGrade,     "Max_Grade >= 10%",    valColor.at(2) },
        { TrackModel::MaxGrade,     "Max_Grade >= 5%",     valColor.at(3) },
        { TrackModel::MaxGrade,     "Max_Grade >= 2%",     valColor.at(4) },

        { TrackModel::MinGrade,     "Min_Grade <= -20%",   valColor.at(0) },
        { TrackModel::MinGrade,     "Min_Grade <= -15%",   valColor.at(1) },
        { TrackModel::MinGrade,     "Min_Grade <= -10%",   valColor.at(2) },
        { TrackModel::MinGrade,     "Min_Grade <= -5%",    valColor.at(3) },
        { TrackModel::MinGrade,     "Min_Grade <= -2%",    valColor.at(4) },

        { TrackModel::Energy,       "Energy >= 2000kcal",  valColor.at(0) },
        { TrackModel::Energy,       "Energy >= 1500kcal",  valColor.at(1) },
        { TrackModel::Energy,       "Energy >= 1000kcal",  valColor.at(2) },
        { TrackModel::Energy,       "Energy >= 500kcal",   valColor.at(3) },
        { TrackModel::Energy,       "Energy >= 250kcal",   valColor.at(4) },
    });
}

void CfgData::defaultPointColorizers()
{
    const auto valColor = valColorSet();

    pointColorizer.appendRows({
        { PointModel::Ele,   "Elevation >= 5000m",   valColor.at(0) },
        { PointModel::Ele,   "Elevation >= 4000m",   valColor.at(1) },
        { PointModel::Ele,   "Elevation >= 3000m",   valColor.at(2) },
        { PointModel::Ele,   "Elevation >= 2000m",   valColor.at(3) },
        { PointModel::Ele,   "Elevation >= 1000m",   valColor.at(4) },

        { PointModel::Grade, "Grade >= 20%",         valColor.at(0) },
        { PointModel::Grade, "Grade >= 10%",         valColor.at(1) },
        { PointModel::Grade, "Grade >= 2%",          valColor.at(2) },
        { PointModel::Grade, "Grade <= -2%",         valColor.at(3) },
        { PointModel::Grade, "Grade <= -10%",        valColor.at(4) },
        { PointModel::Grade, "Grade <= -20%",        valColor.at(5) },

        { PointModel::Power, "Power >= 500w",        valColor.at(0) },
        { PointModel::Power, "Power >= 250w",        valColor.at(1) },
        { PointModel::Power, "Power >= 150w",        valColor.at(2) },
        { PointModel::Power, "Power >= 100w",        valColor.at(3) },
        { PointModel::Power, "Power >= 50w",         valColor.at(4) },

        { PointModel::Accel, "Accel >= 0.05 m/s^2",  accelColor.at(0) },
        { PointModel::Accel, "Accel <= -0.05 m/s^2", accelColor.at(1) },
    });
}

void CfgData::reset()
{
    *this = CfgData(*mainWindow);
}

// If the CfgData loaded was old, we might want to update some values.
void CfgData::updateFormat()
{
    if (cfgDataVersion == currentCfgDataVersion)  // up to date
        return;

    const auto find = [this](const char* name) {
        return tags.findRow(QModelIndex(), name, TagModel::Name, Util::RawDataRole);
    };

    // To version 1: add new icons, tweak some values.
    if (cfgDataVersion < 1) {
        if (const QModelIndex idx = find("Run"); idx.isValid()) {
            tags.setData(TagModel::RR, idx, 0.063, Util::RawDataRole);
            tags.setData(TagModel::Weight, idx, 1.0, Util::RawDataRole);
        }

        if (const QModelIndex idx = find("Hike"); idx.isValid()) {
            tags.setData(TagModel::RR, idx, 0.045, Util::RawDataRole);
            tags.setData(TagModel::Weight, idx, 1.0, Util::RawDataRole);
        }

        // Try to add new icons, if we can find the category for them.  If the user already changed things
        // around too much, we can't do it.
        if (const QModelIndex idx = find("Activity"); idx.isValid() && tags.isCategory(idx)) {
            if (!find("Soccer").isValid())
                tags.appendRow(false, tag("Activity", "Soccer",      0xff779777), idx);
        }

        if (const QModelIndex idx = find("Transport"); idx.isValid() && tags.isCategory(idx)) {
            if (!find("HangGlide").isValid())
                tags.appendRow(false, tag("Transport", "HangGlide",  0xffa14885), idx);

            if (!find("Horse").isValid())
                tags.appendRow(false, tag("Transport", "Horse",      0xffdf9140), idx);

            if (!find("Nordic").isValid())
                tags.appendRow(false, tag("Transport", "Nordic",     0xffa9b7dc), idx);

            if (!find("Surf").isValid())
                tags.appendRow(false, tag("Transport", "Surf",       0xff1367cc), idx);
        }

        cfgDataVersion = 1;
    }

    // To version 2: add new icons
    if (cfgDataVersion < 2) {
        if (const QModelIndex idx = find("Transport"); idx.isValid() && tags.isCategory(idx)) {
            if (!find("Handcycle").isValid())
                tags.appendRow(false, tag("Transport", "Handcycle",  0xff33e1dd, 0.399, 12.0, 0.003, 0.98, 1.0, air), idx);

            if (!find("Parachute").isValid())
                tags.appendRow(false, tag("Transport", "Parachute",  0xffb34775), idx);

            if (!find("Wheelchair").isValid())
                tags.appendRow(false, tag("Transport", "Wheelchair", 0xff55c1dd, 0.399, 12.0, 0.003, 0.98, 1.0, air), idx);
        }

        cfgDataVersion = 2;
    }
}

void CfgData::save(QSettings& settings) const
{
    SL::Save(settings, versionKey, version());

    CfgDataBase::save(settings);

    MemberSave(settings, tags);
    MemberSave(settings, people);
    MemberSave(settings, trackColorizer);
    MemberSave(settings, pointColorizer);
    MemberSave(settings, trkPtColor);

    MemberSave(settings, trkPtLineWidth);
    MemberSave(settings, trkPtMarkerColor);

    MemberSave(settings, eleFilterSize);
    MemberSave(settings, locFilterSize);

    MemberSave(settings, unassignedTrackColor);
    MemberSave(settings, outlineTrackColor);
    MemberSave(settings, defaultTrackWidthC);
    MemberSave(settings, defaultTrackWidthF);
    MemberSave(settings, defaultTrackWidthO);
    MemberSave(settings, currentTrackWidthC);
    MemberSave(settings, currentTrackWidthF);
    MemberSave(settings, currentTrackWidthO);
    MemberSave(settings, defaultTrackAlphaC);
    MemberSave(settings, defaultTrackAlphaF);
    MemberSave(settings, currentTrackAlphaC);
    MemberSave(settings, currentTrackAlphaF);

    MemberSave(settings, defaultPointIcon);
    MemberSave(settings, defaultPointIconSize);
    MemberSave(settings, defaultPointIconProx);
    MemberSave(settings, selectedPointIcon);
    MemberSave(settings, selectedPointIconSize);
    MemberSave(settings, currentPointIcon);
    MemberSave(settings, currentPointIconSize);

    MemberSave(settings, trackNoteIcon);
    MemberSave(settings, colorizeTagIcons);
    MemberSave(settings, iconSizeTrack);
    MemberSave(settings, iconSizeView);
    MemberSave(settings, iconSizeTag);
    MemberSave(settings, iconSizeFilter);
    MemberSave(settings, maxTrackPaneIcons);

    MemberSave(settings, unitsTrkLength);
    MemberSave(settings, unitsLegLength);
    MemberSave(settings, unitsDuration);
    MemberSave(settings, unitsTrkDate);
    MemberSave(settings, unitsTrkTime);
    MemberSave(settings, unitsPointDate);
    MemberSave(settings, unitsElevation);
    MemberSave(settings, unitsLat);
    MemberSave(settings, unitsLon);
    MemberSave(settings, unitsSpeed);
    MemberSave(settings, unitsAccel);
    MemberSave(settings, unitsClimb);
    MemberSave(settings, unitsArea);
    MemberSave(settings, unitsTemp);
    MemberSave(settings, unitsSlope);
    MemberSave(settings, unitsPower);
    MemberSave(settings, unitsEnergy);
    MemberSave(settings, unitsWeight);
    MemberSave(settings, unitsPct);

    MemberSave(settings, backupDataCount);
    MemberSave(settings, dataAutosaveInterval);
    MemberSave(settings, dataAutosavePath);

    MemberSave(settings, cfgDataVersion);
}

void CfgData::load(QSettings& settings)
{
    reset(); // defaults for things not read

    // Don't load incompatible versions
    if (SL::Load(settings, versionKey, 0) != Settings::version())
        return;

    CfgDataBase::load(settings);

    MemberLoad(settings, tags);
    MemberLoad(settings, people);
    MemberLoad(settings, trackColorizer);
    MemberLoad(settings, pointColorizer);
    MemberLoad(settings, trkPtColor);

    MemberLoad(settings, trkPtLineWidth);
    MemberLoad(settings, trkPtMarkerColor);

    MemberLoad(settings, eleFilterSize);
    MemberLoad(settings, locFilterSize);

    MemberLoad(settings, unassignedTrackColor);
    MemberLoad(settings, outlineTrackColor);
    MemberLoad(settings, defaultTrackWidthC);
    MemberLoad(settings, defaultTrackWidthF);
    MemberLoad(settings, defaultTrackWidthO);
    MemberLoad(settings, currentTrackWidthC);
    MemberLoad(settings, currentTrackWidthF);
    MemberLoad(settings, currentTrackWidthO);
    MemberLoad(settings, defaultTrackAlphaC);
    MemberLoad(settings, defaultTrackAlphaF);
    MemberLoad(settings, currentTrackAlphaC);
    MemberLoad(settings, currentTrackAlphaF);

    MemberLoad(settings, defaultPointIcon);
    MemberLoad(settings, defaultPointIconSize);
    MemberLoad(settings, defaultPointIconProx);
    MemberLoad(settings, selectedPointIcon);
    MemberLoad(settings, selectedPointIconSize);
    MemberLoad(settings, currentPointIcon);
    MemberLoad(settings, currentPointIconSize);

    MemberLoad(settings, trackNoteIcon);
    MemberLoad(settings, colorizeTagIcons);
    MemberLoad(settings, iconSizeTrack);
    MemberLoad(settings, iconSizeView);
    MemberLoad(settings, iconSizeTag);
    MemberLoad(settings, iconSizeFilter);
    MemberLoad(settings, maxTrackPaneIcons);

    MemberLoad(settings, unitsTrkLength);
    MemberLoad(settings, unitsLegLength);
    MemberLoad(settings, unitsDuration);
    MemberLoad(settings, unitsTrkDate);
    MemberLoad(settings, unitsTrkTime);
    MemberLoad(settings, unitsPointDate);
    MemberLoad(settings, unitsElevation);
    MemberLoad(settings, unitsLat);
    MemberLoad(settings, unitsLon);
    MemberLoad(settings, unitsSpeed);
    MemberLoad(settings, unitsAccel);
    MemberLoad(settings, unitsClimb);
    MemberLoad(settings, unitsArea);
    MemberLoad(settings, unitsTemp);
    MemberLoad(settings, unitsSlope);
    MemberLoad(settings, unitsPower);
    MemberLoad(settings, unitsEnergy);
    MemberLoad(settings, unitsWeight);
    MemberLoad(settings, unitsPct);

    MemberLoad(settings, backupDataCount);
    MemberLoad(settings, dataAutosaveInterval);
    MemberLoad(settings, dataAutosavePath);

    if (settings.contains("cfgDataVersion"))
        MemberLoad(settings, cfgDataVersion);
    else
        cfgDataVersion = 0;

    updateFormat();
}
