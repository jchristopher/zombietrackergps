/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PERSONMODEL_H
#define PERSONMODEL_H

#include <tuple>
#include <QModelIndex>
#include <QString>

#include <src/core/modelmetadata.h>
#include <src/core/contentaddrmodel.h>

class CfgData;
class PersonItem;
class Units;

class PersonModel final : public ContentAddrModel, public ModelMetaData
{
    Q_OBJECT

public:
    enum {
        Name = 0,     // person name
        Weight,       // weight in Kg
        Efficiency,   // biomechanical efficiency
        _Count,
    };

    PersonModel(const CfgData&, QObject *parent = 0);

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    const Units& units(const QModelIndex& idx) const override;

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static bool           mdIsChartable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static int            mdDataRole(ModelType);
    static const Units&   mdUnits(ModelType, const CfgData&);
    // *** end ModelMetaData API

    PersonModel& operator=(const PersonModel& rhs);

private:
    const CfgData& cfgData;

    PersonItem* getItem(const QModelIndex &idx) const;

    Qt::DropActions supportedDropActions() const override { return Qt::MoveAction; }
    Qt::ItemFlags flags(const QModelIndex&) const override;
};

#endif // PERSONMODEL_H
