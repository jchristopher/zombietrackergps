/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <time.h>
#include "trackupdatethread.h"
#include "trackmodel.h"

TrackUpdateThread::TrackUpdateThread(TrackModel& model) :
    model(model)
{
}

void TrackUpdateThread::run()
{
    static const timespec ts = { 0, 20000000 };  // 0.02s

    while (true) {          // while there's a load still going on
        if (model.updateQueueProcess(*this)) // do some work, if any available
            continue;

        if (!model.loadPending(*this))     // if no work and no load pending, return.
            return;

        nanosleep(&ts, nullptr);           // wait for more work to appear
    }
}
