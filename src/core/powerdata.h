/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POWERDATA_H
#define POWERDATA_H

#include <cmath>

struct PowerData {
    PowerData(float CdA = 0.0, float mass = 0.0, float rr = 0.0, float efficiency = 1.0, 
              float density = 1.225, bool eleVariance = true) :
        CdA(std::max(CdA, 0.0f)),
        mass(std::max(mass, 0.0f)),
        rr(std::max(rr, 0.0f)),
        efficiency(std::clamp(efficiency, 0.001f, 1.0f)),
        m_density(density),
        eleVariance(eleVariance)
    { }

    void clear() {
        CdA = mass = rr = 0.0;
        efficiency = 1.0;
        m_density = 1.225;
    }

    bool isZero() const { return CdA == 0.0 && mass == 0.0 && rr == 0.0 && efficiency == 1.0; }

    // medium density as function of altitude and temperature in degC
    inline float density(float eleM = 0.0, float tempC = 15.0) const {
        if (!eleVariance)
            return m_density;

        static const float pressure0 = 101325.0; // pressure, Pa
        static const float R = 1.0/287.058;      // recip specific gas constant

        const float tempK = tempC+273.15;

        // Approximate pressure v altitude as a linear function, for performance.  Given all
        // the other approximations involved, this isn't our biggest source of error anyway,
        // and it saves an exponential.  It's roughly OK between 0-4000m.
        const float pressure = pressure0 * (1.0 - (eleM * 0.000105));

        return pressure * R / tempK;
    }

    // Values to use for power estimations:
    float CdA;          // drag coefficient * frontal area, m^2
    float mass;         // total vehicle mass (rider+bike, etc)
    float rr;           // rolling resistance
    float efficiency;   // overall efficiency (rider*bike, etc)
    float m_density;    // medium density, kg/m^3
    bool  eleVariance;  // density varies with elevation

    static const constexpr float g = 9.8;
};

#endif // POWERDATA_H
