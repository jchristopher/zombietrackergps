/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKMODEL_H
#define TRACKMODEL_H

#include <QPersistentModelIndex>
#include <QHash>
#include <QQueue>
#include <cassert>
#include <tuple>
#include <limits>

#include <marble/GeoDataLatLonBox.h>

#include <src/core/modelmetadata.h>
#include <src/core/treemodel.h>

#include "src/ui/panes/mappane.h"
#include "geotypes.h"

class TrackItem;
class PointModel;
class CfgData;
class TrackUpdateThread;
class Units;
class QDataStream;

enum TrackType {
    Trk,  // tracks
    Rte,  // routes
    Wpt,  // waypoints
};

Q_DECLARE_METATYPE(TrackType)
QDataStream& operator<<(QDataStream& out, const TrackType&);
QDataStream& operator>>(QDataStream& in, TrackType&);

class TrackModel final : public TreeModel, public ModelMetaData
{
    Q_OBJECT

public:
    // If new data columns are added, matching changes must be made to:
    //    1. The TrackDetailPane display format
    //    2. The binary save format in GeoIoNative
    enum {
        _First = 0,
        Name  = _First,   // GPS track name
        Icon  = Name,     // icon goes in name column
        Type,             // waypoint, route, or track
        Tags,             // track tags, comma separated list
        Color,            // track display color
        Notes,            // track user notes
        Keywords,         // track keywords
        Length,           // track length, m
        BeginDate,        // earliest timestamp in track
        EndDate,          // latest timestamp in track
        BeginTime,        // earliest time (no date) in track
        EndTime,          // latest time (no date) in track
        StoppedTime,      // total stopped time
        MovingTime,       // total moving time
        TotalTime,        // total track time (LastTime - FirstTime)
        MinElevation,     // track min elevation, m
        MaxElevation,     // track max elevation, m
        MinSpeed,         // track min speed, m/s
        AvgOvrSpeed,      // average overall speed, m/s
        AvgMovSpeed,      // average moving speed, m/s
        MaxSpeed,         // track max speed, m/s
        MinGrade,         // min grade, %
        MaxGrade,         // max grade, %
        MinCad,           // min cadence
        AvgMovCad,        // average moving cadence
        MaxCad,           // max cadence
        MinPower,         // min power, watts
        AvgMovPower,      // average moving power, watts
        MaxPower,         // max power, watts
        Energy,           // estimated input energy
        Ascent,           // track ascent, m
        Descent,          // track descent, m
        BasePeak,         // track peak-base, m
        Segments,         // number of track segments
        Points,           // number of points in track
        Area,             // track area
        MinTemp,          // min temperature, C
        AvgTemp,          // avg temperature, C
        MaxTemp,          // max temperature, C
        MinHR,            // min heart rate, bpm
        AvgHR,            // average HR, bpm
        MaxHR,            // max heart rate, bpm
        Laps,             // number of laps (TODO: record lap data)
        MinLon,           // min longitude
        MinLat,           // min latitude
        MaxLon,           // max longitude
        MaxLat,           // max latitude
        _Count,
    };

    TrackModel(const CfgData& cfgData, QObject* parent = 0);

    using TreeModel::data;
    QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const override;

    using TreeModel::setData;
    bool setData(const QModelIndex &idx, const QVariant& value,
                 int role = Qt::DisplayRole) override;

    // NOTE: for efficiency, this is destructive to the newPoint list: it uses std::swap.
    void appendRow(const QString& name, TrackType type,
                   const QStringList& tags,
                   const QColor& color,
                   const QString& notes,
                   const QString& keywords,
                   PointModel& newPoints,
                   const QModelIndex& parent = QModelIndex());

    void eraseTrackSelection(const QModelIndex& idx,
                             const QItemSelectionModel*, const QSortFilterProxyModel*);

    void mergeTrackSegments(const QModelIndex& idx,
                            const QItemSelectionModel*, const QSortFilterProxyModel*);

    void splitTrackSegments(const QModelIndex& idx,
                            const QItemSelectionModel*, const QSortFilterProxyModel*);

    Marble::GeoDataLatLonBox bounds(const QModelIndex& idx) const;

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    const Units& units(const QModelIndex& idx) const override;

    void refresh() override;

    std::tuple<qreal, qreal, qreal, qreal, bool> bounds(const QModelIndexList& selection) const;
    Marble::GeoDataLatLonBox boundsBox(const QModelIndexList& selection) const;

    QString tooltip(const QModelIndex& idx) const;

    // Disable save/restore for this: our data will be populated from a disk DB.
    // We can't use "= delete" here though
    // *** begin Settings API
    void load(QSettings&) override { assert(0); }
    void save(QSettings&) const override { assert(0); }
    // *** end Settings API

    // *** begin Stream Save API
    QDataStream& load(QDataStream&, const QModelIndex& parent = QModelIndex(), bool append = false) override;
    using TreeModel::save;
    // *** end Stream Save API

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static bool           mdIsChartable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static const Units&   mdUnits(ModelType, const CfgData&);
    static inline bool    mdSave(ModelType);
    // *** end ModelMetaData API

    // APIs only for use by selected classes.  Template guards against accidental general use.
    template <class T> const PointModel* geoPoints(const T&, const QModelIndex&) const;
    template <class T> const TrackSegLines& trackLines(const T&, const QModelIndex&) const;
    template <class T> void setAllVisible(const T&, bool);
    template <class T> void setVisible(const T&, const QModelIndex&, bool);
    template <class T> bool isVisible(const T&, const QModelIndex&) const;
    template <class T> void setAreaSelected(const T&, bool);
    template <class T> void setAreaSelected(const T&, const QModelIndex&, bool);
    template <class T> bool isAreaSelected(const T&, const QModelIndex&) const;
    template <class T> void selectPointsWithin(const T&, const Marble::GeoDataLatLonBox&);
    template <class T> bool updateQueueProcess(const T &);
    template <class T> void updateQueueAdd(T* item);
    template <class T> bool loadPending(const T&) const;
    template <class T> QModelIndex personIdx(const T&) const;

    PointModel* geoPoints(const QModelIndex&);
    const PointModel* geoPoints(const QModelIndex&) const;

    // For power estimations
    void setPerson(const QString& person);

    static QString trackTypeName(TrackType);

    QHash<uint, QModelIndex> trackHashes() const; // return set of hashes for contained tracks (unordered!)
    uint trackHash(const QModelIndex& idx) const; // hash for a single track

    bool pointEqual(const QModelIndex& idx, const PointModel&) const;
    bool pointEqual(const QModelIndex& idx0, const QModelIndex& idx1) const;

protected:
    TrackItem* getItem(const QModelIndex &index) const;

    Qt::ItemFlags flags(const QModelIndex&) const override;

private:
    void emitDataChange(int row, int col);

    void beginThreads();
    void endThreads();

    const CfgData&     cfgData;
    QQueue<TrackItem*> updateQueue; // outstanding indexes to update in a BG thread.
    bool               m_loadPending; // for multithreaded processing at load time
    QPersistentModelIndex m_personIdx;

    friend QDataStream& operator<<(QDataStream&, const TrackModel&);
    friend QDataStream& operator>>(QDataStream&, TrackModel&);
};

// Serialization
extern QDataStream& operator<<(QDataStream&, const TrackModel&);
extern QDataStream& operator>>(QDataStream&, TrackModel&);

// Inline for performance
inline bool TrackModel::mdSave(ModelType mt)
{
    switch (mt) {
    case TrackModel::Name:         [[fallthrough]];
    case TrackModel::Type:         [[fallthrough]];
    case TrackModel::Tags:         [[fallthrough]];
    case TrackModel::Color:        [[fallthrough]];
    case TrackModel::Notes:        [[fallthrough]];
    case TrackModel::Keywords:     return true;
    default:                       return false;
    }
}

#endif // TRACKMODEL_H

