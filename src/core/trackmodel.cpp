/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QThreadPool>
#include <QDataStream>
#include <src/util/util.h>
#include <src/util/units.h>
#include <src/util/versionedstream.h>

#include "pointmodel.h"
#include "trackupdatethread.h"
#include "trackmodel.h"
#include "trackitem.h"
#include "pointmodel.h"

TrackModel::TrackModel(const CfgData& cfgData, QObject* parent) :
    TreeModel(new TrackItem(cfgData), parent),
    cfgData(cfgData),
    m_loadPending(false)
{
    setHorizontalHeaderLabels(headersList<TrackModel>());
}

void TrackModel::appendRow(const QString& name, TrackType type,
                           const QStringList& tags,
                           const QColor& color,
                           const QString& notes,
                           const QString& keywords,
                           PointModel& newPoints,
                           const QModelIndex& parent)
{
    TreeItem::ItemData data;

    // TrackModel namespace not needed here, but it improves greppability.
    data.resize(TrackModel::_Count);

    if (!name.isEmpty())     data[TrackModel::Name]     = name;
    if (!tags.isEmpty())     data[TrackModel::Tags]     = tags;
    if (color.isValid())     data[TrackModel::Color]    = color;
    if (!notes.isEmpty())    data[TrackModel::Notes]    = notes;
    if (!keywords.isEmpty()) data[TrackModel::Keywords] = keywords;
    data[TrackModel::Type]     = type;

    TreeModel::appendRow(data, parent);

    const QModelIndex newIdx = index(rowCount()-1, 0, parent);

    getItem(newIdx)->append(*this, newPoints);

    emit dataChanged(newIdx, rowSibling(columnCount()-1, newIdx));
}

void TrackModel::eraseTrackSelection(const QModelIndex& idx,
                                     const QItemSelectionModel* selectionModel,
                                     const QSortFilterProxyModel* filter)
{
    if (!idx.isValid())
        return;

    getItem(idx)->geoPoints.removeRows(selectionModel, filter);
    getItem(idx)->update();
}

void TrackModel::mergeTrackSegments(const QModelIndex &idx, const QItemSelectionModel* selectionModel,
                                    const QSortFilterProxyModel* filter)
{
    if (!idx.isValid())
        return;

    getItem(idx)->geoPoints.mergeSegments(selectionModel, filter);
    getItem(idx)->update();
}

void TrackModel::splitTrackSegments(const QModelIndex &idx, const QItemSelectionModel* selectionModel,
                                    const QSortFilterProxyModel* filter)
{
    if (!idx.isValid())
        return;

    getItem(idx)->geoPoints.splitSegments(selectionModel, filter);
    getItem(idx)->update(true);
}

Marble::GeoDataLatLonBox TrackModel::bounds(const QModelIndex& idx) const
{
    return getItem(idx)->bounds();
}

QVariant TrackModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (QVariant val = ModelMetaData::headerData<TrackModel>(section, orientation, role); val.isValid())
        return val;

    return TreeModel::headerData(section, orientation, role);
}

const Units& TrackModel::units(const QModelIndex& idx) const
{
    if (idx.isValid())
        return getItem(idx)->units(idx.column());  // use tag specific override if available

    return mdUnits(idx.column(), cfgData);
}


QString TrackModel::tooltip(const QModelIndex& idx) const
{
    static const QVector<ModelType> tooltipColumns = {
        TrackModel::BeginDate,
        TrackModel::Length,
        TrackModel::TotalTime,
        TrackModel::MovingTime,
        TrackModel::MaxElevation,
        TrackModel::AvgMovSpeed,
        TrackModel::MaxSpeed,
        TrackModel::Ascent,
        TrackModel::MaxGrade,
        TrackModel::AvgMovPower,
        TrackModel::AvgHR,
        TrackModel::MaxHR,
        TrackModel::Energy,
    };

    const TrackItem* item = getItem(idx);

    QString tooltip = QString("<b><u><nobr><big>") +
            item->data(TrackModel::Name, Qt::DisplayRole).toString() +
            + "</big></nobr></u></b><p>";

    tooltip += "<table>";

    for (const auto col : tooltipColumns) {
        const QVariant color = cfgData.trackColorizer.colorize(rowSibling(col, idx), Qt::ForegroundRole);

        if (const QVariant colData = item->data(col, Qt::DisplayRole); colData.isValid()) {
            tooltip += "<tr><td><b>" + mdName(col) + " </b></td><td>";
            if (color.isValid())
                tooltip += "<font color=\"" + color.value<QColor>().name() +  "\">";
            tooltip += colData.toString() + "</td></tr>";
        }
    }

    tooltip += "</table>";

    return tooltip;
}

void TrackModel::beginThreads()
{
    // Use threads for recalculation.
    m_loadPending = true;
    for (int t = 0; t < QThread::idealThreadCount(); ++t)
        QThreadPool::globalInstance()->start(new TrackUpdateThread(*this));
}

void TrackModel::endThreads()
{
    m_loadPending = false;
    QThreadPool::globalInstance()->waitForDone(); // wait for all threads to complete
    assert(updateQueue.isEmpty());                // we must have updated everything
}

// Handle settings changes: recalculate per-track data.
void TrackModel::refresh()
{
    // Use threads for recalculation.
    beginThreads();
    Util::recurse(*this, [this](const QModelIndex& idx) { getItem(idx)->refresh(*this); });
    endThreads();

    TreeModel::refresh();   // let parent class sent data changed event
}

std::tuple<qreal, qreal, qreal, qreal, bool>
TrackModel::bounds(const QModelIndexList& selection) const
{
    static const qreal NaN = std::numeric_limits<qreal>::quiet_NaN();

    qreal maxlat = NaN;
    qreal maxlon = NaN;
    qreal minlat = NaN;
    qreal minlon = NaN;

    for (const auto& idx : selection) {
        maxlat = std::fmax(maxlat, data(TrackModel::MaxLat, idx, Util::RawDataRole).toDouble());
        maxlon = std::fmax(maxlon, data(TrackModel::MaxLon, idx, Util::RawDataRole).toDouble());
        minlat = std::fmin(minlat, data(TrackModel::MinLat, idx, Util::RawDataRole).toDouble());
        minlon = std::fmin(minlon, data(TrackModel::MinLon, idx, Util::RawDataRole).toDouble());
    }

    return std::make_tuple(maxlat, maxlon, minlat, minlon,
                           (!std::isnan(maxlat) && !std::isnan(maxlon) && !std::isnan(minlat) && !std::isnan(minlon)));
}

// TODO: share with TrackLegModel
Marble::GeoDataLatLonBox TrackModel::boundsBox(const QModelIndexList &selection) const
{
    const auto [maxlat, maxlon, minlat, minlon, valid] = bounds(selection);

    if (!valid)
        return Marble::GeoDataLatLonBox();

    return Marble::GeoDataLatLonBox(maxlat, minlat, maxlon, minlon,
                                    Marble::GeoDataCoordinates::Degree);
}

Qt::ItemFlags TrackModel::flags(const QModelIndex& idx) const
{
    const Qt::ItemFlags flags = TreeModel::flags(idx) | ModelMetaData::flags<TrackModel>(idx);

    switch (idx.column()) {
    case TrackModel::Color:
    case TrackModel::Tags:
    case TrackModel::Notes:
        return flags & ~Qt::ItemIsSelectable;
    default:
        return flags;
    }
}

TrackItem* TrackModel::getItem(const QModelIndex& idx) const
{
    return static_cast<TrackItem*>(TreeModel::getItem(idx));
}

QString TrackModel::mdName(ModelType mt)
{
    switch (mt) {
    case TrackModel::Name:         return QObject::tr("Name");
    case TrackModel::Type:         return QObject::tr("Type");
    case TrackModel::Tags:         return QObject::tr("Tags");
    case TrackModel::Color:        return QObject::tr("Color");
    case TrackModel::Notes:        return QObject::tr("Notes");
    case TrackModel::Keywords:     return QObject::tr("Keywords");
    case TrackModel::Length:       return QObject::tr("Length");
    case TrackModel::BeginDate:    return QObject::tr("Begin Date");
    case TrackModel::EndDate:      return QObject::tr("End Date");
    case TrackModel::BeginTime:    return QObject::tr("Begin Time");
    case TrackModel::EndTime:      return QObject::tr("End Time");
    case TrackModel::StoppedTime:  return QObject::tr("Stopped Time");
    case TrackModel::MovingTime:   return QObject::tr("Moving Time");
    case TrackModel::TotalTime:    return QObject::tr("Total Time");
    case TrackModel::MinElevation: return QObject::tr("Min Elev");
    case TrackModel::MaxElevation: return QObject::tr("Max Elev");
    case TrackModel::MinSpeed:     return QObject::tr("Min Speed");
    case TrackModel::AvgOvrSpeed:  return QObject::tr("Overall Spd");
    case TrackModel::AvgMovSpeed:  return QObject::tr("Moving Spd");
    case TrackModel::MaxSpeed:     return QObject::tr("Max Speed");
    case TrackModel::MinGrade:     return QObject::tr("Min Grade");
    case TrackModel::MaxGrade:     return QObject::tr("Max Grade");
    case TrackModel::MinCad:       return QObject::tr("Min Cad");
    case TrackModel::AvgMovCad:    return QObject::tr("Moving Cad");
    case TrackModel::MaxCad:       return QObject::tr("Max Cad");
    case TrackModel::MinPower:     return QObject::tr("Min Pow");
    case TrackModel::AvgMovPower:  return QObject::tr("Moving Pow");
    case TrackModel::MaxPower:     return QObject::tr("Max Pow");
    case TrackModel::Energy:       return QObject::tr("Energy");
    case TrackModel::Ascent:       return QObject::tr("Ascent");
    case TrackModel::Descent:      return QObject::tr("Descent");
    case TrackModel::BasePeak:     return QObject::tr("Peak-Base");
    case TrackModel::Segments:     return QObject::tr("Segs");
    case TrackModel::Points:       return QObject::tr("Points");
    case TrackModel::Area:         return QObject::tr("Area");
    case TrackModel::MinTemp:      return QObject::tr("Min T");
    case TrackModel::AvgTemp:      return QObject::tr("Avg T");
    case TrackModel::MaxTemp:      return QObject::tr("Max T");
    case TrackModel::MinHR:        return QObject::tr("Min HR");
    case TrackModel::AvgHR:        return QObject::tr("Avg HR");
    case TrackModel::MaxHR:        return QObject::tr("Max HR");
    case TrackModel::Laps:         return QObject::tr("Lap Count");
    case TrackModel::MinLon:       return QObject::tr("Min Lon");
    case TrackModel::MinLat:       return QObject::tr("Min Lat");
    case TrackModel::MaxLon:       return QObject::tr("Max Lon");
    case TrackModel::MaxLat:       return QObject::tr("Max Lat");

    case TrackModel::_Count:       break;
    }

    assert(0 && "Unknown TrackModel value");
    return "";
}

bool TrackModel::mdIsEditable(ModelType mt)
{
    switch (mt) {
    case TrackModel::Name:         [[fallthrough]];
    case TrackModel::Tags:         [[fallthrough]];
    case TrackModel::Color:        [[fallthrough]];
    case TrackModel::Notes:        [[fallthrough]];
    case TrackModel::Keywords:     return true;
    default:                       return false;
    }
}

// Return true if this data can be placed in a chart
bool TrackModel::mdIsChartable(ModelType mt)
{
    switch (mt) {
    case TrackModel::Length:       [[fallthrough]];
    case TrackModel::StoppedTime:  [[fallthrough]];
    case TrackModel::MovingTime:   [[fallthrough]];
    case TrackModel::TotalTime:    [[fallthrough]];
    case TrackModel::MinElevation: [[fallthrough]];
    case TrackModel::MaxElevation: [[fallthrough]];
    case TrackModel::MinSpeed:     [[fallthrough]];
    case TrackModel::AvgOvrSpeed:  [[fallthrough]];
    case TrackModel::AvgMovSpeed:  [[fallthrough]];
    case TrackModel::MaxSpeed:     [[fallthrough]];
    case TrackModel::MinGrade:     [[fallthrough]];
    case TrackModel::MaxGrade:     [[fallthrough]];
    case TrackModel::MinCad:       [[fallthrough]];
    case TrackModel::AvgMovCad:    [[fallthrough]];
    case TrackModel::MaxCad:       [[fallthrough]];
    case TrackModel::MinPower:     [[fallthrough]];
    case TrackModel::AvgMovPower:  [[fallthrough]];
    case TrackModel::MaxPower:     [[fallthrough]];
    case TrackModel::Energy:       [[fallthrough]];
    case TrackModel::Ascent:       [[fallthrough]];
    case TrackModel::Descent:      [[fallthrough]];
    case TrackModel::BasePeak:     [[fallthrough]];
    case TrackModel::Segments:     [[fallthrough]];
    case TrackModel::Points:       [[fallthrough]];
    case TrackModel::Area:         [[fallthrough]];
    case TrackModel::MinTemp:      [[fallthrough]];
    case TrackModel::AvgTemp:      [[fallthrough]];
    case TrackModel::MaxTemp:      [[fallthrough]];
    case TrackModel::MinHR:        [[fallthrough]];
    case TrackModel::AvgHR:        [[fallthrough]];
    case TrackModel::MaxHR:        [[fallthrough]];
    case TrackModel::Laps:         [[fallthrough]];
    case TrackModel::MinLon:       [[fallthrough]];
    case TrackModel::MinLat:       [[fallthrough]];
    case TrackModel::MaxLon:       [[fallthrough]];
    case TrackModel::MaxLat:       return true;

    default:                       return false;
    }
}

// tooltip for column header
QString TrackModel::mdTooltip(ModelType mt)
{
#define cd_header(i)       "<p><b><u>" i ":</u></b><p>"
#define cd_info cd_header("Column Information")

    static const QByteArray can_edit =
            cd_header("Editable")
            "You can edit this column by double "
            "clicking on it within row.  You can edit multiple rows at once by selecting "
            "several rows (shift or ctrl click) and then doubling clicking while holding "
            "the shift or ctrl key.";

    switch (mt) {
    case TrackModel::Name:
        return QObject::tr(cd_info
                           "Descriptive track name.") +
                QObject::tr(can_edit);

    case TrackModel::Type:
        return QObject::tr(cd_info
                       "<i></i>Data type: Waypoint, Route, or Track.");

    case TrackModel::Tags:
        return QObject::tr(cd_info
                           "Tags applied to this track.  The master list of available tags may "
                           "be edited from the Settings dialog.") +
                QObject::tr(can_edit);

    case TrackModel::Color:
        return QObject::tr(cd_info
                           "Track display color.") +
                QObject::tr(can_edit);

    case TrackModel::Notes:
        return QObject::tr(cd_info
                           "User notes.") +
                QObject::tr(can_edit);

    case TrackModel::Keywords:
        return QObject::tr(cd_info
                           "Track keywords.") +
                QObject::tr(can_edit);

    case TrackModel::Length:
        return QObject::tr(cd_info
                           "Total track length.");

    case TrackModel::BeginDate:
        return QObject::tr(cd_info
                           "Earliest datestamp, including date + time of day.");

    case TrackModel::EndDate:
        return QObject::tr(cd_info
                           "Last datestamp, including date + time of day.");

    case TrackModel::BeginTime:
        return QObject::tr(cd_info
                           "Earliest timestamp, not including date.");

    case TrackModel::EndTime:
        return QObject::tr(cd_info
                           "Last timestamp, not including date.");

    case TrackModel::StoppedTime:
        return QObject::tr(cd_info
                           "Total stopped time.");

    case TrackModel::MovingTime:
        return QObject::tr(cd_info
                           "Total moving time.");

    case TrackModel::TotalTime:
        return QObject::tr(cd_info
                           "Total track duration (last timestamp - first timestamp)");

    case TrackModel::MinElevation:
        return QObject::tr(cd_info
                           "Min (lowest) elevation contained (if any).");

    case TrackModel::MaxElevation:
        return QObject::tr(cd_info
                           "Max (highest) elevation contained (if any).");

    case TrackModel::MinSpeed:
        return QObject::tr(cd_info
                           "Min (slowest) speed contained (if any).");

    case TrackModel::AvgOvrSpeed:
        return QObject::tr(cd_info
                           "Average overall speed.");

    case TrackModel::AvgMovSpeed:
        return QObject::tr(cd_info
                           "Average moving speed.");

    case TrackModel::MaxSpeed:
        return QObject::tr(cd_info
                           "Max (fastest) speed contained (if any).");

    case TrackModel::MinGrade:
        return QObject::tr(cd_info
                           "Min grade, in percent.");

    case TrackModel::MaxGrade:
        return QObject::tr(cd_info
                           "Max grade, in percent.");

    case TrackModel::MinCad:
        return QObject::tr(cd_info
                           "Min cadence.");

    case TrackModel::AvgMovCad:
        return QObject::tr(cd_info
                           "Average moving cadence.");

    case TrackModel::MaxCad:
        return QObject::tr(cd_info
                           "Max cadence.");

    case TrackModel::MinPower:
        return QObject::tr(cd_info
                           "Min power.");

    case TrackModel::AvgMovPower:
        return QObject::tr(cd_info
                           "Average moving power.");

    case TrackModel::MaxPower:
        return QObject::tr(cd_info
                           "Max power.");

    case TrackModel::Energy:
        return QObject::tr(cd_info
                           "Estimated total input energy.");

    case TrackModel::Ascent:
        return QObject::tr(cd_info
                           "Total ascent.");

    case TrackModel::Descent:
        return QObject::tr(cd_info
                           "Total descent.");

    case TrackModel::BasePeak:
        return QObject::tr(cd_info
                           "Peak elevation - base elevation.");

    case TrackModel::Segments:
        return QObject::tr(cd_info
                           "Number of separate segments in the track.");

    case TrackModel::Points:
        return QObject::tr(cd_info
                           "Number of sample points in the entire track.");

    case TrackModel::Area:
        return QObject::tr(cd_info
                           "Covered area.");

    case TrackModel::MinTemp:
        return QObject::tr(cd_info
                           "Min (lowest) temperature, if avaialble.");

    case TrackModel::AvgTemp:
        return QObject::tr(cd_info
                           "Average temperature, if available.");

    case TrackModel::MaxTemp:
        return QObject::tr(cd_info
                           "Max (highest) temperature, if avaialble.");

    case TrackModel::MinHR:
        return QObject::tr(cd_info
                           "Min (lowest) heart rate, if available.");

    case TrackModel::AvgHR:
        return QObject::tr(cd_info
                           "Average heart rate, if available.");

    case TrackModel::MaxHR:
        return QObject::tr(cd_info
                           "Max (highest) heart rate, if available.");

    case TrackModel::Laps:
        return QObject::tr(cd_info
                           "Number of laps, if available.");

    case TrackModel::MinLon:
        return QObject::tr(cd_info
                           "Min longitude.");

    case TrackModel::MinLat:
        return QObject::tr(cd_info
                           "Min latitude.");

    case TrackModel::MaxLon:
        return QObject::tr(cd_info
                           "Max longitude.");

    case TrackModel::MaxLat:
        return QObject::tr(cd_info
                           "Max latitude.");

    case TrackModel::_Count:
        break;
    }

    assert(0 && "Unknown TrackModel value");
    return "";
}

// tooltip for container column header
QString TrackModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);  // just pass through the tooltip
}

Qt::Alignment TrackModel::mdAlignment(ModelType mt)
{
    switch (mt) {
    case TrackModel::Length:       [[fallthrough]];
    case TrackModel::BeginDate:    [[fallthrough]];
    case TrackModel::EndDate:      [[fallthrough]];
    case TrackModel::BeginTime:    [[fallthrough]];
    case TrackModel::EndTime:      [[fallthrough]];
    case TrackModel::StoppedTime:  [[fallthrough]];
    case TrackModel::MovingTime:   [[fallthrough]];
    case TrackModel::TotalTime:    [[fallthrough]];
    case TrackModel::MinElevation: [[fallthrough]];
    case TrackModel::MaxElevation: [[fallthrough]];
    case TrackModel::MinSpeed:     [[fallthrough]];
    case TrackModel::AvgOvrSpeed:  [[fallthrough]];
    case TrackModel::AvgMovSpeed:  [[fallthrough]];
    case TrackModel::MaxSpeed:     [[fallthrough]];
    case TrackModel::MinGrade:     [[fallthrough]];
    case TrackModel::MaxGrade:     [[fallthrough]];
    case TrackModel::MinCad:       [[fallthrough]];
    case TrackModel::AvgMovCad:    [[fallthrough]];
    case TrackModel::MaxCad:       [[fallthrough]];
    case TrackModel::MinPower:     [[fallthrough]];
    case TrackModel::Energy:       [[fallthrough]];
    case TrackModel::AvgMovPower:  [[fallthrough]];
    case TrackModel::MaxPower:     [[fallthrough]];
    case TrackModel::Ascent:       [[fallthrough]];
    case TrackModel::Descent:      [[fallthrough]];
    case TrackModel::BasePeak:     [[fallthrough]];
    case TrackModel::Segments:     [[fallthrough]];
    case TrackModel::Points:       [[fallthrough]];
    case TrackModel::Area:         [[fallthrough]];
    case TrackModel::MinTemp:      [[fallthrough]];
    case TrackModel::AvgTemp:      [[fallthrough]];
    case TrackModel::MaxTemp:      [[fallthrough]];
    case TrackModel::MinHR:        [[fallthrough]];
    case TrackModel::AvgHR:        [[fallthrough]];
    case TrackModel::MaxHR:        [[fallthrough]];
    case TrackModel::Laps:         [[fallthrough]];
    case TrackModel::MinLon:       [[fallthrough]];
    case TrackModel::MinLat:       [[fallthrough]];
    case TrackModel::MaxLon:       [[fallthrough]];
    case TrackModel::MaxLat:       return Qt::AlignRight   | Qt::AlignVCenter;
    default:                       return Qt::AlignLeft    | Qt::AlignVCenter;
    }
}

const Units& TrackModel::mdUnits(ModelType mt, const CfgData& cfgData)
{
    static const Units rawString(Format::String);

    switch (mt) {
    case TrackModel::Length:         return cfgData.unitsTrkLength;
    case TrackModel::StoppedTime:    [[fallthrough]];
    case TrackModel::MovingTime:     [[fallthrough]];
    case TrackModel::TotalTime:      return cfgData.unitsDuration;
    case TrackModel::BeginDate:      [[fallthrough]];
    case TrackModel::EndDate:        return cfgData.unitsTrkDate;
    case TrackModel::BeginTime:      [[fallthrough]];
    case TrackModel::EndTime:        return cfgData.unitsTrkTime;
    case TrackModel::MinElevation:   [[fallthrough]];
    case TrackModel::MaxElevation:   return cfgData.unitsElevation;
    case TrackModel::MinSpeed:       [[fallthrough]];
    case TrackModel::AvgOvrSpeed:    [[fallthrough]];
    case TrackModel::AvgMovSpeed:    [[fallthrough]];
    case TrackModel::MaxSpeed:       return cfgData.unitsSpeed;
    case TrackModel::MinGrade:       [[fallthrough]];
    case TrackModel::MaxGrade:       return cfgData.unitsSlope;
    case TrackModel::MinPower:       [[fallthrough]];
    case TrackModel::AvgMovPower:    [[fallthrough]];
    case TrackModel::MaxPower:       return cfgData.unitsPower;
    case TrackModel::Energy:         return cfgData.unitsEnergy;
    case TrackModel::Ascent:         [[fallthrough]];
    case TrackModel::Descent:        [[fallthrough]];
    case TrackModel::BasePeak:       return cfgData.unitsClimb;
    case TrackModel::Area:           return cfgData.unitsArea;
    case TrackModel::MinTemp:        [[fallthrough]];
    case TrackModel::AvgTemp:        [[fallthrough]];
    case TrackModel::MaxTemp:        return cfgData.unitsTemp;
    case TrackModel::MinLat:         [[fallthrough]];
    case TrackModel::MaxLat:         return cfgData.unitsLat;
    case TrackModel::MinLon:         [[fallthrough]];
    case TrackModel::MaxLon:         return cfgData.unitsLon;
    default:
        return rawString;
    }
}

QString TrackModel::trackTypeName(TrackType tt)
{
    switch (tt) {
    case TrackType::Trk: return tr("Trk");
    case TrackType::Rte: return tr("Rte");
    case TrackType::Wpt: return tr("Wpt");
    default:             return tr("Unk");
    }
}

PointModel* TrackModel::geoPoints(const QModelIndex& idx)
{
    TrackItem* lm = getItem(idx);
    return (lm != nullptr) ? &lm->geoPoints : nullptr;
}

const PointModel* TrackModel::geoPoints(const QModelIndex& idx) const
{
    const TrackItem* lm = getItem(idx);
    return (lm != nullptr) ? &lm->geoPoints : nullptr;
}

// Only for use by save classes and the TrackMap.  We only provide template specializations
// for classes that should have access.
class TrackMap;
template<>
const TrackSegLines& TrackModel::trackLines(const TrackMap&, const QModelIndex& idx) const
{
    return getItem(idx)->trackLines;
}

class TrackPane;
template<> void TrackModel::setAllVisible(const TrackPane&, bool v)
{
    Util::recurse(*this, [this, v](const QModelIndex& idx) {
        getItem(idx)->setVisible(v);
    });
}

template<> void TrackModel::setVisible(const TrackPane&, const QModelIndex& idx, bool v)
{
    getItem(idx)->setVisible(v);
}

class AreaDialog;

template<> bool TrackModel::isVisible(const AreaDialog&, const QModelIndex& idx) const
{
    return getItem(idx)->isVisible();
}

template<> bool TrackModel::isVisible(const TrackMap&, const QModelIndex& idx) const
{
    return getItem(idx)->isVisible();
}

template<> void TrackModel::setAreaSelected(const AreaDialog&, bool v)
{
    Util::recurse(*this, [this, v](const QModelIndex& idx) {
        getItem(idx)->setAreaSelect(v);
    });
}

template<> void TrackModel::setAreaSelected(const AreaDialog&, const QModelIndex& idx, bool v)
{
    getItem(idx)->setAreaSelect(v);
}

template<> bool TrackModel::isAreaSelected(const AreaDialog&, const QModelIndex& idx) const
{
    return getItem(idx)->isAreaSelected();
}

template <> void TrackModel::selectPointsWithin(const AreaDialog&, const Marble::GeoDataLatLonBox& region)
{
    Util::recurse(*this, [this, &region](const QModelIndex& idx) {
        getItem(idx)->selectPointsWithin(region);
    });
}

template<> void TrackModel::updateQueueAdd(TrackItem* item)
{
    QMutexLocker lock(&mutex);
    updateQueue.enqueue(item);
}

template<> bool TrackModel::updateQueueProcess(const TrackUpdateThread&)
{
    TrackItem* nextItem = nullptr;
    { // mutex scope
        QMutexLocker lock(&mutex);

        if (updateQueue.isEmpty())  // nothing to do
            return false;

        nextItem = updateQueue.dequeue(); // dequeue the next work item
    }
        
    nextItem->update(); // do the work
    return true;
}

template<> bool TrackModel::loadPending(const TrackUpdateThread&) const
{
    return m_loadPending;
}

template<> QModelIndex TrackModel::personIdx(const TrackItem&) const
{
    return m_personIdx;
}

bool TrackModel::pointEqual(const QModelIndex &idx, const PointModel& points) const
{
    return getItem(idx)->pointEqual(points);
}

bool TrackModel::pointEqual(const QModelIndex &idx0, const QModelIndex &idx1) const
{
    return getItem(idx0)->pointEqual(*getItem(idx1));
}

void TrackModel::emitDataChange(int row, int col)
{
    emit dataChanged(index(row, col), index(row, col));
}

QVariant TrackModel::data(const QModelIndex& idx, int role) const
{
    if (!idx.isValid())
        return QVariant();

    if (const QVariant val = getItem(idx)->data(idx.column(), role); val.isValid()) {
        return cfgData.trackColorizer.maybeUse(val, idx, role);
    } else {
        if (role == Qt::ToolTipRole)
            return tooltip(idx);
    }

    return cfgData.trackColorizer.colorize(idx, role);
}

bool TrackModel::setData(const QModelIndex& idx, const QVariant& value, int role)
{
    bool changed;
    const bool rc = getItem(idx)->setData(idx.column(), value, role, changed, m_personIdx);

    if (rc && changed) {
        emit dataChanged(idx, idx);

        // Changing tags also changes power data
        if (idx.column() == TrackModel::Tags)
            for (const auto c : { MinPower, AvgMovPower, MaxPower, Energy })
                emitDataChange(idx.row(), c);
    }

    return rc;
}

void TrackModel::setPerson(const QString& person)
{
    if (person.isEmpty())
        return;

    m_personIdx = cfgData.people.keyIdx(person);

    // Update all tracks to use power data for the new person.
    Util::recurse(*this, [this](const QModelIndex& idx) {
        getItem(idx)->setPersonIdx(m_personIdx);
    });
}

QDataStream& TrackModel::load(QDataStream& stream, const QModelIndex& parent, bool append)
{
    updateQueue.reserve(128);

    // Start background threads to recalculate thread data on load, for super-speed!
    TreeModel::load(stream, parent, append,
        [this]() { beginThreads(); },
        [this]() { endThreads(); }
    );

    return stream;
}

QDataStream& operator<<(QDataStream& stream, const TrackModel& model)
{
    return model.save(stream);
}

QDataStream& operator>>(QDataStream& stream, TrackModel& model)
{
    return model.load(stream);
}

QDataStream& operator<<(QDataStream& out, const TrackType& tt)
{
    return out << std::underlying_type<Marble::MapQuality>::type(tt);
}

QDataStream& operator>>(QDataStream& in, TrackType& tt)
{
    return in >> (std::underlying_type<Marble::MapQuality>::type&)(tt);
}

QHash<uint, QModelIndex> TrackModel::trackHashes() const
{
    QHash<uint, QModelIndex> hashes;

    Util::recurse(*this, [this, &hashes](const QModelIndex& idx) {
        hashes.insertMulti(qHash(*getItem(idx)), idx);
    });

    return hashes;
}

uint TrackModel::trackHash(const QModelIndex& idx) const
{
    return qHash(*getItem(idx));
}
