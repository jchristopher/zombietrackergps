/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <marble/MarbleMath.h>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <QDataStream>
#include <QItemSelectionModel>
#include <QSortFilterProxyModel>

#include <src/util/math.h>
#include <src/util/util.h>
#include <src/util/roles.h>
#include <src/util/units.h>
#include "cfgdata.h"
#include "pointmodel.h"
#include "tagmodel.h"
#include "trackitem.h"
#include "personmodel.h"
#include "src/ui/windows/mainwindow.h"

PointModel::PointModel(TrackItem* trackItem, const CfgData& cfgData) :
    TreeModel(nullptr),
    eleFilterSizeCache(-1),
    locFilterSizeCache(-1),
    m_dataChangedTimer(this),
    m_trackItem(trackItem),
    cfgData(cfgData)
{ 
    setupTimers();
}


PointModel::PointModel() :
    PointModel(nullptr, MainWindow::mainWindowStatic->constCfgData())
{
}

void PointModel::setupTimers()
{
    m_dataChangedTimer.setSingleShot(true);
    connect(&m_dataChangedTimer, &QTimer::timeout, this, &PointModel::processDataChanged);
}

void PointModel::sortItems()
{
    if (!m_timeDirty)
        return;

    // TODO: handle timestamp changes that move across segments
    int parent = 0;
    for (auto& trkseg : *this) {
        std::sort(trkseg.begin(), trkseg.end(),
                      [](const PointItem& lhs, const PointItem& rhs) {
                          return lhs.time() < rhs.time();
                        });

        if (!trkseg.empty()) {
            const QModelIndex parentIdx = index(parent, 0);
            emit dataChanged(index(0, 0, parentIdx),
                             index(trkseg.size(), columnCount(parentIdx), parentIdx));
        }

        parent++;
    }

    m_timeDirty = false;
}

void PointModel::processDataChanged()
{
    if (m_trackItem == nullptr)
        return;

    sortItems();
    m_trackItem->update(*this);
}

// We keep a cache of the index start for each segment, for performance when returning
// same in data() method.
void PointModel::updateStartIdx()
{
    m_startIdxCache.clear();
    m_startIdxCache.reserve(size());

    uint start = 0;
    for (const auto& it : *this) {
        m_startIdxCache.append(start);
        start += it.size();
    }
}

template <int N>
void PointModel::filter(value_type& trkSeg, int filterSize,
                        const std::array<qreal PointItem::*, N>& out,
                        const std::array<qreal PointItem::*, N>& in)
{
    if (filterSize <= 2) {
        for (int i = 0; i < trkSeg.size(); ++i)
            for (int n = 0; n < N; ++n)
                 trkSeg[i].*(out[n]) = std::numeric_limits<qreal>::quiet_NaN();
        return;
    }

    // If filter size is even, make it the next higher odd.
    filterSize    |= 0x1;

    const qreal timeSpacing = avgTimeSpacing(trkSeg);    // average sample point spacing
    const int   fHalf       = filterSize / 2;            // half filter width rounded down
    const qreal timeWidth   = timeSpacing * filterSize * 0.6;  // filter time width

    for (int i = 0; i < trkSeg.size(); ++i) {
        std::array<qreal, N> sum = { 0.0 };
        qreal weight = 0.0;

        // Process convolution neighborhood
        for (int j = i-fHalf; j <= i+fHalf; ++j) {
            if (j < 0 || j >= trkSeg.size())
                continue;

            const float dT    = std::abs(trkSeg[i].time().msecsTo(trkSeg[j].time()));
            const qreal trust = 1.0 - std::clamp(qreal(dT) / timeWidth, 0.0, 1.0);

            weight += trust;
            for (int n = 0; n < N; ++n)  // for each input value
                sum[n] += trkSeg[j].*(in[n]) * trust;
        }

        for (int n = 0; n < N; ++n) // for each output value
            trkSeg[i].*(out[n]) = sum[n] / weight;
    }
}

std::tuple<qreal, qreal, qreal, qreal, bool>
PointModel::bounds(const QModelIndexList& selection) const
{
    static const qreal NaN = std::numeric_limits<qreal>::quiet_NaN();

    qreal maxlat = NaN;
    qreal maxlon = NaN;
    qreal minlat = NaN;
    qreal minlon = NaN;

    for (const auto& idx : selection) {
        if (const PointItem* pt = getItem(idx); pt != nullptr) {
            maxlat = std::fmax(maxlat, pt->lat());
            maxlon = std::fmax(maxlon, pt->lon());
            minlat = std::fmin(minlat, pt->lat());
            minlon = std::fmin(minlon, pt->lon());
        }
    }

    return std::make_tuple(maxlat, maxlon, minlat, minlon,
                           (!std::isnan(maxlat) && !std::isnan(maxlon) && !std::isnan(minlat) && !std::isnan(minlon)));
}

Marble::GeoDataLatLonBox PointModel::boundsBox(const QModelIndexList &selection) const
{
    const auto [maxlat, maxlon, minlat, minlon, valid] = bounds(selection);

    if (!valid)
        return Marble::GeoDataLatLonBox();

    return Marble::GeoDataLatLonBox(maxlat, minlat, maxlon, minlon,
                                    Marble::GeoDataCoordinates::Degree);
}

// Determine average time spacing of sample points
qreal PointModel::avgTimeSpacing(const value_type& trkSeg)
{
    if (trkSeg.size() < 2)  // not enough data to filter
        return 0.0;

    return qreal(trkSeg.back().time().msecsTo(trkSeg.front().time())) / trkSeg.size();
}

// Calculates leg distance (to next point, not previous!)
void PointModel::calcPointValues(QList::value_type& pt, qreal& accumDistance)
{
    float smoothGrade = 0.0;  // for smoothing
    bool  hasGrade = false;

    if (pt.isEmpty())
        return;

    for (int i = 0; i < (pt.size() - 1); ++i) {
        // Don't use filtered distances for this: it causes troubles with short duration legs
        pt[i].m_length =
                Marble::distanceSphere(Math::toRad(pt[i].lon(false)), Math::toRad(pt[i].lat(false)),
                                       Math::toRad(pt[i+1].lon(false)), Math::toRad(pt[i+1].lat(false))) *
                earth_r_m;


        pt[i].m_dist     = accumDistance;
        pt[i].m_duration = pt[i].time().msecsTo(pt[i+1].time());
        pt[i].m_vert     = pt[i+1].ele() - pt[i].ele();

        accumDistance += pt[i].m_length;

        // Once in a while, for some reason, tracks have points out of order, resulting in
        // negative durations.  This fixes that by sorting and starting over.  Thankfully,
        // this seems quite rare.
        if (pt[i].m_duration < 0.0) {
            m_timeDirty = true;
            sortItems();
            i = -1;
            continue;
        }

        // This lags behind by one step, because we need the next point's speed.
        if (i > 0)
            pt[i-1].m_accel    = (pt[i].speed() - pt[i-1].speed()) * 1000.0 / pt[i-1].duration();

        if (pt[i].speed() > 1.0 && pt[i].length() > 4.0) {
            smoothGrade = Math::mix(smoothGrade, float(pt[i].vert() / pt[i].length()), 0.2f);
            hasGrade = (fabs(smoothGrade) < 0.15 || pt[0].time().msecsTo(pt[i].time()) > 120000);
        }

        if (hasGrade) {
            pt[i].m_grade        = smoothGrade;
            pt[i].m_gradeSinAtan = std::sin(std::atan(smoothGrade));
        }
    }

    pt[pt.size() - 1].m_dist = accumDistance;
}

// Apply time smart smoothing convolutions (nondestructive to original data)
void PointModel::filter()
{
    qreal accumDistance = 0.0;

    // Process each separate segment individually: no filtering across segments.
    for (auto& trkSeg : *this) {
        if (trkSeg.size() > 3) { // only filter if there is enough data
            // Refilter elevations
            // Don't re-filter if last used filter size matches current one
            if (eleFilterSizeCache != cfgData.eleFilterSize)
                filter<1>(trkSeg, cfgData.eleFilterSize,
                { &PointItem::m_fltEle },
                { &PointItem::m_ele } );

            // Refilter locations
            // Don't re-filter if last used filter size matches current one
            if (locFilterSizeCache != cfgData.locFilterSize)
                filter<2>(trkSeg, cfgData.locFilterSize,
                { &PointItem::m_fltLon, &PointItem::m_fltLat },
                { &PointItem::m_lon,    &PointItem::m_lat } );

            eleFilterSizeCache = cfgData.eleFilterSize;
            locFilterSizeCache = cfgData.locFilterSize;
        }

        calcPointValues(trkSeg, accumDistance);  // do after filtering, so it can use filtered elevations
    }
}

// Return timestamp for first track point, if any, else invalid date.
const QDateTime& PointModel::trackStartTime() const
{
    static const QDateTime invalid;

    return (!isEmpty() && !at(0).isEmpty()) ? at(0).at(0).time() : invalid;
}

qreal PointModel::trackTotalDistance() const
{
    return (!isEmpty() && !last().isEmpty()) ? last().last().distance() : 0.0;
}


int PointModel::trackTotalPoints() const
{
    int total = 0;
    for (const auto& trkSeg : *this)
        total += trkSeg.size();

    return total;
}

// Wrapper to supply some data held by the PointModel suhc as power, and call the PointItem::data
inline QVariant PointModel::data(const PointItem* item, ModelType col, const QModelIndex& idx, int role, bool filtered) const
{
    const uint startIdx = (qintptr(idx.internalId()) >= 0) ? m_startIdxCache[idx.internalId()] : 0;

    return item->data(col, startIdx + idx.row(), role, filtered, trackStartTime(), powerData(), cfgData);
}

QString PointModel::tooltip(const QModelIndex& idx) const
{
    static const QVector<ModelType> tooltipColumns = {
        PointModel::Time,
        PointModel::Elapsed,
        PointModel::Lat,
        PointModel::Lon,
        PointModel::Ele,
        PointModel::Distance,
        PointModel::Grade,
        PointModel::Temp,
        PointModel::Hr,
        PointModel::Cad,
        PointModel::Power,
        PointModel::Course,
        PointModel::Bearing,
    };

    const QModelIndex idxIdx = idx.sibling(idx.row(), PointModel::Index);

    QString tooltip = QString("<b><u><nobr><big>Index: ") +
        data(idxIdx, Qt::DisplayRole).toString() +
        + "</big></nobr></u></b><p>";

    tooltip += "<table>";

    for (const auto col : tooltipColumns) {
        const QVariant color = cfgData.pointColorizer.colorize(rowSibling(col, idx), Qt::ForegroundRole);
        const QModelIndex columnIdx = idx.sibling(idx.row(), col);

        if (const QVariant colData = data(columnIdx, Qt::DisplayRole); colData.isValid()) {
            tooltip += "<tr><td><b>" + mdName(col) + " </b></td><td>";
            if (color.isValid())
                tooltip += "<font color=\"" + color.value<QColor>().name() +  "\">";
            tooltip += colData.toString() + "</td></tr>";
        }
    }

    tooltip += "</table>";

    return tooltip;
}

QVariant PointModel::data(const QModelIndex& idx, int role) const
{
    verify(idx);

    if (!idx.isValid() || idx.column() >= columnCount())
        return QVariant();

    // Segment level
    if (qintptr(idx.internalId()) < 0) {
        if (role == Qt::DisplayRole)
            if (idx.column() == 0)
                return QString(QObject::tr("Seg %1")).arg(idx.row());
        return QVariant();
    }

    // Two level hierarchy: track segments / track points
    const PointItem* pt  = getItem(idx);
    if (pt == nullptr)
        return QVariant();

    const bool filtered = (role != Qt::EditRole);

    // Text alignment:
    if (role == Qt::TextAlignmentRole)
        return QVariant(mdAlignment(idx.column()));

    const QVariant rawData = data(pt, idx.column(), idx, role, filtered);

    switch (role) {
    case Qt::EditRole:       [[fallthrough]];
    case Util::PlotRole:     [[fallthrough]];
    case Util::RawDataRole:  return rawData;

    case Qt::DisplayRole:    [[fallthrough]];
    case Util::CopyRole:
        if (rawData.isValid())
            return cfgData.pointColorizer.maybeUse(units(idx)(rawData), idx, role);
        break;

    case Qt::ToolTipRole:    return tooltip(idx);
    }

    return cfgData.pointColorizer.colorize(idx, role);
}

QVariant PointModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (QVariant val = ModelMetaData::headerData<PointModel>(section, orientation, role); val.isValid())
        return val;

    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole)
            return mdName(section);
    }

    return QAbstractItemModel::headerData(section, orientation, role);
}

QModelIndex PointModel::index(int row, int column, const QModelIndex& parent) const
{
    // Workaround for: https://bugreports.qt.io/browse/QTBUG-44962
    if (wrongModel(parent))
        return QModelIndex();

    // Two level hierarchy: track segments / track points
    if (!verify(parent).isValid())
        return createIndex(row, column, -1);  // This is the top level (trksegs)

    return createIndex(row, column, parent.row());
}

QModelIndex PointModel::parent(const QModelIndex& idx) const
{
    // Workaround for: https://bugreports.qt.io/browse/QTBUG-44962
    if (wrongModel(idx))
        return QModelIndex();

    if (!verify(idx).isValid() || qintptr(idx.internalId()) < 0)
        return QModelIndex();

    return createIndex(idx.internalId(), 0, -1);
}

int PointModel::rowCount(const QModelIndex& parent) const
{
    // Workaround for: https://bugreports.qt.io/browse/QTBUG-44962
    if (wrongModel(parent))
        return 0;

    // Two level hierarchy: track segments / track points
    if (!verify(parent).isValid())
        return size();

    if (qintptr(parent.internalId()) >= 0)
        return 0;
    else
        return at(parent.row()).size();
}

Qt::ItemFlags PointModel::flags(const QModelIndex& idx) const
{
    return ModelMetaData::flags<PointModel>(verify(idx)) | QAbstractItemModel::flags(idx);
}

bool PointModel::setData(const QModelIndex& idx, const QVariant& value, int role)
{
    bool changed;
    const bool result = getItem(idx)->setData(idx.column(), value, role, changed, cfgData);

    if (result && changed) {
        emit dataChanged(idx, idx);

        if (idx.column() == PointModel::Time)
            m_timeDirty = true;

        // recalculate after data is set, but use timer to avoid recalc-spam
        switch (idx.column()) {
        case PointModel::Time: [[fallthrough]];
        case PointModel::Lat:  [[fallthrough]];
        case PointModel::Lon:  [[fallthrough]];
        case PointModel::Ele:  m_dataChangedTimer.start(250);
        default: break;
        }
    }

    return result;
}

bool PointModel::setHeaderData(int /*section*/, Qt::Orientation /*orientation*/,
                                 const QVariant& /*value*/, int /*role*/)
{
    assert(0);
    return false; // not supported
}

bool PointModel::insertRows(int position, int rows, const QModelIndex& parent)
{
    beginInsertRows(parent, position, position + rows - 1);

    if (!parent.isValid()) {
        while (--rows >= 0)
            insert(position, value_type());  // segment level
    } else {
        (*this)[parent.row()].insert(position, rows, value_type::value_type());
        // point level
    }

    updateStartIdx();
    endInsertRows();

    return true;
}

void PointModel::removeRows(const QItemSelectionModel* selectionModel, const QSortFilterProxyModel* proxy,
                            const QModelIndex& parent)
{
    TreeModel::removeRows(selectionModel, proxy, parent);
    updateStartIdx();
}

bool PointModel::removeRows(int position, int rows, const QModelIndex& parent)
{
    if (rows == 0)
        return true;

    if (!parent.isValid()) {
        // parent is root
        beginRemoveRows(parent, position, position + rows - 1);
        erase(begin() + position, begin() + position + rows);
        endRemoveRows();
    } else {
        // parent is trkseg
        assert(qintptr(parent.internalId()) == -1);

        beginRemoveRows(parent, position, position + rows - 1);
        value_type& segs = (*this)[parent.row()];
        segs.erase(segs.begin() + position, segs.begin() + position + rows);
        endRemoveRows();

        // Remove the segment if it is now empty.
        if (segs.isEmpty())
            removeRows(indexOf(segs), 1, QModelIndex());
    }

    updateStartIdx();

    return true;
}

void PointModel::removeEmptySegments()
{
    for (int seg = size() - 1; seg >= 0; --seg)
        if (at(seg).isEmpty())
            removeRows(seg, 1);

    // removeRows() handles updating the point index values
}

bool PointModel::isRowSelected(int row, const QItemSelectionModel* selectionModel,
                               const QSortFilterProxyModel* proxy) const
{
    return selectionModel->isRowSelected(Util::MapUp(proxy, index(row, 0)).row(), QModelIndex());
}

// We could make a very good initial guess by starting dist/trackTotalDistance() through
// the points, which would be faster than a pure binary chop.  This is OK for now since it only
// has to keep up with mouse movement events.  If performance is even more critical, this can be
// revisited.
std::tuple<const PointItem*, const PointItem*> PointModel::getItemBound(qreal dist) const
{
    // First, find first segment whose final distance is not less than 'dist'
    const auto segIt = std::lower_bound(begin(), end(), dist, [](const value_type& trkSeg, qreal dist) {
        return !trkSeg.empty() && trkSeg.last().distance() < dist;
    });

    if (segIt == end())
        return std::make_tuple(nullptr, nullptr);

    // Binary chop within that segment.
    const auto upperIt = std::lower_bound(segIt->begin(), segIt->end(), dist, [](const PointItem& trkPt, qreal dist) {
        return trkPt.distance() < dist;
    });

    auto lowerIt = upperIt;
    if (lowerIt == segIt->begin()) {
        if (segIt != begin()) // last item from previous segment
            lowerIt = (segIt - 1)->begin() + segIt->size() - 1;
    } else {
        --lowerIt;
    }

    return std::make_tuple(lowerIt, upperIt);
}

// Interpolate data in column 'mt' at position 'dist' (even if between sample points) and return
// interpolated value.
qreal PointModel::interpolate(qreal dist, ModelType mt) const
{
    const auto [lower, upper] = getItemBound(dist);
    if (lower == nullptr || upper == nullptr) // bogus
        return std::numeric_limits<qreal>::quiet_NaN();

    const qreal interp = (dist - lower->distance()) / (upper->distance() - lower->distance());

    return Util::Lerp(lower->data<qreal>(mt, -1, Util::RawDataRole, true, trackStartTime(), powerData(), cfgData),
                      upper->data<qreal>(mt, -1, Util::RawDataRole, true, trackStartTime(), powerData(), cfgData),
                      interp);
}

int PointModel::selectedSegments(const QItemSelectionModel* selectionModel, const QSortFilterProxyModel* proxy) const
{
    int count = 0;

    for (int row = 0; row < size(); ++row)
        if (isRowSelected(row, selectionModel, proxy))
            ++count;

    return count;
}

const Units& PointModel::units(const QModelIndex& idx) const
{
    if (idx.isValid() && m_trackItem != nullptr) {
        // Override speed with per-tag speed unit, if available, by asking the track for its speed unit.
        if (idx.column() == PointModel::Speed)
            return m_trackItem->units(TrackModel::AvgMovSpeed);
    }

    return mdUnits(idx.column(), cfgData);
}

// Merge selected segments into a single segment
void PointModel::mergeSegments(const QItemSelectionModel* selectionModel, const QSortFilterProxyModel* proxy)
{
    if (selectionModel == nullptr || proxy == nullptr)
        return;

    // Merge all if no selections
    const bool all = (selectedSegments(selectionModel, proxy) == 0);

    int first = -1;
    // append first
    for (int row = 0; row < size(); ++row) {
        if (all || isRowSelected(row, selectionModel, proxy)) {
            if (first >= 0) {
                const int currentRowCount  = at(first).size();
                const int insertedRowCount = at(row).size();

                if (insertedRowCount > 0) {
                    beginInsertRows(index(first, 0), currentRowCount, currentRowCount + insertedRowCount - 1);
                    (*this)[first].append(at(row));
                    endInsertRows();
                }
            } else
                first = row;
        }
    }

    // Remove appended ones (in reverse order, to avoid invalidation).
    for (int row = size() - 1; row >= 0; --row)
        if (all || isRowSelected(row, selectionModel, proxy))
            if (row != first)
                removeRow(row);

    updateStartIdx();
}

// Split track into segments at selected points.
void PointModel::splitSegments(const QItemSelectionModel* selectionModel, const QSortFilterProxyModel* proxy)
{
    if (selectionModel == nullptr || proxy == nullptr)
        return;

    QModelIndexList rows = selectionModel->selectedRows();
    Util::MapDown(rows);

    // reverse order to avoid invalidation
    std::sort(rows.begin(), rows.end(), [](const QModelIndex& lhs, const QModelIndex& rhs) {
        return lhs.row() > rhs.row();
    });

    std::remove_if(rows.begin(), rows.end(), [](const QModelIndex& idx) { return !idx.isValid(); });

    QModelIndexList splits; // split points

    // Remove middles of contiguous ranges.  Note we're sorted from last to first.
    for (int i = rows.size() - 1; i >= 0; --i) { // always leave first point
        if (i == (rows.size()-1) || rows[i].row() != (rows[i+1].row()+1))
            splits.prepend(rows[i]);
        else if (i == 0 || rows[i].row() != (rows[i-1].row()-1))
            if (rows[i].row() != (rowCount(parent(rows[i])) - 1))
                splits.prepend(index(rows[i].row()+1, 0, rows[i].parent()));
    }

    for (const auto& split : splits) {
        if (split.row() == 0)  // skip if it would move the whole segment, leaving an empty one.
            continue;

        const int thisSegment = split.parent().row();
        const int segInsertionPoint = thisSegment + 1;  // insert BEFORE this spot.
        const int rowsToInsert = at(thisSegment).size() - split.row();
        const QModelIndex segInsertionIdx = index(segInsertionPoint, 0);

        insertRow(segInsertionPoint);
        (*this)[segInsertionPoint].reserve(rowsToInsert);

        // Insert copies of segments.  (move semantics not worthwhile on the small/flat PointItems)
        beginInsertRows(segInsertionIdx, 0, rowsToInsert - 1);
        for (int r = 0; r < rowsToInsert; ++r)
            (*this)[segInsertionPoint].append((*this)[thisSegment][split.row() + r]);
        endInsertRows();

        // Remove old segments
        removeRows(split.row(), rowsToInsert, index(thisSegment, 0));
    }

    updateStartIdx();
}

QVariant PointModel::siblingData(int column, const QModelIndex &idx, int role) const
{
    return data(index(idx.row(), column, parent(idx)), role);
}

QString PointModel::mdName(ModelType mt)
{
    switch (mt) {
    case Index:     return QObject::tr("Index");
    case Time:      return QObject::tr("Time");
    case Elapsed:   return QObject::tr("Elapsed");
    case Lon:       return QObject::tr("Longitude");
    case Lat:       return QObject::tr("Latitude");
    case Ele:       return QObject::tr("Elevation");
    case Length:    return QObject::tr("Length");
    case Distance:  return QObject::tr("Distance");
    case Vert:      return QObject::tr("Vertical");
    case Grade:     return QObject::tr("Grade");
    case Duration:  return QObject::tr("Duration");
    case Temp:      return QObject::tr("Temp");
    case Depth:     return QObject::tr("Depth");
    case Speed:     return QObject::tr("Speed");
    case Accel:     return QObject::tr("Accel");
    case Hr:        return QObject::tr("Heart Rate");
    case Cad:       return QObject::tr("Cadence");
    case Power:     return QObject::tr("Power");
    case Course:    return QObject::tr("Course");
    case Bearing:   return QObject::tr("Bearing");
    case _Count:    break;
    }

    assert(0 && "Unknown GeoPoint column");
    return "";
}

bool PointModel::mdIsEditable(ModelType mt)
{
    switch (mt) {
    case Time:   [[fallthrough]];
    case Lat:    [[fallthrough]];
    case Lon:    [[fallthrough]];
    case Ele:    [[fallthrough]];
    case Temp:   [[fallthrough]];
    case Depth:  [[fallthrough]];
    case Hr:     [[fallthrough]];
    case Cad:    return true;
    default:     return false;
    }
}

QString PointModel::mdTooltip(ModelType mt)
{
#define cd_header(i)       "<p><b><u>" i ":</u></b><p>"
#define cd_info cd_header("Column Information")

    static const QByteArray can_edit =
            cd_header("Editable")
            "You can edit this column by double "
            "clicking on it within row.  You can edit multiple rows at once by selecting "
            "several rows (shift or ctrl click) and then doubling clicking while holding "
            "the shift or ctrl key.";

    switch (mt) {
    case Index:     return QObject::tr(cd_info "Index of point within its track segment");
    case Time:      return QObject::tr(cd_info "Timestamp for point") + QObject::tr(can_edit);
    case Elapsed:   return QObject::tr(cd_info "Elapsed time within track");
    case Lon:       return QObject::tr(cd_info "Longitude for point") + QObject::tr(can_edit);
    case Lat:       return QObject::tr(cd_info "Latitude for point") + QObject::tr(can_edit);
    case Ele:       return QObject::tr(cd_info "Elevation at point") + QObject::tr(can_edit);
    case Length:    return QObject::tr(cd_info "Horizontal distance to the next point");
    case Distance:  return QObject::tr(cd_info "Distance from track start to this point");
    case Vert:      return QObject::tr(cd_info "Vertical distance to the next point");
    case Grade:     return QObject::tr(cd_info "Grade at this point");
    case Duration:  return QObject::tr(cd_info "Duration of this leg");
    case Temp:      return QObject::tr(cd_info "Temperature") + QObject::tr(can_edit);
    case Depth:     return QObject::tr(cd_info "Depth") + QObject::tr(can_edit);
    case Speed:     return QObject::tr(cd_info "Speed at this point");
    case Accel:     return QObject::tr(cd_info "Acceleration at this point");
    case Hr:        return QObject::tr(cd_info "Heart Rate at this point") + QObject::tr(can_edit);
    case Cad:       return QObject::tr(cd_info "Cadence at this point") + QObject::tr(can_edit);
    case Power:     return QObject::tr(cd_info "Power at this point");
    case Course:    return QObject::tr(cd_info "Course to next point");
    case Bearing:   return QObject::tr(cd_info "Bearing");
    case _Count:    break;
    }

    assert(0 && "Unknown GeoPoint column");
    return "";
}

QString PointModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);   // just pass through the tooltip
}

Qt::Alignment PointModel::mdAlignment(ModelType)
{
    return Qt::AlignRight | Qt::AlignVCenter;
}

const Units& PointModel::mdUnits(ModelType mt, const CfgData& cfgData)
{
    static const Units rawFloat(Format::Float);
    static const Units rawInt(Format::Int);
    static const Units indexUnits(Format::Int, 4, true);

    switch (mt) {
    case PointModel::Index:    return indexUnits;
    case PointModel::Time:     return cfgData.unitsPointDate;
    case PointModel::Lon:      return cfgData.unitsLon;
    case PointModel::Lat:      return cfgData.unitsLat;
    case PointModel::Ele:      return cfgData.unitsElevation;
    case PointModel::Length:   return cfgData.unitsLegLength;
    case PointModel::Distance: return cfgData.unitsTrkLength; // use track units for total distance
    case PointModel::Vert :    [[fallthrough]];
    case PointModel::Depth:    return cfgData.unitsClimb;
    case PointModel::Grade :   return cfgData.unitsSlope;
    case PointModel::Elapsed:  [[fallthrough]];
    case PointModel::Duration: return cfgData.unitsDuration;
    case PointModel::Temp:     return cfgData.unitsTemp;
    case PointModel::Speed:    return cfgData.unitsSpeed;
    case PointModel::Accel:    return cfgData.unitsAccel;
    case PointModel::Hr:       return rawInt;
    case PointModel::Cad:      return rawInt;
    case PointModel::Power:    return cfgData.unitsPower;
    case PointModel::Course:   return rawFloat;  // TODO: ...
    case PointModel::Bearing:  return rawFloat;  // TODO: ...
    }

    assert(0);
    return rawFloat;
}

bool PointModel::mdIsChartable(ModelType mt)
{
    switch (mt) {
    case PointModel::Index:    [[fallthrough]];
    case PointModel::Time:     [[fallthrough]];
    case PointModel::Length:   [[fallthrough]];
    case PointModel::Distance: [[fallthrough]];
    case PointModel::Vert :    [[fallthrough]];
    case PointModel::Duration: return false;
    default:                 return true;
    }
}

QDataStream& operator<<(QDataStream& stream, const PointModel& p)
{
    return stream << static_cast<const PointModel::container_type&>(p);
}

QDataStream& operator>>(QDataStream& stream, PointModel& p)
{
    return stream >> static_cast<PointModel::container_type&>(p);
}

uint qHash(const PointModel& p, uint seed)
{
    return qHashRange(p.begin(), p.end(), seed);
}

class TrackMap;
template<>
bool PointModel::is(const TrackMap&, const QModelIndex &idx, const PointItem& point) const
{
    return getItem(idx) == &point;
}

class PointPane;
template<> void PointModel::unselectAll(const PointPane&)
{
    for (auto& trkseg : *this)
        for (auto& pt : trkseg)
            pt.set(false, PointItem::Select);
}

class TrackItem;
template<> void PointModel::resetHasData(const TrackItem&)
{
    m_hasData.reset();
}

template<> void PointModel::accumulateHasData(const TrackItem&, const PointItem& pt)
{
    for (int d = _First; d < _Count; ++d)
        m_hasData[d] = m_hasData[d] || pt.hasData(d);
}

class AreaDialog;
template<> void PointModel::accumulateHasData(const AreaDialog&, std::bitset<_Count>& set) const
{
    set |= m_hasData;
}

template<> QModelIndex PointModel::areaExtreme(const AreaDialog&) const
{
    return m_areaExtreme;
}

template<> void PointModel::select(const PointPane&, const QModelIndex& idx, bool newState)
{
    if (auto* pt = getItem(idx); pt != nullptr)
        pt->set(newState, PointItem::Select);
}

template <> void PointModel::invalidateFilter(const TrackItem&)
{
    eleFilterSizeCache = -1;
    locFilterSizeCache = -1;
}

bool PointModel::selectPointsWithin(const Marble::GeoDataLatLonBox& bounds)
{
    bool found = false;

    for (auto& trkseg : *this) {
        for (auto& pt : trkseg) {
            if (bounds.contains(Math::toRad(pt.lon()), Math::toRad(pt.lat())))
                pt.set(found = true, PointItem::AreaSelect);
            else
                pt.set(false, PointItem::AreaSelect);
        }
    }

    return found;
}

class AreaDialog;

template<>
std::tuple<qreal, int> PointModel::extremeVal(AreaDialog&, ModelType mt, Extreme type) const
{
    int count     = 0;
    qreal extreme = (type == Extreme::Avg)  ? 0.0 :
                    (type == Extreme::High) ? -std::numeric_limits<qreal>::max() :
                    (type == Extreme::Low)  ? std::numeric_limits<qreal>::max() : 0.0;

    QModelIndex extremeIdx;

    m_areaExtreme = QModelIndex();

    int trkRow = 0;
    for (auto& trkseg : *this) {
        const QModelIndex parent = index(trkRow++, 0);

        int ptRow = 0;
        for (auto& pt : trkseg) {
            if (pt.is(PointItem::AreaSelect) && pt.hasData(mt)) {
                const qreal val = pt.data(mt, -1, Util::RawDataRole, false, trackStartTime(), powerData(), cfgData).toDouble();
                switch (type) {
                case Extreme::High:
                    if (val > extreme) {
                        extreme    = val;
                        extremeIdx = index(ptRow, 0, parent);
                        count      = 1;
                    }
                    break;
                case Extreme::Low:
                    if (val < extreme) {
                        extreme    = val;
                        extremeIdx = index(ptRow, 0, parent);
                        count      = 1;
                    }
                    break;
                case Extreme::Avg:
                    extreme += val;
                    ++count;
                }
            }

            ++ptRow;
        }
    }

    if (extremeIdx.isValid())
        m_areaExtreme = extremeIdx;

    if (count > 0 && type == Extreme::Avg)
        extreme /= count;

    return std::make_tuple(extreme, count);
}

// Set power parameters as function of tag and person
void PointModel::setPowerData(const QModelIndex& tagIdx, const QModelIndex& personIdx)
{
    if (!tagIdx.isValid() || !personIdx.isValid()) {
        m_powerData.clear();
        return;
    }

    const qreal weight =
            cfgData.people.data(PersonModel::Weight, personIdx, Util::RawDataRole).toDouble() +
            cfgData.tags.data(TagModel::Weight, tagIdx, Util::RawDataRole).toDouble();

    const qreal CdA     = cfgData.tags.data(TagModel::CdA, tagIdx, Util::RawDataRole).toDouble();
    const qreal rr      = cfgData.tags.data(TagModel::RR, tagIdx, Util::RawDataRole).toDouble();

    const QVariant personEffVar = cfgData.people.data(PersonModel::Efficiency, personIdx, Util::RawDataRole);
    const QVariant tagEffVar    = cfgData.tags.data(TagModel::Efficiency, tagIdx, Util::RawDataRole);
    const QVariant bioPctVar    = cfgData.tags.data(TagModel::BioPct, tagIdx, Util::RawDataRole);

    const qreal personEff  = personEffVar.isValid() ? personEffVar.toDouble() : 1.0;
    const qreal tagEff     = tagEffVar.isValid()    ? tagEffVar.toDouble()    : 1.0;
    const qreal bioPct     = bioPctVar.isValid()    ? bioPctVar.toDouble()    : 1.0;

    // System efficiency: vehicle efficiency always applies, but apply only a % of the bio efficiency
    // depending on the vehicle's % power coming from biology.
    const qreal efficiency = (1.0 - (1.0 - personEff) * bioPct) * tagEff;

    const QString medium = cfgData.tags.data(TagModel::Medium, tagIdx, Util::RawDataRole).toString();
    qreal density     = 1.225;  // default to air density
    bool  eleVariance = true;   // whether to vary density with altitude
    
    if (const auto it = TagModel::medium.find(medium); it != TagModel::medium.end()) {
        density     = std::get<0>(it.value());
        eleVariance = std::get<1>(it.value());
    }

    m_powerData = PowerData(CdA, weight, rr, efficiency, density, eleVariance);
}
