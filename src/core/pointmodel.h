/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POINTMODEL_H
#define POINTMODEL_H

#include <cassert>
#include <limits>
#include <array>
#include <tuple>
#include <bitset>
#include <QList>
#include <QVector>
#include <QModelIndexList>
#include <QTimer>
#include <marble/GeoDataLatLonBox.h>

#include <src/core/modelmetadata.h>
#include <src/core/treemodel.h>
#include <src/util/roles.h>
#include "pointitem.h"
#include "powerdata.h"

class CfgData;
class QItemSelectionModel;
class QSortFilterProxyModel;
class Units;
class QDataStream;
class TrackItem;

// Each element in the vector tracks a new point.  Each element in the list tracks
// a separate route segment.  We want the inner container to be a vector to get
// effecient contiguous storage.
class PointModel final : public QList<QVector<PointItem>>, public TreeModel, public ModelMetaData
{
public:
    enum {
        _First,
        Index = _First,
        Time,
        Elapsed,
        Lon,
        Lat,
        Ele,
        Length,
        Distance,
        Vert,
        Grade,
        Duration,
        Temp,
        Depth,
        Speed,
        Accel,
        Hr,
        Cad,
        Power,
        Course,
        Bearing,
        _Count
    };

    PointModel();
    PointModel(TrackItem*, const CfgData&);

    void filter();
    // void update start index cache, for data() performance
    void updateStartIdx();

    // Count of selected segments
    int selectedSegments(const QItemSelectionModel*, const QSortFilterProxyModel*) const;

    using TreeModel::units;
    const Units& units(const QModelIndex& idx) const override;

    static const constexpr qreal earth_r_m = 6371008.8; // mean radius in m between equatorial and polar

    typedef QList<QVector<PointItem>> container_type;

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static const Units&   mdUnits(ModelType, const CfgData&);
    static bool           mdIsChartable(ModelType);
    // *** end ModelMetaData API

    std::tuple<qreal, qreal, qreal, qreal, bool> bounds(const QModelIndexList& selection) const;
    Marble::GeoDataLatLonBox boundsBox(const QModelIndexList& selection) const;

    bool selectPointsWithin(const Marble::GeoDataLatLonBox&);

    // Something of a kludge, just for the TrackMap drawing function performance.. TODO: remove
    template <class T> bool is(const T&, const QModelIndex& idx, const PointItem&) const;

    template <class T> void unselectAll(const T&);
    template <class T> void select(const T&, const QModelIndex&, bool newState);
    template <class T> void resetHasData(const T&);
    template <class T> void accumulateHasData(const T&, const PointItem&);
    template <class T> void accumulateHasData(const T&, std::bitset<_Count>&) const;
    template <class T> QModelIndex areaExtreme(const T&) const;
    template <class T> void invalidateFilter(const T&);

    bool hasData(ModelType mt) const { return m_hasData.test(mt); }

    enum Extreme { High, Low, Avg };
    template <class T> std::tuple<qreal, int> extremeVal(T&, ModelType, Extreme) const;

    inline QVariant data(const PointItem*, ModelType col, const QModelIndex&,
                         int role = Qt::DisplayRole, bool filtered = true) const;

    // *** begin QAbstractItemModel API
    using TreeModel::data;
    QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const override;

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& idx) const override;

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const override { return _Count; }

    Qt::ItemFlags flags(const QModelIndex& idx) const override;
    bool setData(const QModelIndex& idx, const QVariant& value,
                 int role = Qt::DisplayRole) override;
    bool setHeaderData(int section, Qt::Orientation orientation,
                       const QVariant& value, int role = Qt::DisplayRole) override;
    bool insertColumns(int /*position*/, int /*columns*/,
                       const QModelIndex& = QModelIndex()) override { return false; }
    bool removeColumns(int /*position*/, int /*columns*/,
                       const QModelIndex& = QModelIndex()) override { return false; }
    bool insertRows(int position, int rows,
                    const QModelIndex& parent = QModelIndex()) override;
    bool removeRows(int position, int rows,
                    const QModelIndex& parent = QModelIndex()) override;
    using QAbstractItemModel::insertRow;
    // *** end QAbstractItemModel API

    // Remove selected rows
    void removeRows(const QItemSelectionModel*, const QSortFilterProxyModel* = nullptr,
                    const QModelIndex& parent = QModelIndex());

    // Remove segments with no data points
    void removeEmptySegments();

    // Merge selected track segments
    void mergeSegments(const QItemSelectionModel*, const QSortFilterProxyModel* = nullptr);

    // Split segments at selected points.
    void splitSegments(const QItemSelectionModel*, const QSortFilterProxyModel* = nullptr);

    QVariant siblingData(int column, const QModelIndex& idx, int role = Qt::DisplayRole) const;

    void setPowerData(const QModelIndex& tagIdx, const QModelIndex& personIdx);
    const PowerData& powerData() const { return m_powerData; }
    const QDateTime& trackStartTime() const;
    qreal            trackTotalDistance() const;
    int              trackTotalPoints() const;

    // Shortcut to get to data if we have a PointItem already.
    template <typename T> T ptData(const PointItem&, ModelType, int row, bool flt = true) const;

    // Interpolate value in column 'mt' for given distance
    qreal interpolate(qreal dist, ModelType mt) const;

private:
    void setupTimers();
    void processDataChanged();
    void sortItems();

    // Workaround for: https://bugreports.qt.io/browse/QTBUG-44962
    bool wrongModel(const QModelIndex& idx) const {
        return idx.model() != this && idx.model() != nullptr;
    }

    bool isRowSelected(int row, const QItemSelectionModel*, const QSortFilterProxyModel*) const;

    // Obtain first item whose distance() is not less than 'dist'
    std::tuple<const PointItem*, const PointItem*> getItemBound(qreal dist) const;

    const PointItem* getItem(const QModelIndex& idx) const {
        return const_cast<const PointItem*>(const_cast<PointModel*>(this)->getItem(idx));
    }

    PointItem* getItem(const QModelIndex& idx) {
        if (!idx.isValid() || qintptr(idx.internalId()) < 0 || idx.model() != this)
            return nullptr;

        return &(*this)[idx.internalId()][idx.row()];
    }

    const QModelIndex& verify(const QModelIndex& idx) const {
        assert(idx.model() == nullptr || idx.model() == this);
        return idx;
    }

    // Determine average time spacing of sample points
    static qreal avgTimeSpacing(const value_type&);

    // filter single vector of points, for all members in "in", to members in "out".
    template <int N>
    static void filter(value_type&, int filterSize,
                       const std::array<qreal PointItem::*, N>& out,
                       const std::array<qreal PointItem::*, N>& in);

    void calcPointValues(value_type&, qreal& accumDistance);

    QString tooltip(const QModelIndex& idx) const;

    int                 eleFilterSizeCache; // for efficiency: avoid needless re-filters
    int                 locFilterSizeCache; // ...

    PowerData           m_powerData;
    QVector<uint>       m_startIdxCache;
    std::bitset<_Count> m_hasData;
    QTimer              m_dataChangedTimer; // to avoid recalc spam
    mutable QModelIndex m_areaExtreme;      // index of area extreme point.
    bool                m_timeDirty;        // time changed: needs a resort

    TrackItem*          m_trackItem;        // owner track
    const CfgData&      cfgData;
};

template <typename T>
inline T PointModel::ptData(const PointItem& pt, ModelType mt, int row, bool flt) const
{
    return pt.data<T>(mt, row, Util::RawDataRole, flt, trackStartTime(), powerData(), cfgData);
}

// Serialization
extern QDataStream& operator<<(QDataStream&, const PointModel&);
extern QDataStream& operator>>(QDataStream&, PointModel&);

// Hash
uint qHash(const PointModel&, uint seed = 0x76920aba);

#endif // POINTMODEL_H
