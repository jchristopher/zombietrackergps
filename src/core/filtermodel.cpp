/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include "filtermodel.h"
#include "filteritem.h"
#include "cfgdata.h"

FilterModel::FilterModel(const CfgData& cfgData, QObject* parent) :
    TreeModel(new FilterItem(), parent),
    m_cfgData(cfgData)
{
    setHorizontalHeaderLabels(headersList<FilterModel>());
}

QVariant FilterModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (QVariant val = ModelMetaData::headerData<FilterModel>(section, orientation, role); val.isValid())
        return val;

    return TreeModel::headerData(section, orientation, role);
}

const Units& FilterModel::units(const QModelIndex& idx) const
{
    return mdUnits(idx.column(), cfgData());
}

Qt::ItemFlags FilterModel::flags(const QModelIndex& idx) const
{
    return TreeModel::flags(idx) | ModelMetaData::flags<FilterModel>(idx) |
        Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
}

void FilterModel::load(QSettings& settings)
{
    TreeModel::load(settings);
    setHorizontalHeaderLabels(headersList<FilterModel>());  // put header labels back, in case not present in save.
}

QString FilterModel::mdName(ModelType d)
{
    switch (d) {
    case FilterModel::Name:    return QObject::tr("Name");
    case FilterModel::Query:   return QObject::tr("Query");
    case FilterModel::_Count:  break;
    }

    assert(0 && "Unknown TrackData value");
    return "";
}

bool FilterModel::mdIsEditable(ModelType)
{
    return true;
}

// tooltip for container column header
QString FilterModel::mdTooltip(ModelType td)
{
#define cd_header(i)       "<p><b><u>" i ":</u></b><p>"
#define cd_info cd_header("Column Information")

    static const QByteArray can_edit =
            cd_header("Editable")
            "You can edit data in this column by selecting the Edit context menu.";

    switch (td) {
    case FilterModel::Name:
        return QObject::tr(cd_info "Descriptive name.") + QObject::tr(can_edit);

    case FilterModel::Query: return QObject::tr(cd_info "Query string.");

    case FilterModel::_Count:
        break;
    }

    assert(0 && "Unknown TrackData value");
    return nullptr;
}

// tooltip for container column header
QString FilterModel::mdWhatsthis(ModelType td)
{
    return mdTooltip(td);  // just pass through the tooltip
}

Qt::Alignment FilterModel::mdAlignment(ModelType)
{
    return Qt::AlignLeft | Qt::AlignVCenter;
}

const Units& FilterModel::mdUnits(ModelType, const CfgData&)
{
    static const Units rawString(Format::String);
    return rawString;
}

int FilterModel::mdDataRole(ModelType)
{
    return Util::RawDataRole;
}
