/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef VIEWMODEL_H
#define VIEWMODEL_H

#include <QModelIndex>

#include <src/core/modelmetadata.h>
#include <src/core/treemodel.h>

class ViewItem;
class CfgData;
class Units;

class ViewModel final : public TreeModel, public ModelMetaData
{
    Q_OBJECT

public:
    enum {
        _First    = 0,
        Name      = _First,
        Icon      = Name,  // icon goes in name column
        CenterLat,
        CenterLon,
        Heading,
        Zoom,
        _Count,
    };

    ViewModel(const CfgData& cfgData, QObject *parent = 0);

    void setRow(const QString& name, qreal centerLat, qreal centerLon, qreal heading, int zoom,
                const QModelIndex& idx);

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    const Units& units(const QModelIndex& idx) const override;

    // *** begin Settings API
    using TreeModel::save;
    void load(QSettings& settings) override;
    // *** end Settings API

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static const Units&   mdUnits(ModelType, const CfgData&);
    // *** end ModelMetaData API

private:
    const CfgData& cfgData;

    Qt::DropActions supportedDropActions() const override { return Qt::MoveAction; }
    Qt::ItemFlags flags(const QModelIndex&) const override;
};

#endif // TAGMODEL_H
