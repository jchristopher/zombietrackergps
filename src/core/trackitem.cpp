/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cmath>
#include <cassert>
#include <limits>
#include <QApplication>
#include <QDataStream>

#include <src/util/math.h>
#include <src/util/roles.h>
#include "src/core/cfgdata.h"
#include "trackitem.h"
#include "trackmodel.h"                 // for the headers

QFont TrackItem::smallerFont;

TrackItem::TrackItem(const CfgData& cfgData, TreeItem *parent) :
    TreeItem(parent),
    geoPoints(this, cfgData),
    m_isVisible(true),
    m_areaSelected(false),
    cfgData(cfgData)
{
    setupSmallFont();
}

TrackItem::TrackItem(const CfgData& cfgData, const TreeItem::ItemData& data, TreeItem *parent) :
    TreeItem(data, parent, Util::RawDataRole),
    geoPoints(this, cfgData),
    m_isVisible(true),
    m_areaSelected(false),
    cfgData(cfgData)
{
}

TrackItem::~TrackItem()
{
}

void TrackItem::setupSmallFont()
{
    smallerFont = QApplication::font();
    smallerFont.setPointSize(smallerFont.pointSize() * 0.75);
}

QVariant TrackItem::nthTagData(int n, ModelType column, int role) const
{
    // Little painful way to extract the string, but avoids dynamic allocations.
    const QStringList tags = TreeItem::data(TrackModel::Tags, Util::RawDataRole).value<QStringList>();

    if (n >= tags.size())
        return QVariant();

    // find first tag if possible and use its icon.  TODO: render all of them.
    return cfgData.tags.value(tags[n], column, role);
}

QVariant TrackItem::trackColor(const QVariant& rawData) const
{
    if (rawData.type() == QVariant::Color)
        return rawData; // Use track-specific color if there is one
    else if (const QVariant tagColor = nthTagData(0, TagModel::Color, Qt::BackgroundRole);
             tagColor.type() == QVariant::Color)
        return tagColor; // Use tag color if there is one.
    else
        return cfgData.unassignedTrackColor; // Otherwise, unassigned.
}

const Units& TrackItem::units(ModelType mt) const
{
    static const Units unitsFloat(Format::Float);

    switch (mt) {
    case TrackModel::MinSpeed:      [[fallthrough]];
    case TrackModel::AvgOvrSpeed:   [[fallthrough]];
    case TrackModel::AvgMovSpeed:   [[fallthrough]];
    case TrackModel::MaxSpeed: {
        const QStringList tags = TreeItem::data(TrackModel::Tags, Util::RawDataRole).value<QStringList>();
        return tags.isEmpty() ? cfgData.unitsSpeed : cfgData.tags.units(tags.front(), TagModel::UnitSpeed);
    }

    default:
        return TrackModel::mdUnits(mt, cfgData);
    }
}

QVariant TrackItem::data(int column, int role) const
{
    if (parent() == nullptr)
        return TreeItem::data(column, role);

    const QVariant& rawData = TreeItem::data(column, Util::RawDataRole);

    switch (role) {
    case Util::RawDataRole: // small performance tweak to avoid redundant TreeItem::data()
        return rawData;

    case Qt::TextAlignmentRole:
        return QVariant(TrackModel::mdAlignment(column));

    case Qt::SizeHintRole:
        switch (column) {
        case TrackModel::Notes:
            return cfgData.iconSizeTrack; // TODO: actualSize...
        }
        break;

    case Qt::DecorationRole:
        switch (column) {
        case TrackModel::Notes:
            if (!rawData.isNull())
                return QIcon(cfgData.trackNoteIcon);
            break;

        case TrackModel::Tags:
            return nthTagData(0, TagModel::Icon, Qt::DecorationRole);
        }
        break;

    case Qt::ToolTipRole:
        if (column == TrackModel::Notes)
            return rawData;
        break;

    case Qt::EditRole:    [[fallthrough]];
    case Util::PlotRole:
        return rawData;

    case Qt::ForegroundRole:
        if (column == TrackModel::Color) {
            if (QVariant fgColor = trackColor(TreeItem::data(column, Util::RawDataRole));
                    fgColor.type() == QVariant::Color)
                return (fgColor.value<QColor>().lightness() > 127) ? QColor(QRgb(0x00000000)) : QColor(QRgb(0x00ffffffff));
        }
        break;

    case Qt::BackgroundRole:
        if (column == TrackModel::Color)
            return trackColor(rawData);
        break;

    case Qt::FontRole:
        if (column == TrackModel::Color)
            return smallerFont;
        break;

    case Qt::DisplayRole: [[fallthrough]];
    case Util::CopyRole:
        // Inidicator of how color was determined.
        if (column == TrackModel::Color && role == Qt::DisplayRole) {
            if (rawData.type() == QVariant::Color)
                return QObject::tr("(track)");
            else if (nthTagData(0, TagModel::Color, Qt::BackgroundRole).type() == QVariant::Color)
                return QObject::tr("(tag)");
            else
                return QObject::tr("(default)");
        }

        if (rawData.isValid()) {
            switch (column) {
            case TrackModel::Type:
                return TrackModel::trackTypeName(rawData.value<TrackType>());
            case TrackModel::Tags:
                if (role == Util::CopyRole && rawData.type() == QVariant::StringList)
                    return static_cast<const QStringList*>(rawData.constData())->join(", ");
                break;
            case TrackModel::Notes:
                if (role == Qt::DisplayRole)
                    return QString(); // we render an icon.
                break;
            default:
                return units(column)(rawData);
            }
        }
    }

    // We didn't handle anything specially for the copy: get the display value instead
    if (role == Util::CopyRole)
        role = Qt::DisplayRole;

    return TreeItem::data(column, role);
}

bool TrackItem::setData(int column, const QVariant &value, int role, bool &changed,
                        const QModelIndex& personIdx)
{
    if (role == Qt::BackgroundRole)
        return false;

    if (role == Qt::EditRole)
        role = Util::RawDataRole;

    const bool rc = TreeItem::setData(column, value, role, changed);

    // If tags are set, update power data.
    if (column == TrackModel::Tags)
        setPersonIdx(personIdx);

    return rc;
}

int TrackItem::columnCount() const
{
    return TrackModel::_Count;
}

TrackItem *TrackItem::factory(const TreeItem::ItemData& data, TreeItem* parent)
{
    assert(dynamic_cast<TrackItem*>(parent) != nullptr);

    return new TrackItem(cfgData, data, parent);
}

// Append segments from geoPoints.  NOTE: for efficiency, this is destructive
// to the input list: it uses std::swap.
void TrackItem::append(TrackModel& model, PointModel& newPoints)
{
    // Add it to our geo point master data
    for (auto& pointVec : newPoints) {
        geoPoints.append(PointModel::value_type());
        geoPoints.back().swap(pointVec); // for efficiency, swap instead of copy
    }

    geoPoints.setPowerData(firstPowerTag(), model.personIdx(*this));
    update();
}

void TrackItem::append(const PointModel& newPoints)
{
    // Add it to our geo point master data
    for (auto& pointVec : newPoints)
        geoPoints.append(pointVec);

    update();
}

// Refresh placemark data from base GeoPoints
void TrackItem::refresh(TrackModel& model)
{
    // Use current tag and person data.
    geoPoints.setPowerData(firstPowerTag(), model.personIdx(*this));
    trackLines.clear();
    threadedUpdate(model);
}

// Update (in same thread) after geo data change
template<> void TrackItem::update(const PointModel&)
{
    geoPoints.invalidateFilter(*this);
    update(true);
}

void TrackItem::update(bool force)
{
    updateTrackData();         // update track metadata
    updateTrackLines(force);   // create placemarks for new data
}

void TrackItem::threadedUpdate(TrackModel& model)
{
    static const bool threadedUpdate = true;

    if (threadedUpdate)
        model.updateQueueAdd(this); // background thread update
    else
        update();                   // foreground version for debugging
}

// Adds any data present in the geoPoints data to the placemark data.
void TrackItem::updateTrackLines(bool force)
{
    // If segments were removed, refresh from scratch.
    if (geoPoints.size() < trackLines.size() || force)
        trackLines.clear();

    trackLines.resize(geoPoints.size());

    // Add it to any maps we are tracking
    auto segLines = trackLines.begin();
    for (auto& trkseg : geoPoints) {
        int addFrom = segLines->size();

        // we already have all the points in this track segment
        if (trkseg.size() == addFrom) {
            ++segLines;
            continue;
        }

        // Items were removed.  Refresh segment lines from scratch.
        if (trkseg.size() < addFrom) {
            segLines->clear();
            addFrom = 0;
        }

        segLines->reserve(trkseg.size());
        for (auto pt = trkseg.begin() + addFrom; pt != trkseg.end(); ++pt)
            segLines->append(pt->as<Marble::GeoDataCoordinates>());

        ++segLines;
    }
}

// Converts lon/lat to projected X/Y
std::pair<qreal, qreal> TrackItem::projectPt(const PointItem* pt)
{
    static const qreal NaN = std::numeric_limits<qreal>::quiet_NaN();
    static const qreal lat_dist = Math::toRad(1.0) * PointModel::earth_r_m;

    if (pt == nullptr)
        return std::make_pair(NaN, NaN);

    return std::make_pair(pt->lon() * cos(Math::toRad(pt->lat())) * lat_dist,
                          pt->lat() * lat_dist);
}

bool TrackItem::pointEqual(const TrackItem& other) const
{
    return geoPoints == other.geoPoints;
}

bool TrackItem::pointEqual(const PointModel& other) const
{
    return geoPoints == other;
}

// This happens when the user changes the configured person, or the
// track tags, so it's a more common event than updateTrackData.  We
// pull it out here.
void TrackItem::updatePowerData()
{
    static const qreal NaN  = std::numeric_limits<qreal>::quiet_NaN();

    qreal     minPower    = NaN;
    qreal     avgMovPower = NaN;
    qreal     maxPower    = NaN;
    qreal     inputWattHr = NaN;
    qreal     wattmsec    = 0; // for average power

    const qint64 movingTime = data(TrackModel::MovingTime, Util::RawDataRole).toLongLong() / mStonS;

    if (movingTime == 0)
        return;

    for (const auto& trkSeg : geoPoints) {
        for (auto& pt : trkSeg) {
            if (pt.hasPower(PointItem::Either)) {
                if (const qreal power = pt.power(geoPoints.powerData()); power > 1.0) {
                    minPower  = std::fmin(minPower, power);
                    maxPower  = std::fmax(maxPower, power);
                    wattmsec  += power * pt.duration();
                }
            }
        }
    }

    // Helper to return data, or an empty variant if it was unset (nan)
    const auto issetf  = [](qreal d) -> QVariant { return std::isnan(d) ? QVariant() : d; };

    if (!std::isnan(minPower) && !std::isnan(maxPower)) {
        avgMovPower = wattmsec / movingTime;
        inputWattHr = (wattmsec * 1.0_DatemS / 1.0_DateH) / std::max(geoPoints.powerData().efficiency, 0.001f);
    }

    setData(TrackModel::MinPower,      issetf(minPower),      Util::RawDataRole);
    setData(TrackModel::AvgMovPower,   issetf(avgMovPower),   Util::RawDataRole);
    setData(TrackModel::MaxPower,      issetf(maxPower),      Util::RawDataRole);
    setData(TrackModel::Energy,        issetf(inputWattHr),   Util::RawDataRole);
}

// Recalculate the track metadata from the GeoPoints.
void TrackItem::updateTrackData()
{
    static const qreal NaN  = std::numeric_limits<qreal>::quiet_NaN();
    static const uint8_t badHrCad = std::numeric_limits<uint8_t>::max();

    qreal     length      = 0.0;
    QDateTime beginDate;  // default constructor sets invalid flag
    QDateTime endDate;    // ...
    qint64    stoppedTime = 0;
    qint64    movingTime  = 0;
    qint64    totalTime   = 0;
    qreal     minEle      = NaN;
    qreal     maxEle      = NaN;
    qreal     minSpeed    = NaN;
    qreal     avgOvrSpeed = NaN;
    qreal     avgMovSpeed = NaN;
    qreal     maxSpeed    = NaN;
    qreal     minGrade    = NaN;
    qreal     maxGrade    = NaN;
    uint8_t   minCad      = badHrCad;
    uint8_t   avgMovCad   = badHrCad;
    uint8_t   maxCad      = badHrCad;
    qreal     ascent      = 0.0;
    qreal     descent     = 0.0;
    qreal     baseToPeak  = 0.0;
    int       points      = 0;
    qreal     area        = NaN;
    qreal     minTemp     = NaN;
    qreal     avgTemp     = NaN;
    qreal     maxTemp     = NaN;
    uint8_t   minHR       = badHrCad;
    uint8_t   avgHR       = badHrCad;
    uint8_t   maxHR       = badHrCad;
    qreal     minLon      = NaN;
    qreal     maxLon      = NaN;
    qreal     minLat      = NaN;
    qreal     maxLat      = NaN;

    qint64    cadmsec    = 0; // for average pedal RPM
    qint64    hrmsec     = 0; // for average heart rate
    qint64    tempmsec   = 0; // for average temperature

    // Apply smoothing filters to the data
    geoPoints.updateStartIdx();
    geoPoints.filter();
    geoPoints.resetHasData(*this);

    const PointItem* lastPt = nullptr;
    if (!geoPoints.empty() && !geoPoints.back().empty())
        lastPt = &geoPoints.back().back();

    auto [prevProjX, prevProjY] = projectPt(lastPt);

    for (const auto& trkSeg : geoPoints) {
        qreal smoothSpeed = 0.0;  // for smoothing
        points += trkSeg.size();

        if (std::isnan(area) && points >= 3)
            area = 0.0;

        for (auto& pt : trkSeg) {
            geoPoints.accumulateHasData(*this, pt);

            if (pt.hasLoc()) {
                minLon    = std::fmin(minLon,   pt.lon());
                maxLon    = std::fmax(maxLon,   pt.lon());
                minLat    = std::fmin(minLat,   pt.lat());
                maxLat    = std::fmax(maxLat,   pt.lat());
            }

            if (pt.hasTime()) {
                beginDate = std::min(beginDate.isValid() ? beginDate : pt.time(), pt.time());
                endDate   = std::max(endDate.isValid() ? endDate : pt.time(),  pt.time());
            }

            if (pt.hasEle()) {
                minEle    = std::fmin(minEle,   pt.ele());
                maxEle    = std::fmax(maxEle,   pt.ele());
            }

            if (pt.hasCad() && pt.cad() > 5) { // ignore very low values
                minCad    = std::min(minCad,    pt.cad());
                maxCad    = std::max((maxCad == badHrCad) ? decltype(maxCad)(0) : maxCad, pt.cad());
            }

            if (pt.hasAtemp()) {
                minTemp   = std::fmin(minTemp,  pt.atemp());
                maxTemp   = std::fmax(maxTemp,  pt.atemp());
            }

            if (pt.hasHr() && pt.hr() > 40) { // ignore very low values
                minHR     = std::min(minHR,     pt.hr());
                maxHR     = std::max((maxHR == badHrCad) ? decltype(maxHR)(0) : maxHR, pt.hr());
            }

            if (pt.hasLength()) {
                length += pt.length();

                // Smooth out the speed a little (there are occasional spikes)  TODO: use convolution
                smoothSpeed              = Math::mix(smoothSpeed, pt.speed(), 0.25);
                const bool isMoving      = (smoothSpeed >= stoppedThresholdSpeed);

                // Times, in mS
                totalTime   += pt.duration();
                movingTime  += (isMoving ? pt.duration() : 0);
                stoppedTime += (isMoving ? 0 : pt.duration());

                // Calculate ascent, descent, and grade
                if (isMoving) {
                    minSpeed = std::fmin(minSpeed, smoothSpeed);
                    maxSpeed = std::fmax(maxSpeed, smoothSpeed);

                    // Early track data often has dodgy elevations, so skip that.
                    if (pt.hasVert()) {
                        ascent  += ((pt.grade() >= 0) ?  pt.vert() : 0);
                        descent += ((pt.grade() < 0)  ? -pt.vert() : 0);

                        if (pt.hasGrade()) {
                            minGrade = std::fmin(minGrade, pt.grade());
                            maxGrade = std::fmax(maxGrade, pt.grade());
                        }
                    }
                }
            }

            // For avg moving cadence/hr
            if (pt.hasCad())   cadmsec  += pt.cad()   * pt.duration();
            if (pt.hasHr())    hrmsec   += pt.hr()    * pt.duration();
            if (pt.hasAtemp()) tempmsec += pt.atemp() * pt.duration();

            if (!std::isnan(area)) {
                const auto& [projX, projY] = projectPt(&pt);
                area     += (projX + prevProjX) * (projY - prevProjY);
                prevProjX = projX;
                prevProjY = projY;
            }
        }
    }

    if (totalTime > 0) {
        avgOvrSpeed = length * 1000.0 / totalTime;

        if (!(minHR == maxHR && minHR == badHrCad))
            avgHR  = hrmsec / totalTime;

        if (tempmsec > 0)
            avgTemp = tempmsec / totalTime;
    }

    if (movingTime > 0) {
        avgMovSpeed = length * 1000.0 / movingTime;

        if (!(minCad == maxCad && minCad == badHrCad))
            avgMovCad   = cadmsec / movingTime;
    }

    area = std::abs(area) * 0.5;

    // Helper to return data, or an empty variant if it was unset (nan)
    const auto issetf  = [](qreal d) -> QVariant { return std::isnan(d) ? QVariant() : d; };
    const auto isnonf0 = [](qreal d) -> QVariant { return d == 0.0 ? QVariant() : d; };

    const auto issetu8 = [](uint8_t d) -> QVariant {
        return (d == std::numeric_limits<decltype(d)>::max()) ? QVariant() : d;
    };

    // Units class wants nS
    stoppedTime *= mStonS;
    movingTime  *= mStonS;
    totalTime   *= mStonS;

    // Time of date (not including date).  Because QTime has no notion of time zones, we have to
    // do the conversion here.
    const QTime beginTime = cfgData.unitsTrkTime.isUTC() ? beginDate.time() : beginDate.toLocalTime().time();
    const QTime endTime   = cfgData.unitsTrkTime.isUTC() ? endDate.time() : endDate.toLocalTime().time();

    // base/peak diff
    baseToPeak = maxEle - minEle;

    setData(TrackModel::Length,        isnonf0(length),       Util::RawDataRole);
    setData(TrackModel::BeginDate,     beginDate,             Util::RawDataRole);
    setData(TrackModel::EndDate,       endDate,               Util::RawDataRole);
    setData(TrackModel::BeginTime,     beginTime,             Util::RawDataRole);
    setData(TrackModel::EndTime,       endTime,               Util::RawDataRole);
    setData(TrackModel::StoppedTime,   stoppedTime,           Util::RawDataRole);
    setData(TrackModel::MovingTime,    movingTime,            Util::RawDataRole);
    setData(TrackModel::TotalTime,     totalTime,             Util::RawDataRole);
    setData(TrackModel::MinElevation,  issetf(minEle),        Util::RawDataRole);
    setData(TrackModel::MaxElevation,  issetf(maxEle),        Util::RawDataRole);
    setData(TrackModel::MinSpeed,      issetf(minSpeed),      Util::RawDataRole);
    setData(TrackModel::AvgOvrSpeed,   issetf(avgOvrSpeed),   Util::RawDataRole);
    setData(TrackModel::AvgMovSpeed,   issetf(avgMovSpeed),   Util::RawDataRole);
    setData(TrackModel::MaxSpeed,      issetf(maxSpeed),      Util::RawDataRole);
    setData(TrackModel::MinGrade,      issetf(minGrade),      Util::RawDataRole);
    setData(TrackModel::MaxGrade,      issetf(maxGrade),      Util::RawDataRole);
    setData(TrackModel::MinCad,        issetu8(minCad),       Util::RawDataRole);
    setData(TrackModel::AvgMovCad,     issetu8(avgMovCad),    Util::RawDataRole);
    setData(TrackModel::MaxCad,        issetu8(maxCad),       Util::RawDataRole);
    setData(TrackModel::Ascent,        isnonf0(ascent),       Util::RawDataRole);
    setData(TrackModel::Descent,       isnonf0(descent),      Util::RawDataRole);
    setData(TrackModel::BasePeak,      issetf(baseToPeak),    Util::RawDataRole);
    setData(TrackModel::Segments,      geoPoints.size(),      Util::RawDataRole);
    setData(TrackModel::Points,        points,                Util::RawDataRole);
    setData(TrackModel::Area,          issetf(area),          Util::RawDataRole);
    setData(TrackModel::MinTemp,       issetf(minTemp),       Util::RawDataRole);
    setData(TrackModel::AvgTemp,       issetf(avgTemp),       Util::RawDataRole);
    setData(TrackModel::MaxTemp,       issetf(maxTemp),       Util::RawDataRole);
    setData(TrackModel::MinHR,         issetu8(minHR),        Util::RawDataRole);
    setData(TrackModel::AvgHR,         issetu8(avgHR),        Util::RawDataRole);
    setData(TrackModel::MaxHR,         issetu8(maxHR),        Util::RawDataRole);
    setData(TrackModel::MinLon,        issetf(minLon),        Util::RawDataRole);
    setData(TrackModel::MinLat,        issetf(minLat),        Util::RawDataRole);
    setData(TrackModel::MaxLon,        issetf(maxLon),        Util::RawDataRole);
    setData(TrackModel::MaxLat,        issetf(maxLat),        Util::RawDataRole);

    // Update power after we've worked out the moving time above
    updatePowerData();
}

Marble::GeoDataLatLonBox TrackItem::bounds() const
{
    return Marble::GeoDataLatLonBox(data(TrackModel::MaxLat, Util::RawDataRole).toDouble(),
                                    data(TrackModel::MinLat, Util::RawDataRole).toDouble(),
                                    data(TrackModel::MaxLon, Util::RawDataRole).toDouble(),
                                    data(TrackModel::MinLon, Util::RawDataRole).toDouble(),
                                    Marble::GeoDataCoordinates::Degree);
}

QDataStream& TrackItem::save(QDataStream& stream, const TreeModel& /*model*/) const
{
    if (stream.status() != QDataStream::Ok)
        return stream;

    // Don't invoke TreeItem::save here, which would save everything.  We'll save a subset
    // of our data.  The rest is recalculated on load.
    if (const auto rawData = itemData.find(Util::RawDataRole); rawData != itemData.end())
        for (int mt = TrackModel::_First; mt < TrackModel::_Count; ++mt)
            if (TrackModel::mdSave(mt))
                stream << mt << (*rawData).at(mt);

    stream << guard << data(TrackModel::Icon, Util::IconNameRole);

    return stream << geoPoints;
}

QDataStream& TrackItem::load(QDataStream& stream, TreeModel& model)
{
    if (stream.status() != QDataStream::Ok)
        return stream;

    // Don't invoke TreeItem::load here: we have a custom save/load to handle a subset
    // of our data.  The rest is recalculated on load.
    itemData.clear();

    ModelType mt;
    auto& rawData = *itemData.insert(Util::RawDataRole, ItemData(TrackModel::_Count));
    while (true) {
        stream >> mt;
        if (mt < 0 || mt >= TrackModel::_Count)
            break;
        stream >> rawData[mt];
    }

    if (unsigned(mt) != guard) {
        stream.setStatus(QDataStream::ReadCorruptData);
        return stream;
    }

    // Load icon data
    auto& iconData = *itemData.insert(Util::IconNameRole, ItemData(TrackModel::Icon + 1));
    stream >> iconData[TrackModel::Icon];

    // Load geo data
    stream >> geoPoints;

    // Update everything
    createIcons();
    refresh(static_cast<TrackModel&>(model));

    return stream;
}

void TrackItem::selectPointsWithin(const Marble::GeoDataLatLonBox& region)
{
    if (!region.intersects(bounds()))
        return;

    if (geoPoints.selectPointsWithin(region))
        setAreaSelect(true);
}

void TrackItem::setPersonIdx(const QModelIndex& personIdx)
{
    geoPoints.setPowerData(firstPowerTag(), personIdx);
    updatePowerData();  // update our min/max/avg power
}

// Look for first tag that has power data, and return its index.
QModelIndex TrackItem::firstPowerTag() const
{
    const QVariant tagsVar = data(TrackModel::Tags, Util::RawDataRole);

    // Return first tag we find that has some power data attached to it.
    if (tagsVar.type() == QVariant::StringList)
        for (const auto& tag : *static_cast<const QStringList*>(tagsVar.constData()))
            if (const QModelIndex tagIdx = cfgData.tags.keyIdx(tag); tagIdx.isValid())
                if (cfgData.tags.data(TagModel::CdA, tagIdx, Util::RawDataRole).isValid() ||
                    cfgData.tags.data(TagModel::Weight, tagIdx, Util::RawDataRole).isValid() ||
                    cfgData.tags.data(TagModel::RR, tagIdx, Util::RawDataRole).isValid())
                    return tagIdx;

    return QModelIndex();
}

void TrackItem::shallowCopy(const TreeItem* src)
{
    TreeItem::shallowCopy(src);

    const TrackItem* trkSrc = static_cast<const TrackItem*>(src);

    geoPoints.clear();
    append(trkSrc->geoPoints);
    trackLines     = trkSrc->trackLines;
    m_isVisible    = trkSrc->m_isVisible;
    m_areaSelected = trkSrc->m_areaSelected;
}

uint qHash(const TrackItem& trk, uint /*seed*/)
{
    return qHash(trk.geoPoints); // just pass through, so we can compare hashes to PointModel;
}
