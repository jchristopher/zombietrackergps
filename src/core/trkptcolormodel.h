/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRKPTCOLORMODEL_H
#define TRKPTCOLORMODEL_H

#include <QColor>
#include <src/core/colorlistmodel.h>

class TrkPtColorModel final : public ColorListModel
{
public:
    TrkPtColorModel(QObject *parent = 0);

    QColor operator[](ModelType mt) const;

private:
    void addMissing() override;

    QVector<int> map; // since we skip some columns, map column ID to our row.
};

#endif // TRKPTCOLORMODEL_H
