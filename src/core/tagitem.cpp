/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <QFileInfo>

#include <src/core/cfgdata.h>
#include <src/util/roles.h>
#include "tagmodel.h"
#include "tagitem.h"

TagItem::TagItem(const CfgData& cfgData, TreeItem* parent) :
    TreeItem(parent),
    cfgData(cfgData),
    id(-1),
    unitsSpeed(Format::SpeedMPS, 2)
{
    init();
}

TagItem::TagItem(const CfgData& cfgData, const TreeItem::ItemData& data, TreeItem* parent) :
    TreeItem(parent),
    cfgData(cfgData),
    id(-1),
    unitsSpeed(Format::SpeedMPS, 2)
{
    init(data);
}

void TagItem::init(const TreeItem::ItemData& data)
{
    // We set both the icon and the icon name, so we can later restore the icon from its named file
    // rather than from the bitmap data.
    if (data.size() > TagModel::Icon && data[TagModel::Icon].isValid())
        setIcon(TagModel::Icon, data[TagModel::Icon].toString());

    for (int t = 0; t < data.size(); ++t)
        if (t != TagModel::Name && t != TagModel::Icon && data.at(t).isValid())
            setData(t, data[t], TagModel::mdDataRole(t));

    const bool hasName = (data.size() > TagModel::Name) && data[TagModel::Name].isValid();
    setData(TagModel::Name,  hasName ? data[TagModel::Name] : "New Tag",  Util::RawDataRole);
}

void TagItem::shallowCopy(const TreeItem* src)
{
    TreeItem::shallowCopy(src);
    id         = static_cast<const TagItem*>(src)->id;
    unitsSpeed = static_cast<const TagItem*>(src)->unitsSpeed;
}

QVariant TagItem::data(int column, int role) const
{
    if (parent() == nullptr)
        return TreeItem::data(column, role);

    switch (role) {
    case Qt::TextAlignmentRole:
        return QVariant(TagModel::mdAlignment(column));

    case Qt::DecorationRole:
        if (column == TagModel::Icon)
            return cfgData.svgColorizer(TreeItem::data(column, Util::IconNameRole).toString(),
                                        TreeItem::data(TagModel::Color, Qt::BackgroundRole),
                                        cfgData.colorizeTagIcons);
        break;

    case Qt::EditRole: {
        QVariant val = TreeItem::data(column, TagModel::mdDataRole(column));

        if (column == TagModel::UnitSpeed)
            return val;

        // Basic defaults if editing data that hasn't been set.
        if (!val.isValid()) {
            switch (column) {
            case TagModel::CdA:        val = 0.5;     break;
            case TagModel::Weight:     val = 10.0_kg; break;
            case TagModel::RR:         val = 0.003;   break;
            case TagModel::Efficiency: val = 0.9;     break;
            case TagModel::BioPct:     val = 1.0;     break;
            case TagModel::Medium:     val = "Air";   break;
            default:                   break;
            }
        }

        return TagModel::mdUnits(column, cfgData).to(val);
    }

    case Qt::DisplayRole:
    case Util::CopyRole: {
        if (role == Util::CopyRole) {
            const QVariant value = TreeItem::data(column, TagModel::mdDataRole(column));
            switch (column) {
            case TagModel::Icon:   return value.value<QIcon>().name();
            case TagModel::Color:  return value.value<QColor>().name();
            default:               break;
            }
        }

        const QVariant value = TreeItem::data(column, Util::RawDataRole);
        if (value.isValid())
            return TagModel::mdUnits(column, cfgData)(value);
        break;
    }
    }

    return TreeItem::data(column, role);
}

bool TagItem::setData(int column, const QVariant &value, int role, bool &changed)
{
    if (role == Qt::EditRole) {
        role = TagModel::mdDataRole(column);
        QVariant unitVal = TagModel::mdUnits(column, cfgData).from(value);

        // Reset some columns if set to 0
        switch (column) {
        case TagModel::CdA:        [[fallthrough]];
        case TagModel::Weight:     [[fallthrough]];
        case TagModel::RR:         [[fallthrough]];
        case TagModel::Efficiency: [[fallthrough]];
        case TagModel::BioPct:
            if (value.toFloat() < 0.0)
                unitVal.clear();
            break;
        case TagModel::Medium:
            if (value.toString() == "None")
                unitVal.clear();
            break;
        case TagModel::UnitSpeed:
            setUnits(column, unitVal.toString());
            break;
        default:
            break;
        }

        return TreeItem::setData(column, unitVal, role, changed);
    }

    return TreeItem::setData(column, value, role, changed);
}

bool TagItem::setIcon(int column, const QString &iconFile)
{
    // Don't set the icon itself.  We'll get that from a colorization cache.
    return setData(column, iconFile, Util::IconNameRole);
}

bool TagItem::isCategory() const
{
    return data(TagModel::Name, Util::PrivateDataRole).value<bool>();
}

void TagItem::setCategory(bool category)
{
    setData(TagModel::Name, category, Util::PrivateDataRole);
}

int TagItem::columnCount() const
{
    return TagModel::_Count;
}

TagItem *TagItem::factory(const TreeItem::ItemData& data, TreeItem* parent)
{
    assert(dynamic_cast<TagItem*>(parent) != nullptr);

    return new TagItem(cfgData, data, parent);
}

void TagItem::setUnits(ModelType mt, const QString& name)
{
    switch (mt) {
    case TagModel::UnitSpeed:
        // refresh unit
        if (name.isEmpty())
            unitsSpeed = Units(Format::_Invalid);
        else {
            unitsSpeed = cfgData.unitsSpeed;
            unitsSpeed.setFormat(name);
        }
        break;
    default:
        assert(0);
    }
}

const Units& TagItem::getUnits(ModelType mt) const
{
    static const Units unitsFloat(Format::Float);

    switch (mt) {
    case TagModel::UnitSpeed:  return unitsSpeed.isValid() ? unitsSpeed : cfgData.unitsSpeed;
    default:                   assert(0); return unitsFloat;
    }
}

void TagItem::load(QSettings& settings)
{
    TreeItem::load(settings);
    setCategory(childCount() > 0);  // force category on or off

    // refresh unit
    setUnits(TagModel::UnitSpeed, data(TagModel::UnitSpeed, Util::RawDataRole).toString());
}

QDataStream& TagItem::load(QDataStream& stream, TreeModel& model)
{
    assert(0);
    return TreeItem::load(stream, model);
}
