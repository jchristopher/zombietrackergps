/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QItemSelectionModel>
#include <QItemSelection>
#include <QAbstractProxyModel>

#include <src/util/roles.h>
#include <src/util/util.h>
#include "selectionsummary.h"
#include "trackmodel.h"
#include "pointmodel.h"

SelectionSummary::SelectionSummary()
{
    clear(0, 0);
}

void SelectionSummary::clear(int totalItems, int visibleItems)
{
    total    = totalItems;
    visible  = visibleItems;
    selected = 0;
    length   = 0.0;
    ascent   = 0.0;
    descent  = 0.0;
    duration = 0;
}

template <>
void SelectionSummary::accumulate(const TrackModel& model, const QModelIndex& idx, int sign)
{
    length   += model.data(TrackModel::Length,    idx, Util::RawDataRole).value<qreal>()  * sign;
    ascent   += model.data(TrackModel::Ascent,    idx, Util::RawDataRole).value<qreal>()  * sign;
    descent  += model.data(TrackModel::Descent,   idx, Util::RawDataRole).value<qreal>()  * sign;
    duration += model.data(TrackModel::TotalTime, idx, Util::RawDataRole).value<qint64>() * sign;
}

template <>
void SelectionSummary::accumulate(const PointModel& model, const QModelIndex& idx, int sign)
{
    length    += model.siblingData(PointModel::Length, idx, Util::RawDataRole).value<qreal>() * sign;
    const qreal vert = model.siblingData(PointModel::Vert, idx, Util::RawDataRole).value<qreal>();
    duration += model.siblingData(PointModel::Duration, idx, Util::RawDataRole).value<qint64>() * sign;

    if (vert > 0.0)
        ascent  += vert * sign;
    else
        descent -= vert * sign;
}

template <class MODEL>
void SelectionSummary::trackSelected(const MODEL& model, const QItemSelectionModel* selectionModel,
                                     const QItemSelection& selected, const QItemSelection& deselected)
{
    // If no selection, clear everything out.
    if (selectionModel != nullptr && !selectionModel->hasSelection()) {
        clear(total, visible);
        return;
    }

    // Otherwise, remove the old, add the new.
    const auto modSelection = [this, &selectionModel, &model](const QItemSelection& selection, int sign) {
        for (const auto& sr : selection) {
            for (int r = sr.top(); r <= sr.bottom(); ++r) {
                const QModelIndex idx = Util::MapDown(selectionModel->model()->index(r, 0, sr.parent()));
                this->selected += sign;
                accumulate(model, idx, sign);
            }
        }
    };

    if (selectionModel != nullptr) {
        modSelection(selected, 1);
        modSelection(deselected, -1);
    }
}

template <>
void SelectionSummary::update(const TrackModel& model, const QAbstractProxyModel& proxy,
                              const QItemSelectionModel* selectionModel,
                              const QItemSelection& selected, const QItemSelection& deselected)
{
    total   = model.rowCount();
    visible = proxy.rowCount();

    trackSelected(model, selectionModel, selected, deselected);
}

template <>
void SelectionSummary::update(const PointModel& model, const QAbstractProxyModel& proxy,
                              const QItemSelectionModel* selectionModel,
                              const QItemSelection& selected, const QItemSelection& deselected)
{
    total   = model.rowCount();
    visible = 0;

    for (int row = 0; row < proxy.rowCount(); ++row) {
        const QModelIndex rowIdx = proxy.index(row, 0);
        if (const int rowChildren = proxy.rowCount(rowIdx); rowChildren > 0)
            visible += rowChildren;
        else
            visible++;
    }

    trackSelected(model, selectionModel, selected, deselected);
}
