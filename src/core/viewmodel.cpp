/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <src/util/roles.h>
#include <src/util/units.h>
#include "cfgdata.h"
#include "viewmodel.h"
#include "viewitem.h"

ViewModel::ViewModel(const CfgData& cfgData, QObject *parent) :
    TreeModel(new ViewItem(cfgData), parent),
    cfgData(cfgData)
{  
    setHorizontalHeaderLabels(headersList<ViewModel>());
}

void ViewModel::setRow(const QString& name, qreal centerLat, qreal centerLon, qreal heading, int zoom,
                       const QModelIndex& idx)
{
    // ViewModel namespace not needed here, but use it to improve greppability.
    setData(ViewModel::Name,      idx, name,      Util::RawDataRole);
    setData(ViewModel::CenterLat, idx, centerLat, Util::RawDataRole);
    setData(ViewModel::CenterLon, idx, centerLon, Util::RawDataRole);
    setData(ViewModel::Heading,   idx, heading,   Util::RawDataRole);
    setData(ViewModel::Zoom,      idx, zoom,      Util::RawDataRole);
}

QVariant ViewModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (QVariant val = ModelMetaData::headerData<ViewModel>(section, orientation, role); val.isValid())
        return val;

    return TreeModel::headerData(section, orientation, role);
}

const Units& ViewModel::units(const QModelIndex& idx) const
{
    return mdUnits(idx.column(), cfgData);
}

Qt::ItemFlags ViewModel::flags(const QModelIndex& idx) const
{
    return TreeModel::flags(idx) | ModelMetaData::flags<ViewModel>(idx) |
            Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
}

void ViewModel::load(QSettings& settings)
{
    TreeModel::load(settings);
    setHorizontalHeaderLabels(headersList<ViewModel>());
}

QString ViewModel::mdName(ModelType d)
{
    switch (d) {
    case ViewModel::Name:         return QObject::tr("Name");
    case ViewModel::CenterLat:    return QObject::tr("Lat");
    case ViewModel::CenterLon:    return QObject::tr("Lon");
    case ViewModel::Heading:      return QObject::tr("Heading");
    case ViewModel::Zoom:         return QObject::tr("Zoom");

    case ViewModel::_Count:       break;
    }

    assert(0 && "Unknown TrackData value");
    return "";
}

bool ViewModel::mdIsEditable(ModelType)
{
    return true;
}

// tooltip for container column header
QString ViewModel::mdTooltip(ModelType mt)
{
#define cd_header(i)       "<p><b><u>" i ":</u></b><p>"
#define cd_info cd_header("Column Information")

    static const QByteArray can_edit =
            cd_header("Editable")
            "You can edit data in this column by selecting the Edit context menu.";

    switch (mt) {
    case ViewModel::Name:
        return QObject::tr(cd_info
                           "Descriptive name.") +
                QObject::tr(can_edit);

    case ViewModel::CenterLat:
        return QObject::tr(cd_info
                           "View center latitude") +
                QObject::tr(can_edit);

    case ViewModel::CenterLon:
        return QObject::tr(cd_info
                           "View center longitude.") +
                QObject::tr(can_edit);

    case ViewModel::Heading:
        return QObject::tr(cd_info
                           "View heading in degrees.");

    case ViewModel::Zoom:
        return QObject::tr(cd_info
                           "View zoom level.");

    case ViewModel::_Count:
        break;
    }

    assert(0 && "Unknown TrackData value");
    return nullptr;
}

// tooltip for container column header
QString ViewModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);  // just pass through the tooltip
}

Qt::Alignment ViewModel::mdAlignment(ModelType mt)
{
    switch (mt) {
    case ViewModel::Name:         return Qt::AlignLeft    | Qt::AlignVCenter;
    default:                      return Qt::AlignRight   | Qt::AlignVCenter;
    }
}

const Units& ViewModel::mdUnits(ModelType mt, const CfgData& cfgData)
{
    static const Units rawString(Format::String);

    switch (mt) {
    case ViewModel::CenterLat: return cfgData.unitsLat;
    case ViewModel::CenterLon: return cfgData.unitsLon;
    default:                   return rawString;
    }
}

