/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKCMPPANE_H
#define TRACKCMPPANE_H

#include <QScopedPointer>
#include <QPixmap>
#include <QTimer>
#include "chartbase.h"

#include <src/ui/widgets/chartviewzoom.h>
#include <src/core/query.h>

#include "src/core/selectionsummary.h"

namespace Ui {
class TrackCmpPane;
}

class MainWindow;
class TrackModel;

namespace QtCharts {
class QBarSet;
class QHorizontalBarSeries;
class QBarCategoryAxis;
class QValueAxis;
}

class TrackCmpPane final : public ChartBase
{
    Q_OBJECT

public:
    explicit TrackCmpPane(MainWindow& mainWindow, QWidget *parent = 0);
    ~TrackCmpPane();

    // *** begin Pane API
    void showAll() override;
    void newConfig() override;
    void focusIn() override;
    // *** end Pane API

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

    void setBarWidth(int val);
    void setAxesShown(bool shown) const override;
    void setLegendShown(bool /*shown*/) const override { }
    bool axesShown() const override;
    bool legendShown() const override { return false; }
    void setBarValuesShown(bool show);

private slots:
    void setPlotColumn(int index);
    void on_TrackCmpPane_toggled(bool checked) { paneToggled(checked); }
    void processRowsInserted(const QModelIndex& parent, int first, int last);
    void processRowsRemoved(const QModelIndex& parent, int first, int last);
    void processDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>&);
    void currentTrackChanged(const QModelIndex&);
    void hovered(bool status, int index, QtCharts::QBarSet*);
    void clicked(int index, QtCharts::QBarSet*);
    void queryTextChanged(const QString&);       // signal from this widget
    void externQueryTextChanged(const QString&); // signal from other widgets
    void on_lockToQuery_toggled(bool checked);
    void on_sortDirection_toggled(bool checked);
    void on_action_Set_Bar_Width_triggered();
    void on_action_Bar_Values_triggered(bool checked);
    void on_action_Page_Up_triggered();
    void on_action_Page_Down_triggered();
    void on_action_Show_Axes_triggered(bool checked);

private:
    void setupActionIcons();
    void setupTimers();
    void setupDataSelector();
    void setupChart();
    void setupMenus();
    void setupSignals();
    void setupCompleter();
    void setupFilterStatusIcons();
    void updateChart() override;
    void refreshChart(int delayMs = 100); // 0 to avoid deferring
    void showFilterStatusIcon() const;
    void resizeEvent(QResizeEvent*) override;
    void showContextMenu(const QPoint& pos);
    void setSortDirection(bool state);
    void setLockToQuery(bool state);
    void setQueryString(const QString&);
    void highlightCurrent();
    void reshowTooltip();
    inline void setBarColorFromModel(const TrackModel& model, QtCharts::QBarSet* barSet, int modelRow);

    Ui::TrackCmpPane*                 ui;
    QStandardItemModel                graphDataModel; // for show-column combo box
    QVector<int>                      columnMap;      // map combobox position to column
    QTimer                            updateTimer;    // avoid update spam
    QTimer                            currentTimer;   // because setting bar color repaints whole graph!
    QTimer                            tooltipTimer;   // workaround for Qt Bug: tooltip disappears over GL

    QtCharts::QHorizontalBarSeries*   barSeries;      // we only need one.
    QtCharts::QBarCategoryAxis*       axisY;          // ...
    QtCharts::QValueAxis*             axisX;          // ...

    Query::Context                    m_queryCtx;        // for query language
    QScopedPointer<const Query::Base> m_queryRoot;       // root of node filter tree

    QPixmap                           filterEmptyIcon;   // emblem for empty filter
    QPixmap                           filterValidIcon;   // emblem for valid filter
    QPixmap                           filterInvalidIcon; // emblem for invalid filter

    QVector<int>                      chartToRow;        // chart index to model row map
    QVector<int>                      rowToChart;        // model row to chart index map
    int                               currentRow;        // for current item highlighting, in model space
    int                               prevRow;           // ...
    ModelType                         plotColumn;        // which column we should plot
    int                               barWidth;          // bar width

    QString                           tooltipText;       // last tooltip text, for auto-reshow workaround
    QPoint                            tooltipPos;        // last tooltip position

    SelectionSummary                  selectionSummary;  // for status bar info

    static const constexpr char* rowProperty    = "zt-row";
};

#endif // TRACKCMPPANE_H
