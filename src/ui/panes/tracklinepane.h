/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKLINECHART_H
#define TRACKLINECHART_H

#include <QVector>
#include <QTimer>
#include "chartbase.h"
#include <src/ui/widgets/chartviewzoom.h>

namespace Ui {
class TrackLinePane;
}

class MainWindow;
class PointModel;

namespace QtCharts {
class QValueAxis;
class QLineSeries;
class QAbstractAxis;
}

class TrackLinePane final : public ChartBase
{
    Q_OBJECT

public:
    explicit TrackLinePane(MainWindow& mainWindow, QWidget *parent = 0);
    ~TrackLinePane();

//  void selectAll() override;
//  void selectNone() override;
    void showAll() override;
    void newConfig() override;

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

private slots:
    void processRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void processModelAboutToBeReset();
    void currentTrackChanged(const QModelIndex&);
    void currentPointChanged(const QModelIndex&);
    void toggleColumn(QStandardItem*);
    void updateAxes();
    void showContextMenu(const QPoint& pos);
    void pan(QMouseEvent* event, int relX, int relY);
    void mouseMove(QMouseEvent* event);
    void endPan();
    void on_TrackLinePane_toggled(bool checked) { paneToggled(checked); }
    void on_action_Show_Axes_toggled(bool);
    void on_action_Show_Legend_toggled(bool);
    void on_action_Reset_Zoom_triggered();
    void on_action_Zoom_In_X_triggered();
    void on_action_Zoom_Out_X_triggered();
    void on_action_Pan_Left_triggered();
    void on_action_Pan_Right_triggered();
    void on_action_Page_Left_triggered();
    void on_action_Page_Right_triggered();

private:
    PointModel* currentPoint();
    const PointModel* currentPoint() const;
    void pan(int relX, int relY);

    void enableActions(); // enable proper set of actions for current state
    void setupActionIcons();
    void setupDataSelector();
    void setupChart();
    void setupMenus();
    void setupSignals();
    void setupTimers();
    void updateChart() override;
    void clearChart();
    void refreshChart();
    void updateYAxis();
    void initYAxis(QtCharts::QLineSeries*, const Units&, qreal yMin, qreal yMax);
    void initXAxis(QtCharts::QLineSeries*);
    void initLabels();
    void initMarker();
    void updateXAxis();
    void wheelEvent(QWheelEvent*) override;
    void resizeEvent(QResizeEvent*) override;
    void setZoom(float);
    void resetPanZoom();
    void zoomIn(float);
    void zoomOut(float);
    void setAxesShown(bool shown) const override;
    void setLegendShown(bool shown) const override;
    bool axesShown() const override;
    bool legendShown() const override;
    void clearMarkerSeries();
    bool isChecked(int col) const;
    QtCharts::QLineSeries* newSeries(int col);

    void drawActiveMarker(qreal xPos);
    void drawMarkerText(qreal xPos);

    inline qreal xViewExtentInUnits() const;
    inline qreal xAxisStart() const;
    inline qreal trackTotalDistanceUnits() const;

    Ui::TrackLinePane*     ui;
    QPersistentModelIndex  currentPointIdx;  // current selection
    QStandardItemModel     graphDataModel;   // for show-column combo box
    QVector<int>           columnMap;        // map PointModel column to combobox position

    const float            zoomStepWheel;    // zoom step multiplier for wheel events
    const float            zoomStepMenu;     // zoom step multiplier for kb/menu events
    float                  zoomLevel;        // current zoom
    const float            initZoom;         // init zoom level
    const float            minZoom;          // min zoom level
    const float            maxZoom;          // max zoom level
    float                  xBegin;           // X axis start point, [0..1]

    QTimer                 axisUpdateTimer;  // avoid axis update spam

    QtCharts::QLineSeries* activeMarker;     // active marker using a line series
    QVector<QtCharts::QLineSeries*> valueLabels; // for value labels
    QVector<ModelType>     valueColumn;      // columns matching those labels.
};

#endif // TRACKLINECHART_H
