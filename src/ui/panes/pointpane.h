/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POINTPANE_H
#define POINTPANE_H

#include <QPersistentModelIndex>

#include <src/ui/widgets/doublespindelegate.h>
#include <src/ui/widgets/spindelegate.h>
#include <src/ui/widgets/datetimedelegate.h>

#include "src/core/selectionsummary.h"
#include "datacolumnpane.h"

namespace Ui {
class PointPane;
}

class MainWindow;
class QModelIndex;
class PointModel;

class PointPane final : public DataColumnPane
{
    Q_OBJECT

public:
    explicit PointPane(MainWindow& mainWindow, QWidget *parent = 0);
    ~PointPane();

    void newConfig() override;

    void zoomToSelection() override;
    void deleteSelection() override;

private slots:
    void on_PointPane_toggled(bool checked) { paneToggled(checked); }
    void doubleClicked(const QModelIndex&);
    void showContextMenu(const QPoint&);
    void currentTrackChanged(const QModelIndex &current);
    void filterTextChanged(const QString&) override;
    void processSelectionChanged(const QItemSelection&, const QItemSelection&);
    void processRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void processModelAboutToBeReset();
    void processSelectedPointsChanged();
    void on_action_Merge_Segments_triggered();
    void on_action_Split_Segments_triggered();

private:
    void focusIn() override;

    PointModel* currentPoints();
    const PointModel* currentPoints() const;

    void setupActionIcons();
    void setupContextMenus();
    void setupSignals() override;
    void setupTimers();
    void setupDelegates();
    void gotoSelection(const QModelIndexList&) const;
    const ColSet& defHideColumns() const override;

    Ui::PointPane*         ui;
    QPersistentModelIndex  currentPointsIdx;

    DateTimeDelegate       dateTimeDelegate;
    DoubleSpinDelegate     latDelegate;
    DoubleSpinDelegate     lonDelegate;
    DoubleSpinDelegate     eleDelegate;
    DoubleSpinDelegate     tempDelegate;
    DoubleSpinDelegate     depthDelegate;
    SpinDelegate           hrDelegate;
    SpinDelegate           cadDelegate;

    static PointModel*     empty;

    // These are to avoid processing the entire selection list each time it changes.
    SelectionSummary    selectionSummary;
};

#endif // POINTPANE_H
