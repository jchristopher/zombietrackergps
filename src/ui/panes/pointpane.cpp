/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "pointpane.h"
#include "ui_pointpane.h"

#include <src/util/util.h>
#include <src/util/icons.h>
#include "src/ui/windows/mainwindow.h"
#include "src/core/pointmodel.h"
#include "src/core/viewmodel.h"

PointModel* PointPane::empty = nullptr;

PointPane::PointPane(MainWindow& mainWindow, QWidget *parent) :
    DataColumnPane(mainWindow, PaneClass::Points, parent),
    ui(new Ui::PointPane),
    dateTimeDelegate(this),
    latDelegate(this, -90.0, 90.0, 12, 1),
    lonDelegate(this, 0, 259.99999999999, 12, 1),
    eleDelegate(this, -10000, 360000, 2, 1),
    tempDelegate(this, -100, 400, 2, 1),
    depthDelegate(this, -360000, 360000, 2, 1),
    hrDelegate(this, 0, 255),
    cadDelegate(this, 0, 255)
{
    ui->setupUi(this);

    if (empty == nullptr)
        empty = new PointModel(nullptr, mainWindow.constCfgData());

    setupView(ui->pointView, empty);
    setWidgets<PointModel>(ui->filterPoints, nullptr, ui->showColumns, ui->filterCtrl, ui->filterIsValid,
                         defHideColumns());

    // Don't display top (segment) level in flattened view.
    setFlattenPredicate([](const QAbstractItemModel*, const QModelIndex& idx) {
        return idx.parent().isValid();
    });

    setupActionIcons();
    setupContextMenus();
    setupSignals();
    setupTimers();
    setupDelegates();
}

PointPane::~PointPane()
{
    delete ui;
}

void PointPane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Merge_Segments, "merge");
    Icons::defaultIcon(ui->action_Split_Segments, "split");
}

void PointPane::setupContextMenus()
{
    paneMenu.addAction(mainWindow().getMainAction(MainAction::ZoomToSelection));
    paneMenu.addAction(mainWindow().getMainAction(MainAction::DeleteSelection));
    paneMenu.addAction(ui->action_Split_Segments);
    paneMenu.addAction(ui->action_Merge_Segments);
    paneMenu.addSeparator();

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &PointPane::customContextMenuRequested, this, &PointPane::showContextMenu);

    connect(&mainWindow(), &MainWindow::selectedPointsChanged, this, &PointPane::processSelectedPointsChanged);
}

void PointPane::setupSignals()
{
    DataColumnPane::setupSignals();

    // Notify ourselves and the filter about current container changes
    connect(&mainWindow(), &MainWindow::currentTrackChanged, this, &PointPane::currentTrackChanged);

    // Selection changes
    connect(selectionModel(), &QItemSelectionModel::selectionChanged, this, &PointPane::processSelectionChanged);

    // Row deletion from model
    connect(&mainWindow().trackModel(), &TrackModel::rowsAboutToBeRemoved, this, &PointPane::processRowsAboutToBeRemoved);
    // Model reset
    connect(&mainWindow().trackModel(), &TrackModel::modelAboutToBeReset, this, &PointPane::processModelAboutToBeReset);

    // Double click to zoom-to
    connect(ui->pointView, &QTreeView::doubleClicked, this, &PointPane::doubleClicked);
}

void PointPane::setupTimers()
{
}

void PointPane::setupDelegates()
{
    QTreeView& view = *ui->pointView;

    dateTimeDelegate.setSelector(selectionModel());
    latDelegate.setSelector(selectionModel());
    lonDelegate.setSelector(selectionModel());
    eleDelegate.setSelector(selectionModel());
    tempDelegate.setSelector(selectionModel());
    depthDelegate.setSelector(selectionModel());
    hrDelegate.setSelector(selectionModel());
    cadDelegate.setSelector(selectionModel());

    view.setItemDelegateForColumn(PointModel::Time,  &dateTimeDelegate);
    view.setItemDelegateForColumn(PointModel::Lat,   &latDelegate);
    view.setItemDelegateForColumn(PointModel::Lon,   &lonDelegate);
    view.setItemDelegateForColumn(PointModel::Ele,   &eleDelegate);
    view.setItemDelegateForColumn(PointModel::Temp,  &tempDelegate);
    view.setItemDelegateForColumn(PointModel::Depth, &depthDelegate);
    view.setItemDelegateForColumn(PointModel::Hr,    &hrDelegate);
    view.setItemDelegateForColumn(PointModel::Cad,   &cadDelegate);
}

const PointPane::ColSet& PointPane::defHideColumns() const
{
    static const ColSet hide = {
        PointModel::Time,   // hide these columns by default
        PointModel::Lat,
        PointModel::Lon,
        PointModel::Length,
        PointModel::Vert,
        PointModel::Duration,
        PointModel::Temp,
        PointModel::Depth,
        PointModel::Accel,
        PointModel::Hr,
        PointModel::Cad,
        PointModel::Power,
        PointModel::Course,
        PointModel::Bearing,
    };

    return hide;
}

void PointPane::newConfig()
{
    ui->pointView->expandAll();

    eleDelegate.setSuffix(QString(" ") + cfgData().unitsElevation.suffix(1000.0));
    dateTimeDelegate.setFormat(cfgData().unitsPointDate.dateFormat());
}

void PointPane::filterTextChanged(const QString& regex)
{
    DataColumnPane::filterTextChanged(regex);

    if (PointModel* points = currentPoints(); points != nullptr) {
        selectionSummary.refresh(*points, topFilter(), selectionModel());
        mainWindow().updateStatus(selectionSummary);
    }
}

// For performance reasons (selections are needed within the TrackMap render loop), we cache the results of
// the selection in the model.  This approach also lets us avoid rebuilding the entire selection set upon
// each change, and instead do it incrementally.
void PointPane::processSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
    PointModel* points = currentPoints();
    if (points == nullptr)
        return;

    const auto modSelection = [this, points](const QItemSelection& selection, bool newState) {
        // map through our filter stack
        for (const auto& sr : selection)
            for (int r = sr.top(); r <= sr.bottom(); ++r)
                points->select(*this, Util::MapDown(sr.model()->index(r, 0, sr.parent())), newState);
    };

    modSelection(selected,   true);
    modSelection(deselected, false);

    selectionSummary.update(*points, topFilter(), selectionModel(), selected, deselected);
    mainWindow().updateStatus(selectionSummary);
}

void PointPane::processRowsAboutToBeRemoved(const QModelIndex& parent, int first, int last)
{
    if (!currentPointsIdx.isValid())
        return;

    if (currentPointsIdx.parent() == parent && currentPointsIdx.row() >= first && currentPointsIdx.row() <= last) {
        currentPointsIdx = QModelIndex();
        setSourceModel(empty);
    }
}

void PointPane::processModelAboutToBeReset()
{
    currentPointsIdx = QModelIndex();
    setSourceModel(empty);
}

void PointPane::processSelectedPointsChanged()
{
    const bool hasItems     = PointPane::hasItems();
    const bool hasSelection = PointPane::hasSelection();

    ui->action_Merge_Segments->setEnabled(hasItems && currentPoints() != nullptr &&
                                          currentPoints()->size() > 1);

    ui->action_Split_Segments->setEnabled(hasItems && hasSelection);
}

void PointPane::currentTrackChanged(const QModelIndex& current)
{
    // Return if no row change.
    if (currentPointsIdx.model() == current.model() && currentPointsIdx.row() == current.row())
        return;

    if (PointModel* oldPoints = currentPoints(); oldPoints != nullptr)
        oldPoints->unselectAll(*this);

    currentPointsIdx = current.sibling(current.row(), 0);

    PointModel* newPoints = currentPoints();
    setSourceModel(newPoints != nullptr ? newPoints : empty);

    expandAll();
}

void PointPane::showContextMenu(const QPoint& pos)
{
    paneMenu.exec(mapToGlobal(pos));
}

void PointPane::focusIn()
{
    DataColumnPane::focusIn();

    if (const PointModel* points = currentPoints(); points != nullptr) {
        selectionSummary.refresh(*points, topFilter(), selectionModel());
        mainWindow().updateStatus(selectionSummary);
    }
}

PointModel* PointPane::currentPoints()
{
    if (!currentPointsIdx.isValid())
        return nullptr;

    return mainWindow().trackModel().geoPoints(currentPointsIdx);
}

const PointModel* PointPane::currentPoints() const
{
    return const_cast<const PointModel*>(const_cast<PointPane*>(this)->currentPoints());
}

void PointPane::gotoSelection(const QModelIndexList& selection) const
{
    MapPane* mapPane = mainWindow().findPane<MapPane>();

    if (ui == nullptr || ui->pointView == nullptr || mapPane == nullptr || selection.isEmpty())
        return;

    if (const PointModel* points = currentPoints(); points != nullptr)
        mapPane->zoomTo(points->boundsBox(selection));
}

void PointPane::doubleClicked(const QModelIndex& idx)
{
    // If it's an editable column edit, rather than view the selection
    if (TrackModel::mdIsEditable(idx.column()))
        return;

    gotoSelection(getSelections());
}

void PointPane::zoomToSelection()
{
    gotoSelection(getSelections());
}

void PointPane::deleteSelection()
{
    const QModelIndexList rows = selectionModel()->selectedRows();

    int segments = 0, points = 0;
    for (const auto& idx : rows) {
        if (Util::MapDown(idx).parent().isValid())
            ++points;
        else
           ++segments;
    }

    mainWindow().trackModel().eraseTrackSelection(currentPointsIdx, selectionModel(), &topFilter());
    selectionModel()->clearSelection();

    mainWindow().statusMessage(UiType::Success, tr("Deleted ") +
                               (segments > 0 ? QString::number(segments) + tr(" segments") : "") +
                               ((segments > 0 && points > 0) ? tr(" and ") : "") +
                               (points > 0 ? QString::number(points) + tr(" points") : "") + ".");

    // TODO: set current item to something that makes sense
}

void PointPane::on_action_Merge_Segments_triggered()
{
    mainWindow().trackModel().mergeTrackSegments(currentPointsIdx, selectionModel(), &topFilter());
    selectionModel()->clearSelection();
    // TODO: set current item to something that makes sense
}

void PointPane::on_action_Split_Segments_triggered()
{
    mainWindow().trackModel().splitTrackSegments(currentPointsIdx, selectionModel(), &topFilter());
    selectionModel()->clearSelection();
}
