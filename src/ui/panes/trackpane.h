/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKPANE_H
#define TRACKPANE_H

#include <QModelIndexList>
#include <QTimer>

#include "src/core/selectionsummary.h"
#include <src/ui/widgets/colordelegate.h>
#include <src/ui/widgets/texteditordelegate.h>
#include "src/ui/widgets/tagselectordelegate.h"
#include "datacolumnpane.h"

namespace Ui {
class TrackPane;
}

class MainWindow;

class TrackPane final : public DataColumnPane
{
    Q_OBJECT

public:
    explicit TrackPane(MainWindow& mainWindow, QWidget *parent = 0);
    ~TrackPane();

    void zoomToSelection() override;
    void deleteSelection() override;
    void gotoSelection(const QModelIndexList&) const;
    void addFilterInteractive();

private slots:
    void on_TrackPane_toggled(bool checked) { paneToggled(checked); }
    void doubleClicked(const QModelIndex&);
    void showContextMenu(const QPoint&);
    void on_action_Reset_Track_Color_triggered();
    void on_action_Reset_Track_Note_triggered();
    void on_action_Set_Icon_triggered();
    void on_action_Edit_triggered();
    void filterTextChanged(const QString&) override;
    void processSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected);
    void processSelectedTracksChanged();
    void updateVisibility();
    void on_action_Change_Tags_triggered();
    void on_action_Select_Duplicates_triggered();
    void on_action_Select_All_Duplicates_triggered();
    void on_action_Create_Filter_from_Query_triggered();
    void on_action_Unset_Icon_triggered();
    void on_action_Duplicate_Tracks_triggered();

private:
    void setupActionIcons();
    void setupContextMenus();
    void setupDelegates();
    void setupTimers();
    void setupSignals() override;

    void focusIn() override;
    void newConfig() override;
    void selectDuplicates(bool all);
    void duplicateSelection();
    void resizeToFit(int defer = -1) override;

    const ColSet& defHideColumns() const override;

    Ui::TrackPane*      ui;

    ColorDelegate       colorDelegate;
    TextEditorDelegate  noteDelegate;
    TagSelectorDelegate tagDelegate;
    QTimer              mapUpdateTimer;
    QModelIndex         menuIdx;

    // These are to avoid processing the entire selection list each time it changes.
    SelectionSummary    selectionSummary;
};

#endif // TRACKPANE_H
