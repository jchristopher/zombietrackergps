/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKDETAILPANE_H
#define TRACKDETAILPANE_H

#include <QHeaderView>
#include <QVector>
#include <QMenu>
#include <QTimer>
#include <map>

#include <src/ui/widgets/colordelegate.h>
#include <src/ui/widgets/texteditordelegate.h>
#include <src/ui/widgets/detaildelegate.h>
#include "src/ui/widgets/tagselectordelegate.h"
#include "src/ui/filters/subtreefilter.h"
#include "src/ui/filters/trackdetailfilter.h"
#include "src/ui/filters/flattenfilter.h"
#include "pane.h"

namespace Ui {
class TrackDetailPane;
}

class MainWindow;
class QModelIndex;
class QItemSelectionModel;

class TrackDetailPane final : public Pane
{
    Q_OBJECT

public:
    explicit TrackDetailPane(MainWindow& mainWindow, QWidget* parent = nullptr);
    ~TrackDetailPane();

    bool hasSelection() const override;
    bool isSelected(const QModelIndex& idx) const override;
    QModelIndexList getSelections() const override;
    bool viewIsTree() const override;

protected:
    void showAll() override;
    void expandAll() override;
    void collapseAll() override;
    void selectAll() override;
    void selectNone() override;
    void resizeToFit(int defer = -1) override;
    void copySelected() const override;
    void viewAsTree(bool) override;
    void newConfig() override;

private slots:
    void on_TrackDetailPane_toggled(bool checked) { paneToggled(checked); }
    void showActionContextMenu(const QPoint&);
    void on_lockToCurrentButton_toggled(bool checked);
    void reexpandTree();
    void currentTrackChanged(const QModelIndex &current);
    void filterTextChanged(const QString&);

private:
    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

    void setupDetailsView();
    void setupRowMap();
    void setupSignals();
    void setupDelegates();
    void setupContextMenus();               // build context menus
    void setupTimers();
    void resizeDeferredHook();
    QItemSelectionModel* selectionModel() const;

    Ui::TrackDetailPane*  ui;

    QMenu                 actionMenu;        // for container actions
    QHeaderView           detailsHeader;

    SubTreeFilter         subtreeFilter;
    FlattenFilter         flattenFilter;
    TrackDetailFilter     detailsFilter;

    DetailDelegate<ColorDelegate>         colorDelegate;
    DetailDelegate<TextEditorDelegate>    noteDelegate;
    DetailDelegate<TagSelectorDelegate>   tagDelegate;

    QTimer                resizeTimer;
    QTimer                initialExpandTimer;

    std::map<QString, bool> expandState;  // see C++ comment for reexpandTree
};

#endif // TRACKDETAILPANE_H
