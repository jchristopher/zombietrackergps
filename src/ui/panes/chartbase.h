/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHARTBASE_H
#define CHARTBASE_H

#include <QtMath>

#include "pane.h"

#include <src/ui/widgets/chartviewzoom.h>

class MainWindow;

namespace QtCharts {
class QChart;
}

class ChartBase : public Pane
{
public:
    explicit ChartBase(MainWindow& mainWindow, PaneClass pc, QWidget *parent = 0);
    ~ChartBase();

protected slots:
    virtual void updateChart() = 0;

protected:
    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

    void setupChart();
    virtual bool axesShown() const = 0;
    virtual bool legendShown() const = 0;
    virtual void setAxesShown(bool shown) const;
    virtual void setLegendShown(bool shown) const;

    inline static void  looseNiceNumbers(qreal &min, qreal &max, int &ticksCount);
    inline static qreal niceNumber(qreal x, bool ceiling);

    QtCharts::QChart*      chart;              // chart to display
    ChartViewZoom          chartView;          // main chart view area

    const QColor           labelHighlight;
    const QColor           labelNormal;
    const QColor           labelSelected;
    const QPen             majorGridPen;
    const QPen             minorGridPen;
};

// See comment for LooseNiceNumbers.  This is Qt's code.
inline qreal ChartBase::niceNumber(qreal x, bool ceiling)
{
    qreal z = qPow(10, qFloor(std::log10(x)));
    qreal q = x / z; //q<10 && q>=1;

    if (ceiling) {
        if (q <= 1.0) q = 1;
        else if (q <= 2.0) q = 2;
        else if (q <= 5.0) q = 5;
        else q = 10;
    } else {
        if (q < 1.5) q = 1;
        else if (q < 3.0) q = 2;
        else if (q < 7.0) q = 5;
        else q = 10;
    }
    return q * z;
}

// QValueAxis::setRange() is a slow function.  QValueAxis::setNiceNumbers calls it itself,
// but we have to ALSO call it to supply the range it starts with.  To avoid two heavy invocations,
// we manually calculate the "nice numbers" here.  This is a 2X difference!
// This is Qt's code.
inline void ChartBase::looseNiceNumbers(qreal &min, qreal &max, int &ticksCount)
{
    qreal range = niceNumber(max - min, true);
    qreal step = niceNumber(range / (ticksCount - 1), false);
    min = qFloor(min / step);
    max = qCeil(max / step);
    ticksCount = int(max - min) + 1;
    min *= step;
    max *= step;
}

#endif // CHARTBASE_H
