/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QInputDialog>

#include "filterpane.h"
#include "ui_filterpane.h"

#include <src/util/util.h>
#include <src/util/icons.h>
#include "src/ui/windows/mainwindow.h"
#include "src/core/filtermodel.h"

FilterPane::FilterPane(MainWindow& mainWindow, QWidget *parent) :
    DataColumnPane(mainWindow, PaneClass::Filter, parent),
    ui(new Ui::FilterPane),
    m_queryCtx(&mainWindow.trackModel()),
    queryDelegate(cfgData(), m_queryCtx, this),
    model(mainWindow.filterModel())
{
    ui->setupUi(this);

    setupView(ui->viewCategories, &model);
    setWidgets<FilterModel>(ui->filterCategories, nullptr, ui->showColumns, ui->filterCtrl, ui->filterIsValid,
                              defHideColumns());

    setupActionIcons();
    setupContextMenus();
    setupSignals();
    setupDelegates();
    newConfig();
}

FilterPane::~FilterPane()
{
    delete ui;
}

void FilterPane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Edit_Query,    "edit-entry");
    Icons::defaultIcon(ui->action_Activate,      "view-filter");
    Icons::defaultIcon(ui->action_Set_Icon,      "image-x-icon");
    Icons::defaultIcon(ui->action_Edit_Name,     "edit-rename");
    Icons::defaultIcon(ui->action_Unset_Icon,    "edit-clear");
    Icons::defaultIcon(ui->action_Deactivate,    "edit-clear");
    Icons::defaultIcon(ui->action_Update_Filter, "view-refresh");
    Icons::defaultIcon(ui->action_Duplicate,     "edit-duplicate");
}

void FilterPane::setupContextMenus()
{
    paneMenu.addActions({ ui->action_Activate,
                          ui->action_Deactivate });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Create_New_Filter,
                          ui->action_Duplicate,
                          mainWindow().getMainAction(MainAction::DeleteSelection) });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Edit_Name,
                          ui->action_Edit_Query,
                          ui->action_Update_Filter,
                          ui->action_Set_Icon,
                          ui->action_Unset_Icon });
    paneMenu.addSeparator();
    setupActionContextMenu(paneMenu);

    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &FilterPane::customContextMenuRequested, this, &FilterPane::showContextMenu);
}

void FilterPane::setupSignals()
{
    DataColumnPane::setupSignals();

    QTreeView& view = *ui->viewCategories;
    connect(&view, &QTreeView::doubleClicked, this, &FilterPane::doubleClicked);
}

void FilterPane::setupDelegates()
{
    QTreeView& view = *ui->viewCategories;

    queryDelegate.setSelector(view.selectionModel());

    view.setItemDelegateForColumn(FilterModel::Query, &queryDelegate);
}

void FilterPane::newConfig()
{
    DataColumnPane::newConfig();
    ui->viewCategories->setIconSize(cfgData().iconSizeFilter);
}

const FilterPane::ColSet& FilterPane::defHideColumns() const
{
    static const ColSet hide = {
        FilterModel::Query, // hide these columns by default
    };

    return hide;
}

void FilterPane::showContextMenu(const QPoint& pos)
{
    menuIdx = clickPosIndex(pos);

    ui->action_Activate->setEnabled(hasSelection());
    ui->action_Edit_Query->setEnabled(hasSelection());
    ui->action_Edit_Name->setEnabled(hasSelection());
    ui->action_Set_Icon->setEnabled(hasSelection());

    paneMenu.exec(mapToGlobal(pos));
}

void FilterPane::addFilterInteractive(const QString& query)
{
    if (ui == nullptr)
        return;

    if (!m_queryCtx.isValidQuery(query)) {
        mainWindow().statusMessage(UiType::Warning, tr("Invalid Query"));
        return;
    }

    bool ok;
    const QString newName = QInputDialog::getText(this, tr("Filter Name"),
                                                  tr("Filter name:"), QLineEdit::Normal,
                                                  "My Track Filter", &ok);

    if (!ok || newName.isEmpty()) {
        mainWindow().statusMessage(UiType::Warning, tr("Canceled"));
        return;
    }

    // Add current query the model.
    model.appendRow({ newName, query });

    mainWindow().statusMessage(UiType::Success, tr("Added filter: ") + newName);
}

PaneBase* FilterPane::getFilterPane() const
{
    if (TrackPane* pane = mainWindow().findPane<TrackPane>(); pane != nullptr)
        return pane;

    mainWindow().statusMessage(UiType::Warning, tr("No Pane found for filter"));
    return nullptr;
}

void FilterPane::unsetFilter()
{
    PaneBase* pane = getFilterPane();
    if (pane == nullptr)
        return;

    pane->setQuery("");
}

// True if anything is selected in a node's subtree, but NOT including the
// node itself.
bool FilterPane::hasSubtreeSelection(const QModelIndex& parent) const
{
    for (int row = 0; row < model.rowCount(parent); ++row) {
        const QModelIndex idx = model.index(row, 0, parent);
        if (isSelected(idx) || hasSubtreeSelection(idx))
            return true;
    }

    return false;
}

// Absolutely dreadfully inefficient due to dual recursions, but performance doesn't matter here: this
// only happens when the user applies the filter (rarely), not when the filter is used (often).
void FilterPane::buildFilterString(QString& aggregateNames, QString& aggregateQuery, const QModelIndex& idx, bool firstAtDepth)
{
    const bool subtreeSelected = hasSubtreeSelection(idx);
    const bool nodeSelected = isSelected(idx);

    if (!nodeSelected && !subtreeSelected)
        return;

    if (idx.isValid()) {
        const QString name  = model.data(FilterModel::Name, idx, Util::RawDataRole).toString();
        const QString query = model.data(FilterModel::Query, idx, Util::RawDataRole).toString();

        if (!aggregateNames.isEmpty())
            aggregateNames.append("+");
        aggregateNames.append(name);

        if (!firstAtDepth)
            aggregateQuery.append(" | ");
    
        aggregateQuery.append(query);

        if (subtreeSelected)
            aggregateQuery.append(" & ( ");
    }

    firstAtDepth = true;
    const int queryStrSize = aggregateQuery.size();

    for (int row = 0; row < model.rowCount(idx); ++row) {
        buildFilterString(aggregateNames, aggregateQuery, model.index(row, 0, idx), firstAtDepth);
        if (aggregateQuery.size() > queryStrSize)
            firstAtDepth = false;
    }

    if (idx.isValid())
        if (subtreeSelected)
            aggregateQuery.append(" )");
}

void FilterPane::setFilter()
{
    PaneBase* pane = getFilterPane();
    if (pane == nullptr)
        return;

    QString aggregateNames;
    QString aggregateQuery;

    aggregateNames.reserve(128);
    aggregateQuery.reserve(256);

    buildFilterString(aggregateNames, aggregateQuery);

    pane->setQuery(aggregateQuery);

    mainWindow().statusMessage(UiType::Info, tr("Using filter(s): ") + aggregateNames);
}

void FilterPane::doubleClicked(const QModelIndex&)
{
    setFilter();
}

void FilterPane::on_action_Create_New_Filter_triggered()
{
    PaneBase* pane = getFilterPane();
    if (pane == nullptr)
        return;

    addFilterInteractive(pane->getQuery());
}

void FilterPane::on_action_Update_Filter_triggered()
{
    PaneBase* pane = getFilterPane();
    if (pane == nullptr)
        return;

    int count = 0;
    for (const auto idx : getSelections()) {
        model.setData(FilterModel::Query, idx, pane->getQuery(), Util::RawDataRole);
        ++count;
    }

    mainWindow().statusMessage(UiType::Success, QString::number(count) + tr(" filter(s) updated."));
}

void FilterPane::on_action_Activate_triggered()
{
    setFilter();
}

void FilterPane::deleteSelection()
{
    const int count = selectionModel()->selectedRows().size();

    model.removeRows(selectionModel(), &topFilter());

    mainWindow().statusMessage(UiType::Success, tr("Deleted ") + QString::number(count) + tr(" filters."));
}

void FilterPane::on_action_Edit_Name_triggered()
{
    if (menuIdx.isValid()) {
        setColumnHidden(FilterModel::Name, false);
        ui->viewCategories->edit(menuIdx.sibling(menuIdx.row(), FilterModel::Name));
    }
}

void FilterPane::on_action_Edit_Query_triggered()
{
    if (menuIdx.isValid()) {
        setColumnHidden(FilterModel::Query, false);
        ui->viewCategories->edit(menuIdx.sibling(menuIdx.row(), FilterModel::Query));
    }
}

void FilterPane::on_action_Set_Icon_triggered()
{
    setIcon(FilterModel::Icon);
}

void FilterPane::on_action_Unset_Icon_triggered()
{
    clearIcon(FilterModel::Icon);
}

void FilterPane::on_action_Deactivate_triggered()
{
    unsetFilter();
}

void FilterPane::on_action_Duplicate_triggered()
{
    for (const auto idx : getSelections()) {
        const QVariant name  = model.data(FilterModel::Name, idx, Util::RawDataRole);
        const QVariant query = model.data(FilterModel::Query, idx, Util::RawDataRole);

        model.appendRow({ name, query });
    }
}
