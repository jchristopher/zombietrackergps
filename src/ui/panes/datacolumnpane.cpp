/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/util/roles.h>
#include <src/util/util.h>
#include "src/ui/windows/mainwindow.h"
#include "datacolumnpane.h"

IconSelector* DataColumnPane::iconSelector = nullptr;

DataColumnPane::DataColumnPane(MainWindow& mainWindow, PaneClass paneClass, QWidget *parent, bool useFlattener) :
    DataColumnPaneBase(mainWindow, int(paneClass), parent,  useFlattener)
{
    // This is expensive to create, so we use a static one: only one can be displayed at a time anyway,
    // since it's modal in our use.
    if (iconSelector == nullptr)
        iconSelector = new IconSelector(":art/tags", true);
}

// This is needed to make the MOC hapy
DataColumnPane::~DataColumnPane()
{
}

const CfgData &DataColumnPane::cfgData() const
{
    return mainWindow().cfgData();
}

const MainWindow& DataColumnPane::mainWindow() const
{
    return static_cast<const MainWindow&>(DataColumnPaneBase::mainWindow());
}

MainWindow& DataColumnPane::mainWindow()
{
    return static_cast<MainWindow&>(DataColumnPaneBase::mainWindow());
}

void DataColumnPane::setIcon(ModelType mt)
{
    const QModelIndex idx = Util::MapDown(treeView->currentIndex());
    if (!idx.isValid())
        return;

    TreeModel& model = *static_cast<TreeModel*>(Util::MapDown(treeView->model()));

    iconSelector->setCurrentPath(model.data(mt, idx, Util::IconNameRole).toString());

    const QRect itemPos = treeView->visualRect(treeView->currentIndex());

    // Place it over the item in question.
    iconSelector->setGeometry(Util::MapOnScreen(treeView, treeView->pos() + itemPos.center(),
                                                iconSelector->size()));

    if (iconSelector->exec() != QDialog::Accepted)
        return;

    for (const auto& selIdx : getSelections())
        model.setSiblingIcon(mt, Util::MapDown(selIdx), iconSelector->iconFile());
}

void DataColumnPane::clearIcon(ModelType mt)
{
    TreeModel& model = *static_cast<TreeModel*>(Util::MapDown(treeView->model()));

    for (const auto idx : getSelections())
        model.clearIcon(model.rowSibling(mt, Util::MapDown(idx)));
}
