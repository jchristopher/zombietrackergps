/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QtMath>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChart>
#include <QtCharts/QValueAxis>
#include <QtCharts/QLegendMarker>
#include <QApplication>
#include <QFontMetricsF>

#include "tracklinepane.h"
#include "ui_tracklinepane.h"

#include <src/util/icons.h>
#include "src/ui/windows/mainwindow.h"
#include "src/core/trackmodel.h"

TrackLinePane::TrackLinePane(MainWindow& mainWindow, QWidget *parent) :
    ChartBase(mainWindow, PaneClass::LineChart, parent),
    ui(new Ui::TrackLinePane),
    graphDataModel(this),
    columnMap(PointModel::_Count, -1),
    zoomStepWheel(1.1),
    zoomStepMenu(1.5),
    zoomLevel(1.0),
    initZoom(zoomLevel),
    minZoom(1 / pow(zoomStepWheel, 50)),
    maxZoom(1.25),
    xBegin(0.0),
    axisUpdateTimer(this),
    activeMarker(nullptr)
{
    ui->setupUi(this);
    setPaneFilterBar(ui->filterCtrl);

    setupActionIcons();
    setupDataSelector();
    setupChart();
    setupMenus();
    setupSignals();
    setupTimers();
}

TrackLinePane::~TrackLinePane()
{
    deleteUI(ui);
}

void TrackLinePane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Show_Axes,   "labplot-axis-vertical");
    Icons::defaultIcon(ui->action_Show_Legend, "description");
    Icons::defaultIcon(ui->action_Reset_Zoom,  "zoom-fit-best");
    Icons::defaultIcon(ui->action_Zoom_In_X,   "x-zoom-in");
    Icons::defaultIcon(ui->action_Zoom_Out_X,  "x-zoom-out");
    Icons::defaultIcon(ui->action_Pan_Left,    "arrow-left");
    Icons::defaultIcon(ui->action_Pan_Right,   "arrow-right");
    Icons::defaultIcon(ui->action_Page_Left,   "arrow-left-double");
    Icons::defaultIcon(ui->action_Page_Right,  "arrow-right-double");
}

void TrackLinePane::setupChart()
{
    ChartBase::setupChart();

    if (chart == nullptr)
        return;

    chart->addAxis(new QtCharts::QValueAxis(), Qt::AlignBottom);

    ui->verticalLayout->addWidget(&chartView);

    updateAxes();
}

void TrackLinePane::setupMenus()
{
    // Add actions to our widget, so key sequences work when we're focused or when the
    // pane header bar is hidden.
    for (auto action : {
         ui->action_Page_Left,
         ui->action_Page_Right,
         ui->action_Pan_Left,
         ui->action_Pan_Right,
         ui->action_Zoom_In_X,
         ui->action_Zoom_Out_X,
         ui->action_Reset_Zoom,
         ui->action_Show_Legend,
         ui->action_Show_Axes })
    {
        action->setShortcutContext(Qt::WidgetWithChildrenShortcut);
        addAction(action);
    }

    ui->showAxes->setDefaultAction(ui->action_Show_Axes);
    ui->showLegend->setDefaultAction(ui->action_Show_Legend);
    ui->xZoomIn->setDefaultAction(ui->action_Zoom_In_X);
    ui->xZoomOut->setDefaultAction(ui->action_Zoom_Out_X);
    ui->panLeft->setDefaultAction(ui->action_Pan_Left);
    ui->panRight->setDefaultAction(ui->action_Pan_Right);

    ui->action_Show_Axes->setChecked(true);
    ui->action_Show_Legend->setChecked(true);

    paneMenu.addActions({ ui->action_Pan_Left,
                          ui->action_Pan_Right,
                          ui->action_Page_Left,
                          ui->action_Page_Right });
    paneMenu.addSeparator();

    paneMenu.addActions({ ui->action_Zoom_In_X,
                          ui->action_Zoom_Out_X,
                          ui->action_Reset_Zoom });
    paneMenu.addSeparator();

    paneMenu.addActions({ ui->action_Show_Legend,
                          ui->action_Show_Axes });
    paneMenu.addSeparator();

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(&chartView, &ChartViewZoom::customContextMenuRequested, this, &TrackLinePane::showContextMenu);
}

void TrackLinePane::setupSignals()
{
    // Notify ourselves when the current track changes
    connect(&mainWindow(), &MainWindow::currentTrackChanged,    this, &TrackLinePane::currentTrackChanged);
    connect(&mainWindow(), &MainWindow::currentTrackPointChanged, this, &TrackLinePane::currentPointChanged);

    // Mouse panning
    connect(&chartView, &ChartViewZoom::mouseMove, this, &TrackLinePane::mouseMove);
    connect(&chartView, &ChartViewZoom::mousePan, this,
            static_cast<void(TrackLinePane::*)(QMouseEvent* event, int, int)>(&TrackLinePane::pan));
    connect(&chartView, &ChartViewZoom::mouseEndPan, this, &TrackLinePane::endPan);

    // Row deletion from model
    connect(&mainWindow().trackModel(), &TrackModel::rowsAboutToBeRemoved, this, &TrackLinePane::processRowsAboutToBeRemoved);
    // Model reset
    connect(&mainWindow().trackModel(), &TrackModel::modelAboutToBeReset, this, &TrackLinePane::processModelAboutToBeReset);
}

void TrackLinePane::setupTimers()
{
    // Wait a little bit after the last active container change, and then resize columns.
    axisUpdateTimer.setSingleShot(true);
    connect(&axisUpdateTimer, &QTimer::timeout, this, &TrackLinePane::updateAxes);
}

void TrackLinePane::setupDataSelector()
{
    // Populate combobox for column selection
    graphDataModel.appendRow(new QStandardItem(tr("Chart Data")));

    ModelMetaData::setupComboBox<PointModel>(*ui->graphData, graphDataModel, nullptr,
                                           [this](ModelType mt, QStandardItem* item) {
                                                item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
                                                item->setCheckState(mt == PointModel::Ele ? Qt::Checked : Qt::Unchecked);
                                                columnMap[mt] = graphDataModel.rowCount();
                                                return item;
                                           },
                                           [](ModelType mt) { return PointModel::mdIsChartable(mt); });

    // connect column select behavior
    connect(&graphDataModel, &QStandardItemModel::itemChanged, this, &TrackLinePane::toggleColumn);
}

// enable proper set of actions for current state
void TrackLinePane::enableActions()
{
    ui->action_Pan_Left->setEnabled(xBegin < 1.0);
    ui->action_Page_Left->setEnabled(xBegin < 1.0);
    ui->action_Pan_Right->setEnabled(xBegin > 0.0);
    ui->action_Page_Right->setEnabled(xBegin > 0.0);
    ui->action_Zoom_In_X->setEnabled(zoomLevel > minZoom);
    ui->action_Zoom_Out_X->setEnabled(zoomLevel < maxZoom);
}

void TrackLinePane::showContextMenu(const QPoint& pos)
{
    enableActions();
    paneMenu.exec(chartView.mapToGlobal(pos));
}

inline qreal TrackLinePane::trackTotalDistanceUnits() const
{
    const PointModel* model = currentPoint();
    if (model == nullptr)
        return 1.0;

    return cfgData().unitsTrkLength.toDouble(model->trackTotalDistance());
}

inline qreal TrackLinePane::xViewExtentInUnits() const
{
    return trackTotalDistanceUnits() * zoomLevel;
}

inline qreal TrackLinePane::xAxisStart() const
{
    return xBegin * trackTotalDistanceUnits();
}

void TrackLinePane::initXAxis(QtCharts::QLineSeries* series)
{
    series->attachAxis(chart->axisX());
}

void TrackLinePane::initYAxis(QtCharts::QLineSeries* series, const Units& yAxisUnits, qreal yMin, qreal yMax)
{
    auto* yAxis = new QtCharts::QValueAxis();

    const int minorTickCount = (chart->plotArea().height() >= 160) ? 4 : 0;

    yAxis->setMinorTickCount(minorTickCount);  // ...
    yAxis->setLabelsColor(series->color().rgb()); // rgb() to avoid alpha channel for labels.
    yAxis->setLinePen(majorGridPen);
    yAxis->setGridLinePen(majorGridPen);
    yAxis->setMinorGridLinePen(minorGridPen);
    yAxis->setLabelFormat(yAxisUnits.chartLabelFormat(yMax));
    yAxis->setLabelsVisible(true);
    chart->addAxis(yAxis, Qt::AlignLeft);

    // see comment above looseNiceNumbers for motivation to avoid QValueAxis::applyNiceNumbers()
    int tickCount = 5;       // TODO: react to height
    looseNiceNumbers(yMin, yMax, tickCount);

    yAxis->setTickCount(tickCount);
    yAxis->setRange(yMin, yMax);
    series->attachAxis(yAxis);
}

// Create series to draw value labels (using point labels)
void TrackLinePane::initLabels()
{
    const auto chartSeries = chart->series();
    const QFontMetricsF fontMetric(QApplication::font());

    QPointF labelPos(0.05, 1.0 + fontMetric.descent() / chart->plotArea().height());

    int col = 0;
    for (auto series : chartSeries) {
        if (col >= valueColumn.size())  // skip marker any labels
            break;

        if (col < valueLabels.size()) {
            valueLabels.at(col)->replace(0, labelPos);
        } else {
            auto lineSeries  = static_cast<const QtCharts::QLineSeries*>(series);
            auto* labelSeries = new QtCharts::QLineSeries();

            chart->addSeries(labelSeries);
            // Disable legend marker
            chart->legend()->markers().back()->setVisible(false);

            labelSeries->append(labelPos);
            labelSeries->setUseOpenGL(false); // GL series can't draw labels, so disable it.
            labelSeries->setPointLabelsVisible(true);
            labelSeries->setPointLabelsColor(lineSeries->color().rgb()); // rgb() to avoid alpha channel
            labelSeries->setPointLabelsFont(QApplication::font());
            labelSeries->setPointLabelsFormat("");
            labelSeries->setPointLabelsClipping(false);

            valueLabels.append(labelSeries);
        }

        const Units& yAxisUnits = currentPoint()->units(valueColumn.at(col++));

        // TODO: improve width estimate
        labelPos.rx() += fontMetric.size(Qt::TextSingleLine, yAxisUnits(10000.0)).width() / chart->plotArea().width();
    }
}

// Create series for drawing the active marker
void TrackLinePane::initMarker()
{
    if (activeMarker != nullptr)
        return;

    activeMarker = new QtCharts::QLineSeries();
    activeMarker->setUseOpenGL(false); // using GL interferes with mouse drag
    const QPen markerPen(cfgData().trkPtMarkerColor, 1.0);
    activeMarker->setPen(markerPen);
    activeMarker->setPointsVisible(true);
    chart->addSeries(activeMarker);

    // Disable legend marker for the active marker
    chart->legend()->markers().back()->setVisible(false);
}

QtCharts::QLineSeries* TrackLinePane::newSeries(int col)
{
    auto* series = new QtCharts::QLineSeries();
    series->setUseOpenGL(true); // without this, performance is catastrophic.
    series->setName(currentPoint()->headerData(col, Qt::Horizontal).toString());
    const QPen seriesPen(cfgData().trkPtColor[col], cfgData().trkPtLineWidth);
    series->setPen(seriesPen);
    series->setPointsVisible(false);
    chart->addSeries(series); // add series to chart
    initXAxis(series);        // we don't recreate the X axis each time.

    return series;
}

bool TrackLinePane::isChecked(int col) const
{
    return columnMap.at(col) >= 0 &&
        graphDataModel.item(columnMap.at(col))->checkState() == Qt::Checked;
}

void TrackLinePane::updateChart()
{
    const PointModel* model = currentPoint();
    if (model == nullptr) {
        clearChart();
        return;
    }

    const Units& xAxisUnits = cfgData().unitsTrkLength;

    const auto seriesList     = chart->series();
    const bool existingSeries = !seriesList.isEmpty();

    // Remove old Y axes
    for (auto& yAxis : chart->axes(Qt::Vertical)) {
        chart->removeAxis(yAxis);
        delete yAxis;
    }

    QVector<QPointF> points;
    points.reserve(model->trackTotalPoints());

    valueColumn.clear();

    int pos = 0;
    // Go backwards, to draw higher priority columns over lower.
    for (int col = PointModel::_Count - 1; col >= PointModel::_First; --col) {
        if (!isChecked(col))  // Skip if not plotted, or unchartable
            continue;

        valueColumn.append(col);
        const Units& yAxisUnits = model->units(col);

        int   row  = 0;
        qreal yMin = std::numeric_limits<qreal>::max();
        qreal yMax = -std::numeric_limits<qreal>::max();

        points.clear(); // clear vector between columns

        // Add track point data to the chart.
        // Per Qt documentation, QXYSeries::replace() with a vector argument is much faster than
        // repeated appends.  So that's what we'll do here.
        for (const auto& trkSeg : *model) {
            for (const auto& pt : trkSeg) {
                if (pt.hasDistance() && pt.hasData(col)) {
                    const qreal yVal        = model->ptData<qreal>(pt, col, row++, true);
                    const qreal yValInUnits = yAxisUnits.toDouble(yVal);
                    const qreal distInUnits = xAxisUnits.toDouble(pt.distance());

                    yMin = std::fmin(yMin, yValInUnits);
                    yMax = std::fmax(yMax, yValInUnits);

                    points.append(QPointF(distInUnits, yValInUnits));
                }
            }
        }

        // reuse existing series if possible to avoid dynamic allocations
        QtCharts::QLineSeries* series = 
            existingSeries ? static_cast<decltype(series)>(seriesList.at(pos++)) : newSeries(col);

        series->replace(points);  // add our data to series

        initYAxis(series, yAxisUnits, yMin, yMax);
    }

    initLabels();
    initMarker();
    updateAxes();
}

void TrackLinePane::clearChart()
{
    // Remove current series
    chart->removeAllSeries();
    activeMarker = nullptr;
    valueLabels.clear();
    valueColumn.clear();
}

void TrackLinePane::refreshChart()
{
    clearChart();
    updateChart();
}

void TrackLinePane::setAxesShown(bool shown) const
{
    ChartBase::setAxesShown(shown);
    ui->action_Show_Axes->setChecked(shown);
}

void TrackLinePane::setLegendShown(bool shown) const
{
    ChartBase::setLegendShown(shown);
    ui->action_Show_Legend->setChecked(shown);
}

bool TrackLinePane::axesShown() const
{
    return ui != nullptr && ui->action_Show_Axes->isChecked();
}

bool TrackLinePane::legendShown() const
{
    return ui != nullptr && ui->action_Show_Legend->isChecked();
}

void TrackLinePane::updateXAxis()
{
    const PointModel* model = currentPoint();
    if (chart == nullptr || model == nullptr || model->isEmpty())
        return;

    const Units& xAxisUnits = cfgData().unitsTrkLength;

    const qreal distanceMeters = model->trackTotalDistance() * zoomLevel;
    const float distanceUnits  = xViewExtentInUnits();
    const QString labelFormat  = xAxisUnits.chartLabelFormat(distanceUnits);

    // Calculate how many label ticks to display.
    // Allow for a bit of padding between labels
    // TODO: should probably round distanceMeters to match label rounding.
    const int padWidth   = QApplication::fontMetrics().size(Qt::TextSingleLine, "M").width();
    const QString labelForWidth = xAxisUnits(distanceMeters);
    const int labelWidth = QApplication::fontMetrics().size(Qt::TextSingleLine, labelForWidth).width() + padWidth;

    const int minorTickCount = (labelWidth > 40) ? 4 : 0;

    QtCharts::QValueAxis* xAxis = dynamic_cast<QtCharts::QValueAxis*>(chart->axisX());

    // See comment above looseNiceNumbers for motivation of not using QValueAxis::applyNiceNumbers()
    int tickCount  = chart->plotArea().width() / std::max(labelWidth, 1);
    qreal minVal = xAxisStart();
    qreal maxVal = minVal + distanceUnits;
    looseNiceNumbers(minVal, maxVal, tickCount);

    xAxis->setTickCount(tickCount);
    xAxis->setMinorTickCount(minorTickCount);
    xAxis->setRange(minVal, maxVal);
    xAxis->setLabelsVisible(true);
    xAxis->setLinePen(majorGridPen);
    xAxis->setGridLinePen(majorGridPen);
    xAxis->setMinorGridLinePen(minorGridPen);
    xAxis->setLabelsColor(labelNormal);
    xAxis->setLabelFormat(labelFormat);
}

void TrackLinePane::updateYAxis()
{
    // No dynamic changes: everything is handled during creation.
}

void TrackLinePane::updateAxes()
{
    updateXAxis();
    updateYAxis();
    setAxesShown(axesShown());
}

void TrackLinePane::resizeEvent(QResizeEvent *event)
{
    ChartBase::resizeEvent(event);

    initLabels();
    axisUpdateTimer.start(100);
}

void TrackLinePane::wheelEvent(QWheelEvent *event)
{
    if (event->angleDelta().y() > 0)
        zoomIn(zoomStepWheel);
    else if (event->angleDelta().y() < 0)
        zoomOut(zoomStepWheel);
}

void TrackLinePane::zoomIn(float val)
{
    setZoom(zoomLevel / val);
}

void TrackLinePane::zoomOut(float val)
{
    setZoom(zoomLevel * val);
}

void TrackLinePane::setZoom(float val)
{
    if (chart == nullptr)
        return;

    zoomLevel = Util::Clamp(val, minZoom, maxZoom);
    updateAxes();
}

void TrackLinePane::resetPanZoom()
{
    zoomLevel = 1.0;
    xBegin    = 0.0;
}

void TrackLinePane::pan(int relX, int /*relY*/)
{
    const PointModel* model = currentPoint();
    if (model == nullptr)
        return;

    QtCharts::QValueAxis* xAxis = dynamic_cast<QtCharts::QValueAxis*>(chart->axisX());

    // Reset zoom level to whatever it currently is.
    zoomLevel = (xAxis->max() - xAxis->min()) / trackTotalDistanceUnits();

    // X: ...
    xBegin += float(-relX) * zoomLevel / float(std::max(chartView.width(), 1));
    xBegin = std::clamp(xBegin, 0.0f, 1.0f);

    const float xBeginUnits   = xAxisStart();
    const float distanceUnits = xViewExtentInUnits();

    xAxis->setRange(xBeginUnits, xBeginUnits + distanceUnits);
}

void TrackLinePane::pan(QMouseEvent* event, int relX, int relY)
{
    // Only left click drag
    if ((event->buttons() & Qt::LeftButton) == 0)
        return;

    pan(relX, relY);
}

void TrackLinePane::endPan()
{
    // Update marker text, if marker is active
    if (activeMarker != nullptr && activeMarker->count() > 0) {
        const qreal markerXPos = activeMarker->at(0).x() * chart->plotArea().width() + chart->plotArea().x();
        drawActiveMarker(markerXPos);
    }
}

void TrackLinePane::mouseMove(QMouseEvent* event)
{
    if (chart->plotArea().contains(event->localPos()))
        drawActiveMarker(event->x());
    else
        clearMarkerSeries();
}

void TrackLinePane::toggleColumn(QStandardItem*)
{
    refreshChart();
}

void TrackLinePane::processRowsAboutToBeRemoved(const QModelIndex& parent, int first, int last)
{
    if (!currentPointIdx.isValid())
        return;

    if (currentPointIdx.parent() == parent && currentPointIdx.row() >= first && currentPointIdx.row() <= last) {
        currentPointIdx = QModelIndex();
        updateChart();
    }
}

void TrackLinePane::currentTrackChanged(const QModelIndex& current)
{
    // Return if no row change.
    if (currentPointIdx.model() == current.model() && currentPointIdx.row() == current.row())
        return;

    currentPointIdx = current.sibling(current.row(), 0);

    resetPanZoom();

    updateChart();
}

void TrackLinePane::clearMarkerSeries()
{
    if (activeMarker == nullptr)
        return;

    activeMarker->clear();

    for (auto label : valueLabels)
        label->setPointLabelsFormat("");
}

void TrackLinePane::drawMarkerText(qreal xPos)
{
    // Find distance of the marker, in native distance units (meters)
    const Units& xAxisUnits = cfgData().unitsTrkLength;
    const qreal distance = xAxisUnits.fromDouble(chart->mapToValue(QPointF(xPos, 0.5)).x());

    const PointModel* model = currentPoint();
    if (model == nullptr)
        return;

    // Update the label values
    int pos = 0;
    for (auto label : valueLabels) {
        // Find value at this distance.
        const ModelType col     = valueColumn.at(pos++);
        const Units& yAxisUnits = model->units(col);
        const double interpVal  = model->interpolate(distance, col);

        label->setPointLabelsFormat(std::isnan(interpVal) ? "" : yAxisUnits(interpVal));
    }
}

void TrackLinePane::drawActiveMarker(qreal xPos)
{
    if (activeMarker == nullptr || chart == nullptr)
        return;

    activeMarker->clear();

    // Draw in unit axis from [0..1]
    const qreal xPos01 = (xPos - chart->plotArea().x()) / chart->plotArea().width();

    // Draw vertical line at the horizontal position of this item.
    activeMarker->append(xPos01, 0.0);
    activeMarker->append(xPos01, 1.0);

    drawMarkerText(xPos);

    chart->update(); // Force repaint for new text. Expensive, but unable to find other option.
}

void TrackLinePane::currentPointChanged(const QModelIndex& current)
{
    clearMarkerSeries();

    const PointModel* model = currentPoint();
    if (chart == nullptr || model == nullptr || !current.isValid() || current.model() != model)
        return;

    const auto series = chart->series();
    if (series.isEmpty())
        return;

    const Units& xAxisUnits   = cfgData().unitsTrkLength;
    const qreal distanceM     = model->data(PointModel::Distance, current, Util::RawDataRole).toDouble();
    const qreal distanceUnits = xAxisUnits.toDouble(distanceM);
    const QPointF itemPos     = chart->mapToPosition(QPointF(distanceUnits, 0.0), series.front());

    drawActiveMarker(itemPos.x());
}

void TrackLinePane::processModelAboutToBeReset()
{
    currentPointIdx = QModelIndex();
    updateChart();
}

// TODO: share with PointPane
PointModel* TrackLinePane::currentPoint()
{
    if (!currentPointIdx.isValid())
        return nullptr;

    return mainWindow().trackModel().geoPoints(currentPointIdx);
}

// TODO: share with PointPane
const PointModel* TrackLinePane::currentPoint() const
{
    return const_cast<const PointModel*>(const_cast<TrackLinePane*>(this)->currentPoint());
}

void TrackLinePane::newConfig()
{
    ChartBase::newConfig();

    refreshChart();
    setZoom(zoomLevel);

    // This seems necessary to refresh the legend labels.  Unclear why.
    chart->legend()->setVisible(false);
    chart->legend()->setVisible(axesShown());
}

void TrackLinePane::showAll()
{
    resetPanZoom();
    updateAxes();
}

void TrackLinePane::on_action_Show_Axes_toggled(bool shown)
{
    setAxesShown(shown);
}

void TrackLinePane::on_action_Show_Legend_toggled(bool shown)
{
    setLegendShown(shown);
}

void TrackLinePane::on_action_Reset_Zoom_triggered()
{
    showAll();
    enableActions();
}

void TrackLinePane::on_action_Zoom_In_X_triggered()
{
    zoomIn(zoomStepMenu);
    enableActions();
}

void TrackLinePane::on_action_Zoom_Out_X_triggered()
{
    zoomOut(zoomStepMenu);
    enableActions();
}

void TrackLinePane::on_action_Pan_Left_triggered()
{
    pan(-chart->plotArea().width() * 0.1, 0);
    enableActions();
}

void TrackLinePane::on_action_Pan_Right_triggered()
{
    pan(chart->plotArea().width() * 0.1, 0);
    enableActions();
}

void TrackLinePane::on_action_Page_Left_triggered()
{
    pan(-chart->plotArea().width() * 0.8, 0);
    enableActions();
}

void TrackLinePane::on_action_Page_Right_triggered()
{
    pan(chart->plotArea().width() * 0.8, 0);
    enableActions();
}

void TrackLinePane::save(QSettings& settings) const
{
    ChartBase::save(settings);

    MemberSave(settings, zoomLevel);
    MemberSave(settings, xBegin);

    if (ui != nullptr) {
        settings.beginWriteArray("graphData"); {
            for (int row = 0; row < graphDataModel.rowCount(); ++row) {
                settings.setArrayIndex(row);
                SL::Save(settings, "checked", graphDataModel.item(row)->checkState() == Qt::Checked);
                SL::Save(settings, "name", graphDataModel.data(graphDataModel.index(row, 0)).toString());
            }
        } settings.endArray();
    }
}

void TrackLinePane::load(QSettings& settings)
{
    ChartBase::load(settings);

    MemberLoad(settings, zoomLevel);
    MemberLoad(settings, xBegin);

    if (ui != nullptr) {
        const int eleRow = columnMap.at(PointModel::Ele);  // default check the elevation entry
        const int rows = std::min(graphDataModel.rowCount(), settings.beginReadArray("graphData")); {
            for (int row = 0; row < rows; ++row) {
                settings.setArrayIndex(row);
                const QString name = SL::Load<QString>(settings, "name", "");
                // String search, in case the displayed columns differ between save state and model.
                if (const auto items = graphDataModel.findItems(name); !items.isEmpty())
                    items.front()->setCheckState(SL::Load(settings, "checked", row == eleRow) ?
                                                     Qt::Checked : Qt::Unchecked);
            }
        } settings.endArray();
    }
}
