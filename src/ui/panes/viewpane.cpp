/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QHash>

#include "viewpane.h"
#include "ui_viewpane.h"

#include <src/util/util.h>
#include <src/util/icons.h>
#include "src/ui/windows/mainwindow.h"
#include "src/core/viewmodel.h"
#include "mappane.h"

ViewPane::ViewPane(MainWindow& mainWindow, QWidget *parent) :
    DataColumnPane(mainWindow, PaneClass::View, parent),
    ui(new Ui::ViewPane)
{
    ui->setupUi(this);

    setupView(ui->viewView, &mainWindow.viewModel());
    setWidgets<ViewModel>(ui->filterView, nullptr, ui->showColumns, ui->filterCtrl, ui->filterIsValid,
                          defHideColumns());

    setupActionIcons();
    setupContextMenus();
    setupSignals();
    newConfig();
}

ViewPane::~ViewPane()
{
    delete ui;
}

void ViewPane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Goto,       "mark-location-symbolic");
    Icons::defaultIcon(ui->action_Edit,       "document-edit");
    Icons::defaultIcon(ui->action_Update,     "mark-location-symbolic");
    Icons::defaultIcon(ui->action_New,        "mark-location-symbolic");
    Icons::defaultIcon(ui->action_Set_Icon,   "image-x-icon");
    Icons::defaultIcon(ui->action_Unset_Icon, "edit-clear");
}

void ViewPane::setupContextMenus()
{
    paneMenu.addActions({ ui->action_Goto,
                          ui->action_New,
                          ui->action_Edit,
                          ui->action_Set_Icon,
                          ui->action_Unset_Icon });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Update,
                          mainWindow().getMainAction(MainAction::DeleteSelection) });
    paneMenu.addSeparator();

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &ViewPane::customContextMenuRequested, this, &ViewPane::showContextMenu);
}

void ViewPane::setupSignals()
{
    DataColumnPane::setupSignals();

    QTreeView& view = *ui->viewView;
    connect(&view, &QTreeView::doubleClicked, this, &ViewPane::doubleClicked);
}

void ViewPane::newConfig()
{
    DataColumnPane::newConfig();
    ui->viewView->setIconSize(cfgData().iconSizeView);
}

void ViewPane::showContextMenu(const QPoint& pos)
{
    paneMenu.exec(mapToGlobal(menuPos = pos));
}

void ViewPane::doubleClicked(const QModelIndex& idx)
{
    gotoIndex(idx);
}

void ViewPane::gotoIndex(const QModelIndex& idx) const
{
    MapPane* mapPane = mainWindow().findPane<MapPane>();

    if (ui == nullptr || ui->viewView == nullptr || mapPane == nullptr || !idx.isValid())
        return;

    mapPane->gotoView(Util::MapDown(idx));
}

const ViewPane::ColSet& ViewPane::defHideColumns() const
{
    static const ColSet hide = {
        ViewModel::CenterLat,  // hide these columns by default
        ViewModel::CenterLon,
        ViewModel::Heading,
        ViewModel::Zoom,
    };

    return hide;
}

void ViewPane::on_action_Goto_triggered()
{
    gotoIndex(ui->viewView->currentIndex());
}

void ViewPane::deleteSelection()
{
    if (ui == nullptr || ui->viewView == nullptr || selectionModel() == nullptr)
        return;

    const int count = selectionModel()->selectedRows().size();

    mainWindow().viewModel().removeRows(selectionModel(), &topFilter());

    mainWindow().statusMessage(UiType::Success, tr("Removed ") + QString::number(count) +
                               tr(" view presets."));
}

void ViewPane::on_action_Edit_triggered()
{
    const QModelIndex idx = clickPosIndex(menuPos);

    if (idx.isValid())
        ui->viewView->edit(idx);
}

void ViewPane::on_action_Update_triggered()
{
    MapPane* mapPane = mainWindow().findPane<MapPane>();

    if (ui == nullptr || ui->viewView == nullptr || mapPane == nullptr)
        return;

    const QModelIndex idx = Util::MapDown(ui->viewView->currentIndex());

    if (!idx.isValid())
        return;

    mapPane->updateViewPreset(idx);
}

void ViewPane::on_action_New_triggered()
{
    if (MapPane* mapPane = mainWindow().findPane<MapPane>())
        mapPane->addViewPresetInteractive();
}

void ViewPane::on_action_Set_Icon_triggered()
{
    setIcon(ViewModel::Icon);
}

void ViewPane::on_action_Unset_Icon_triggered()
{
    clearIcon(ViewModel::Icon);
}

// Helper.  There's probably a better way.
QString ViewPane::nameFromTopIndex(const QModelIndex& idx) const
{
    return topFilter().data(topFilter().index(idx.row(), ViewModel::Name, idx.parent()),
                            Util::RawDataRole).toString();
}

void ViewPane::save(QSettings& settings) const
{
    DataColumnPane::save(settings);

    // Save expansion state for each header node in the detail view.
    settings.beginGroup("Expanded"); {
        Util::recurse(topFilter(), [this, &settings](const QModelIndex& idx) {
            if (topFilter().hasChildren(idx))
                SL::Save(settings, nameFromTopIndex(idx), ui->viewView->isExpanded(idx));
        });
    } settings.endGroup();
}

void ViewPane::load(QSettings& settings)
{
    DataColumnPane::load(settings);

    QHash<QString, bool> expandState;

    // Restore expansion state for each header node in the detail view.
    settings.beginGroup("Expanded"); {
        const QStringList keys = settings.childKeys();

        for (const auto& key : keys) {
            bool expanded = true;
            SL::Load(settings, key, expanded);
            expandState[key] = expanded;
        }
    } settings.endGroup();

    Util::recurse(topFilter(), [this, &expandState](const QModelIndex& idx) {
        if (topFilter().hasChildren(idx))
            if (const auto it = expandState.find(nameFromTopIndex(idx)); it != expandState.end())
                ui->viewView->setExpanded(idx, it.value());
    });
}
