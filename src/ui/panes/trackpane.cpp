/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QSet>
#include "trackpane.h"
#include "filterpane.h"
#include "ui_trackpane.h"

#include <src/util/icons.h>
#include <src/util/util.h>
#include <src/util/roles.h>
#include "src/ui/windows/mainwindow.h"
#include "src/core/trackmodel.h"
#include "src/core/trackitem.h"

TrackPane::TrackPane(MainWindow& mainWindow, QWidget *parent) :
    DataColumnPane(mainWindow, PaneClass::Track, parent, false),
    ui(new Ui::TrackPane),
    colorDelegate(this, false, tr("Track Color"), true, Util::RawDataRole),
    noteDelegate(this, tr("Edit Track Note")),
    tagDelegate(this, cfgData(), tr("Select Track Tags"), cfgData().tags.keyRole()),
    mapUpdateTimer(this)
{
    ui->setupUi(this);

    setupView(ui->trackView, &mainWindow.trackModel());
    setWidgets<TrackModel>(ui->filterTrack, ui->filterColumn, ui->showColumns, ui->filterCtrl, ui->filterIsValid,
                           defHideColumns());

    setupActionIcons();
    setupContextMenus();
    setupSignals();
    setupDelegates();
    setupTimers();
    newConfig();
}

TrackPane::~TrackPane()
{
    delete ui;
}

void TrackPane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Reset_Track_Color,        "gkt-restore-defaults");
    Icons::defaultIcon(ui->action_Reset_Track_Note,         "gkt-restore-defaults");
    Icons::defaultIcon(ui->action_Edit,                     "document-edit");
    Icons::defaultIcon(ui->action_Set_Icon,                 "preferences-desktop-filetype-association");
    Icons::defaultIcon(ui->action_Change_Tags,              "tag");
    Icons::defaultIcon(ui->action_Select_Duplicates,        "edit-select");
    Icons::defaultIcon(ui->action_Select_All_Duplicates,    "edit-select");
    Icons::defaultIcon(ui->action_Create_Filter_from_Query, "view-filter");
    Icons::defaultIcon(ui->action_Unset_Icon,               "edit-clear");
    Icons::defaultIcon(ui->action_Duplicate_Tracks,         "edit-duplicate");
}

void TrackPane::setupContextMenus()
{
    paneMenu.addActions({ mainWindow().getMainAction(MainAction::ZoomToSelection),
                          mainWindow().getMainAction(MainAction::DeleteSelection),
                          mainWindow().getMainAction(MainAction::SelectPerson) });
    paneMenu.addSeparator();
    paneMenu.addAction(ui->action_Create_Filter_from_Query);
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Edit,
                          ui->action_Set_Icon,
                          ui->action_Change_Tags });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Unset_Icon,
                          ui->action_Reset_Track_Color,
                          ui->action_Reset_Track_Note });
    paneMenu.addSeparator();
    paneMenu.addActions({ ui->action_Duplicate_Tracks,
                          ui->action_Select_Duplicates,
                          ui->action_Select_All_Duplicates });
    paneMenu.addSeparator();

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &TrackPane::customContextMenuRequested, this, &TrackPane::showContextMenu);

    // Tie our config button to main window action
    ui->selectPerson->setDefaultAction(mainWindow().getMainAction(MainAction::SelectPerson));
}

void TrackPane::setupSignals()
{
    DataColumnPane::setupSignals();

    // Double click
    QTreeView& view = *ui->trackView;
    connect(&view, &QTreeView::doubleClicked, this, &TrackPane::doubleClicked);

    // Selection changes
    connect(selectionModel(), &QItemSelectionModel::selectionChanged, this, &TrackPane::processSelectionChanged);
    connect(&mainWindow(), &MainWindow::selectedTracksChanged, this, &TrackPane::processSelectedTracksChanged);
}

void TrackPane::setupTimers()
{
    // Wait a little bit after the typing to update the map visibility.
    mapUpdateTimer.setSingleShot(true);
    connect(&mapUpdateTimer, &QTimer::timeout, this, &TrackPane::updateVisibility);
}

void TrackPane::addFilterInteractive()
{
    FilterPane* filterPane = mainWindow().findPane<FilterPane>();

    if (filterPane == nullptr) {
        mainWindow().statusMessage(UiType::Warning, tr("No Filter Pane found"));
        return;
    }

    filterPane->addFilterInteractive(getQuery());
}

void TrackPane::resizeToFit(int defer)
{
    DataColumnPane::resizeToFit(defer);
    TrackItem::setupSmallFont();
}

void TrackPane::gotoSelection(const QModelIndexList& selection) const
{
    MapPane* mapPane = mainWindow().findPane<MapPane>();

    if (ui == nullptr || ui->trackView == nullptr || mapPane == nullptr || selection.isEmpty())
        return;

    mapPane->zoomTo(mainWindow().trackModel().boundsBox(selection));
}

void TrackPane::setupDelegates()
{
    if (ui == nullptr)
        return;

    QTreeView& view = *ui->trackView;

    colorDelegate.setSelector(selectionModel());
    noteDelegate.setSelector(selectionModel());
    tagDelegate.setSelector(selectionModel());

    colorDelegate.setPadSize(6, 6);
    colorDelegate.setMaxSize(72, -1);

    view.setItemDelegateForColumn(TrackModel::Color, &colorDelegate);
    view.setItemDelegateForColumn(TrackModel::Notes, &noteDelegate);
    view.setItemDelegateForColumn(TrackModel::Tags,  &tagDelegate);
}

const TrackPane::ColSet& TrackPane::defHideColumns() const
{
    static const ColSet hide = {
        TrackModel::Keywords,
        TrackModel::EndDate,
        TrackModel::BeginTime,
        TrackModel::EndTime,
        TrackModel::StoppedTime,
        TrackModel::MinSpeed,
        TrackModel::AvgOvrSpeed,
        TrackModel::MinGrade,
        TrackModel::MinCad,
        TrackModel::MinElevation,
        TrackModel::AvgMovCad,
        TrackModel::MaxCad,
        TrackModel::MinPower,
        TrackModel::Segments,
        TrackModel::Points,
        TrackModel::Area,
        TrackModel::Descent,
        TrackModel::MinHR,
        TrackModel::MinTemp,
        TrackModel::AvgTemp,
        TrackModel::MaxTemp,
        TrackModel::MinTemp,
        TrackModel::AvgTemp,
        TrackModel::MaxTemp,
        TrackModel::Laps,
        TrackModel::MinLon,
        TrackModel::MinLat,
        TrackModel::MaxLon,
        TrackModel::MaxLat,
    };

    return hide;
}

// For performance reasons (selections are needed within the TrackMap render loop), we cache some results of
// the selection.
void TrackPane::processSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
    selectionSummary.update(mainWindow().trackModel(), topFilter(), selectionModel(), selected, deselected);
    mainWindow().updateStatus(selectionSummary);
}

void TrackPane::processSelectedTracksChanged()
{
    const bool hasSelection = TrackPane::hasSelection();
    const bool hasVisible   = topFilter().rowCount() > 0;

    // Update which UI actions are enabled, given the selection state.
    ui->action_Reset_Track_Color->setEnabled(hasSelection);
    ui->action_Reset_Track_Note->setEnabled(hasSelection);
    ui->action_Set_Icon->setEnabled(hasSelection);
    ui->action_Change_Tags->setEnabled(hasSelection);
    ui->action_Select_Duplicates->setEnabled(hasSelection);
    ui->action_Select_All_Duplicates->setEnabled(hasVisible);
    ui->action_Duplicate_Tracks->setEnabled(hasSelection);
}

void TrackPane::showContextMenu(const QPoint& pos)
{
    // Enable menus as appropriate
    menuIdx = clickPosIndex(pos);

    ui->action_Create_Filter_from_Query->setEnabled(!ui->filterTrack->text().isEmpty());
    ui->action_Edit->setEnabled(TrackModel::mdIsEditable(menuIdx.column()));

    paneMenu.exec(mapToGlobal(pos));
}

void TrackPane::doubleClicked(const QModelIndex& idx)
{
    // If it's an editable column edit, rather than view the selection
    if (TrackModel::mdIsEditable(idx.column()))
        return;

    gotoSelection(getSelections());
}

void TrackPane::zoomToSelection()
{
    gotoSelection(getSelections());
}

void TrackPane::deleteSelection()
{
    const int count = selectionModel()->selectedRows().size();

    mainWindow().trackModel().removeRows(selectionModel(), &topFilter());

    mainWindow().statusMessage(UiType::Success, tr("Deleted ") + QString::number(count) + tr(" tracks."));
}

void TrackPane::focusIn()
{
    DataColumnPane::focusIn();
    updateVisibility(); // update visibility for the new focus
}

void TrackPane::newConfig()
{
    DataColumnPane::newConfig();
    ui->trackView->setIconSize(cfgData().iconSizeTrack);
}

void TrackPane::on_action_Reset_Track_Color_triggered()
{
    TrackModel& model = mainWindow().trackModel();

    const QModelIndexList selection = getSelections();
    for (auto idx : selection)
        model.setData(TrackModel::Color, idx, QVariant(), Util::RawDataRole);

    mainWindow().statusMessage(UiType::Success, tr("Track color reset for ") +
                               QString::number(selection.size()) + " tracks.");
}

void TrackPane::on_action_Reset_Track_Note_triggered()
{
    TrackModel& model = mainWindow().trackModel();

    const QModelIndexList selection = getSelections();
    for (auto idx : selection)
        model.setData(TrackModel::Notes, idx, QVariant(), Util::RawDataRole);

    mainWindow().statusMessage(UiType::Success, tr("Track note reset for ") +
                               QString::number(selection.size()) + " tracks.");
}

void TrackPane::updateVisibility()
{
    TrackModel& model = mainWindow().trackModel();

    // Let the MapPane know which are visible in our view.
    model.setAllVisible(*this, false);

    Util::recurse(topFilter(), [this, &model](const QModelIndex& idx) {
        model.setVisible(*this, Util::MapDown(idx), true);
    });

    selectionSummary.refresh(model, topFilter(), selectionModel());
    mainWindow().updateStatus(selectionSummary);
    sort(); // resort our own view

    emit mainWindow().visibleTracksChanged(); // send notification
}

// Select track duplicates
void TrackPane::selectDuplicates(bool all)
{
    const TrackModel& model = mainWindow().trackModel();

    const auto modelHashes = model.trackHashes(); // for deduplication

    QSet<int> selectedHash;

    if (!all)
        for (const auto& idx : getSelections())
            selectedHash.insert(model.trackHash(idx));

    selectionModel()->clearSelection();
    selectionModel()->clearCurrentIndex();

    int dupeCount = 0;

    for (const auto& key : modelHashes.uniqueKeys()) {
        if (!all && !selectedHash.contains(key))
            continue;

        const auto keyRange = modelHashes.equal_range(key);
        for (auto rangeIt = keyRange.first; rangeIt != keyRange.second; ++rangeIt) {
            if (rangeIt == keyRange.first ||                  // skip first of identical set
                !model.pointEqual(*keyRange.first, *rangeIt)) // skip if doesn't compare equal
                continue;

            if (select(*rangeIt, QItemSelectionModel::Select | QItemSelectionModel::Rows))
                ++dupeCount;
        }
    }

    mainWindow().statusMessage(UiType::Info, tr("Selected ") + QString::number(dupeCount) + " duplicate tracks.");
}

void TrackPane::duplicateSelection()
{
    TrackModel& model = mainWindow().trackModel();

    const QModelIndexList selection = getSelections();

    selectionModel()->clearSelection();
    selectionModel()->clearCurrentIndex();

    for (const auto& idx : selection) {
        model.insertRow(model, idx, model.rowCount());
        select(idx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
    }

    topFilter().invalidate(); // refilter
}

void TrackPane::filterTextChanged(const QString& query)
{
    emit mainWindow().trackQueryChanged(query);

    DataColumnPane::filterTextChanged(query);
    mapUpdateTimer.start(500);
}

void TrackPane::on_action_Edit_triggered()
{
    if (menuIdx.isValid())
        ui->trackView->edit(menuIdx);
}

void TrackPane::on_action_Set_Icon_triggered()
{
    setIcon(TrackModel::Icon);
}

void TrackPane::on_action_Unset_Icon_triggered()
{
    clearIcon(TrackModel::Icon);
}

void TrackPane::on_action_Change_Tags_triggered()
{
    if (menuIdx.isValid())
        ui->trackView->edit(menuIdx.sibling(menuIdx.row(), TrackModel::Tags));
}

void TrackPane::on_action_Select_Duplicates_triggered()
{
    selectDuplicates(false);
}

void TrackPane::on_action_Select_All_Duplicates_triggered()
{
    selectDuplicates(true);
}

void TrackPane::on_action_Create_Filter_from_Query_triggered()
{
    addFilterInteractive();
}

void TrackPane::on_action_Duplicate_Tracks_triggered()
{
    duplicateSelection();
}
