/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef VIEWPANE_H
#define VIEWPANE_H

#include "datacolumnpane.h"

namespace Ui {
class ViewPane;
}

class MainWindow;
class QModelIndex;

class ViewPane final : public DataColumnPane
{
    Q_OBJECT

public:
    explicit ViewPane(MainWindow& mainWindow, QWidget *parent = 0);
    ~ViewPane();

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

private slots:
    void on_ViewPane_toggled(bool checked) { paneToggled(checked); }
    void on_action_Goto_triggered();
    void on_action_New_triggered();
    void on_action_Edit_triggered();
    void on_action_Update_triggered();
    void showContextMenu(const QPoint&);
    void doubleClicked(const QModelIndex&);
    void on_action_Set_Icon_triggered();
    void on_action_Unset_Icon_triggered();

private:
    void setupActionIcons();
    void setupContextMenus();
    void setupSignals() override;
    void gotoIndex(const QModelIndex&) const;
    const ColSet& defHideColumns() const override;
    void newConfig() override;
    void deleteSelection() override;

    // Helper
    QString nameFromTopIndex(const QModelIndex& idx) const;

    Ui::ViewPane* ui;
    QPoint        menuPos;

    std::map<QString, bool> expandState;  // see C++ comment for reexpandTree
};

#endif // VIEWPANE_H
