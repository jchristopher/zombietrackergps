/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include "chartbase.h"
#include "src/ui/windows/mainwindow.h"

ChartBase::ChartBase(MainWindow& mainWindow, PaneClass pc, QWidget *parent) :
    Pane(mainWindow, pc, parent),
    chart(new QtCharts::QChart()),
    chartView(nullptr),
    labelHighlight(QApplication::palette().highlight().color()),
    labelNormal(QApplication::palette().text().color()),
    labelSelected(QApplication::palette().link().color()),
    majorGridPen(QApplication::palette().light(), 2, Qt::SolidLine),
    minorGridPen(QApplication::palette().midlight(), 1, Qt::DashLine)
{
}

ChartBase::~ChartBase()
{
}

void ChartBase::setupChart()
{
    if (chart == nullptr)
        return;

    chartView.setChart(chart);  // takes ownership of chart
    chartView.setRenderHint(QPainter::Antialiasing);

    chart->setBackgroundVisible(true);
    chart->setPlotAreaBackgroundVisible(true);
    chart->setPlotAreaBackgroundBrush(QApplication::palette().base());
    chart->setBackgroundBrush(QApplication::palette().window());

    chart->setMargins(QMargins(0,0,0,0));
    chart->legend()->setContentsMargins(0,0,0,0);
    chart->legend()->setAlignment(Qt::AlignTop);
    chart->legend()->setLabelColor(labelNormal);

    disableToolTipsFor(&chartView);
}

void ChartBase::setAxesShown(bool shown) const
{
    if (chart == nullptr)
        return;

    for (auto& yAxis : chart->axes())
        yAxis->setVisible(shown);
}

void ChartBase::setLegendShown(bool shown) const
{
    if (chart == nullptr)
        return;

    chart->legend()->setVisible(shown);
}

void ChartBase::save(QSettings& settings) const
{
    Pane::save(settings);

    SL::Save(settings, "showAxes",   axesShown());
    SL::Save(settings, "showLegend", legendShown());
}

void ChartBase::load(QSettings& settings)
{
    Pane::load(settings);

    setAxesShown(SL::Load(settings, "showAxes", true));
    setLegendShown(SL::Load(settings, "showLegend", true));
}
