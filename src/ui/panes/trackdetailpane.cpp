/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "trackdetailpane.h"
#include "ui_trackdetailpane.h"
#include "src/ui/windows/mainwindow.h"
#include <src/util/roles.h>
#include <src/util/util.h>
#include <src/util/icons.h>

#include "src/core/trackmodel.h"

TrackDetailPane::TrackDetailPane(MainWindow& mainWindow, QWidget *parent) :
    Pane(mainWindow, PaneClass::TrackDetail, parent),
    ui(new Ui::TrackDetailPane),
    detailsHeader(Qt::Horizontal, this),
    subtreeFilter(this),
    flattenFilter(this, [](const QAbstractItemModel* model, const QModelIndex& idx) {
        return model->rowCount(idx) == 0;
    }),
    detailsFilter(this),
    colorDelegate(detailsFilter.dataIndex(TrackModel::Color, TrackDetailFilter::Data), this,
                  nullptr, false, tr("Track Color"), true, Util::RawDataRole),
    noteDelegate(detailsFilter.dataIndex(TrackModel::Notes, TrackDetailFilter::Data), this,
                 nullptr, tr("Edit Track Note")),
    tagDelegate(detailsFilter.dataIndex(TrackModel::Tags, TrackDetailFilter::Data), this,
                nullptr, cfgData(), tr("Select Track Tags"), cfgData().tags.keyRole()),
    resizeTimer(this),
    initialExpandTimer(this)
{
    ui->setupUi(this);

    setPaneFilterBar(ui->detailsFilterCtrl);
    setupDetailsView();
    setupContextMenus();
    setupSignals();
    setupDelegates();
    setupTimers();
    viewAsTree(true);
    newConfig();
}

TrackDetailPane::~TrackDetailPane()
{
    delete ui;
}

void TrackDetailPane::setupSignals()
{
    // Notify ourselves and the filter about active container changes
    connect(&mainWindow(), &MainWindow::currentTrackChanged, this, &TrackDetailPane::currentTrackChanged);

    // Filter text changes.
    connect(ui->filterDetails, &QLineEdit::textChanged, this, &TrackDetailPane::filterTextChanged);
}

void TrackDetailPane::setupDelegates()
{
    if (ui == nullptr)
        return;

    QTreeView& view = *ui->viewDetails;

    view.setItemDelegateForRow(detailsFilter.dataRow(TrackModel::Color), &colorDelegate);
    view.setItemDelegateForRow(detailsFilter.dataRow(TrackModel::Notes), &noteDelegate);
    view.setItemDelegateForRow(detailsFilter.dataRow(TrackModel::Tags),  &tagDelegate);

    colorDelegate().setPadSize(6, 6);
    colorDelegate().setMaxSize(72, -1);
}

void TrackDetailPane::setupTimers()
{
    // Wait a little bit after the last active container change, and then resize columns.
    resizeTimer.setSingleShot(true);
    connect(&resizeTimer, &QTimer::timeout, this, &TrackDetailPane::resizeDeferredHook);

    // We can't expand the tree in the constructor.  This is a kludge to fake it.
    initialExpandTimer.setSingleShot(true);
    connect(&initialExpandTimer, &QTimer::timeout, this, &TrackDetailPane::reexpandTree);
    initialExpandTimer.start(0);  // expand the tree after a little bit of idle time
}

void TrackDetailPane::setupContextMenus()
{
    // View context menus
    {
        QTreeView& view = *ui->viewDetails;

        setupActionContextMenu(actionMenu);
        // container action context menu
        view.setContextMenuPolicy(Qt::CustomContextMenu);
        connect(&view, &QTreeView::customContextMenuRequested, this, &TrackDetailPane::showActionContextMenu);
    }

    // Pane header menus
    Pane::setupPaneHeaderContextMenus();
}

void TrackDetailPane::filterTextChanged(const QString& regex)
{
    subtreeFilter.setQueryString(regex);

    if (regex.size() == 0)  // if filter is cleared, re-show everything
        showAll();
}

QModelIndexList TrackDetailPane::getSelections() const
{
    if (!hasSelection())
        return QModelIndexList();

    return { Util::MapDown(detailsFilter.getSelection()) };
}

bool TrackDetailPane::hasSelection() const
{
    return detailsFilter.hasSelection();
}

bool TrackDetailPane::isSelected(const QModelIndex &idx) const
{
    return hasSelection() && detailsFilter.getSelection() == idx;
}

void TrackDetailPane::showActionContextMenu(const QPoint& pos)
{
    actionMenu.exec(ui->viewDetails->mapToGlobal(pos));
}

void TrackDetailPane::setupDetailsView()
{
    QTreeView& view = *ui->viewDetails;

    detailsFilter.setSourceModel(&mainWindow().trackModel());
    flattenFilter.setSourceModel(&detailsFilter);
    subtreeFilter.setSourceModel(&detailsFilter);  // start out with tree view

    view.setModel(&subtreeFilter);
    view.setHeader(&detailsHeader);
    view.setAlternatingRowColors(true);
    view.setSelectionMode(QAbstractItemView::ExtendedSelection);
    view.setSelectionBehavior(QAbstractItemView::SelectRows);
    view.setSortingEnabled(false);
    view.setUniformRowHeights(true);

    subtreeFilter.setFilterCaseSensitivity(cfgData().getFilterCaseSensitivity());
    subtreeFilter.setDynamicSortFilter(false);

    detailsHeader.setSectionResizeMode(QHeaderView::Interactive);
    detailsHeader.setDefaultAlignment(Qt::AlignLeft);

    setFocusProxy(ui->viewDetails);
}

void TrackDetailPane::showAll()
{
    ui->filterDetails->clear();
    expandAll();
}

void TrackDetailPane::expandAll()
{
    ui->viewDetails->expandAll();
}

void TrackDetailPane::collapseAll()
{
    ui->viewDetails->collapseAll();
}

QItemSelectionModel* TrackDetailPane::selectionModel() const
{
    return ui->viewDetails->selectionModel();
}

void TrackDetailPane::copySelected() const
{
    subtreeFilter.copyToClipboard(*ui->viewDetails,
                                  subtreeFilter.sortedSelection(selectionModel()),
                                  cfgData().rowSeparator, cfgData().colSeparator,
                                  Util::CopyRole);
}

void TrackDetailPane::viewAsTree(bool asTree)
{
    if (asTree)
        subtreeFilter.setSourceModel(&detailsFilter);
    else
        subtreeFilter.setSourceModel(&flattenFilter);
}


bool TrackDetailPane::viewIsTree() const
{
    return subtreeFilter.sourceModel() != &flattenFilter;
}

void TrackDetailPane::selectAll()
{
    ui->viewDetails->selectAll();
}

void TrackDetailPane::selectNone()
{
    ui->viewDetails->clearSelection();
}

void TrackDetailPane::newConfig()
{
    Pane::newConfig();

    QTreeView& view = *ui->viewDetails;

    subtreeFilter.setFilterCaseSensitivity(cfgData().getFilterCaseSensitivity());

    view.setIconSize(cfgData().iconSizeTrack);
}

void TrackDetailPane::resizeToFit(int defer)
{
    if (defer < 0)
        resizeDeferredHook();
    else
        resizeTimer.start(defer);
}

void TrackDetailPane::resizeDeferredHook()
{
    Util::ResizeViewForData(*ui->viewDetails);
}

void TrackDetailPane::currentTrackChanged(const QModelIndex& current)
{
    // If lock button is selected, keep current selection.
    if (ui->lockToCurrentButton->isChecked())
        return;

    detailsFilter.currentItemChanged(current);

    // After a small delay, resize columns (so we don't spam the resize each time)
    resizeToFit(500);  // expand the tree after a little bit of idle time
}


void TrackDetailPane::on_lockToCurrentButton_toggled(bool checked)
{
    ui->lockToCurrentButton->setIcon(checked ? Icons::get("object-locked") :
                                               Icons::get("object-unlocked"));
}

// Annoyingly, we cannot expand tree nodes during construction or loading (the viewer
// widget simply ignores the request), so we defer it here and trigger this by a timer.
// Ungainly.
void TrackDetailPane::reexpandTree()
{
    if (expandState.empty()) {
        expandAll();
        return;
    }

    for (auto& [key, expanded] : expandState)
        if (const QModelIndex headerIdx = detailsFilter.headerIndex(key); headerIdx.isValid())
            ui->viewDetails->setExpanded(Util::MapUp(&subtreeFilter, headerIdx), expanded);

    expandState.clear();
}

void TrackDetailPane::save(QSettings& settings) const
{
    Pane::save(settings);

    SL::Save(settings, "filterDetails", ui->filterDetails->text());

    // Save expansion state for each header node in the detail view.
    settings.beginGroup("Expanded"); {
        Util::recurse(detailsFilter, [this, &settings](const QModelIndex& idx) {
            if (detailsFilter.hasChildren(idx)) {
                const bool isExpanded = ui->viewDetails->isExpanded(Util::MapUp(&subtreeFilter, idx));
                const QString headerName = detailsFilter.data(idx).toString();

                SL::Save(settings, headerName, isExpanded);
            }
        });
    } settings.endGroup();

    // TODO: for locked state, save which container we're locked to, and restore that on load.
}

void TrackDetailPane::load(QSettings& settings)
{
    Pane::load(settings);

    ui->filterDetails->setText(SL::Load<QString>(settings, "filterDetails"));

    // Restore expansion state for each header node in the detail view.  We can't do the
    // expansion here: see comment in reexpandHeaders.
    settings.beginGroup("Expanded"); {
        const QStringList keys = settings.childKeys();

        for (const auto& key : keys) {
            bool expanded = true;
            SL::Load(settings, key, expanded);
            expandState[key] = expanded;
        }
    } settings.endGroup();
}
