/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PANE_H
#define PANE_H

#include <src/ui/panes/panebase.h>
#include "panegroup.h"
#include "src/core/cfgdata.h"

class QModelIndex;
class MainWindow;
class QSplitter;

// Update this when adding more pane types
enum class PaneClass {
    _First = 0,
    Empty     = _First,    // empty (help/info) pane
    Map,                   // map pane
    Filter,                // track categories
    View,                  // view presets
    Track,                 // GPS track pane
    TrackDetail,           // GPS track detail pane
    Points,                // GPS points pane
    LineChart,             // GPS track line graph for single track
    CmpChart,              // GPS track data comparisons
    GpsDevice,             // GPS device pane
    _Count,

    Group = 65536, // avoid namespace used by the main pane classes
};

Q_DECLARE_METATYPE(PaneClass)
QDataStream& operator<<(QDataStream& out, const PaneClass& pc);
QDataStream& operator>>(QDataStream& in, PaneClass& pc);

// Class for operations provided by UI panels
class Pane : public PaneBase
{
    Q_OBJECT

public:
    typedef PaneGroup Container;

    Pane(MainWindow& mainWindow, PaneClass paneClass, QWidget* parent);

    template <typename T>
    static T* factory(PaneClass pc, MainWindowBase& mainWindow) {
        return dynamic_cast<T*>(widgetFactory(pc, mainWindow));
    }

    // Static things we can ask about a pane action or class.
    static QString   name(PaneClass pc);
    static PaneClass findClass(const QString& className); // name to class
    static QString   tooltip(PaneClass pc);
    static QString   whatsthis(PaneClass pc);
    static QString   statusTip(PaneClass pc);
    static QIcon     icon(PaneClass pc);

protected:
    friend class PaneGroupBase;
    const CfgData& cfgData() const override;
    const MainWindow& mainWindow() const;
    MainWindow& mainWindow();

    static QWidget* widgetFactory(PaneClass pc, MainWindowBase& mainWindow);

private:
    static Container* newContainer(MainWindowBase& mainWindow);

    friend class PaneGroup;

    Pane(const Pane&) = delete;
    Pane& operator=(const Pane&) = delete;
};

#endif // PANE_H
