/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <type_traits>
#include <QMenu>
#include <QWidget>

#include "pane.h"
#include <src/util/util.h>
#include <src/util/icons.h>
#include "src/core/cfgdata.h"
#include "src/ui/windows/mainwindow.h"
#include <src/ui/panes/emptypane.h>
#include "src/ui/panes/filterpane.h"
#include "src/ui/panes/pointpane.h"
#include "src/ui/panes/mappane.h"
#include "src/ui/panes/viewpane.h"
#include "src/ui/panes/trackpane.h"
#include "src/ui/panes/trackdetailpane.h"
#include "src/ui/panes/tracklinepane.h"
#include "src/ui/panes/trackcmppane.h"
#include "src/ui/panes/gpsdevicepane.h"

Pane::Pane(MainWindow& mainWindow, PaneClass paneClass, QWidget* parent) :
    PaneBase(mainWindow, int(paneClass), parent)
{
}

const CfgData &Pane::cfgData() const
{
    return mainWindow().cfgData();
}

const MainWindow& Pane::mainWindow() const
{
    return static_cast<const MainWindow&>(PaneBase::mainWindow());
}

MainWindow& Pane::mainWindow()
{
    return static_cast<MainWindow&>(PaneBase::mainWindow());
}

QString Pane::name(PaneClass pc)
{
    switch (pc) {
    case PaneClass::Empty:          return QObject::tr("Empty Pane");
    case PaneClass::Map:            return QObject::tr("Map Display");
    case PaneClass::Filter:         return QObject::tr("GPS Track Filter");
    case PaneClass::View:           return QObject::tr("View Presets");
    case PaneClass::Track:          return QObject::tr("GPS Track List");
    case PaneClass::TrackDetail:    return QObject::tr("GPS Track Detail");
    case PaneClass::Points:         return QObject::tr("GPS Track Points");
    case PaneClass::LineChart:      return QObject::tr("Single Track Line Chart");
    case PaneClass::CmpChart:       return QObject::tr("Multi Track Bar Chart");
    case PaneClass::GpsDevice:      return QObject::tr("GPS Devices");
    case PaneClass::Group:          return QObject::tr("Pane Group");
    case PaneClass::_Count:         break;
    }

    assert(0);
    return QObject::tr("n/a");
}

PaneClass Pane::findClass(const QString& className)
{
    for (PaneClass pc = PaneClass::_First; pc < PaneClass::_Count; Util::inc(pc))
        if (name(pc) == className)
            return pc;

    return PaneClass::Empty; // failed to find it
}

QWidget* Pane::widgetFactory(PaneClass pc, MainWindowBase& mainWindow)
{
    // This can't be done in the pane constructor, since the obj is not fully baked yet.
    const auto setClass = [](PaneBase* pane) {
        pane->setObjectName(pane->metaObject()->className());
        return pane;
    };

    MainWindow& mw = static_cast<MainWindow&>(mainWindow);

    switch (pc) {
    case PaneClass::Empty:          return setClass(new EmptyPane(mw, int(PaneClass::Empty)));
    case PaneClass::Map:            return setClass(new MapPane(mw));
    case PaneClass::Filter:         return setClass(new FilterPane(mw));
    case PaneClass::View:           return setClass(new ViewPane(mw));
    case PaneClass::Track:          return setClass(new TrackPane(mw));
    case PaneClass::TrackDetail:    return setClass(new TrackDetailPane(mw));
    case PaneClass::Points:         return setClass(new PointPane(mw));
    case PaneClass::LineChart:      return setClass(new TrackLinePane(mw));
    case PaneClass::CmpChart:       return setClass(new TrackCmpPane(mw));
    case PaneClass::GpsDevice:      return setClass(new GpsDevicePane(mw));
    case PaneClass::Group:          return newContainer(mainWindow);
    case PaneClass::_Count:         break;
    }

    assert(0);
    return nullptr;
}

// create, set up, and return new pane container
Pane::Container *Pane::newContainer(MainWindowBase& mainWindow)
{
    Pane::Container* splitter = new Pane::Container(mainWindow);

    splitter->setChildrenCollapsible(false); // we allow removing children, but not collapsing.
    splitter->setObjectName(containerName);

    return splitter;
}

QString Pane::tooltip(PaneClass pc)
{
    switch (pc) {
    case PaneClass::Empty:          return QObject::tr("Displays nothing.");
    case PaneClass::Map:            return QObject::tr("Primary map display.");
    case PaneClass::Filter:         return QObject::tr("Query filters for GPS tracks.");
    case PaneClass::View:           return QObject::tr("Map view presets.");
    case PaneClass::Track:          return QObject::tr("Display list of GPS tracks.");
    case PaneClass::TrackDetail:    return QObject::tr("Display detailed information about a single GPS track.");
    case PaneClass::Points:         return QObject::tr("Display data about GPS track sample points.");
    case PaneClass::LineChart:      return QObject::tr("Line chart for data from a single GPS track.");
    case PaneClass::CmpChart:       return QObject::tr("Bar chart to compare data over multiple GPS tracks.");
    case PaneClass::GpsDevice:      return QObject::tr("Show USB connected GPS devices.");
    case PaneClass::Group:          return QObject::tr("Horizontal or vertical group of panes.");
    case PaneClass::_Count:         break;
    }

    assert(0);
    return QObject::tr("n/a");
}

QIcon Pane::icon(PaneClass pc)
{
    switch (pc) {
    case PaneClass::Empty:          return Icons::get("window-new");
    case PaneClass::Map:            return Icons::get("application-x-marble");
    case PaneClass::Filter:         return Icons::get("view-filter");
    case PaneClass::View:           return Icons::get("map-flat");
    case PaneClass::Track:          return Icons::get("view-list-tree");
    case PaneClass::TrackDetail:    return Icons::get("view-list-details");
    case PaneClass::Points:         return Icons::get("view-list-details");
    case PaneClass::LineChart:      return Icons::get("office-chart-line");
    case PaneClass::CmpChart:       return Icons::get("office-chart-bar");
    case PaneClass::GpsDevice:      return Icons::get("kstars_satellites");
    case PaneClass::Group:          return Icons::get("window-new");
    case PaneClass::_Count:         break;
    }

    assert(0);
    return Icons::get("window-new");
}

QString Pane::whatsthis(PaneClass pc)
{
    return tooltip(pc);
}

QString Pane::statusTip(PaneClass pc)
{
    return tooltip(pc);
}

QDataStream& operator<<(QDataStream& out, const PaneClass& pc)
{
    return out << std::underlying_type<PaneClass>::type(pc);
}

QDataStream& operator>>(QDataStream& in, PaneClass& pc)
{
    return in >> (std::underlying_type<PaneClass>::type&)(pc);
}
