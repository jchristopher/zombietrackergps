/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IMPORTDIALOG_H
#define IMPORTDIALOG_H

#include <QDialog>
#include <src/core/settings.h>

class TagSelector;
class CfgData;

namespace Ui {
class ImportDialog;
}

class ImportDialog final : public QDialog, public Settings
{
    Q_OBJECT

public:
    explicit ImportDialog(const CfgData& cfgData, QWidget *parent = 0);
    ~ImportDialog();

    QStringList tags() const;
    QColor  trackColor() const; // returns invalid color if no override
    bool    deduplicate() const;

    QStringList getImportFilenames(const char* filter);

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

private slots:
    void askTrackColor();

private:
    void setupSignals();

    Ui::ImportDialog *ui;
    TagSelector*      tagSelector; // UI adopts, so we don't free it.
    QString           defaultDir;  // which directory to show
};

#endif // IMPORTDIALOG_H
