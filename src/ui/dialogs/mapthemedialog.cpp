/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QStandardItemModel>

#include "mapthemedialog.h"
#include "ui_mapthemedialog.h"

MapThemeDialog::MapThemeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MapThemeDialog),
    mapThemeManager(this)
{
    ui->setupUi(this);

    setupView();
    setupSignals();
}

void MapThemeDialog::setupView()
{
    if (ui == nullptr || ui->mapThemeView == nullptr)
        return;

    QTreeView& view = *ui->mapThemeView;

    view.setModel(mapThemeManager.mapThemeModel());
    view.setAlternatingRowColors(true);
    view.setSelectionBehavior(QAbstractItemView::SelectRows);
    view.setSelectionMode(QAbstractItemView::SingleSelection);
    view.setRootIsDecorated(false);  // act like a list
    view.setEditTriggers(QAbstractItemView::NoEditTriggers);
    view.setHeaderHidden(true);
    view.setUniformRowHeights(true);

    const float iconSize = view.fontInfo().pointSizeF() * 8.0;
    view.setIconSize(QSize(iconSize, iconSize));
}

void MapThemeDialog::setupSignals()
{
    QTreeView& view = *ui->mapThemeView;

    connect(&view, &QTreeView::doubleClicked, this, &MapThemeDialog::selectThemeIndex);
}

MapThemeDialog::~MapThemeDialog()
{
    delete ui;
}

void MapThemeDialog::selectThemeIndex(const QModelIndex& index)
{
    emit themeSelected(mapThemeManager.mapThemeIds()[index.row()]);
}
