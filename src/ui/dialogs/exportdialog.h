/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <QDialog>
#include <src/core/settings.h>

#include "src/geo-io/geoio.h" // for GeoFormat

namespace Ui {
class ExportDialog;
}

class ExportDialog final : public QDialog, public Settings
{
    Q_OBJECT

public:
    explicit ExportDialog(QWidget *parent = 0);
    ~ExportDialog();

    GeoFormat exportFormat() const;
    bool      writeFormatted() const;
    int       indentLevel() const;
    bool      indentSpaces() const;
    bool      exportAll() const;

    QString   getExportFileName(const char* filter = nullptr);

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

private:
    Ui::ExportDialog *ui;
    QString           defaultDir;  // which directory to show
};

#endif // EXPORTDIALOG_H
