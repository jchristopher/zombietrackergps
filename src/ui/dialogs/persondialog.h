/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PERSONDIALOG_H
#define PERSONDIALOG_H

#include <QDialog>
#include <QHeaderView>

namespace Ui {
class PersonDialog;
}

class CfgData;

class PersonDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PersonDialog(CfgData&, QWidget *parent = 0);
    ~PersonDialog();

    void setValue(const QString& value);
    QString value() const;

private slots:
    void on_personView_doubleClicked(const QModelIndex&);

private:
    void setupView();

    Ui::PersonDialog *ui;
    const CfgData&    cfgData;
    QHeaderView       headerView;
};

#endif // PERSONDIALOG_H
