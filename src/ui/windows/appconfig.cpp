/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <tuple>
#include <QSet>
#include <QMessageBox>
#include <QWhatsThis>
#include <QAbstractButton>
#include <QPushButton>
#include <QColorDialog>
#include <QFileDialog>
#include <QCompleter>
#include <QFileSystemModel>

#include "mainwindow.h"

#include <src/util/util.h>
#include <src/util/icons.h>
#include <src/util/ui.h>
#include "src/core/trackmodel.h"
#include "src/ui/dialogs/tagrenamedialog.h"
#include "src/ui/widgets/colorizereditor.h"
#include "appconfig.h"
#include "ui_appconfig.h"

AppConfig::AppConfig(MainWindow* parent) :
    QDialog(parent),
    cfgData(*parent),
    prevCfgData(*parent),
    tags(getCfgData()),
    people(getCfgData()),
    trackColorizer(&parent->trackModel(), getCfgData()),
    pointColorizer(CfgData::emptyPointModel, getCfgData()),
    colorDelegate(this),
    colorAlphaDelegate(this, true),
    pointIconSelector(":art/points", false),
    iconIconSelector(":icons/hicolor", true),
    tagIconSelectorDelegate(":art/tags", true),
    CdADelegate(this, -1.0, 100.0, 2, 0.1, "", " m^2"),
    weightDelegate(this, -1.0, 1e6, 2, 1.0),
    rollResistDelegate(this, -1.0, 10.0, 4, 0.001),
    efficiencyDelegate(this, -1.0, 100.0, 0, 1),
    bioPowerDelegate(this, -1.0, 100.0, 0, 1),
    mediumDelegate(this, TagModel::mediumNames()),
    unitsDelegate(this, Units(Format::SpeedKPH), true),
    tagHeaders(Qt::Horizontal, this),
    peopleHeaders(Qt::Horizontal, this),
    mainWindow(*parent),
    ui(new Ui::AppConfig)
{
    ui->setupUi(this);

    setupTagEditor();
    setupPeopleEditor();
    setupTrackColorizerEditor();
    setupPointColorizerEditor();
    setupUIColors();
    setupTrkPtColors();
    setupUnitsInputs();
    setupSignals();
    setupCompleters();
    setupActionIcons();

    ui->appCfgTabs->setFocus();

    addAction(ui->action_Next_Tab);
    addAction(ui->action_Prev_Tab);
}

AppConfig::~AppConfig()
{
    delete ui;
}

void AppConfig::setupActionIcons()
{
    int tab = 0;
    Icons::defaultIcon(ui->appCfgTabs, tab++, "configure");
    Icons::defaultIcon(ui->appCfgTabs, tab++, "preferences-desktop-icons");
    Icons::defaultIcon(ui->appCfgTabs, tab++, "map-globe");
    Icons::defaultIcon(ui->appCfgTabs, tab++, "tag");
    Icons::defaultIcon(ui->appCfgTabs, tab++, "system-users");
    Icons::defaultIcon(ui->appCfgTabs, tab++, "measure");
    Icons::defaultIcon(ui->appCfgTabs, tab++, "office-chart-bar");
}

void AppConfig::setupTagEditor()
{
    QTreeView& view = *ui->tagTreeView;

    // Most view config is set in the form editor
    view.setModel(&tags);
    view.setHeader(&tagHeaders);

    for (auto& delegate : QVector<std::tuple<ModelType, DelegateBase*>>( {
                { TagModel::Icon,       &tagIconSelectorDelegate },
                { TagModel::Color,      &colorDelegate },
                { TagModel::CdA,        &CdADelegate },
                { TagModel::Weight,     &weightDelegate },
                { TagModel::Efficiency, &efficiencyDelegate },
                { TagModel::RR,         &rollResistDelegate },
                { TagModel::BioPct,     &bioPowerDelegate },
                { TagModel::Medium,     &mediumDelegate },
                { TagModel::UnitSpeed,  &unitsDelegate } } )) {

        std::get<DelegateBase*>(delegate)->setSelector(view.selectionModel());
        view.setItemDelegateForColumn(std::get<ModelType>(delegate),
                                      std::get<DelegateBase*>(delegate));
    }

    tagHeaders.setSectionResizeMode(QHeaderView::Interactive);
    tagHeaders.setDefaultAlignment(Qt::AlignLeft);
    tagHeaders.setSectionsMovable(true);
}

void AppConfig::setupPeopleEditor()
{
    QTreeView& view = *ui->peopleTreeView;

    // Most view config is set in the form editor
    view.setModel(&people);
    view.setHeader(&peopleHeaders);

    weightDelegate.setSelector(view.selectionModel());
    efficiencyDelegate.setSelector(view.selectionModel());
    
    view.setItemDelegateForColumn(PersonModel::Weight,     &weightDelegate);
    view.setItemDelegateForColumn(PersonModel::Efficiency, &efficiencyDelegate);

    peopleHeaders.setSectionResizeMode(QHeaderView::Interactive);
    peopleHeaders.setDefaultAlignment(Qt::AlignLeft);
    peopleHeaders.setSectionsMovable(true);
}

void AppConfig::setupTrackColorizerEditor()
{
    ColorizerEditor* trackColorizerEditor = new ColorizerEditor(cfgData, trackColorizer,
                                                                TrackModel::headersList<TrackModel>());

    ui->appCfgTabs->addTab(trackColorizerEditor, Icons::get("color-profile"), "Track Colorizer");
}


void AppConfig::setupPointColorizerEditor()
{
    ColorizerEditor* pointColorizerEditor = new ColorizerEditor(cfgData, pointColorizer,
                                                                PointModel::headersList<PointModel>());

    ui->appCfgTabs->addTab(pointColorizerEditor, Icons::get("color-profile"), "Point Colorizer");
}

void AppConfig::setupUnitsInputs()
{
    cfgData.unitsTrkLength.addToComboBox(ui->unitsTrkLength, Format::_DistNonAuto);
    cfgData.unitsLegLength.addToComboBox(ui->unitsLegLength, Format::_DistNonAuto);
    cfgData.unitsElevation.addToComboBox(ui->unitsElevation);
    cfgData.unitsClimb.addToComboBox(ui->unitsClimb);
    cfgData.unitsDuration.addToComboBox(ui->unitsDuration);
    cfgData.unitsTrkDate.addToComboBox(ui->unitsTrkDate);
    cfgData.unitsTrkTime.addToComboBox(ui->unitsTrkTime);
    cfgData.unitsPointDate.addToComboBox(ui->unitsPointDate);
    cfgData.unitsLat.addToComboBox(ui->unitsLatLon);
    cfgData.unitsSpeed.addToComboBox(ui->unitsSpeed);
    cfgData.unitsAccel.addToComboBox(ui->unitsAccel);
    cfgData.unitsArea.addToComboBox(ui->unitsArea);
    cfgData.unitsTemp.addToComboBox(ui->unitsTemp);
    cfgData.unitsSlope.addToComboBox(ui->unitsSlope);
    cfgData.unitsPower.addToComboBox(ui->unitsPower);
    cfgData.unitsEnergy.addToComboBox(ui->unitsEnergy);
    cfgData.unitsWeight.addToComboBox(ui->unitsWeight);
    cfgData.unitsPct.addToComboBox(ui->unitsPct);
}

void AppConfig::setupSignals()
{
}

void AppConfig::setupUIColors()
{
    QTreeView& view = *ui->cfgUiColors;

    view.setModel(&uiColor);
    view.setItemDelegateForColumn(UiColorModel::Color, &colorDelegate);

    Util::ResizeViewForData(view);
}

void AppConfig::setupCompleters()
{
    QFileSystemModel* fsModel = new QFileSystemModel();
    fsModel->setRootPath("");
    fsModel->setFilter(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Hidden);
    ui->dataSavePathEdit->setCompleter(new QCompleter(fsModel, this));
}

void AppConfig::setupTrkPtColors()
{
    QTreeView& view = *ui->trkPtColor;

    view.setModel(&trkPtColor);
    view.setItemDelegateForColumn(TrkPtColorModel::Color, &colorAlphaDelegate);

    Util::ResizeViewForData(view);
}

// populate GUI data
void AppConfig::show()
{
    // Remember tag identifiers, so we can batch-rename or remove later if needed
    cfgData.tags.setUniqueIds();

    updateUIFromCfg();
    prevCfgData = cfgData; // save original values
    QDialog::show();
}

void AppConfig::updateUIFromCfg()
{
    if (ui == nullptr)
        return;

    // Cut and paste
    ui->cfgRowSeparator->setText(cfgData.rowSeparator);
    ui->cfgColSeparator->setText(cfgData.colSeparator);

    // Filter behavior
    ui->caseSensitiveFilters->setChecked(cfgData.caseSensitiveFilters);
    ui->caseSensitiveSorting->setChecked(cfgData.caseSensitiveSorting);

    // UI stuff
    ui->cfgWarnOnClose->setChecked(cfgData.warnOnClose);
    ui->cfgWarnOnRemove->setChecked(cfgData.warnOnRemove);
    ui->cfgWarnOnRevert->setChecked(cfgData.warnOnRevert);
    ui->cfgWarnOnExit->setChecked(cfgData.warnOnExit);
    ui->inlineCompletion->setChecked(cfgData.inlineCompletion);
    ui->completionListSize->setValue(cfgData.completionListSize);
    ui->completionListSize->setDisabled(cfgData.inlineCompletion);

    // Convolution filter sizes
    ui->eleFilterSize->setValue(cfgData.eleFilterSize | 0x1);
    ui->locFilterSize->setValue(cfgData.locFilterSize | 0x1);

    // Track line display
    Util::SetTBColor(ui->unassignedTrackColor, cfgData.unassignedTrackColor);
    Util::SetTBColor(ui->outlineTrackColor,    cfgData.outlineTrackColor);

    ui->defaultTrackWidthC->setValue(cfgData.defaultTrackWidthC);
    ui->defaultTrackWidthF->setValue(cfgData.defaultTrackWidthF);
    ui->defaultTrackWidthO->setValue(cfgData.defaultTrackWidthO);
    ui->currentTrackWidthC->setValue(cfgData.currentTrackWidthC);
    ui->currentTrackWidthF->setValue(cfgData.currentTrackWidthF);
    ui->currentTrackWidthO->setValue(cfgData.currentTrackWidthO);
    ui->defaultTrackAlphaC->setValue(cfgData.defaultTrackAlphaC);
    ui->defaultTrackAlphaF->setValue(cfgData.defaultTrackAlphaF);
    ui->currentTrackAlphaC->setValue(cfgData.currentTrackAlphaC);
    ui->currentTrackAlphaF->setValue(cfgData.currentTrackAlphaF);

    // Track point display
    ui->defaultPointIcon->setIcon(QIcon(defaultPointIcon = cfgData.defaultPointIcon));
    ui->defaultPointIconSize->setValue(cfgData.defaultPointIconSize);
    ui->defaultPointIconProx->setValue(cfgData.defaultPointIconProx);
    ui->selectedPointIcon->setIcon(QIcon(selectedPointIcon = cfgData.selectedPointIcon));
    ui->selectedPointIconSize->setValue(cfgData.selectedPointIconSize);
    ui->currentPointIcon->setIcon(QIcon(currentPointIcon = cfgData.currentPointIcon));
    ui->currentPointIconSize->setValue(cfgData.currentPointIconSize);

    // Track Icons
    ui->trackNoteIcon->setIcon(QIcon(trackNoteIcon = cfgData.trackNoteIcon));
    ui->brokenIcon->setIcon(QIcon(brokenIcon = cfgData.brokenIcon));
    ui->filterEmptyIcon->setIcon(QIcon(filterEmptyIconName = cfgData.filterEmptyIconName));
    ui->filterValidIcon->setIcon(QIcon(filterValidIconName = cfgData.filterValidIconName));
    ui->filterInvalidIcon->setIcon(QIcon(filterInvalidIconName = cfgData.filterInvalidIconName));
    ui->colorizeTagIcons->setChecked(cfgData.colorizeTagIcons);
    ui->iconSizeTrack->setValue(cfgData.iconSizeTrack.width());
    ui->iconSizeView->setValue(cfgData.iconSizeView.width());
    ui->iconSizeTag->setValue(cfgData.iconSizeTag.width());
    ui->iconSizeFilter->setValue(cfgData.iconSizeFilter.width());
    ui->maxTrackPaneIcons->setValue(cfgData.maxTrackPaneIcons);

    // Units
    ui->unitsTrkLength->setCurrentIndex(cfgData.unitsTrkLength.rangeIdx(Format::_DistNonAuto));
    ui->unitsLegLength->setCurrentIndex(cfgData.unitsLegLength.rangeIdx(Format::_DistNonAuto));
    ui->unitsDuration->setCurrentIndex(cfgData.unitsDuration.rangeIdx());
    ui->unitsTrkDate->setCurrentIndex(cfgData.unitsTrkDate.rangeIdx());
    ui->unitsTrkTime->setCurrentIndex(cfgData.unitsTrkTime.rangeIdx());
    ui->unitsPointDate->setCurrentIndex(cfgData.unitsPointDate.rangeIdx());
    ui->unitsElevation->setCurrentIndex(cfgData.unitsElevation.rangeIdx());
    ui->unitsLatLon->setCurrentIndex(cfgData.unitsLat.rangeIdx());
    ui->unitsSpeed->setCurrentIndex(cfgData.unitsSpeed.rangeIdx());
    ui->unitsAccel->setCurrentIndex(cfgData.unitsAccel.rangeIdx());
    ui->unitsClimb->setCurrentIndex(cfgData.unitsClimb.rangeIdx());
    ui->unitsArea->setCurrentIndex(cfgData.unitsArea.rangeIdx());
    ui->unitsTemp->setCurrentIndex(cfgData.unitsTemp.rangeIdx());
    ui->unitsSlope->setCurrentIndex(cfgData.unitsSlope.rangeIdx());
    ui->unitsPower->setCurrentIndex(cfgData.unitsPower.rangeIdx());
    ui->unitsEnergy->setCurrentIndex(cfgData.unitsEnergy.rangeIdx());
    ui->unitsWeight->setCurrentIndex(cfgData.unitsWeight.rangeIdx());
    ui->unitsPct->setCurrentIndex(cfgData.unitsPct.rangeIdx());

    ui->unitsFmtTrkLength->setValue(cfgData.unitsTrkLength.precision());
    ui->unitsFmtLegLength->setValue(cfgData.unitsLegLength.precision());
    ui->unitsFmtDuration->setValue(cfgData.unitsDuration.precision());
    ui->unitsFmtElevation->setValue(cfgData.unitsElevation.precision());
    ui->unitsFmtLatLon->setValue(cfgData.unitsLat.precision());
    ui->unitsFmtSpeed->setValue(cfgData.unitsSpeed.precision());
    ui->unitsFmtAccel->setValue(cfgData.unitsAccel.precision());
    ui->unitsFmtClimb->setValue(cfgData.unitsClimb.precision());
    ui->unitsFmtArea->setValue(cfgData.unitsArea.precision());
    ui->unitsFmtTemp->setValue(cfgData.unitsTemp.precision());
    ui->unitsFmtSlope->setValue(cfgData.unitsSlope.precision());
    ui->unitsFmtPower->setValue(cfgData.unitsPower.precision());
    ui->unitsFmtEnergy->setValue(cfgData.unitsEnergy.precision());
    ui->unitsFmtWeight->setValue(cfgData.unitsWeight.precision());
    ui->unitsFmtPct->setValue(cfgData.unitsPct.precision());

    ui->unitsPadDuration->setChecked(cfgData.unitsDuration.leadingZeros());
    ui->unitsPadLatLon->setChecked(cfgData.unitsLat.leadingZeros());

    ui->trackDsUTC->setChecked(cfgData.unitsTrkDate.isUTC());
    ui->pointDsUTC->setChecked(cfgData.unitsPointDate.isUTC());

    ui->trkPtLineWidth->setValue(cfgData.trkPtLineWidth);
    Util::SetTBColor(ui->trkPtMarkerColor, cfgData.trkPtMarkerColor);

    // Backups and files
    ui->backupUICount->setValue(cfgData.backupUICount);
    ui->dataAutosaveInterval->setValue(cfgData.dataAutosaveInterval);
    ui->dataSavePathEdit->setText(cfgData.dataAutosavePath);

    // Models
    tags           = cfgData.tags;
    people         = cfgData.people;
    trackColorizer = cfgData.trackColorizer;
    pointColorizer   = cfgData.pointColorizer;
    uiColor        = cfgData.uiColor;
    trkPtColor     = cfgData.trkPtColor;

    // Our own icon size!
    ui->tagTreeView->setIconSize(cfgData.iconSizeTag);

    // Our own delegate suffixes
    efficiencyDelegate.setSuffix(QString(" ") + cfgData.unitsPct.suffix(0.5));
    bioPowerDelegate.setSuffix(QString(" ") + cfgData.unitsPct.suffix(0.5));
    weightDelegate.setSuffix(QString(" ") + cfgData.unitsWeight.suffix(100.0_kg));

    cfgData.applyGlobal();

    ui->tagTreeView->expandAll();
    Util::ResizeViewForData(*ui->tagTreeView);
}

void AppConfig::updateCfgFromUI()
{
    if (ui == nullptr)
        return;

    // Cut and paste
    cfgData.rowSeparator         = ui->cfgRowSeparator->text();
    cfgData.colSeparator         = ui->cfgColSeparator->text();

    // Filter behavior
    cfgData.caseSensitiveFilters = ui->caseSensitiveFilters->isChecked();
    cfgData.caseSensitiveSorting = ui->caseSensitiveSorting->isChecked();

    // UI stuff
    cfgData.warnOnClose          = ui->cfgWarnOnClose->isChecked();
    cfgData.warnOnRemove         = ui->cfgWarnOnRemove->isChecked();
    cfgData.warnOnRevert         = ui->cfgWarnOnRevert->isChecked();
    cfgData.warnOnExit           = ui->cfgWarnOnExit->isChecked();
    cfgData.inlineCompletion     = ui->inlineCompletion->isChecked();
    cfgData.completionListSize   = ui->completionListSize->value();

    // Convolution filter sizes
    cfgData.eleFilterSize        = ui->eleFilterSize->value();
    cfgData.locFilterSize        = ui->locFilterSize->value();

    // Track line display.
    cfgData.unassignedTrackColor = Util::GetTBColor(ui->unassignedTrackColor);
    cfgData.outlineTrackColor    = Util::GetTBColor(ui->outlineTrackColor);

    cfgData.defaultTrackWidthC = ui->defaultTrackWidthC->value();
    cfgData.defaultTrackWidthF = ui->defaultTrackWidthF->value();
    cfgData.defaultTrackWidthO = ui->defaultTrackWidthO->value();
    cfgData.currentTrackWidthC = ui->currentTrackWidthC->value();
    cfgData.currentTrackWidthF = ui->currentTrackWidthF->value();
    cfgData.currentTrackWidthO = ui->currentTrackWidthO->value();
    cfgData.defaultTrackAlphaC = ui->defaultTrackAlphaC->value();
    cfgData.defaultTrackAlphaF = ui->defaultTrackAlphaF->value();
    cfgData.currentTrackAlphaC = ui->currentTrackAlphaC->value();
    cfgData.currentTrackAlphaF = ui->currentTrackAlphaF->value();

    // Track point display
    cfgData.defaultPointIcon      = defaultPointIcon;
    cfgData.defaultPointIconSize  = ui->defaultPointIconSize->value();
    cfgData.defaultPointIconProx  = ui->defaultPointIconProx->value();
    cfgData.selectedPointIcon     = selectedPointIcon;
    cfgData.selectedPointIconSize = ui->selectedPointIconSize->value();
    cfgData.currentPointIcon      = currentPointIcon;
    cfgData.currentPointIconSize  = ui->currentPointIconSize->value();
    cfgData.maxTrackPaneIcons   = ui->maxTrackPaneIcons->value();

    // If this state changed, we have to clear the colorization cached.
    if (cfgData.colorizeTagIcons != ui->colorizeTagIcons->isChecked())
        cfgData.svgColorizer.clear();

    // Track Icons
    cfgData.trackNoteIcon         = trackNoteIcon;
    cfgData.brokenIcon            = brokenIcon;
    cfgData.filterEmptyIconName   = filterEmptyIconName;
    cfgData.filterValidIconName   = filterValidIconName;
    cfgData.filterInvalidIconName = filterInvalidIconName;
    cfgData.colorizeTagIcons      = ui->colorizeTagIcons->isChecked();
    cfgData.iconSizeTrack         = QSize(ui->iconSizeTrack->value(),
                                          ui->iconSizeTrack->value() * 100 / 145);
    cfgData.iconSizeView          = QSize(ui->iconSizeView->value(),
                                          ui->iconSizeView->value() * 100 / 145);
    cfgData.iconSizeTag           = QSize(ui->iconSizeTag->value(),
                                          ui->iconSizeTag->value() * 100 / 145);
    cfgData.iconSizeFilter        = QSize(ui->iconSizeFilter->value(),
                                          ui->iconSizeFilter->value() * 100 / 145);

    // Units
    cfgData.unitsTrkLength.setIdx(ui->unitsTrkLength->currentIndex(), Format::_DistNonAuto);
    cfgData.unitsLegLength.setIdx(ui->unitsLegLength->currentIndex(), Format::_DistNonAuto);
    cfgData.unitsDuration.setIdx(ui->unitsDuration->currentIndex());
    cfgData.unitsTrkDate.setIdx(ui->unitsTrkDate->currentIndex());
    cfgData.unitsTrkTime.setIdx(ui->unitsTrkTime->currentIndex());
    cfgData.unitsPointDate.setIdx(ui->unitsPointDate->currentIndex());
    cfgData.unitsElevation.setIdx(ui->unitsElevation->currentIndex());
    cfgData.unitsLat.setIdx(ui->unitsLatLon->currentIndex());
    cfgData.unitsLon.setIdx(ui->unitsLatLon->currentIndex());
    cfgData.unitsSpeed.setIdx(ui->unitsSpeed->currentIndex());
    cfgData.unitsAccel.setIdx(ui->unitsAccel->currentIndex());
    cfgData.unitsClimb.setIdx(ui->unitsClimb->currentIndex());
    cfgData.unitsArea.setIdx(ui->unitsArea->currentIndex());
    cfgData.unitsTemp.setIdx(ui->unitsTemp->currentIndex());
    cfgData.unitsSlope.setIdx(ui->unitsSlope->currentIndex());
    cfgData.unitsPower.setIdx(ui->unitsPower->currentIndex());
    cfgData.unitsEnergy.setIdx(ui->unitsEnergy->currentIndex());
    cfgData.unitsWeight.setIdx(ui->unitsWeight->currentIndex());
    cfgData.unitsPct.setIdx(ui->unitsPct->currentIndex());

    cfgData.unitsTrkLength.setPrecision(ui->unitsFmtTrkLength->value());
    cfgData.unitsLegLength.setPrecision(ui->unitsFmtLegLength->value());
    cfgData.unitsDuration.setPrecision(ui->unitsFmtDuration->value());
    cfgData.unitsElevation.setPrecision(ui->unitsFmtElevation->value());
    cfgData.unitsLat.setPrecision(ui->unitsFmtLatLon->value());
    cfgData.unitsLon.setPrecision(ui->unitsFmtLatLon->value());
    cfgData.unitsSpeed.setPrecision(ui->unitsFmtSpeed->value());
    cfgData.unitsAccel.setPrecision(ui->unitsFmtAccel->value());
    cfgData.unitsClimb.setPrecision(ui->unitsFmtClimb->value());
    cfgData.unitsArea.setPrecision(ui->unitsFmtArea->value());
    cfgData.unitsTemp.setPrecision(ui->unitsFmtTemp->value());
    cfgData.unitsSlope.setPrecision(ui->unitsFmtSlope->value());
    cfgData.unitsPower.setPrecision(ui->unitsFmtPower->value());
    cfgData.unitsEnergy.setPrecision(ui->unitsFmtEnergy->value());
    cfgData.unitsWeight.setPrecision(ui->unitsFmtWeight->value());
    cfgData.unitsPct.setPrecision(ui->unitsFmtPct->value());

    cfgData.unitsDuration.setLeadingZeros(ui->unitsPadDuration->isChecked());
    cfgData.unitsLat.setLeadingZeros(ui->unitsPadLatLon->isChecked());
    cfgData.unitsLon.setLeadingZeros(ui->unitsPadLatLon->isChecked());

    cfgData.unitsTrkDate.setUTC(ui->trackDsUTC->isChecked());
    cfgData.unitsTrkTime.setUTC(ui->trackDsUTC->isChecked()); // TIMEstamp UTC from DATEstamp config
    cfgData.unitsPointDate.setUTC(ui->pointDsUTC->isChecked());

    cfgData.trkPtLineWidth   = ui->trkPtLineWidth->value();
    cfgData.trkPtMarkerColor = Util::GetTBColor(ui->trkPtMarkerColor);

    // Backups and files
    cfgData.backupUICount        = ui->backupUICount->value();
    cfgData.dataAutosaveInterval = ui->dataAutosaveInterval->value();
    cfgData.dataAutosavePath     = ui->dataSavePathEdit->text();

    // Models
    cfgData.tags           = tags;
    cfgData.people         = people;
    cfgData.trackColorizer = trackColorizer;
    cfgData.pointColorizer   = pointColorizer;
    cfgData.uiColor        = uiColor;
    cfgData.trkPtColor     = trkPtColor;

    // Our own icon size!
    ui->tagTreeView->setIconSize(cfgData.iconSizeTag);

    cfgData.applyGlobal();
}

void AppConfig::reset()
{
    cfgData.reset();
    updateUIFromCfg();
    mainWindow.newConfig();
}

void AppConfig::resetDefaultDialog()
{
    QMessageBox msg(QMessageBox::Warning,
                    tr("Reset to Defaults?"),
                    tr("Reset to defaults?  This will reset your current configuration."), 0, this);

    msg.addButton(tr("&Reset"), QMessageBox::AcceptRole);
    msg.addButton(tr("&Cancel"), QMessageBox::RejectRole);

    if (msg.exec() == QMessageBox::AcceptRole)
        reset();
}

void AppConfig::resetPreviousDialog()
{
    QMessageBox msg(QMessageBox::Warning,
                    tr("Reset to Previous?"),
                    tr("Reset to previous configuration?  This will erase your current configuration."), 0, this);

    msg.addButton(tr("&Reset"), QMessageBox::AcceptRole);
    msg.addButton(tr("&Cancel"), QMessageBox::RejectRole);

    if (msg.exec() == QMessageBox::AcceptRole) {
        cfgData = prevCfgData; // restore original values
        updateUIFromCfg();
        mainWindow.newConfig();
    }
}

void AppConfig::on_appCfgButtons_clicked(QAbstractButton *button)
{
    if (button == ui->appCfgButtons->button(QDialogButtonBox::StandardButton::RestoreDefaults))
        resetDefaultDialog();

    if (button == ui->appCfgButtons->button(QDialogButtonBox::StandardButton::Reset))
        resetPreviousDialog();

    if (ui->appCfgButtons->buttonRole(button) == QDialogButtonBox::RejectRole) {
        cfgData = prevCfgData; // restore original values
        mainWindow.newConfig();
        reject();
    }

    // Rename track tags, if needed
    if (ui->appCfgButtons->buttonRole(button) == QDialogButtonBox::AcceptRole) {
        if (!applyTagRenaming())
            return;
        updateCfgFromUI();
        mainWindow.newConfig();
        accept();
    }

    if (ui->appCfgButtons->buttonRole(button) == QDialogButtonBox::ApplyRole) {
        updateCfgFromUI();
        mainWindow.newConfig();
    }

    if (ui->appCfgButtons->buttonRole(button) == QDialogButtonBox::HelpRole)
        QWhatsThis::enterWhatsThisMode();
}

// Apply tag renamings & deletions to tracks
bool AppConfig::applyTagRenaming()
{
    const auto oldToNew = tags.newIdNames(prevCfgData.tags);

    TrackModel& tracks = mainWindow.trackModel();

    // No tracks.
    if (tracks.rowCount() == 0)
        return true;

    // See if there are tags in use, and ask what to do.

    int tracksWithRenaming = 0;
    int tracksWithDeletion = 0;
    QSet<QString> inUseDeleted;
    QSet<QString> inUseRenamed;

    Util::recurse(tracks, [&](const QModelIndex& idx) {  // For each track
        QStringList tags = tracks.data(TrackModel::Tags, idx, Util::RawDataRole).value<QStringList>();
        for (auto tag = tags.begin(); tag != tags.end(); ++tag) {  // for each tag in track
            if (const auto it = oldToNew.find(*tag); it != oldToNew.end()) { // see if it's in rename set
                if (it.value().isNull()) {
                    inUseDeleted.insert(it.key());
                    ++tracksWithDeletion;
                } else {
                    inUseRenamed.insert(it.key());
                    ++tracksWithRenaming;
                }
            }
        }
    });

    // Nothing matters to the tracks we have.
    if (tracksWithRenaming == 0 && tracksWithDeletion == 0)
        return true;

    TagRenameDialog renameOpt(inUseRenamed, inUseDeleted, this);
    if (renameOpt.exec() != QDialog::Accepted)
        return false;

    if (renameOpt.isNoOp())
        return true;

    // Batch-rename track tags.  This is a little expensive.
    MainWindow::SaveCursor cursor(mainWindow, Qt::WaitCursor);

    Util::recurse(tracks, [&tracks, &renameOpt, &oldToNew](const QModelIndex& idx) {  // For each track
        QStringList tags = tracks.data(TrackModel::Tags, idx, Util::RawDataRole).value<QStringList>();
        bool updated = false;

        for (auto tag = tags.begin(); tag != tags.end(); ) {  // for each tag in track
            bool deleted = false;

            if (const auto it = oldToNew.find(*tag); it != oldToNew.end()) { // see if it's in rename set
                if (!it.value().isNull()) {
                    if (renameOpt.tagRenameUpdate()) {
                        *tag = it.value();   // it was renamed: update the name.
                        updated = true;
                    } else if (renameOpt.tagRenameRemove()) {
                        tag = tags.erase(tag); // it was renamed: requested removal.
                        deleted = updated = true;
                    }
                } else {
                    if (renameOpt.tagRemoveRemove()) {
                        // TODO: option to apply other tag
                        tag = tags.erase(tag); // it was removed: erase it.
                        deleted = updated = true;
                    }
                }
            }

            if (!deleted)
                ++tag;
        }

        // Replace tag list, if we made any changes
        if (updated) {
            tags.removeDuplicates();
            tracks.setData(TrackModel::Tags, idx, tags, Util::RawDataRole);
        }
    });

    return true;
}

void AppConfig::addNewTag(bool category, const TreeItem::ItemData& data)
{
    const auto addAndFocus = [this, category, &data](const QModelIndex& parent) {
        tags.appendRow(category, data, parent);
        const QModelIndex newIdx = tags.index(tags.rowCount(parent) - 1, 0, parent);
        ui->tagTreeView->setCurrentIndex(newIdx);
        ui->tagTreeView->edit(newIdx); // enter edit mode
    };

    const QModelIndex currentSeletion = ui->tagTreeView->currentIndex();
    const QModelIndex currentModel = Util::MapDown(currentSeletion);

    ui->tagTreeView->setExpanded(currentSeletion, true);

    if (!currentModel.isValid() || tags.isCategory(currentModel)) {
        addAndFocus(currentModel);
        return;
    }

    addAndFocus(tags.parent(currentModel));
}

void AppConfig::on_addTag_clicked()
{
    addNewTag(false);
}

void AppConfig::on_addTagHeader_clicked()
{
    addNewTag(true, {"New Category"});
}

void AppConfig::on_delTag_clicked()
{
    if (ui->tagTreeView->selectionModel() == nullptr)
        return;

    tags.removeRows(ui->tagTreeView->selectionModel());
}

void AppConfig::getIcon(IconSelector& selector, QString& file, QToolButton* b)
{
    selector.setGeometry(Util::MapOnScreen(b, b->pos(), selector.size()));
    selector.setCurrentPath(file);

    if (selector.exec() == QDialog::Accepted) {
        b->setIcon(selector.icon());
        file = selector.iconFile();
    }
}

void AppConfig::on_defaultPointIcon_clicked()
{
    getIcon(pointIconSelector, defaultPointIcon, ui->defaultPointIcon);
}

void AppConfig::on_selectedPointIcon_clicked()
{
    getIcon(pointIconSelector, selectedPointIcon, ui->selectedPointIcon);
}

void AppConfig::on_currentPointIcon_clicked()
{
    getIcon(pointIconSelector, currentPointIcon, ui->currentPointIcon);
}

void AppConfig::on_action_Next_Tab_triggered()
{
    Util::NextTab(ui->appCfgTabs);
}

void AppConfig::on_action_Prev_Tab_triggered()
{
    Util::PrevTab(ui->appCfgTabs);
}

void AppConfig::on_trackNoteIcon_clicked()
{
    getIcon(iconIconSelector, trackNoteIcon, ui->trackNoteIcon);
}

void AppConfig::on_brokenIcon_clicked()
{
    getIcon(iconIconSelector, brokenIcon, ui->brokenIcon);
}

void AppConfig::on_peopleAdd_clicked()
{
    people.appendRow({"New Person", 60.0_kg, 0.22});
    const QModelIndex newIdx = people.index(people.rowCount() - 1, 0);
    ui->peopleTreeView->setCurrentIndex(newIdx);
    ui->peopleTreeView->edit(newIdx); // enter edit mode
}

void AppConfig::on_peopleRemove_clicked()
{
    if (ui->peopleTreeView->selectionModel() == nullptr)
        return;

    people.removeRows(ui->peopleTreeView->selectionModel());
}

void AppConfig::on_filterValidIcon_clicked()
{
    getIcon(iconIconSelector, filterValidIconName, ui->filterValidIcon);
}

void AppConfig::on_filterInvalidIcon_clicked()
{
    getIcon(iconIconSelector, filterInvalidIconName, ui->filterInvalidIcon);
}

void AppConfig::on_filterEmptyIcon_clicked()
{
    getIcon(iconIconSelector, filterEmptyIconName, ui->filterEmptyIcon);
}

void AppConfig::on_trkPtMarkerColor_clicked()
{
    Util::SetTBColor(ui->trkPtMarkerColor,
               QColorDialog::getColor(cfgData.trkPtMarkerColor,
                                      this, tr("Track Line Pane marker color")));
}

void AppConfig::on_outlineTrackColor_clicked()
{
    Util::SetTBColor(ui->outlineTrackColor,
               QColorDialog::getColor(cfgData.outlineTrackColor,
                                      this, tr("Outline track color")));
}

void AppConfig::on_unassignedTrackColor_clicked()
{
    Util::SetTBColor(ui->unassignedTrackColor,
               QColorDialog::getColor(cfgData.unassignedTrackColor,
                                      this, tr("Unassigned track color")));
}

void AppConfig::on_dataSavePathSelect_clicked()
{
    const QString showDir = ui->dataSavePathEdit->text().isEmpty() ?
                mainWindow.currentSettingsDirectory() :
                ui->dataSavePathEdit->text();

    const QString dir = QFileDialog::getExistingDirectory(this, "GPS Data Save Directory", showDir);

    if (dir.isEmpty()) {
        mainWindow.statusMessage(UiType::Warning, tr("Canceled."));
        return;
    }

    ui->dataSavePathEdit->setText(dir);
}
