/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef APPCONFIG_H
#define APPCONFIG_H

#include <QDialog>
#include <QHeaderView>
#include <QSortFilterProxyModel>

#include <src/ui/widgets/colordelegate.h>
#include <src/ui/widgets/comboboxdelegate.h>
#include <src/ui/widgets/doublespindelegate.h>
#include <src/ui/widgets/iconselectordelegate.h>
#include <src/ui/widgets/unitsdelegate.h>
#include <src/ui/dialogs/iconselector.h>
#include <src/core/colorizermodel.h>
#include "src/core/cfgdata.h"

namespace Ui {
class AppConfig;
}

class MainWindow;
class QAbstractButton;
class QToolButton;

class AppConfig : public QDialog
{
    Q_OBJECT

public:
    explicit AppConfig(MainWindow *parent = nullptr);
    ~AppConfig();

    const CfgData& getCfgData() const { return cfgData; }
    CfgData& getCfgData() { return cfgData; }

    void show();
    void reset();

private slots:
    void on_appCfgButtons_clicked(QAbstractButton *button);
    void on_addTag_clicked();
    void on_addTagHeader_clicked();
    void on_delTag_clicked();
    void on_defaultPointIcon_clicked();
    void on_selectedPointIcon_clicked();
    void on_currentPointIcon_clicked();
    void on_action_Next_Tab_triggered();
    void on_action_Prev_Tab_triggered();
    void on_trackNoteIcon_clicked();
    void on_brokenIcon_clicked();
    void on_peopleAdd_clicked();
    void on_peopleRemove_clicked();
    void on_filterValidIcon_clicked();
    void on_filterInvalidIcon_clicked();
    void on_filterEmptyIcon_clicked();
    void on_trkPtMarkerColor_clicked();
    void on_outlineTrackColor_clicked();
    void on_unassignedTrackColor_clicked();
    void on_dataSavePathSelect_clicked();

private:
    void setupActionIcons();
    void setupTagEditor();
    void setupPeopleEditor();
    void setupTrackColorizerEditor();
    void setupPointColorizerEditor();
    void setupUnitsInputs();
    void setupSignals();
    void setupUIColors();
    void setupCompleters();
    void setupTrkPtColors();
    void resetDefaultDialog();
    void resetPreviousDialog();
    void updateUIFromCfg();
    void updateCfgFromUI();
    void getIcon(IconSelector& selector, QString&, QToolButton*);
    void addNewTag(bool category, const TreeItem::ItemData& = {});
    bool applyTagRenaming();

    CfgData               cfgData;               // current values
    CfgData               prevCfgData;           // initial values, for later rejection

    TagModel              tags;                  // UI doesn't store these things itself:
    PersonModel           people;                // ...
    ColorizerModel        trackColorizer;        // ...
    ColorizerModel        pointColorizer;        // ...
    UiColorModel          uiColor;               // ...
    TrkPtColorModel       trkPtColor;            // ...
    QString               defaultPointIcon;      // QIcon doesn't store the file path...
    QString               selectedPointIcon;     // ...
    QString               currentPointIcon;      // ...
    QString               trackNoteIcon;         // ...
    QString               brokenIcon;            // ...
    QString               filterEmptyIconName;   // ...
    QString               filterValidIconName;   // ...
    QString               filterInvalidIconName; // ...

    ColorDelegate         colorDelegate;
    ColorDelegate         colorAlphaDelegate;
    IconSelector          pointIconSelector;
    IconSelector          iconIconSelector;
    IconSelectorDelegate  tagIconSelectorDelegate;
    DoubleSpinDelegate    CdADelegate;
    DoubleSpinDelegate    weightDelegate;
    DoubleSpinDelegate    rollResistDelegate;
    DoubleSpinDelegate    efficiencyDelegate;
    DoubleSpinDelegate    bioPowerDelegate;
    ComboBoxDelegate      mediumDelegate;
    UnitsDelegate         unitsDelegate;

    QHeaderView           tagHeaders;
    QHeaderView           peopleHeaders;

    MainWindow&           mainWindow;
    Ui::AppConfig*        ui;
};

#endif // APPCONFIG_H
