/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "docdialog.h"

// Fields: indent level, heading text, HTML file
DocDialog::DocDialog(QWidget *parent) :
    DocDialogBase(parent)
{
    setupPageInfo();
    setupTOC();
}

DocDialog::~DocDialog()
{
}

void DocDialog::setupPageInfo()
{
    pageInfo = {
           { 0, "Introduction",       "Intro.html" },
           { 0, "User Interface",     "Interface.html" },
           { 1, "Data Pane Features", "DataPanes.html" },
           { 1, "Status Bar",         "StatusBar.html" },
           { 1, "Color Themes",       "Themes.html" },
           { 0, "Importing GPS Data", nullptr },
           { 1, "From Files",         "ImportFile.html" },
           { 1, "From GPS Devices",   "ImportDevice.html" },
           { 0, "Exporting GPS Data", "ExportFile.html" },
           { 0, "Queries",            nullptr },
           { 1, "Data Queries",       "DataQueries.html" },
           { 1, "Area Searches",      "AreaSearch.html" },
           { 0, "Track Display",      nullptr },
           { 1, "Colors",             "TrackColorization.html" },
           { 1, "Notes",              "TrackNotes.html" },
           { 0, "Panes",              nullptr },
           { 1, "Track",              "TrackPane.html" },
           { 1, "Map",                "MapPane.html" },
           { 1, "Track Points",       "PointPane.html" },
           { 1, "View",               "ViewPane.html" },
           { 1, "Filters",            "FilterPane.html" },
           { 1, "Track Details",      "TrackDetailPane.html" },
           { 1, "Line Chart",         "LinePane.html" },
           { 1, "Track Comparison",   "TrackCmpPane.html" },
           { 1, "GPS Device",         "GpsDevPane.html" },
           { 0, "Configuration",      nullptr },
           { 1, "Offline Mode",       "OfflineMode.html" },
           { 1, "Tags",               "ConfigTags.html" },
           { 1, "Units",              "ConfigUnits.html" },
           { 1, "People",             "ConfigPeople.html" },
           { 1, "Colorizers",         "ConfigColorizer.html" },
           { 0, "Bugs & Limitations", "Bugs.html" },
           { 0, "Roadmap",            "Roadmap.html" },
    };
}
