/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <QApplication>

#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    AboutBase(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    setup(ui->aboutTabs, ui->action_Next_Tab, ui->action_Prev_Tab);

    const QString appTitleText =
            QString("<html><head/><body><p><span style=\" font-size:18pt; font-style:italic;\">") +
            QApplication::applicationDisplayName() + " " + QApplication::applicationVersion() +
            "</span></p></body></html>";

    ui->aboutZTTitle_1->setText(appTitleText);
    ui->aboutZTTitle_2->setText(appTitleText);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
