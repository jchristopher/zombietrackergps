/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <limits>
#include <QIcon>
#include <QPen>
#include <QMouseEvent>
#include <QRect>
#include <marble/GeoPainter.h>
#include <marble/MarbleWidgetInputHandler.h>
#include <marble/GeoDataLatLonBox.h>
#include <marble/MarbleModel.h>
#include <marble/HttpDownloadManager.h>

#include "src/core/trackmodel.h"
#include "src/ui/windows/mainwindow.h"
#include "src/core/cfgdata.h"
#include "src/core/pointmodel.h"
#include <src/util/math.h>
#include <src/util/util.h>

#include "trackmap.h"

TrackMap::TrackMap(MainWindow& mainWindow, TrackModel& trackModel) :
    m_mainWindow(mainWindow),
    m_trackModel(trackModel),
    hqUpdateTimer(this)
{
    setupIcons();
    setupSignals();
    setupTimers();
}

void TrackMap::newConfig()
{
    setupIcons();
    update();
}

const CfgData& TrackMap::cfgData() const
{
    return mainWindow().cfgData();
}

void TrackMap::setupIcons()
{
    const auto scale = [this](const QIcon& icon, int newSize) {
        const QSize actualSize = icon.actualSize(QSize(newSize, newSize));
        return icon.pixmap(actualSize).scaledToWidth(newSize, Qt::SmoothTransformation);
    };

    defaultPointIcon  = scale(QIcon(cfgData().defaultPointIcon),  cfgData().defaultPointIconSize);
    selectedPointIcon = scale(QIcon(cfgData().selectedPointIcon), cfgData().selectedPointIconSize);
    currentPointIcon  = scale(QIcon(cfgData().currentPointIcon),  cfgData().currentPointIconSize);
}

void TrackMap::setupSignals()
{
    connect(&mainWindow(), &MainWindow::currentTrackChanged, this, &TrackMap::currentTrackChanged);
    connect(&mainWindow(), &MainWindow::currentTrackPointChanged, this, &TrackMap::currentPointChanged);
    connect(&mainWindow(), &MainWindow::selectedPointsChanged, this, &TrackMap::selectedPointChanged);
    connect(&mainWindow(), &MainWindow::visibleTracksChanged, this, &TrackMap::visibleTracksChanged);

    connect(this, &TrackMap::viewContextChanged, this, &TrackMap::newViewContext);

    // Progress bar
    connect(model()->downloadManager(), &Marble::HttpDownloadManager::progressChanged, this, &TrackMap::handleProgress);
    connect(model()->downloadManager(), &Marble::HttpDownloadManager::jobRemoved, this, &TrackMap::handleJobRemoved);
}

void TrackMap::setupTimers()
{
    // Wait a little bit after the last active container change, and then resize columns.
    hqUpdateTimer.setSingleShot(true);
    connect(&hqUpdateTimer, &QTimer::timeout, this, &TrackMap::hqUpdate);
}

void TrackMap::setOfflineMode(bool offline)
{
    model()->downloadManager()->setDownloadEnabled(!offline);
}

template <typename T> T TrackMap::get(int mt, const QModelIndex& idx, int role) const
{
    return trackModel().data(mt, idx, role).value<T>();
}

void TrackMap::handleProgress(int active, int queued)
{
    mainWindow().initProgress(active + queued);
}

void TrackMap::handleJobRemoved()
{
    mainWindow().updateProgress(1, true);
}

void TrackMap::customPaint(Marble::GeoPainter* painter)
{
    if (painter == nullptr)
        return;

    drawTrackLines(painter);
    drawTrackPts(painter);
    drawSelectionBox(painter);
}

void TrackMap::newViewContext(Marble::ViewContext newViewContext)
{
    if (newViewContext == Marble::Still)
        update();  // map doesn't always do this when it should.
}

void TrackMap::hqUpdate()
{
    setViewContext(Marble::Still);
    update();  // force refresh
}

void TrackMap::resizeEvent(QResizeEvent* event)
{
    setViewContext(Marble::Animation);
    hqUpdateTimer.start(1500);

    Marble::MarbleWidget::resizeEvent(event);
}

// Draw track points for current track
void TrackMap::drawTrackLines(Marble::GeoPainter *painter)
{
    painter->setCompositionMode(QPainter::CompositionMode_SourceOver);

    // TODO: clamp on max number of drawn visible tracks, especially during movement.

    // Performance is important here.  Don't capture any more stuff.
    Util::recurse(trackModel(), [this, painter](const QModelIndex& idx) {
        if (!isCurrent(idx)) // we'll draw current track last
            drawSingleTrack(painter, idx);
    });

    // Paint current track last so it isn't buried.
    if (currentTrackIdx.isValid())
        drawSingleTrack(painter, currentTrackIdx);
}

// Helper to draw an entire track segment once in a given color and pen width.
inline void TrackMap::drawTrackSeg(Marble::GeoPainter* painter, const QColor& color, float trackWidth,
                                   const TrackSegLines& trackSegLines)
{
    QPen trackPen(color);
    trackPen.setWidthF(trackWidth);

    painter->setPen(trackPen);

    for (const auto& geoLine : trackSegLines)
        painter->drawPolyline(geoLine);
}

void TrackMap::drawSingleTrack(Marble::GeoPainter* painter, const QModelIndex& idx)
{
    static const float movingTrackWidth = 1.0;

    const TrackSegLines& trackSegLines = trackModel().trackLines(*this, idx);

    if (!trackModel().isVisible(*this, idx))
        return;

    // Magic 0.2 is experimentally derived from zooming the map.
    static const float zoomFactor = 0.2;
    const float zoomInterp = std::clamp(float(distance() * zoomFactor), 0.0f, 1.0f);

    const QColor trackColor = get<QColor>(TrackModel::Color, idx, Qt::BackgroundRole);

    const bool current = isCurrent(idx);
    const bool still   = (viewContext() == Marble::Still);

    const int alpha = current ?
                int(Math::mix(float(cfgData().currentTrackAlphaC),
                              float(cfgData().currentTrackAlphaF), zoomInterp)) :
                int(Math::mix(float(cfgData().defaultTrackAlphaC),
                              float(cfgData().defaultTrackAlphaF), zoomInterp));

    const float currentWidthForZoom    = Math::mix(cfgData().currentTrackWidthC,
                                                   cfgData().currentTrackWidthF, zoomInterp);

    const float defaultWidthForZoom    = Math::mix(cfgData().defaultTrackWidthC,
                                                   cfgData().defaultTrackWidthF, zoomInterp);

    const float defaultWidthForContext = still ? defaultWidthForZoom : movingTrackWidth;

    static const float minOutlineWidth = 2.5;

    const float outlineWidth           = current ? cfgData().currentTrackWidthO :
                                         (defaultWidthForContext >= minOutlineWidth) ? cfgData().defaultTrackWidthO : 0.0;

    const float trackWidthForDraw      = current ? currentWidthForZoom : defaultWidthForContext;

    const QColor trackColorDraw   = QColor(trackColor.red(), trackColor.green(), trackColor.blue(), alpha);
    const QColor outlineColorDraw = QColor(cfgData().outlineTrackColor.red(),
                                           cfgData().outlineTrackColor.green(),
                                           cfgData().outlineTrackColor.blue(), alpha);

    // Draw outline first, and main color on top.
    if (outlineWidth > 0.1f)
        drawTrackSeg(painter, outlineColorDraw, outlineWidth + trackWidthForDraw, trackSegLines);

    // Now draw main track in its color.
    drawTrackSeg(painter, trackColorDraw, trackWidthForDraw, trackSegLines);
}

inline void TrackMap::drawSinglePt(Marble::GeoPainter* painter, const PointItem& pt,
                                   qreal screenPointThreshold, const QPixmap& pixmap, bool draw,
                                   bool force)
{
    qreal x, y;

    if (screenCoordinates(pt.lon(), pt.lat(), x, y)) {
        if (!force && !std::isnan(prevX) && Math::distSqr(prevX - x, prevY - y) < screenPointThreshold)
            return;

        const Marble::GeoDataCoordinates coords = pt.as<Marble::GeoDataCoordinates>();
        if (draw)
            painter->drawPixmap(coords, pixmap);
        prevX = x;
        prevY = y;
    }
}

// Draw track points for current track
void TrackMap::drawTrackPts(Marble::GeoPainter* painter)
{
    static const qreal NaN  = std::numeric_limits<qreal>::quiet_NaN();

    const PointModel* geoPoints = trackModel().geoPoints(currentTrackIdx);

    if (geoPoints == nullptr || !currentTrackIdx.isValid() || !trackModel().isVisible(*this, currentTrackIdx))
        return;

    const qreal screenPointThreshold = Math::sqr(cfgData().defaultPointIconProx);

    const int currentRow = currentPointIdx.row(); // for performance
    const PointItem* currentPointItem = nullptr;
    bool  hasSelection = false;

    // To avoid overdrawing the active point with other ones, we render the points in three layers:
    //   Bottom: normal points
    //   Middle: selected points
    //   Top:    current point

    // First, normal points
    for (const auto& trkseg : *geoPoints) {
        prevX = prevY = NaN;  // to avoid drawing points too close together in screenspace.
        int ptNum = 0;
        for (const auto& pt : trkseg) {
            // Attempt to short circuit calling geoPoints.is() if possible, because this is part of the
            // render loop and performance matters.
            const bool isCurrent  = (ptNum++ == currentRow) && geoPoints->is(*this, currentPointIdx, pt);
            const bool isSelected = pt.is(PointItem::Select);
            hasSelection = hasSelection || isSelected;

            drawSinglePt(painter, pt, screenPointThreshold, defaultPointIcon, !isCurrent && !isSelected, false);

            if (isCurrent)
                currentPointItem = &pt;
        }
    }

    // Second, selected points, only if there are any.
    if (hasSelection) {
        for (const auto& trkseg : *geoPoints) {
            prevX = prevY = NaN;  // to avoid drawing points too close together in screenspace.
            for (const auto& pt : trkseg) {
                // The pointer comparison is to avoid calling geoPoints.is() on every single item, because
                // this happens in the draw loop and performance matters here.
                const bool isCurrent  = (&pt == currentPointItem);
                const bool isSelected = pt.is(PointItem::Select);

                drawSinglePt(painter, pt, screenPointThreshold, selectedPointIcon, !isCurrent && isSelected, false);
            }
        }
    }

    // Last, draw current point on top of any other points, so it's easily visible.
    if (currentPointItem != nullptr)
        drawSinglePt(painter, *currentPointItem, screenPointThreshold, currentPointIcon, true, true);
}

void TrackMap::mousePressEvent(QMouseEvent* event)
{
    if (event->modifiers() != Qt::ControlModifier)
        return;

    startSelectRegion(event->pos());
    event->accept();
}

void TrackMap::mouseReleaseEvent(QMouseEvent* event)
{
    if (!regionSelectionStarted())
        return;

    endSelectRegion(event->pos());
}

void TrackMap::mouseMoveEvent(QMouseEvent* event)
{
    if (!regionSelectionStarted())
        return;

    selectionEndCoordinate = event->pos();

    event->accept();
}

Marble::GeoDataCoordinates TrackMap::widgetGeoCoords(const QPoint& point)
{
    qreal lon, lat;
    if (!geoCoordinates(point.x(), point.y(), lon, lat, Marble::GeoDataCoordinates::Radian))
        return Marble::GeoDataCoordinates();

    return Marble::GeoDataCoordinates(lon, lat, 0.0, Marble::GeoDataCoordinates::Radian);
}

void TrackMap::drawSelectionBox(Marble::GeoPainter* painter)
{
    if (!regionSelectionInProgress())
        return;

    const QRect screenRect(selectionStartCoordinate, selectionEndCoordinate);

    const Marble::GeoDataCoordinates centerCoord = widgetGeoCoords(screenRect.center());
    if (!centerCoord.isValid())
        return;

    QPen boxPen(QColor(QRgb(0xff008040)));
    boxPen.setWidth(2);

    painter->setPen(boxPen);
    painter->setBrush(QBrush());

    painter->drawRect(centerCoord, screenRect.width(), screenRect.height(), false);
}

void TrackMap::startSelectRegion(const QPoint& point)
{
    selectionStartCoordinate = point;
    selectionEndCoordinate = QPoint();
}

void TrackMap::endSelectRegion(const QPoint& point)
{
    const Marble::GeoDataCoordinates start = widgetGeoCoords(selectionStartCoordinate);
    const Marble::GeoDataCoordinates end = widgetGeoCoords(point);

    const Marble::GeoDataLatLonBox region(std::max(start.latitude(), end.latitude()),
                                          std::min(start.latitude(), end.latitude()),
                                          std::max(start.longitude(), end.longitude()),
                                          std::min(start.longitude(), end.longitude()),
                                          Marble::GeoDataCoordinates::Radian);

    selectionStartCoordinate = QPoint();  // reset to no drag selection in progress.
    selectionEndCoordinate = QPoint();

    mainWindow().showBoxSelectionDialog(region);
}

void TrackMap::deferredUpdate()
{
    // force low quality refreshes while current track is changing a lot.
    setViewContext(Marble::Animation);
    update();

    // trigger delayed high quality refresh
    hqUpdateTimer.start(500);
}

void TrackMap::currentTrackChanged(const QModelIndex& idx)
{
    currentTrackIdx = trackModel().rowSibling(0, Util::MapDown(idx));
    deferredUpdate();
}

void TrackMap::visibleTracksChanged()
{
    deferredUpdate();
}

void TrackMap::currentPointChanged(const QModelIndex& idx)
{
    currentPointIdx = idx;
    deferredUpdate();
}

void TrackMap::selectedPointChanged()
{
    deferredUpdate();
}

// TODO: At some point, the regionSelected signal from MarbleWidget changed
// between a list of doubles, and a GeoDataLatLonBox.  This should change
// accordingly (the connect() should fail to compile when that happens).
void TrackMap::processRegionSelected(const QList<double>& coords)
{
    const Marble::GeoDataLatLonBox region(coords[1], coords[3], coords[2], coords[0],
            Marble::GeoDataCoordinates::Degree);

    mainWindow().showBoxSelectionDialog(region);
}
