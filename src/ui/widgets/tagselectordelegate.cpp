/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QApplication>

#include <src/core/cfgdata.h>
#include "src/core/tagmodel.h"
#include "src/util/roles.h"
#include "src/util/ui.h"
#include "src/ui/dialogs/tagselectordialog.h"
#include "tagselectordelegate.h"

TagSelectorDelegate::TagSelectorDelegate(QObject* /*parent*/,
                                         const CfgData& cfgData,
                                         const QString &winTitle, bool winBorders, int role) :
    DelegateBase(nullptr, true, winTitle, winBorders, role),
    ellipsisIcon(Util::IsLightTheme() ? ":art/ui/Ellipsis-Light.svg" : ":art/ui/Ellipsis-Dark.svg"),
    cfgData(cfgData)
{
}

QWidget* TagSelectorDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& idx) const
{
    TagSelectorDialog* editor = new TagSelectorDialog(cfgData, parent);
    if (editor == nullptr)
        return nullptr;

    if (const QAbstractItemModel* model = dynamic_cast<const QAbstractItemModel*>(idx.model()); model != nullptr) {
        editor->setTags(model->data(idx, role).value<QStringList>());
        setPopup(editor);
    }

    return editor;
}

void TagSelectorDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const
{
    updateResultFromEditor(editor, model, idx, [&](bool& accepted) {
        TagSelectorDialog* dialog = dynamic_cast<TagSelectorDialog*>(editor);

        accepted = (dialog->result() == QDialog::Accepted);
        return dialog->tags();
    });
}

inline void TagSelectorDelegate::drawIcon(QPainter* painter, const QIcon& icon, const QSize requestSize,
                                          QRect& rect, int fixedRight, const QStyleOptionViewItem& option,
                                          Qt::Alignment align) const
{
    const QSize size = icon.actualSize(requestSize);
    rect.setRight(std::min(rect.left() + size.width(), fixedRight));

    icon.paint(painter, rect, align,
               (option.state & QStyle::State_Selected) ? QIcon::Selected :
               (option.state & QStyle::State_Active)   ? QIcon::Active   : QIcon::Normal,
               (option.state & QStyle::State_Off   )   ? QIcon::Off : QIcon::On);

    rect.setLeft(rect.left() + size.width() + CfgData::iconPad.width());
}

void TagSelectorDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& idx) const
{
    const QAbstractItemModel& model = *idx.model();
    const QVariant tagVar = model.data(idx, Util::RawDataRole);

    if (tagVar.type() == QVariant::StringList) {
        const QStringList& tags = *static_cast<const QStringList*>(tagVar.constData());
        int count = 0;
        QRect rect = option.rect;
        rect.setBottom(rect.bottom() - CfgData::iconPad.height() / 2);
        rect.setTop(rect.top() + CfgData::iconPad.height() / 2);

        const int displayedIconCount = std::min(cfgData.maxTrackPaneIcons, tags.size());

        const bool useEllipsis = tags.size() > cfgData.maxTrackPaneIcons;

        const int fixedRight = rect.right();

        const int widthPerIcon = (rect.width() - (CfgData::iconPad.width() * displayedIconCount) -
                                  (useEllipsis ? cfgData.iconSizeTrack.width() / ellipsisWidthRatio : 0)) /
                std::max(displayedIconCount, 1);

        const QSize requestSize  = QSize(std::min(cfgData.iconSizeTrack.width(), widthPerIcon),
                                         cfgData.iconSizeTrack.height());
        const QSize ellipsisSize = QSize(cfgData.iconSizeTrack.width() / ellipsisWidthRatio, requestSize.height());

        for (const auto& tag : tags) {
            if (++count > cfgData.maxTrackPaneIcons) {
                rect.setBottom(rect.bottom() - ellipsisSize.height() / 8);

                drawIcon(painter, ellipsisIcon, ellipsisSize, rect, fixedRight, option,
                         Qt::AlignLeft | Qt::AlignBottom);
                break;
            }

            if (const QVariant icon = cfgData.tags.value(tag, TagModel::Icon, Qt::DecorationRole);
                    icon.type() == QVariant::Icon)
                drawIcon(painter, icon.value<QIcon>(), requestSize, rect, fixedRight, option);
        }

        return;
    }

    return DelegateBase::paint(painter, option, idx);
}

QSize TagSelectorDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& idx) const
{
    const QAbstractItemModel& model = *idx.model();
    const QVariant tagVar = model.data(idx, Util::RawDataRole);

    if (tagVar.type() == QVariant::StringList) {
        const QStringList& tags = *static_cast<const QStringList*>(tagVar.constData());
        int count = 0;
        QSize hint(0, 0);

        for (const auto& tag : tags) {
            if (++count > cfgData.maxTrackPaneIcons) {
                // Room for ellipsis
                hint.setWidth(hint.width() + cfgData.iconSizeTrack.width() / ellipsisWidthRatio);
                break;
            }

            if (const QVariant icon = cfgData.tags.value(tag, TagModel::Icon, Qt::DecorationRole);
                    icon.type() == QVariant::Icon) {
                const QSize size = icon.value<QIcon>().actualSize(cfgData.iconSizeTrack);
                hint = QSize(hint.width() + size.width() + CfgData::iconPad.width(),
                             std::max(hint.height(), size.height() + CfgData::iconPad.height()));
            }
        }

        return hint;
    }

    return DelegateBase::sizeHint(option, idx);
}
