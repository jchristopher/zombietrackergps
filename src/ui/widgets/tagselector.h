/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TAGSELECTOR_H
#define TAGSELECTOR_H

#include <QWidget>

#include <src/ui/filters/contentfilter.h>
#include "src/core/tagmodel.h"

namespace Ui {
class TagSelector;
}

class CfgData;

class TagSelector final : public QWidget
{
    Q_OBJECT

public:
    explicit TagSelector(const CfgData& cfgData, QWidget *parent = 0);
    ~TagSelector();

    QStringList tags() const;
    void setTags(const QStringList& tags);

private slots:
    void activeDoubleClicked();
    void availDoubleClicked();
    void on_action_Left_triggered();
    void on_action_Right_triggered();
    void on_action_Up_triggered();
    void on_action_Down_triggered();

private:
    void setupActionIcons();
    void setupTagSelectors();
    void setupSignals();
    void setupButtons();
    void moveSelections(int rel);
    void toActive();
    void toAvail();
    void setVisible(bool visible) override;
    void update(); // refresh sizes, filters, and so forth

    Ui::TagSelector   *ui;
    const CfgData&     cfgData;

    TagModel           activeTags;   // these are the active tags we're editing.
    ContentFilter      activeFilter; // filter out items from active set
};

#endif // TAGSELECTOR_H
