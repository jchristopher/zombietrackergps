/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOBASE_H
#define GEOIOBASE_H

#include <QObject>
#include <QModelIndex>
#include <QString>
#include <QFile>

#include "geoio.h"

class GeoLoad;
class GeoSave;

// Interface to reading GPX/KML/etc formats, to be derived from for each format.
class GeoLoadBase : public QObject
{
public:
    GeoLoadBase(GeoLoad& geoLoad);

    virtual bool load(const QString& path) = 0; // load zero or more tracks, appending.  returns success.
    virtual bool is(const QString& path) = 0;   // return true if this is the file type we load

protected:
    // Parse data for tracks.  append() adds the accumulated data to the model.
    struct ParseTrk {
        void clear() {
            name.clear();
            desc.clear();
            tags.clear();
            keywords.clear();
            geoPoint.clear();
            displayColor    = QColor::fromRgb(200, 200, 200);
            hasDisplayColor = false;
            laps = 0;
        }

        QString     name;
        QString     desc;
        QStringList tags;
        QString     keywords;
        QColor      displayColor;
        bool        hasDisplayColor;
        PointModel  geoPoint;
        uint32_t    laps;

        void append(GeoLoad&);
        void newSegment();
    };

    // Report progress to the progres bar.
    // We report differences, not positions, because we may be part of a multi-file read.
    void reportRead(qint64 b) { geoLoad.reportRead(b - lastPos); lastPos = b; }

    GeoLoad& geoLoad;
    qint64   lastPos;

    GeoLoadBase(const GeoLoadBase&)            = delete;
    GeoLoadBase& operator=(const GeoLoadBase&) = delete;
};

// Interface to reading GPX/KML/etc formats, to be derived from for each format.
class GeoSaveBase : public QObject
{
public:
    GeoSaveBase(GeoSave& geoSave);

    virtual bool save(const QString& path) = 0; // save tracks from selection.  returns success.

protected:
    // Report progress to the progres bar.
    void reportWrite(qint64 i) { geoSave.reportWrite(i); }

    // convenience function:
    template <typename T> T get(int mt, const QModelIndex& idx) const;

    GeoSave& geoSave;

    GeoSaveBase(const GeoSaveBase&)            = delete;
    GeoSaveBase& operator=(const GeoSaveBase&) = delete;
};

#endif // GEOIOBASE_H
