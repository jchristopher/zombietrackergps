/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOKML_H
#define GEOIOKML_H

#include <QVector>
#include <QString>
#include <QDateTime>
#include <QColor>

#include "src/core/pointmodel.h"
#include "geoioxml.h"

class GeoLoadKml final : public GeoLoadXml
{
public:
    using GeoLoadXml::GeoLoadXml;

    bool load(const QString& path) override;
    bool is(const QString& path) override;

    static const QString name;

private:
    void parseKml();
    void parseDocument();
    void parseFolder();
    void parsePlacemark();
    void parseTrack();
    inline void parseWhen();
    inline void parseCoord();
    void parseExtendedData();
    void parseSchemaData();
    void parseSimpleArrayData();

    struct Coord { qreal lon, lat, ele; };

    // Collect some info from parsing the track data
    struct {
        void clear() {
            name.clear();
            desc.clear();
            geoPoint.clear();
            timeArr.clear();
            coordArr.clear();
            cadArr.clear();
            hrArr.clear();
            powerArr.clear();
        }

        bool coherent() const {
            return (timeArr.size() == coordArr.size() &&
                    (cadArr.isEmpty() || timeArr.size() == cadArr.size()) &&
                    (hrArr.isEmpty() || timeArr.size() == hrArr.size()) &&
                    (powerArr.isEmpty() || timeArr.size() == powerArr.size()));
        }

        int empty() const { return timeArr.size() == 0; }

        void addDataToGeoPoints() {
            for (int c = 0; c < timeArr.size(); ++c)
                geoPoint.back().append(PointItem(coordArr[c].lon, coordArr[c].lat,
                                                 timeArr[c], coordArr[c].ele,
                                                 hrArr[c], cadArr[c], powerArr[c]));
        }

        QString  name;
        QString  desc;
        PointModel geoPoint;

        QVector<QDateTime>                    timeArr;  // these come in in an inconvenient
        QVector<Coord>                        coordArr; // parallel-array manner from the KML,
        QVector<decltype(PointItem::m_cad)>   cadArr;   // so we store them in intermediate form.
        QVector<decltype(PointItem::m_hr)>    hrArr;    // ...
        QVector<decltype(PointItem::m_power)> powerArr; // ...
    } trackData;

    bool foundKmlElement;
};

class GeoSaveKml final : public GeoSaveXml
{
public:
    using GeoSaveXml::GeoSaveXml;

    bool save(const QString& path) override;

    static const QString name;

private:
    void saveKml();
    void saveDocument();
    void saveFolder();
    void savePlacemark(const QModelIndex&);
    void saveTrk(const QModelIndex&);
    void saveWpt(const QModelIndex&);
    void saveRte(const QModelIndex&);
    void saveExtendedData(const QModelIndex&);
    void saveSchema();
};

#endif // GEOIOKML_H
