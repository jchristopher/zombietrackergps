/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOXML_H
#define GEOIOXML_H

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "src/core/pointmodel.h"
#include "src/core/pointitem.h"
#include "geoiobase.h"

// Subclass of GeoLoadBase for reading XML based formats.
class GeoLoadXml : public GeoLoadBase
{
protected:
    using GeoLoadBase::GeoLoadBase;

    bool done() const { return xml.atEnd() || xml.hasError(); }

    static bool isStart(const QXmlStreamReader::TokenType& token) {
        return token == QXmlStreamReader::StartElement;
    }

    static bool isEnd(const QXmlStreamReader::TokenType& token) {
        return token == QXmlStreamReader::EndElement;
    }

    inline void parseKeys(const std::function<void()>&);

    bool openReader(QFile& file);

    QXmlStreamReader xml;
};

// Helper to parse a set of XML tokens.  Inline for performance.
inline void GeoLoadXml::parseKeys(const std::function<void()>& fn)
{
    const QStringRef& end = xml.name();

    while (!done()) {
        if (const auto token = xml.readNext(); isStart(token)) {
            fn();
        } else if (isEnd(token) && xml.name() == end) {
            break;
        }
    }
}

// Subclass of GeoSaveBase for writing XML based formats.
class GeoSaveXml : public GeoSaveBase
{
protected:
    using GeoSaveBase::GeoSaveBase;

    bool openWriter(QFile& file);

    QXmlStreamWriter xml;
};

#endif // GEOIOXML_H
