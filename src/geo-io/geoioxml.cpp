/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "geoio.h"
#include "geoioxml.h"

bool GeoLoadXml::openReader(QFile& file)
{
    geoLoad.m_errorString.clear();

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;
    
    xml.setDevice(&file);

    if (xml.error() != QXmlStreamReader::NoError) {
        geoLoad.m_errorString = xml.errorString();
        return false;
    }

    return true;
}

bool GeoSaveXml::openWriter(QFile& file)
{
    geoSave.m_errorString.clear();

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
        return false;
    
    xml.setDevice(&file);

    if (xml.hasError()) {
        geoSave.m_errorString = file.errorString();
        return false;
    }

    // Set formatting
    xml.setAutoFormatting(geoSave.m_writeFormatted);
    xml.setAutoFormattingIndent(geoSave.m_indentLevel *
                                      (geoSave.m_indentSpaces ? 1 : -1));

    return true;
}
