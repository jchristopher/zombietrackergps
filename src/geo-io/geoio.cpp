/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QFileInfo>
#include <QMessageBox>

#include <src/util/util.h>

#include "src/ui/dialogs/importdialog.h"
#include "src/ui/dialogs/exportdialog.h"
#include "src/ui/windows/mainwindow.h"
#include "src/core/trackmodel.h"
#include "src/ui/panes/trackpane.h" // for the sort disabler
#include "geoio.h"
#include "geoionative.h"
#include "geoiofit.h"
#include "geoiogpx.h"
#include "geoiokml.h"
#include "geoiotcx.h"

GeoLoad::GeoLoad(MainWindow& mainWindow, TrackModel& trackModel,
                 const QStringList& trackTags, const QColor& trackColor) :
    mainWindow(mainWindow),
    m_model(trackModel),
    m_trackTags(trackTags),
    m_trackColor(trackColor),
    m_deduplicate(false),
    m_totalSize(0),
    m_readSize(0)
{
}

const CfgData& GeoLoad::cfgData()
{
    return mainWindow.constCfgData(); 
}

bool GeoLoad::allNative(const QStringList& trackFiles)
{
    for (const auto& trackFile : trackFiles)
        if (!GeoLoadNative(*this).is(trackFile))
            return false;

    return true;
}

bool GeoLoad::loadInternal(const QString& path)
{
    // Try this first, for performance reasons: i's our internal storage.
    if (GeoLoadNative loader(*this); loader.is(path))
        return loader.load(path);

    if (GeoLoadGpx loader(*this); loader.is(path))
        return loader.load(path);

    if (GeoLoadTcx loader(*this); loader.is(path))
        return loader.load(path);

    if (GeoLoadKml loader(*this); loader.is(path))
        return loader.load(path);

    if (GeoLoadFit loader(*this); loader.is(path))
        return loader.load(path);

    m_errorString = QObject::tr("Unrecognized file type.");
    return false;
}

void GeoLoad::preLoadSetup()
{
    m_errorString.clear();
    m_imported.clear();
    m_duplicates = 0;

    modelHashes = model().trackHashes(); // for deduplication
}

bool GeoLoad::load(const QString& path)
{
    preLoadSetup();

    if (m_totalSize == 0)
        m_totalSize = QFileInfo(path).size();

    MainWindow::ProgressHandler progress(mainWindow, m_totalSize);
    MainWindow::SaveCursor cursor(mainWindow, Qt::WaitCursor);

    // disable sorting for track panes until stack is popped.
    const auto disableSort = TrackPane::DisableSort<TrackPane>(*this);

    return loadInternal(path);
}

bool GeoLoad::load(ImportDialog& importOpts, const QStringList& trackFiles)
{
    preLoadSetup();

    if (trackFiles.isEmpty())
        return false;

    // Skip import dialog for native formats
    if (!allNative(trackFiles)) {
        if (importOpts.exec() != QDialog::Accepted) {
            m_errorString.clear();  // no error message on cancel
            return false;
        }

        // TODO: package these up in a structure
        m_trackTags   = importOpts.tags();
        m_trackColor  = importOpts.trackColor();
        m_deduplicate = importOpts.deduplicate();

        m_totalSize   = totalFileSize(trackFiles);
        m_readSize    = 0;
    }

    MainWindow::ProgressHandler progress(mainWindow, m_totalSize);
    MainWindow::SaveCursor cursor(mainWindow, Qt::WaitCursor);

    // disable sorting for track panes until stack is popped.
    const auto disableSort = TrackPane::DisableSort<TrackPane>(*this);

    for (const auto& trackFile : trackFiles) {
        if (!loadInternal(trackFile)) {
            m_errorString = tr("Error loading file: ") + trackFile + ": " + errorString();
            return false;
        }
    }

    return true;
}

void GeoLoad::reportRead(qint64 b)
{
    m_readSize += b;
    mainWindow.updateProgress(m_readSize);
}

const QStringList& GeoLoad::formatNames()
{
    // Must be in GeoFormat enum order.
    static const QStringList supported = {
        GeoLoadGpx::name,
        GeoLoadTcx::name,
        GeoLoadKml::name,
        GeoLoadFit::name,
    };

    return supported;
}

// Loaders MUST use this to insert into the model.  TODO: hide the model otherwise,
// to enforce that architecturally, but provide a few needed things such as rowCount().
void GeoLoad::appendRow(const QString &name, QString notes,
                        const QStringList& tags,
                        const QString& keywords,
                        TrackType type, PointModel& newPoints)
{
    newPoints.removeEmptySegments(); // remove any empty segments

    if (newPoints.isEmpty())
        return;

    uint newHash = -1;

    // reject duplicates if asked
    if (m_deduplicate) {
        newHash = qHash(newPoints);
        if (const auto it = modelHashes.find(newHash);
                it != modelHashes.end() && model().pointEqual(it.value(), newPoints)) {
            ++m_duplicates;
            return;
        }
    }

    // Make imported notes HTML if they're not already.
    if (!notes.isEmpty() && !notes.contains("<html>"))
        notes = "<html><head></head><body>" + notes + "</body></html>";

    // Explicitly set tags override auto-detected tags
    const QStringList& finalTags = (m_trackTags.isEmpty() ? tags : m_trackTags);

    model().appendRow(name, type, finalTags, m_trackColor, notes, keywords, newPoints);
    const QModelIndex newIdx = model().index(model().rowCount() - 1, 0);

    // Add new hash, so we can reject further duplicates.
    if (m_deduplicate)
        modelHashes.insertMulti(newHash, newIdx);

    m_imported.append(newIdx);
}

const CfgData& GeoSave::cfgData()
{
    return mainWindow.constCfgData(); 
}

const QStringList& GeoSave::formatNames()
{
    // Must be in GeoFormat enum order.
    static const QStringList supported = {
        GeoLoadGpx::name,
        GeoLoadTcx::name,
        GeoLoadKml::name,
        GeoLoadFit::name,
    };

    return supported;
}

qint64 GeoLoad::totalFileSize(const QStringList& files)
{
    qint64 total = 0;

    for (const auto& file : files)
        total += QFileInfo(file).size();

    return total;
}

GeoSave::GeoSave(MainWindow& mainWindow, TrackModel& trackModel,
                 GeoFormat format,
                 bool      writeFormatted,
                 int       indentLevel,
                 bool      indentSpaces,
                 const QModelIndexList& selection) :
    mainWindow(mainWindow),
    m_format(format),
    m_writeFormatted(writeFormatted),
    m_indentLevel(indentLevel),
    m_indentSpaces(indentSpaces),
    m_selection(selection.empty() ? m_allEntries : selection),
    m_model(trackModel)
{
    // If there were no selections, use every index in the model.
    if (m_selection.isEmpty())
        Util::recurse(model(), [this](const QModelIndex& idx) { m_allEntries.append(idx); });
}

bool GeoSave::save(const QString& path)
{
    m_writeCount = 0;
    m_totalCount = m_selection.size();

    MainWindow::ProgressHandler progress(mainWindow, m_totalCount);
    MainWindow::SaveCursor cursor(mainWindow, Qt::WaitCursor);

    switch (m_format) {
    case GeoFormat::Native: return GeoSaveNative(*this).save(path);
    case GeoFormat::Gpx:    return GeoSaveGpx(*this).save(path);
    case GeoFormat::Tcx:    return GeoSaveTcx(*this).save(path);
    case GeoFormat::Kml:    return GeoSaveKml(*this).save(path);
    case GeoFormat::Fit:    return GeoSaveFit(*this).save(path);
    default:
        m_errorString = QObject::tr("Unrecognized save type.");
        return false;
    }
}

bool GeoSave::save(ExportDialog& exportOpts)
{
    m_errorString.clear();

    if (exportOpts.exec() != QDialog::Accepted)
        return false;

    // TODO: package these up in a structure
    m_format         = exportOpts.exportFormat();
    m_writeFormatted = exportOpts.writeFormatted();
    m_indentLevel    = exportOpts.indentLevel();
    m_indentSpaces   = exportOpts.indentSpaces();

    if (!exportOpts.exportAll()) {
        if (m_selection.empty()) {
            const int ret = QMessageBox::question(exportOpts.parentWidget(),
                                                  QObject::tr("Selection"),
                                                  QObject::tr("No track selection: export all tracks?"),
                                                  QMessageBox::Yes | QMessageBox::No,
                                                  QMessageBox::Yes);

            if (ret != QMessageBox::Yes)
                return false;
        }
    }

    const QString trackFile = exportOpts.getExportFileName(trackFileFilter);
    if (trackFile.isEmpty())
        return false;

    if (!save(trackFile)) {
        m_errorString = QObject::tr("Error saving file:<p>") + trackFile + "<p>" + errorString();
        return false;
    }

    return true;
}

void GeoSave::reportWrite(qint64 i)
{
    m_writeCount += i;
    mainWindow.updateProgress(m_writeCount);
}

QDataStream& operator<<(QDataStream& out, const GeoFormat& pc)
{
    return out << std::underlying_type<GeoFormat>::type(pc);
}

QDataStream& operator>>(QDataStream& in, GeoFormat& pc)
{
    return in >> (std::underlying_type<GeoFormat>::type&)(pc);
}
