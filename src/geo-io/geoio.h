/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIO_H
#define GEOIO_H

#include <QObject>
#include <QModelIndex>
#include <QString>
#include <QFile>
#include <QColor>
#include <QHash>

#include "src/core/trackmodel.h"

class ImportDialog;
class ExportDialog;
class TrackModel;
class PointModel;
class MainWindow;
class CfgData;

enum GeoFormat {
    Native = 0,  // Our own binary format for super-fast save/load.  Yay!
    Gpx,         // https://en.wikipedia.org/wiki/GPS_Exchange_Format
    Tcx,         // https://en.wikipedia.org/wiki/Training_Center_XML
    Kml,         // https://en.wikipedia.org/wiki/Keyhole_Markup_Language
    Fit,         // (no public spec yet discovered)
};

Q_DECLARE_METATYPE(GeoFormat)
QDataStream& operator<<(QDataStream& out, const GeoFormat& pc);
QDataStream& operator>>(QDataStream& in, GeoFormat& pc);

// Interface to reading GPX/KML/etc formats.  Selects loader based on the file type.
class GeoLoad final : public QObject
{
public:
    GeoLoad(MainWindow& mainWindow, TrackModel& trackModel,
            const QStringList& tracktags = QStringList(),  // invalid or empty for no tags
            const QColor&      trackColor = QColor()); // set invalid to avoid overriding.

    bool load(const QString& path); // load zero or more tracks, appending.  returns success.
    bool load(ImportDialog&, const QStringList&); // import given files.

    const QString& errorString() const { return m_errorString; }
    const QModelIndexList& imported() const { return m_imported; }

    static const QStringList& formatNames(); // return names for supported save formats.

    TrackModel& model() { return m_model; }
    const CfgData& cfgData();

    // Loaders MUST use this to insert into the model.  TODO: hide the model otherwise,
    // to enforce that architecturally, but provide a few needed things such as rowCount().
    void appendRow(const QString& name, QString notes,
                   const QStringList& tags,
                   const QString& keywords,
                   TrackType type, PointModel& newPoints);

    GeoLoad& requestDeduplicate(bool b) { m_deduplicate = b; return *this; }

    // For progress reporting.
    void reportRead(qint64 b);
    int duplicates() const { return m_duplicates; }

    static const constexpr char* trackFileFilter = "GPS (*.fit *.gpx *.kml *.tcx);;All (*)";

    QString        m_errorString;

private:
    static qint64 totalFileSize(const QStringList& files);
    void preLoadSetup();
    bool loadInternal(const QString& path);

    bool allNative(const QStringList& files);

    MainWindow&  mainWindow;
    TrackModel&  m_model;
    TrackModel*  m_loadModel;
    QStringList  m_trackTags;
    QColor       m_trackColor;
    bool         m_deduplicate;
    qint64       m_totalSize;   // for progress reporting
    qint64       m_readSize;    // amount read so far
    int          m_duplicates;  // rejected duplicate count

    QModelIndexList m_imported; // indexes to our imported tracks

    QHash<uint, QModelIndex> modelHashes; // for deduplication

    GeoLoad(const GeoLoad&)            = delete;
    GeoLoad& operator=(const GeoLoad&) = delete;
};

// Interface to reading GPX/KML/etc formats.  This is a dispatcher to select
// a loader based on the file type.
class GeoSave final
{
public:
    GeoSave(MainWindow& mainWindow, TrackModel& trackModel,
            GeoFormat format,
            bool      writeFormatted    = false,
            int       indentLevel       = 4,
            bool      indentSpaces      = true,
            const QModelIndexList& = QModelIndexList());

    bool save(const QString& path); // save from given index list
    bool save(ExportDialog&);       // UI form: present file selector etc

    const QString& errorString() const { return m_errorString; }

    MainWindow&            mainWindow;
    GeoFormat              m_format;         // save format
    bool                   m_writeFormatted; // true to write formatted output
    int                    m_indentLevel;    // indent level for formatted output
    bool                   m_indentSpaces;   // true to write spaces, false for tabs.
    const QModelIndexList& m_selection;      // selections to save
    QString                m_errorString;    // last error string, if any

    static const QStringList& formatNames(); // return names for supported load formats.

    static const constexpr char* trackFileFilter = GeoLoad::trackFileFilter;

    const TrackModel& model() { return m_model; }
    const CfgData& cfgData();

    // For progress reporting.
    void reportWrite(qint64 items);

private:
    qint64                 m_totalCount;     // for progress reporting
    qint64                 m_writeCount;     // ...
    QModelIndexList        m_allEntries;     // used if passed in selection was empty
    const TrackModel&      m_model;          // model to save data from

    GeoSave(const GeoSave&)            = delete;
    GeoSave& operator=(const GeoSave&) = delete;
};

#endif // GEOIO_H
