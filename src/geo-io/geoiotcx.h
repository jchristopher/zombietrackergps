/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOTCX_H
#define GEOIOTCX_H

#include <QVector>
#include <QString>
#include <QDateTime>
#include <QColor>

#include "geoioxml.h"

class GeoLoadTcx final : public GeoLoadXml
{
public:
    using GeoLoadXml::GeoLoadXml;

    bool load(const QString& path) override;
    bool is(const QString& path) override;

    static const QString name;

private:
    void parseTcx();
    void parseActivities();
    void parseActivity();
    void parseLap();
    void parseTrack();
    void parseTrackpoint();
    void parsePosition();
    void parseHeartRateBpm();

    // These are stored in the class to reduce lambda capture size (for perf).

    ParseTrk   track;    // parse data for a track
    PointItem  pt;       // parse data for a track point
    bool       foundTcxElement;
};

class GeoSaveTcx final : public GeoSaveXml
{
public:
    using GeoSaveXml::GeoSaveXml;

    bool save(const QString& path) override;

    static const QString name;

private:
    void saveTcx();
    void saveActivities();
    void saveActivity(const QModelIndex& idx);
    void saveLap(const PointModel::value_type& trkseg);
    void saveTrack(const PointModel::value_type& trkseg);
    void saveTrackpoint(const PointItem& trkpt);
    void savePosition(const PointItem& trkpt);
};

#endif // GEOIOTCX_H
