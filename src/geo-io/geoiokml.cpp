/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <limits>
#include <QDateTime>
#include <QModelIndexList>

#include <src/util/roles.h>
#include "src/core/trackmodel.h"
#include "src/core/pointmodel.h"
#include "src/version.h"
#include "geoiokml.h"

const QString GeoLoadKml::name = "KML";
const QString GeoSaveKml::name = "KML";

namespace KmlExt {
    static const char* gx  = "http://www.google.com/kml/ext/2.2";
    static const char* kml = "http://www.opengis.net/kml/2.2";
}

namespace KmlTag {
    static const char* kml              = "kml";
    static const char* Document         = "Document";
    static const char* Folder           = "Folder";
    static const char* name             = "name";
    static const char* type             = "type";
    static const char* displayName      = "displayName";
    static const char* Placemark        = "Placemark";
    static const char* Track            = "Track";
    static const char* when             = "when";
    static const char* coord            = "coord";
    static const char* ExtendedData     = "ExtendedData";
    static const char* SchemaData       = "SchemaData";
    static const char* Schema           = "Schema";
    static const char* SimpleArrayData  = "SimpleArrayData";
    static const char* SimpleArrayField = "SimpleArrayField";
    static const char* value            = "value";
    static const char* cadence          = "cadence";
    static const char* heartrate        = "heartrate";
    static const char* power            = "power";

    static const char* schemaName = "schema"; // for extended data schema
}

bool GeoLoadKml::load(const QString& path)
{
    QFile file(path);
    if (!openReader(file))
        return false;

    foundKmlElement = false;

    parseKeys([this]() {
        if (xml.name() == KmlTag::kml) parseKml();
        else xml.skipCurrentElement();
    });

    if (xml.error() == QXmlStreamReader::NoError) {
        if (!foundKmlElement)
            xml.raiseError(QObject::tr("No kml element found in file"));
    }

    if (xml.hasError()) {
        geoLoad.m_errorString = xml.errorString();
        return false;
    }

    return true;
}

bool GeoLoadKml::is(const QString& path)
{
    QFile file(path);

    if (!openReader(file))
        return false;

    while (!done())
        if (const auto token = xml.readNext(); isStart(token))
            return xml.name() == KmlTag::kml;

    return false;
}

void GeoLoadKml::parseKml()
{
    foundKmlElement = true;  // we found a valid KML element.

    parseKeys([this]() {
        if      (xml.name() == KmlTag::Document)   parseDocument();
        else xml.skipCurrentElement();
    });
}

void GeoLoadKml::parseDocument()
{
    parseKeys([this]() {
        if      (xml.name() == KmlTag::Folder)    parseFolder();
        else if (xml.name() == KmlTag::Placemark) parsePlacemark();
        else xml.skipCurrentElement();
    });
}

void GeoLoadKml::parseFolder()
{
    parseKeys([this]() {
        if      (xml.name() == KmlTag::Placemark) parsePlacemark();
        else xml.skipCurrentElement();
    });
}

void GeoLoadKml::parsePlacemark()
{
    trackData.clear();

    parseKeys([this]() {
        const bool gx = (xml.namespaceUri() == KmlExt::gx);

        if      (xml.name() == KmlTag::name)        trackData.name = xml.readElementText();
        else if (xml.name() == KmlTag::Track && gx) parseTrack();
        else xml.skipCurrentElement();
    });

    if (!trackData.coherent()) {
        xml.raiseError(QObject::tr("Array size mismatch"));
        return;
    }

    if (trackData.empty())
        return;

    // create points
    trackData.addDataToGeoPoints();

    // Create a new TrackItem for the track we just made.
    geoLoad.appendRow(trackData.name, trackData.desc, QStringList(), QString(),
                      TrackType::Trk, trackData.geoPoint);
}

void GeoLoadKml::parseWhen()
{
    trackData.timeArr.append(QDateTime::fromString(xml.readElementText(), Qt::ISODate));
}

void GeoLoadKml::parseCoord()
{
    // This is rather inefficient as it involves needless dynamic allocations, but meh, it's
    // just the KML importer... we'll live with it for the ease of expression.
    const QStringList split = xml.readElementText().split(' ', QString::SkipEmptyParts);
    
    if (split.size() != 3) {
        xml.raiseError(QObject::tr("Bad coordinate element"));
        return;
    }

    trackData.coordArr.append({split[0].toDouble(), split[1].toDouble(), split[2].toDouble()});
}

void GeoLoadKml::parseTrack()
{
    reportRead(xml.characterOffset()); // for progress reports

    trackData.geoPoint.push_back(PointModel::value_type());
    trackData.geoPoint.back().reserve(64);

    // TODO: add parsing of waypoints and routes

    parseKeys([this]() {
        const bool gx = (xml.namespaceUri() == KmlExt::gx);
        if      (xml.name() == KmlTag::when)         parseWhen();
        else if (xml.name() == KmlTag::coord && gx)  parseCoord();
        else if (xml.name() == KmlTag::ExtendedData) parseExtendedData();
        else xml.skipCurrentElement();
    });
}

void GeoLoadKml::parseExtendedData()
{
    parseKeys([this]() {
        if   (xml.name() == KmlTag::SchemaData) parseSchemaData();
        else xml.skipCurrentElement();
    });
}

void GeoLoadKml::parseSchemaData()
{
    parseKeys([this]() {
        const bool gx = (xml.namespaceUri() == KmlExt::gx);

        if   (xml.name() == KmlTag::SimpleArrayData && gx) parseSimpleArrayData();
        else xml.skipCurrentElement();
    });
}

void GeoLoadKml::parseSimpleArrayData()
{
    enum {
        None, Cadence, Heartrate, Power
    } attrName = None;

    const QStringRef name = xml.attributes().value("", KmlTag::name);
    if      (name == KmlTag::cadence)   attrName = Cadence;
    else if (name == KmlTag::heartrate) attrName = Heartrate;
    else if (name == KmlTag::power)     attrName = Power;

    // return if this isn't an array we recognize.
    if (attrName == None)
        return;

    parseKeys([this, attrName]() {
        const bool gx = (xml.namespaceUri() == KmlExt::gx);

        if (xml.name() == KmlTag::value && gx) {
            switch (attrName) {
                case None:      break;
                case Cadence:   trackData.cadArr.append(xml.readElementText().toUInt()); break;
                case Heartrate: trackData.hrArr.append(xml.readElementText().toUInt()); break;
                case Power:     trackData.powerArr.append(int(xml.readElementText().toFloat())); break;
            }
        } else xml.skipCurrentElement();
    });
}

void GeoSaveKml::saveExtendedData(const QModelIndex& idx)
{
    if (!get<QVariant>(TrackModel::MinCad,   idx).isValid() &&
        !get<QVariant>(TrackModel::MinHR,    idx).isValid() &&
        !get<QVariant>(TrackModel::MinPower, idx).isValid())
        return;

    const auto saveArray = [this, &idx](ModelType mt, const char* tag,
            const std::function<void(const PointItem&)>& fn) {
        if (get<QVariant>(mt, idx).isValid()) {
            xml.writeStartElement(KmlExt::gx, KmlTag::SimpleArrayData); {
                xml.writeAttribute(KmlTag::name, tag);
                for (const auto& trkseg : *geoSave.model().geoPoints(idx))
                    for (const auto& trkpt : trkseg)
                        fn(trkpt);
            } xml.writeEndElement();
        }
    };

    xml.writeStartElement(KmlTag::ExtendedData); {
        xml.writeStartElement(KmlTag::SchemaData); {
            xml.writeAttribute("schemaUrl", QString("#") + KmlTag::schemaName);

            // Save cadence array
            saveArray(TrackModel::MinCad, KmlTag::cadence, [this](const PointItem& trkpt) {
                xml.writeTextElement(KmlExt::gx, KmlTag::value,
                                     QString::number(trkpt.hasCad() ? trkpt.cad() : 0));
            });

            // Save HR array
            saveArray(TrackModel::MinHR, KmlTag::heartrate, [this](const PointItem& trkpt) {
                xml.writeTextElement(KmlExt::gx, KmlTag::value,
                                     QString::number(trkpt.hasHr() ? trkpt.hr() : 0));
            });

            // Save power array
            saveArray(TrackModel::MinPower, KmlTag::power, [this](const PointItem& trkpt) {
                xml.writeTextElement(KmlExt::gx, KmlTag::value,
                                     QString::number(trkpt.hasPower(PointItem::Measured) ? trkpt.power() : 0));
            });

        } xml.writeEndElement();
    } xml.writeEndElement();
}

void GeoSaveKml::saveTrk(const QModelIndex& idx)
{
    reportWrite(1); // for progress reports

    const QString coordFmt("%1 %2 %3");

    xml.writeStartElement(KmlExt::gx, KmlTag::Track); {
        // Unclear if gx::Track supports multiple segments, so we'll just concat them.
        for (const auto& trkseg : *geoSave.model().geoPoints(idx)) {
            // Save parallel arrays.  It is most likely fine to interleave them, but
            // the example track from Google uses this style, so it may be safer WRT
            // other parsers.
            for (const auto& trkpt : trkseg)
                xml.writeTextElement(KmlTag::when, trkpt.time().toString(Qt::ISODate));

            for (const auto& trkpt : trkseg)
                xml.writeTextElement(KmlExt::gx, KmlTag::coord,
                                     coordFmt.arg(trkpt.lon(false), 0, 'g', 14)
                                     .arg(trkpt.lat(false), 0, 'g', 14)
                                     .arg(trkpt.ele(false), 0, 'g', 4));
        }

        saveExtendedData(idx);
    } xml.writeEndElement();
}

void GeoSaveKml::saveRte(const QModelIndex&)
{
    assert(0); // TODO: ...
}

void GeoSaveKml::saveWpt(const QModelIndex&)
{
    assert(0); // TODO: ...
}

void GeoSaveKml::savePlacemark(const QModelIndex& idx)
{
    xml.writeStartElement(KmlTag::Placemark); {
        xml.writeTextElement(KmlTag::name, get<QString>(TrackModel::Name, idx));

        switch (get<TrackType>(TrackModel::Type, idx)) {
        case TrackType::Wpt: saveWpt(idx); break;
        case TrackType::Rte: saveRte(idx); break;
        case TrackType::Trk: saveTrk(idx); break;
        default: assert(0); break;
        }
    } xml.writeEndElement();
}

void GeoSaveKml::saveSchema()
{
    const auto writeField = [this](const char* tag, const char* type, const char* displayName) {
        xml.writeStartElement(KmlExt::gx, KmlTag::SimpleArrayField); {
            xml.writeAttribute(KmlTag::name, tag);
            xml.writeAttribute(KmlTag::type, type);
            xml.writeTextElement(KmlTag::displayName, displayName);
        } xml.writeEndElement();
    };

    xml.writeStartElement(KmlTag::Schema); {
        xml.writeAttribute("id", KmlTag::schemaName);
        writeField(KmlTag::cadence,   "int",   "Cadence");
        writeField(KmlTag::heartrate, "int"  , "Heart Rate");
        writeField(KmlTag::power,     "float", "Power");
    } xml.writeEndElement();
}

void GeoSaveKml::saveFolder()
{
    xml.writeStartElement(KmlTag::Folder); {
        xml.writeTextElement(KmlTag::name, "Tracks");

        for (const auto& idx : geoSave.m_selection)
            savePlacemark(idx);
    } xml.writeEndElement();
}

void GeoSaveKml::saveDocument()
{
    xml.writeStartElement(KmlTag::Document); {
        saveSchema();
        saveFolder();
    } xml.writeEndElement();
}

void GeoSaveKml::saveKml()
{
    xml.writeStartElement(KmlTag::kml); {
        xml.writeAttribute("xmlns", KmlExt::kml);
        xml.writeNamespace(KmlExt::gx, "gx");

        saveDocument();

    } xml.writeEndElement();
}

bool GeoSaveKml::save(const QString& path)
{
    QFile file(path);
    if (!openWriter(file))
        return false;

    xml.writeStartDocument();
    saveKml();
    xml.writeEndDocument();

    return !xml.hasError();
}
