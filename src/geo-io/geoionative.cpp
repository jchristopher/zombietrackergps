/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "src/core/trackmodel.h"
#include "geoio.h"
#include "geoionative.h"

const QString GeoLoadNative::name = "ZTGPS";
const QString GeoSaveNative::name = "ZTGPS";

namespace {
    const quint32 NativeMagic   = 0xd9baf758;
    const quint32 NativeVersion = 0x1002;
}

GeoLoadNative::GeoLoadNative(GeoLoad& geoLoad) :
    GeoLoadBase(geoLoad)
{
    connect(&geoLoad.model(), &TrackModel::loadPos, this, &GeoLoadNative::loadPos);
}

bool GeoLoadNative::load(const QString& path)
{
    QFile file(path);
    if (!openReader(file))
        return false;

    readStream >> geoLoad.model();

    return readStream.error() == VersionedStream::NoError;
}

bool GeoLoadNative::is(const QString& path)
{
    QFile file(path);

    return openReader(file);
}

bool GeoLoadNative::openReader(QFile& file)
{
    geoLoad.m_errorString.clear();

    if (!readStream.openRead(file, NativeMagic, 0x1000, 0x1004)) {
        geoLoad.m_errorString = readStream.errorString();
        return false;
    }

    return true;
}

GeoSaveNative::GeoSaveNative(GeoSave& geoSave) :
    GeoSaveBase(geoSave)
{
    connect(&geoSave.model(), &TrackModel::saveItem, this, &GeoSaveNative::saveItem);
}

bool GeoSaveNative::save(const QString& path)
{
    QFile file(path);
    if (!openWriter(file))
        return false;

    // TODO: we should abide the selection list
    writeStream << geoSave.model();

    return writeStream.error() == VersionedStream::NoError;
}

bool GeoSaveNative::openWriter(QFile& file)
{
    geoSave.m_errorString.clear();

    if (!writeStream.openWrite(file, NativeMagic, NativeVersion)) {
        geoSave.m_errorString = writeStream.errorString();
        return false;
    }

    return true;
}
