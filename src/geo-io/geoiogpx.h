/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOGPX_H
#define GEOIOGPX_H

#include <QVector>
#include <QString>
#include <QDateTime>
#include <QColor>
#include <tuple>

#include "geoioxml.h"

class GeoLoadGpx final : public GeoLoadXml
{
public:
    using GeoLoadXml::GeoLoadXml;

    bool load(const QString& path) override;
    bool is(const QString& path) override;

    static const QString name;

private:
    void parseGpx();
    void parseMetadata();
    void parseLink();
    void parseAuthor();
    void parseCopyright();
    void parseDisplayColor();
    void parseTrk();
    void parseWpt();
    void parseRte();
    void parseTrkseg();
    void parseTrkpt();
    void parseGpxDataLap();
    void parseExtensionsGpxContext();
    void parseExtensionsTrkContext();
    void parseExtensionsTrkptContext();
    void parseTrackExtension();
    void parseTrackPointExtension();
    void parsePowerExtension();

    struct {
        void clear() {
            links.clear();
            name.clear();
            desc.clear();
            keywords.clear();
        }

        QVector<std::tuple<QString, QString, QString>> links; // tuple: URL, text, type
        QString          name;     // metadata level name
        QString          desc;     // description field
        QString          keywords; // metadata keywords
    } fileMetaData;

    ParseTrk   track;    // parse data for a track
    PointItem  pt;       // parse data for a track point

    // These are stored in the class to reduce lambda capture size (for perf).
    QStringRef href;     // for parsing links
    QString    text;     // for link text, etc.
    QString    linkType; // for links

    bool       foundGpxElement;
};

class GeoSaveGpx final : public GeoSaveXml
{
public:
    using GeoSaveXml::GeoSaveXml;

    bool save(const QString& path) override;

    static const QString name;

private:
    void saveGpx();
    void saveMetadata();
    void saveTrackItem();
    void saveWpt(const QModelIndex& idx);
    void saveTrk(const QModelIndex& idx);
    void saveRte(const QModelIndex& idx);
    void saveTrkseg(const PointModel::value_type&);
    void saveTrkpt(const PointItem&);
    void saveTrkptExtensions(const PointItem&);
};

#endif // GEOIOGPX_H
