/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIOFIT_H
#define GEOIOFIT_H

#include <cstdint>
#include <cassert>
#include <array>
#include <QtGlobal>
#include <QString>
#include <QVector>
#include <QDateTime>

#include "geoiobase.h"

// Data used by both the FIT loader and the saver
class GeoCommonFit
{
protected:
    enum class Sport       : uint8_t;  // sport values (e.g, cycling, running)
    enum class SubSport    : uint8_t;  // sub-sport values (e.g, road, mountain)
    enum class TypeNum     : uint8_t;  // Fit raw data types
    enum class RecordField : uint8_t;  // Record fields of interest to us.
    enum class SportField  : uint8_t;  // Sport fields of interest to us.
    enum class LapField    : uint8_t;  // Lap fields of interest to us.

    struct FileHeader {
        FileHeader() : size(0), version(0), profile(0), dataSize(0), magic{0,0,0,0}, crc(0) { }

        uint8_t   size;       // header size
        uint8_t   version;    // FIT version
        uint16_t  profile;    // profile version
        uint32_t  dataSize;   // data size
        char      magic[4];   // magic
        uint16_t  crc;        // crc

        static const constexpr uint8_t minHeaderLength = 12;
    };

    struct RecordHeader {
        RecordHeader(int localId = 0, int defMessage = 0) :
            localId(localId), reserved(0), devFlag(0), defMessage(defMessage), cmpTimestamp(0) { }

        unsigned char localId      : 4;
        unsigned char reserved     : 1;
        unsigned char devFlag      : 1;
        unsigned char defMessage   : 1;
        unsigned char cmpTimestamp : 1;
    };

    // Compressed time header
    struct CmpTimeHeader {
        CmpTimeHeader() { *reinterpret_cast<char*>(this) = 0; }
        CmpTimeHeader(const RecordHeader& rh) { *(uint8_t*)(this) = *(uint8_t*)(&rh); }
        unsigned char timeOffset   : 5;
        unsigned char localId      : 2;
        unsigned char cmpTimestamp : 1;
    };

    struct FieldType {
        FieldType() { *reinterpret_cast<char*>(this) = 0; }
        FieldType(TypeNum typeNum, uint8_t hasEndian) :
            typeNum(uint8_t(typeNum)), reserved(0), hasEndian(hasEndian)
        { }

        uint8_t typeNum    : 5;  // enum TypeNum above defines this namespace
        uint8_t reserved   : 2;
        uint8_t hasEndian  : 1;  // 0 = no endianness, 1 if has endianness
    };

    struct Field {
        Field() : rawId(0), size(0) { }

        // Union holds different views to the field identifier
        union {
            RecordField record;
            SportField  sport;
            LapField    lap;
            uint8_t     rawId;
        };

        uint8_t      size;  // field data size (it can be arrayed)
        FieldType    type;  // field base type info
    };

    // FIT architecture type
    enum class Architecture : uint8_t {
        Little = 0,
        Big,
    };

#if Q_BYTE_ORDER == Q_LITTLE_ENDIAN
    static const constexpr Architecture currentArchitecture = Architecture::Little;
#else
    static const constexpr Architecture currentArchitecture = Architecture::Big;
#endif

    // Message data record types of interest to us.
    enum class Message : uint16_t {
        UserProfile = 3,
        BikeProfile = 6,
        Sport       = 12,
        Session     = 18,
        Lap         = 19,
        Record      = 20,
        Event       = 21,
        Invalid     = 0xffff,
    };

    // Header for a definition: this matches the disk format.
    struct DefinitionHeader {
        DefinitionHeader(Message globalId = Message::Invalid,
                         Architecture architecture = currentArchitecture) :
            reserved(0), architecture(architecture), globalId(globalId) { }

        void clear() { *this = DefinitionHeader(); }

        uint8_t        reserved;
        Architecture   architecture;  // 0 = little endian, 1 = big endian
        union {
            Message    globalId;      // global message type: see Message enum
            uint16_t   globalIdRaw;   // for raw read
        };
    };

    // A record definition: does not match disk format (fields are individually loaded into vectors)
    struct Definition : public DefinitionHeader {
        Definition() { clear(); }

        void clear() {
            fields.clear();
            devFields.clear();
        }

        QVector<Field> fields;        // vector of fields
        QVector<Field> devFields;     // ...
    };

    static const QDateTime epochDate;

    static_assert(sizeof(RecordHeader) == 1);
    static_assert(sizeof(CmpTimeHeader) == 1);
    static_assert(sizeof(Architecture) == 1);
    static_assert(sizeof(TypeNum) == 1);
    static_assert(sizeof(FieldType) == 1);
    static_assert(sizeof(Field) == 3);
    static_assert(sizeof(DefinitionHeader) == 4);

    template <typename T> inline T invalid() const;
};

class GeoLoadFit final : public GeoLoadBase, public GeoCommonFit
{
public:
    using GeoLoadBase::GeoLoadBase;

    bool load(const QString& path) override;
    bool is(const QString& path) override;

    static const QString name;

    // Should really be private, but having enum defs in the .cpp which use this
    // for some reason requires it to be public.
    static const constexpr uint8_t Timestamp = 253;

private:
    template <typename T> bool read(T& t, Architecture);
    template <typename T> bool read(T& t, int size = sizeof(T));
    template <typename T> bool read(T* t, int size);

    template <typename T, typename VALTYPE, bool endian = true>
    inline T read(int size, Architecture, bool& error, bool& isInvalid);

    template <typename T> T read(const Field& field, Architecture, bool& error, bool& isInvalid);

    inline bool skip(int size);
    inline bool skip(const Field& field, Architecture architecture);
    bool skip(const Definition&);
    bool skip(const QVector<Field>&, Architecture architecture);
    bool parse();
    bool parse(FileHeader& header);
    bool parseRecord();
    bool parseCmpTimestamp(CmpTimeHeader);
    bool parseDefinition(RecordHeader);
    bool parseData(RecordHeader);
    bool parseData(const Definition&);
    bool parseUserProfile(const Definition&);
    bool parseBikeProfile(const Definition&);
    bool parseSport(const Definition&);
    bool parseRecord(const Definition&);
    bool parseEvent(const Definition&);
    bool parseLap(const Definition&);

    void guessTags(Sport sport, SubSport subSport);    // see comment in .cpp

    static inline QDateTime dateTime(uint32_t fit_time);

    void reset();

    ParseTrk   track;    // parse data for a track
    PointItem  pt;       // parse data for a track point

    std::array<Definition, 16> definitions;  // record definitions
    QFile    file;  // file to read from
    uint32_t time;  // current file time: used for time delta increments
    uint32_t prevTime;
};

class GeoSaveFit final : public GeoSaveBase, public GeoCommonFit
{
public:
    using GeoSaveBase::GeoSaveBase;

    bool save(const QString& path) override;

    static const QString name;

private:
    static const QVector<RecordField> recordFields;

    template <typename T> bool write(const T& t, int size = sizeof(T));
    static inline uint32_t dateTime(const QDateTime& time);
    static inline uint16_t crc(uint16_t crc, uint8_t byte);

    static const constexpr int recordId = 0;

    bool saveFileHeader(uint32_t dataSize);
    bool saveCRC();
    bool saveRecordDef();
    bool saveTrkpt(const PointItem&);
    bool saveTrkseg(const PointModel::value_type&);
    bool saveTrack(const QModelIndex&);
    bool saveTracks();

    QFile file;
};

#endif // GEOIOFIT_H
