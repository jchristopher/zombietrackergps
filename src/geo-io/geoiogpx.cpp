/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <limits>
#include <QDateTime>
#include <QModelIndexList>

#include <src/util/roles.h>
#include "src/core/trackmodel.h"
#include "src/core/pointmodel.h"
#include "src/version.h"
#include "geoiogpx.h"

const QString GeoLoadGpx::name = "GPX";
const QString GeoSaveGpx::name = "GPX";

namespace GpxExt {
    static const char* xmlns   = "http://www.topografix.com/GPX/1/1";
    static const char* xsi     = "http://www.w3.org/2001/XMLSchema-instance";
    static const char* wptx1   = "http://www.garmin.com/xmlschemas/WaypointExtension/v1";
    static const char* gpxtrx  = "http://www.garmin.com/xmlschemas/GpxExtensions/v3";
    static const char* gpxtpx  = "http://www.garmin.com/xmlschemas/TrackPointExtension/v1";
    static const char* gpxx    = "http://www.garmin.com/xmlschemas/GpxExtensions/v3";
    static const char* trp     = "http://www.garmin.com/xmlschemas/TripExtensions/v1";
    static const char* adv     = "http://www.garmin.com/xmlschemas/AdventuresExtensions/v1";
    static const char* prs     = "http://www.garmin.com/xmlschemas/PressureExtension/v1";
    static const char* tmd     = "http://www.garmin.com/xmlschemas/TripMetaDataExtensions/v1";
    static const char* vptm    = "http://www.garmin.com/xmlschemas/ViaPointTransportationModeExtensions/v1";
    static const char* ctx     = "http://www.garmin.com/xmlschemas/CreationTimeExtension/v1";
    static const char* gpxacc  = "http://www.garmin.com/xmlschemas/AccelerationExtension/v1";
    static const char* gpxpx   = "http://www.garmin.com/xmlschemas/PowerExtension/v1";
    static const char* vidx1   = "http://www.garmin.com/xmlschemas/VideoExtension/v1";
    static const char* gpxdata = "http://www.cluetrust.com/XML/GPXDATA/1/0";
}

namespace GpxTag {
    static const char* gpx                 = "gpx";
    static const char* metadata            = "metadata";
    static const char* trk                 = "trk";
    static const char* wpt                 = "wpt";
    static const char* link                = "link";
    static const char* time                = "time";
    static const char* href                = "href";
    static const char* text                = "text";
    static const char* type                = "type";
    static const char* desc                = "desc";
    static const char* keywords            = "keywords";
    static const char* bounds              = "bounds";
    static const char* TrackExtension      = "TrackExtension";
    static const char* TrackPointExtension = "TrackPointExtension";
    static const char* PowerExtension      = "PowerExtension";
    static const char* DisplayColor        = "DisplayColor";
    static const char* extensions          = "extensions";
    static const char* trkseg              = "trkseg";
    static const char* name                = "name";
    static const char* trkpt               = "trkpt";
    static const char* lat                 = "lat";
    static const char* lon                 = "lon";
    static const char* ele                 = "ele";
    static const char* atemp               = "atemp";
    static const char* wtemp               = "wtemp";
    static const char* depth               = "depth";
    static const char* hr                  = "hr";
    static const char* cad                 = "cad";
    static const char* cadence             = "cadence";
    static const char* speed               = "speed";
    static const char* course              = "course";
    static const char* bearing             = "bearing";
    static const char* maxlat              = "maxlat";
    static const char* maxlon              = "maxlon";
    static const char* minlat              = "minlat";
    static const char* minlon              = "minlon";
    static const char* lap                 = "lap";
    static const char* PowerInWatts        = "PowerInWatts";
}

bool GeoLoadGpx::load(const QString& path)
{
    QFile file(path);
    if (!openReader(file))
        return false;

    foundGpxElement = false;

    parseKeys([this]() {
        if (xml.name() == GpxTag::gpx) parseGpx();
        else xml.skipCurrentElement();
    });

    if (xml.error() == QXmlStreamReader::NoError) {
        if (!foundGpxElement)
            xml.raiseError(QObject::tr("No gpx element found in file"));
    }

    if (xml.hasError()) {
        geoLoad.m_errorString = xml.errorString();
        return false;
    }

    return true;
}

bool GeoLoadGpx::is(const QString& path)
{
    QFile file(path);

    if (!openReader(file))
        return false;

    while (!done())
        if (const auto token = xml.readNext(); isStart(token))
            return xml.name() == GpxTag::gpx;

    return false;
}

void GeoLoadGpx::parseGpx()
{
    fileMetaData.clear();
    foundGpxElement = true;  // we found a valid GPX track.

    parseKeys([this]() {
        if      (xml.name() == GpxTag::metadata)   parseMetadata();
        else if (xml.name() == GpxTag::trk)        parseTrk();
        else if (xml.name() == GpxTag::wpt)        parseWpt();
        // else if (xml.name() == GpxTag::rte)        parseRte); // TODO: ...
        else if (xml.name() == GpxTag::extensions) parseExtensionsGpxContext();
        else xml.skipCurrentElement();
    });
}

void GeoLoadGpx::parseMetadata()
{
    parseKeys([this]() {
        if      (xml.name() == GpxTag::link) parseLink();
        else if (xml.name() == GpxTag::time) ; //  TODO: ...
        else if (xml.name() == GpxTag::name)     fileMetaData.name     = xml.readElementText();
        else if (xml.name() == GpxTag::desc)     fileMetaData.desc     = xml.readElementText();
        else if (xml.name() == GpxTag::keywords) fileMetaData.keywords = xml.readElementText();
        else xml.skipCurrentElement();
    });
}

void GeoLoadGpx::parseLink()
{
    href = xml.attributes().value("", GpxTag::href);

    parseKeys([this]() {
        if      (xml.name() == GpxTag::text) text = xml.readElementText();
        else if (xml.name() == GpxTag::type) linkType = xml.readElementText();
        else xml.skipCurrentElement();
    });

    fileMetaData.links.append({ href.toString(), text, linkType });
}

void GeoLoadGpx::parseExtensionsGpxContext()
{
    parseKeys([this]() {
        if   (xml.name() == GpxTag::lap) parseGpxDataLap();
        else xml.skipCurrentElement();
    });

    if (const int rowcount = geoLoad.model().rowCount(); track.laps > 0 && rowcount > 0) {
        const QModelIndex idx = geoLoad.model().index(rowcount - 1, TrackModel::Laps);
        geoLoad.model().setData(idx, track.laps, Util::RawDataRole);
    }
}

void GeoLoadGpx::parseGpxDataLap()
{
    ++track.laps;
    // TODO: collect the lap data:
    // index
    // startPoint
    // endPoint
    // elapsedTime
    // calories
    // distance
    // trigger kind
    // intensity
}

void GeoLoadGpx::parseExtensionsTrkContext()
{
    parseKeys([this]() {
        if   (xml.name() == GpxTag::TrackExtension) parseTrackExtension();
        else xml.skipCurrentElement();
    });
}

void GeoLoadGpx::parseExtensionsTrkptContext()
{
    // GPXData extensions are directly under the extension tag, while Garmin TrackPoint extensions
    // are under a TrackPointExtension nested element.
    parseKeys([this]() {
        if (xml.name() == GpxTag::hr && xml.namespaceUri() == GpxExt::gpxdata)
            pt.m_hr  = xml.readElementText().toFloat();
        else if (xml.name() == GpxTag::cadence && xml.namespaceUri() == GpxExt::gpxdata)
            pt.m_cad = xml.readElementText().toFloat();
        else if (xml.name() == GpxTag::TrackPointExtension && xml.namespaceUri() == GpxExt::gpxtpx)
            parseTrackPointExtension();
        else if (xml.name() == GpxTag::PowerExtension && xml.namespaceUri() == GpxExt::gpxpx)
            parsePowerExtension();
        else xml.skipCurrentElement();
    });
}

void GeoLoadGpx::parseTrackExtension()
{
    parseKeys([this]() {
        if   (xml.name() == GpxTag::DisplayColor) parseDisplayColor();
        else xml.skipCurrentElement();
    });
}

void GeoLoadGpx::parseDisplayColor()
{
    // TODO: ...
}

void GeoLoadGpx::parseTrk()
{
    reportRead(xml.characterOffset()); // for progress reports
    track.clear();

    parseKeys([this]() {
        if      (xml.name() == GpxTag::extensions) parseExtensionsTrkContext();
        else if (xml.name() == GpxTag::trkseg)     parseTrkseg();
        else if (xml.name() == GpxTag::name)       track.name = xml.readElementText();
        else if (xml.name() == GpxTag::desc)       track.desc = xml.readElementText();
        else xml.skipCurrentElement();
    });

    // Prepend track global name
    if (!fileMetaData.name.isEmpty())
        track.name = fileMetaData.name + ": " + track.name;

    if (!fileMetaData.desc.isEmpty() && track.desc.isEmpty())
        track.desc = fileMetaData.desc;

    if (!fileMetaData.keywords.isEmpty() && track.keywords.isEmpty())
        track.keywords = fileMetaData.keywords;

    // Create a new TrackItem for the track we just made.
    track.append(geoLoad);
}

void GeoLoadGpx::parseRte()
{
    // TODO: ...
}

void GeoLoadGpx::parseWpt()
{
    // TODO: ...
}

void GeoLoadGpx::parseTrkseg()
{
    // Create a new geo point list set for this track segment
    track.newSegment();

    parseKeys([this]() {
        if   (xml.name() == GpxTag::trkpt) parseTrkpt();
        else xml.skipCurrentElement();
    });
}

void GeoLoadGpx::parseTrkpt()
{
    assert(!track.geoPoint.empty());  // we must have made this parsing the trkseg

    pt.clear();

    for (const auto& attr : xml.attributes()) {
        if      (attr.name() == GpxTag::lat) pt.m_lat = attr.value().toDouble();
        else if (attr.name() == GpxTag::lon) pt.m_lon = attr.value().toDouble();
    }

    parseKeys([this]() {
        if      (xml.name() == GpxTag::ele)  pt.m_ele  = xml.readElementText().toDouble();
        else if (xml.name() == GpxTag::time) pt.m_time = QDateTime::fromString(xml.readElementText(), Qt::ISODate);
        else if (xml.name() == GpxTag::extensions) parseExtensionsTrkptContext();
        else xml.skipCurrentElement();
    });

    // Alas, QVector does not supply emplace methods.
    track.geoPoint.back().append(pt);
}

void GeoLoadGpx::parsePowerExtension()
{
    parseKeys([this]() {
        const bool gpxpx = (xml.namespaceUri() == GpxExt::gpxpx);
        if   (xml.name() == GpxTag::PowerInWatts && gpxpx) pt.m_power = xml.readElementText().toUInt();
        else xml.skipCurrentElement();
    });
}

void GeoLoadGpx::parseTrackPointExtension()
{
    parseKeys([this]() {
        const bool gpxtpx = (xml.namespaceUri() == GpxExt::gpxtpx);

        if      (xml.name() == GpxTag::atemp && gpxtpx)   pt.m_atemp   = xml.readElementText().toFloat();
        else if (xml.name() == GpxTag::wtemp && gpxtpx)   pt.m_wtemp   = xml.readElementText().toFloat();
        else if (xml.name() == GpxTag::depth && gpxtpx)   pt.m_depth   = xml.readElementText().toFloat();
        else if (xml.name() == GpxTag::hr && gpxtpx)      pt.m_hr      = xml.readElementText().toInt();
        else if (xml.name() == GpxTag::cad && gpxtpx)     pt.m_cad     = xml.readElementText().toInt();
        else if (xml.name() == GpxTag::speed && gpxtpx)   pt.m_speed   = xml.readElementText().toFloat();
        else if (xml.name() == GpxTag::course && gpxtpx)  pt.m_course  = xml.readElementText().toFloat();
        else if (xml.name() == GpxTag::bearing && gpxtpx) pt.m_bearing = xml.readElementText().toFloat();
        else xml.skipCurrentElement();
    });
}

void GeoSaveGpx::saveMetadata()
{
    xml.writeStartElement(GpxTag::metadata); {
        // TODO: link:

        xml.writeTextElement(GpxTag::time, // write file creation time
                                   QDateTime::currentDateTime().toString(Qt::ISODate));

        // Bounds for all the saved tracks
        xml.writeEmptyElement(GpxTag::bounds); {
            const auto [maxlat, maxlon, minlat, minlon, valid] = geoSave.model().bounds(geoSave.m_selection);

            if (valid) {
                xml.writeAttribute(GpxTag::maxlat, QString::number(maxlat, 'g', 14));
                xml.writeAttribute(GpxTag::maxlon, QString::number(maxlon, 'g', 14));
                xml.writeAttribute(GpxTag::minlat, QString::number(minlat, 'g', 14));
                xml.writeAttribute(GpxTag::minlon, QString::number(minlon, 'g', 14));
            }
        } // no end needed for empty element
    } xml.writeEndElement();
}

void GeoSaveGpx::saveTrkptExtensions(const PointItem& trkpt)
{
    if (!trkpt.hasExtData())
        return;

    // We write TrackPointExtensionnot not GPXData extension format.  At the moment, it's
    // tough beans if you wanted the other one.
    xml.writeStartElement(GpxTag::extensions); {
        xml.writeStartElement(GpxExt::gpxtpx, GpxTag::TrackPointExtension); {
            if (trkpt.hasAtemp())
                xml.writeTextElement(GpxExt::gpxtpx, GpxTag::atemp, QString::number(trkpt.atemp()));
            if (trkpt.hasWtemp())
                xml.writeTextElement(GpxExt::gpxtpx, GpxTag::wtemp, QString::number(trkpt.wtemp()));
            if (trkpt.hasDepth())
                xml.writeTextElement(GpxExt::gpxtpx, GpxTag::depth, QString::number(trkpt.depth()));
            if (trkpt.hasHr())
                xml.writeTextElement(GpxExt::gpxtpx, GpxTag::hr, QString::number(trkpt.hr()));
            if (trkpt.hasCad())
                xml.writeTextElement(GpxExt::gpxtpx, GpxTag::cad, QString::number(trkpt.cad()));
            if (trkpt.hasSpeed(PointItem::Measured))
                xml.writeTextElement(GpxExt::gpxtpx, GpxTag::speed, QString::number(trkpt.speed()));
            if (trkpt.hasCourse())
                xml.writeTextElement(GpxExt::gpxtpx, GpxTag::course, QString::number(trkpt.course()));
            if (trkpt.hasBearing())
                xml.writeTextElement(GpxExt::gpxtpx, GpxTag::bearing, QString::number(trkpt.bearing()));
        } xml.writeEndElement();

        if (trkpt.hasPower(PointItem::Measured)) {
            xml.writeStartElement(GpxExt::gpxpx, GpxTag::PowerExtension); {
                xml.writeTextElement(GpxExt::gpxpx, GpxTag::PowerInWatts, QString::number(trkpt.power()));
            } xml.writeEndElement();
        }

    } xml.writeEndElement();

    // TODO: save extension data
}

void GeoSaveGpx::saveTrkpt(const PointItem& trkpt)
{
    // FORMAT NOTE: the QString %f format always generates trailing zeros, which we don't want.
    // %g doesn't, and we're depending on the non-scientific-notation format always being shorter
    // for the ranges of interest here.

    xml.writeStartElement(GpxTag::trkpt); {
        if (trkpt.hasLoc()) {
            xml.writeAttribute(GpxTag::lat, QString::number(trkpt.lat(false), 'g', 16));
            xml.writeAttribute(GpxTag::lon, QString::number(trkpt.lon(false), 'g', 16));
        }

        if (trkpt.hasEle())
            xml.writeTextElement(GpxTag::ele,  QString::number(trkpt.ele(false), 'g', 10));

        if (trkpt.hasTime())
            xml.writeTextElement(GpxTag::time, trkpt.time().toString(Qt::ISODate));

        saveTrkptExtensions(trkpt);
    } xml.writeEndElement();
}

void GeoSaveGpx::saveTrkseg(const PointModel::value_type& trkseg)
{
    xml.writeStartElement(GpxTag::trkseg); {
        for (const auto& geoPt : trkseg)
            saveTrkpt(geoPt);
    } xml.writeEndElement();
}

void GeoSaveGpx::saveRte(const QModelIndex&)
{
    assert(0); // TODO: ...
}

void GeoSaveGpx::saveWpt(const QModelIndex&)
{
    assert(0); // TODO: ...
}

void GeoSaveGpx::saveTrk(const QModelIndex& idx)
{
    reportWrite(1); // for progress reports

    xml.writeStartElement(GpxTag::trk); {
        // Some tracks don't have a per-track name
        if (const QString name = get<QString>(TrackModel::Name, idx); !name.isEmpty())
            xml.writeTextElement(GpxTag::name, get<QString>(TrackModel::Name, idx));
        if (const QString desc = get<QString>(TrackModel::Notes, idx); !name.isEmpty())
            xml.writeTextElement(GpxTag::desc, get<QString>(TrackModel::Notes, idx));

        // For each track segment
        for (const auto& trkseg : *geoSave.model().geoPoints(idx))
            saveTrkseg(trkseg);

        // export each segment
    } xml.writeEndElement();
}

void GeoSaveGpx::saveTrackItem()
{
    for (const auto& idx : geoSave.m_selection) {
        switch (get<TrackType>(TrackModel::Type, idx)) {
        case TrackType::Wpt: saveWpt(idx); break;
        case TrackType::Rte: saveRte(idx); break;
        case TrackType::Trk: saveTrk(idx); break;
        default: assert(0); break;
        }
    }
}

void GeoSaveGpx::saveGpx()
{
    xml.writeStartElement(GpxTag::gpx); {
        xml.writeAttribute("creator", appname);
        xml.writeAttribute("version", "1.1"); // GPX schema version, not app version
        xml.writeAttribute("xmlns",   GpxExt::xmlns);

        // TODO: xsi:schemaLocation
        xml.writeNamespace(GpxExt::xsi,      "xsi");
        xml.writeNamespace(GpxExt::wptx1,    "wptx1");
        xml.writeNamespace(GpxExt::gpxtrx,   "gpxtrx");
        xml.writeNamespace(GpxExt::gpxtpx,   "gpxtpx");
        xml.writeNamespace(GpxExt::gpxx,     "gpxx");
        xml.writeNamespace(GpxExt::trp,      "trp");
        xml.writeNamespace(GpxExt::adv,      "adv");
        xml.writeNamespace(GpxExt::prs,      "prs");
        xml.writeNamespace(GpxExt::tmd,      "tmd");
        xml.writeNamespace(GpxExt::vptm,     "vptm");
        xml.writeNamespace(GpxExt::ctx,      "ctx");
        xml.writeNamespace(GpxExt::gpxacc,   "gpxacc");
        xml.writeNamespace(GpxExt::gpxpx,    "gpxpx");
        xml.writeNamespace(GpxExt::vidx1,    "vidx1");
        xml.writeNamespace(GpxExt::gpxdata,  "gpxdata");

        saveMetadata();
        saveTrackItem();
    } xml.writeEndElement();
}

bool GeoSaveGpx::save(const QString& path)
{
    QFile file(path);
    if (!openWriter(file))
        return false;

    xml.writeStartDocument();
    saveGpx();
    xml.writeEndDocument();

    return !xml.hasError();
}

