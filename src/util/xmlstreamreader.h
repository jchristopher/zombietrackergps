/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef XMLSTREAMREADER_H
#define XMLSTREAMREADER_H

#include <functional>
#include <QXmlStreamReader>

// Add a few helpers to QXmlStreamReader
class XmlStreamReader : public QXmlStreamReader
{
public:
    using QXmlStreamReader::QXmlStreamReader;

    inline void parseKeys(const std::function<void(XmlStreamReader&)>&);

protected:
    static bool isStart(const QXmlStreamReader::TokenType& token) {
        return token == QXmlStreamReader::StartElement;
    }

    static bool isEnd(const QXmlStreamReader::TokenType& token) {
        return token == QXmlStreamReader::EndElement;
    }

    bool done() { return atEnd() || hasError(); }
};

// Helper to parse a set of XML tokens.  Inline for performance.
inline void XmlStreamReader::parseKeys(const std::function<void(XmlStreamReader& xml)>& fn)
{
    const QStringRef& end = name();

    while (!done()) {
        if (const auto token = readNext(); isStart(token)) {
            fn(*this);
        } else if (isEnd(token) && name() == end) {
            break;
        }
    }
}

#endif // XMLSTREAMREADER_H
