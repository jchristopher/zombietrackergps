#!/bin/bash

usage() {
    echo 'Refreshes art assets from source files.'
    echo 'Run from directory containing 'source''
    echo
    echo '--dry-run   Show commands but do not execute them.'
    echo '--no-flags  Avoid recoding flags.'
    echo '--no-remove Do not remove old files.'
    exit 5
}

startqrc() {
    local prefix="${2:-/art}"
    echo "<RCC>
    <qresource prefix=\"${prefix}\">" > "${1}"
}

endqrt() {
    echo '    </qresource>
</RCC>
' >> "$1"
}

preview=''
flags=true
remove=true

for f in "$@"; do
    case "$f" in
        --help) usage;;
        --preview|--dry-run) preview=echo;;
        --no-flags)          flags=false;;
        --no-remove)         remove=false;;
    esac
done
     
if [ ! -d source ]; then
    usage
fi

$preview /bin/rm -rf points

art='art.qrc'
artL='art-light.qrc'
artD='art-dark.qrc'

if [ "$preview" != 'echo' ]; then
    startqrc "${art}"
    startqrc "${artL}"
    startqrc "${artD}"
fi

echo 'Points: ------------------------------------------------------------------------'


totalCount=$(find 'source/points' -type f | wc -l)
count=0

find 'source/points' -type f | while read source; do
    target="${source#*/}"
    [ "$preview" != 'echo' ] && mkdir -p "$(dirname "$target")"
    $preview convert -resize 32x32 -strip "$source" "$target"

    [ "$preview" != 'echo' ] && echo "        <file>${target}</file>" >> "${art}"

    [ "$preview" != 'echo' ] && echo -ne "$[++count*100/totalCount]%\r"
done

[ "$preview" != 'echo' ] && echo

echo 'Tags: ------------------------------------------------------------------------'

$remove && $preview /bin/rm -rf tags

tagdarkfg='#f2f2f2'
taglightfg='#4d4d4d'

totalCount=$(find source/tags/{Activity,Category,Transport} -type f | wc -l)
count=0

find source/tags/{Activity,Category,Transport} -type f | while read source; do
    target="${source#*/*/}"
    target="${target%.png}.svg"

    targetL="tags/Light/$target"
    targetD="tags/Dark/$target"
    aliasName="tags/$target"

    basename="$(basename "${source%png}svg")"

    if [ "$preview" = 'echo' ]; then
        $preview "convert \"$source\" ppm:- | potrace -C \"$tagdarkfg\" -s - -o \"$targetD\""
        $preview "convert \"$source\" ppm:- | potrace -C \"$taglightfg\" -s - -o \"$targetL\""
    else
        mkdir -p "$(dirname "$targetL")"
        mkdir -p "$(dirname "$targetD")"

        while [ $(jobs | wc -l) -gt $(nproc) ]; do sleep 0.5; done

        {
            $preview convert "$source" ppm:- | potrace -C "$tagdarkfg" -s - -o "$targetD"
            $preview convert "$source" ppm:- | potrace -C "$taglightfg" -s - -o "$targetL"
        } &

        [ "$preview" != 'echo' ] && echo "        <file alias=\"$aliasName\">${targetL}</file>" >> "${artL}"
        [ "$preview" != 'echo' ] && echo "        <file alias=\"$aliasName\">${targetD}</file>" >> "${artD}"
    fi

    [ "$preview" != 'echo' ] && echo -ne "$[++count*100/totalCount]%\r"
done

[ "$preview" != 'echo' ] && echo

echo 'Weather: ------------------------------------------------------------------------'

$preview mkdir -p 'tags/Weather'
find 'source/tags' -type f -wholename '*/Weather/*' | while read source; do
    target="${source#*/}"
    $preview cp "$source" "$target"
    [ "$preview" != 'echo' ] && echo "        <file>${target}</file>" >> "${art}"
done

[ "$preview" != 'echo' ] && echo '100%'

echo 'Flags: ------------------------------------------------------------------------'
# Some flag files are very large as SVG, so we make little icon bitmaps instead.

# TODO: somehow use gimp to batch-convert the XCF file

totalCount=$(find 'source/tags' -type f -wholename '*/Flags/*' | wc -l)
count=0

find 'source/tags' -type f -wholename '*/Flags/*' | while read source; do
    target="${source#*/}"
    target="${target%.???}"
    target="${target}.jpg"
    [[ "$target" = */Countries/* ]] && quality=85 || quality=80

    while [ $(jobs | wc -l) -gt $[$(nproc) * 2] ]; do sleep 0.5; done

    if $flags; then
        [ "$preview" != 'echo' ] && mkdir -p "$(dirname "$target")"
        $preview convert -quality $quality -strip -resize 72x72 "$source" "$target" &
    fi

    [ "$preview" != 'echo' ] && echo "        <file>${target}</file>" >> "${art}"
    [ "$preview" != 'echo' ] && echo -ne "$[++count*100/totalCount]%\r"
done

echo 'Icons: ------------------------------------------------------------------------'
if [ "$preview" != 'echo' ]; then
    for color in light dark; do
        qrc="icons-${color}.qrc"
        startqrc "${qrc}" "icons"
        find "icons/${color}/hicolor" -iname '*.svg' | while read line; do
            echo "        <file alias=\"${line#*/*/}\">${line}</file>" >> "${qrc}"
        done
        endqrt "${qrc}"
    done
fi

echo 'Logos: ------------------------------------------------------------------------'
if [ "$preview" != 'echo' ]; then
    find 'logos' -type f | sed -e 's:^:        <file>:' -e 's:$:</file>:' >> "${art}"
fi

echo 'UI: ------------------------------------------------------------------------'
if [ "$preview" != 'echo' ]; then
    find 'ui' -type f | grep -v docs | sed -e 's:^:        <file>:' -e 's:$:</file>:' >> "${art}"
fi

echo 'Pubkey: ------------------------------------------------------------------------'
if [ "$preview" != 'echo' ]; then
    gpg --export --armor ldztgps@khasekhemwy.net > '../data/pubkey.asc'
fi
    
[ "$preview" != 'echo' ] && echo

if [ "$preview" != 'echo' ]; then
    endqrt "${art}"
    endqrt "${artL}"
    endqrt "${artD}"
fi
