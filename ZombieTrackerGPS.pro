#-------------------------------------------------
#
# Project created by QtCreator 2019-03-13T21:03:45
#
#-------------------------------------------------

QT       += core gui charts Marble svg xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = zombietrackergps
TEMPLATE = app
PKGVERSION = $$cat(VERSION)    # package version
PKGVERSION = $$replace(PKGVERSION, "\"", "")
             
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES   += $$files(src/*.cpp, true)
HEADERS   += $$files(src/*.h, true)
FORMS     += $$files(src/*.ui, true)
RESOURCES += # no internal resources

# Build art resources
artlist = art art-light art-dark icons-light icons-dark

for (art, artlist) {
    file = $${art}.rcc
    eval($${file}.target = $${file})
    eval($${file}.depends = $${PWD}/art/$${art}.qrc)
    eval($${file}.commands = rcc --compress 6 --threshold 8 --binary $$PWD/art/$${art}.qrc -o $${OUT_PWD}/$${art}.rcc)

    QMAKE_EXTRA_TARGETS += $${file}
    PRE_TARGETDEPS += $${file}
}

# manpage
mangz.target   = $${TARGET}.1.gz
mangz.depends  = $$PWD/man/$${TARGET}.1
mangz.commands = gzip --stdout "$$mangz.depends" > "$$mangz.target"

# for "make tarball"
tarball.target   = tarball
tarball.commands = tar --transform="s:^[.]/:./$${TARGET}-$${PKGVERSION}/:" -C "$$PWD" --exclude '*.pro.user' --exclude-vcs --exclude-backups --owner loopdawg --group loopdawg --xz -cf "/tmp/$${TARGET}_$${PKGVERSION}.tar.xz" .

QMAKE_EXTRA_TARGETS += tarball mangz
PRE_TARGETDEPS      += $${mangz.target}

unix {
    QMAKE_LFLAGS_RELEASE='-Wl,--strip-all,--dynamic-list-cpp-typeinfo'
    QMAKE_LFLAGS_DEBUG='-Wl,--dynamic-list-cpp-typeinfo'
    QMAKE_CXXFLAGS += -std=c++17 -Wall -pedantic
    QMAKE_CXXFLAGS_RELEASE += -DNDEBUG
# CONFIG += c++17    # TODO: not supported yet in qmake, so use QMAKE_CXXFLAGS as just above.

    # static linkage of system libraries if requested
    # TODO: static libc linkage causes R_X86_64_PC32 relocation error.  Haven't found any pie/pic fix yet.
    STATIC=$$(STATIC)
    !isEmpty(STATIC) {
        QMAKE_LFLAGS += -l:libm.a -l:libgcc.a -l:libstdc++.a -l:libpthread.a # -l:libc.a
    }

    # for "make deb"
    system("which dpkg-architecture >/dev/null 2>&1") {
        DEB_TARGET_ARCH = $$system(dpkg-architecture -q DEB_TARGET_ARCH)
        deb.target   = deb
        deb.commands = INSTALL_ROOT=$${OUT_PWD}/debbuild/usr qmake $${_PRO_FILE_}; \
                       rm -rf $${OUT_PWD}/debbuild; \
                       make install; \
                       dpkg-deb --build debbuild $${TARGET}-$${PKGVERSION}_$${DEB_TARGET_ARCH}.deb
        QMAKE_EXTRA_TARGETS += deb
    }

    # for "make rpm"
    system("which rpmbuild >/dev/null 2>&1") {
        rpm.target = rpm
        rpm.commands = INSTALL_ROOT=$${OUT_PWD}/rpmbuild/usr qmake $${_PRO_FILE_}; \
                       rm -rf $${OUT_PWD}/rpmbuild; \
                       make install; \
                       find $${OUT_PWD}/rpmbuild/usr -type f | sed "s:$${OUT_PWD}/rpmbuild::" >> SPECS; \
                       rpmbuild -bb --buildroot $${OUT_PWD}/rpmbuild SPECS
        QMAKE_EXTRA_TARGETS += rpm
    }

    TARGET_MULTIARCH = $$system(gcc -print-multiarch)

    INSTALL_ROOT=$$(INSTALL_ROOT) # use env variable if set
    isEmpty(INSTALL_ROOT) {
       INSTALL_ROOT = /usr/local
    }

    INCLUDEPATH += $$clean_path($$_PRO_FILE_PWD_/../libldutils)
    INCLUDEPATH += /usr/local/include/libldutils
    INCLUDEPATH += /usr/include/libldutils

    LIBS += -L$$clean_path($$_PRO_FILE_PWD_/../libldutils/lib/$${TARGET_MULTIARCH})
    LIBS += -L/usr/lib/$${TARGET_MULTIARCH}
    LIBS += -L/usr/local/lib

    CONFIG(debug, debug|release) {
        LIBS += -lldutilsd
    } else {
        LIBS += -lldutils
    }

    # This should already be covered by the Qt += Marble entry near the top, but for some reason that
    # isn't working on Arch Linux.  This adds that library in explicitly, and should be harmless on
    # other distros.
    LIBS += -lmarblewidget-qt5

    manpage.path     = $${INSTALL_ROOT}/share/man/man1
    manpage.files    = $$files($${OUT_PWD}/*.1.gz)

    sharedoc.path   = $${INSTALL_ROOT}/share/$${TARGET}
    sharedoc.files  = data/sample data/pubkey.asc art/ui/docs art/text/docs \
                     $$files($${OUT_PWD}/*.rcc) \
                     art/logos/projects/$${TARGET}.png

    docs.path    = $${INSTALL_ROOT}/share/doc/$${TARGET}
    docs.files   = debian/copyright

    contains(INSTALL_ROOT, ".*debbuild.*") {
        debfiles.path  = $${OUT_PWD}/debbuild/DEBIAN
        debfiles.commands = sed -e "\"s/Architecture:.*/Architecture: $${DEB_TARGET_ARCH}/\"" $${PWD}/DEBIAN/control > $${debfiles.path}/control
        INSTALLS += debfiles
    }

    contains(INSTALL_ROOT, ".*rpmbuild.*") {
        rpmfiles.path  = $${OUT_PWD}/rpmbuild
        rpmfiles.commands = sed -e "\"s/Version:.*/Version: $${PKGVERSION}/\"" $${PWD}/rpmbuild/SPECS > $${OUT_PWD}/SPECS
        INSTALLS += rpmfiles
    }

    # point the desktop Icon: to the installed icon
    finalroot        = $${INSTALL_ROOT}
    finalroot        ~= s:.*build/:/:
    desktop.path     = $${INSTALL_ROOT}/share/applications
    desktop.commands = sed -e "s:Icon=.*:Icon=$${finalroot}/share/$${TARGET}/$${TARGET}.png:" $${PWD}/data/desktop/$${TARGET}.desktop > $${desktop.path}/$${TARGET}.desktop

    target.path   = $${INSTALL_ROOT}/bin
    INSTALLS += target sharedoc manpage desktop docs
}
