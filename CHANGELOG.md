# Version History

## Version 0.90

* FIT format import support.  This should let many newer GPS devices work.
* Improved device sync to work with more devices (via "File/Import from Device...")
* Simple, single track at a time FIT exporter.
* Added new tag icons for Surf, Nordic, Soccer, HangGlide, and Horse.
* Status bar summary is now recalculated from scratch on pane focus.
* Fix power estimation for running and hiking.
* Fix missing average temperature calculation.
* Arch compilation fixes:
  * Remove use of QFontMetrics::width()
  * Don't use dpkg-architecture command on non-debian systems
  * Fix link issue against libmarblewidget (also effected newer Ubuntus)
* Add OpenStreetMap donation link in About window
* Add missing build dependency to BUILDING
* Minor typo and art asset fixes

## Version 0.80

Initial public alpha.
